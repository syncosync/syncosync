:imagesdir: ../images/
:hide-uri-scheme:

== Functional description

To understand administration and usage of syncosync it is the easiest way to understand it's functionality first. 

=== Wordings

==== syncosync Box
The syncosync box (or sometimes called syncsync system is the hardware which runs the syncosync software e.g. the raspberry Pi.

This box has one or multpiple drives connected which store the local and the remote data

==== syncosync Admin
This is the person which administrate the syncosync box. He/She sets up the connection to the backup buddies system, creates and deletes accounts, reads the admin mails and is informed what to do in disaster scenarios

==== Backup Buddy
The backup buddy is your partner on the other side of the internet who owns also a syncosync system with a harddisk in the same size than yours. Your backup buddy is the one who gives you his syncsoync box when you are in trouble and need access to your data. 

==== Backup Account
The backup account is on your syncsync system to hold data for a computer which is backuped. How you use those and name those accounts is completely up to your situation, normally it is recommended to set up a backup account for each users home directory on any computer to get a clear understanding what is where. 

==== Disaster Swap
One of the methods to get access to your backups in the case of a disaster: if you do not have access to your syncsync system any more, you visit your backup buddy and she/he hands you over her/his syncosync system. On this system you can then activate the Disaster Swap, which means, the box behave like your own syncosync box. You have access to all your backuped data. This is the fastest method to get hands on your precious backup data.

==== Remote Recovery
Also a method to get back your backups from your backup buddies system. You set up a brand new empty syncosync box and this box retrieves all backup data from your backup buddies system. While this is done normally by using an internet connection, this ist not very fast but after the remote recovery you have already two working and synchronized syncosync boxes.





