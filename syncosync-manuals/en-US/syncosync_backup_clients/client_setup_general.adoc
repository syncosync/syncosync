:experimental: true
:icons: font
:icon-set: fa
:doctype: book
:sectnums:
:!chapter-signifier:
:chapter-label:
:imagesdir: ../images/

=== General client setup 

The client should either use rsync or sftp to upload data to the syncosync box. 
As the backup buddy is able to access the data on his harddisk, the backups must be encrypted.
The port to reach the syncosync box for the backup clients (aka intranet port) is 22.

All backup data has to go to the `data/` path.

If the syncosync box is reachable with the hostname `syncosync` and the account is `mybackup`, the backup path could look like: `sftp://mybackup@syncosync:/data` or `rsync://mybackup@syncosync:/data`

If there is a possibility to set the size of backup chunks, go for some medium size, e.g. 50MB. This makes it possible to sync in a secure and acceptable manner. Basically any chunk size should work, but we have had the best experiences in this area.

[IMPORTANT]
The backup destination path on the server must be at least `data/` as this is the first subpath where the account is allowed to write data.

If you want to store multiple different backups in different paths below `data`, this should be possible, but for better overwiew and recovery it is *strongly recommended* to setup different accounts on the syncosync box for different backup profiles.

[WARNING]
Ensure, that your backup is encrypted, so your backup buddy cannot access the data!


