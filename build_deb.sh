#!/bin/bash
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

if [[ "$OSTYPE" == "darwin"* ]]; then
    # Using docker for building the deb package since building debian packages is a pain on MacOS natively
    echo MacOS detected, trying to use the docker image
    docker run -v `pwd`:/syncosync registry.gitlab.com/syncosync/syncosync-cicd-containers/sos-builder /bin/bash -c 'cd /syncosync && ./build_deb.sh'
    exit $?
else
    echo Executing build natively
fi

# Absolute path to this script. /home/user/bin/foo.sh
SCRIPT=$(readlink -f $0)
# Absolute path this script is in. /home/user/bin
SCRIPTPATH=$(dirname $SCRIPT)
echo $SCRIPTPATH
cd $SCRIPTPATH

PROJECT=$(basename $(pwd))

timestamp=$(date +%Y%m%d%H%M%S)
BASEVERSION=$(<package_static/BASEVERSION)
echo $timestamp >package_static/VERSIONTIMESTAMP
VERSION="$BASEVERSION-$timestamp"
echo Building the Angular stuff
cd angular_src/sosui || exit 1
ng_path=$(which ng)
if [ ! -x "$ng_path" ]; then
  echo "Missing Angular in path (ng). Please refer to docs. Hint: sudo npm install -g @angular/cli"
  exit 1
fi
ng analytics off
echo Another npm install would help
echo N | npm install || exit 1
echo Building the Angular SOS UI
ng build --configuration production --localize || exit 1
cd $OLDPWD

echo "processing changelog"

rm debian/changelog
dch --create --newversion="$VERSION" --package="$PROJECT" --empty -M || exit 1
gbp dch --ignore-branch || exit 1

# ok. let's do the real stuff
echo building debian package $PROJECT\_$VERSION-$\_all

debuild -i -us -uc -b -d || exit 1

echo removing previous builds from here
rm ${PROJECT}_*_all.deb
echo copying package for test installation
cp ../${PROJECT}_${VERSION}_all.deb .
echo -n $VERSION > version
