#!/bin/bash
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

set -x
set -e

SCRIPT=$(readlink -f $0)
# Absolute path this script is in. /home/user/bin
SCRIPTPATH=$(dirname $SCRIPT)
echo $SCRIPTPATH
cd $SCRIPTPATH

PROJECT=syncosync
PROJECT_MANUALS=syncosync-manuals

if [ ! -f ../version ]
then
    echo No file version found. Aborting.
    exit 1
else
    VERSION=$(cat ../version)
fi

if [ -z ${REPREPRO_BASE_DIR+x} ]; then
  echo "REPREPRO_BASE_DIR not set. Grab your debian package from ../${PROJECT}_${VERSION}_all.deb"
  exit 1
else
  cp ../${PROJECT}_${VERSION}_all.deb debs_jessie/
  cp ../${PROJECT}_${VERSION}_all.deb debs_buster/
  cp ../${PROJECT}_${VERSION}_all.deb debs_bullseye/
  if [ -f ../${PROJECT_MANUALS}_${VERSION}_all.deb ] ; then
    echo Found manual: ../${PROJECT_MANUALS}_${VERSION}_all.deb
    cp ../${PROJECT_MANUALS}_${VERSION}_all.deb debs_jessie/ 2>/dev/null || :
    cp ../${PROJECT_MANUALS}_${VERSION}_all.deb debs_buster/ 2>/dev/null || :
    cp ../${PROJECT_MANUALS}_${VERSION}_all.deb debs_bullseye/ 2>/dev/null || :
  else
    echo no manuals?
  fi 
  # reprepro update
  reprepro includedeb jessie ./debs_jessie/*.deb
  reprepro list jessie
  reprepro includedeb buster ./debs_buster/*.deb
  reprepro list buster
  reprepro includedeb bullseye ./debs_bullseye/*.deb
  reprepro list bullseye
fi

#get username and password
USER=debsyncosync        #Your username
PASS=$SOSDEB_PASSWD      #Your password
HOST="deb.syncosync.org" #Keep just the address
LCD=$REPREPRO_BASE_DIR   #Your local directory
RCD="/"                  #FTP server directory

lftp -f "
set ftp:ssl-force true
set ssl:verify-certificate no
open $HOST
user $USER $PASS
lcd $LCD
mirror --continue --reverse --delete --verbose dists $RCD/dists
mirror --continue --reverse --delete --verbose pool $RCD/pool
bye
"
