settings {
   statusFile = "/run/lsyncd.status",
   insist = 1
}

sync {
    default.rsync,
    source    = "/mnt/local/syncosync/",
    target    = ":remote/syncosync",
    delay     = 15, 
    rsync     = {
        binary   = "/usr/bin/sosrsyncwrapper.py",
        archive  = true,
	timeout = 60,
	verbose  = false,
        _extra = {"-P", "-e", "\"/usr/bin/ssh -p 55055 -i /etc/syncosync/sshkeys/syncosync -o User=syncosync -o StrictHostKeyChecking=no  -o UserKnownHostsFile=/dev/null\""}
    },
    exitcodes = {
        _verbatim = true,
        [   0 ] = 'ok',
  	[   1 ] = 'again',
	[   2 ] = 'again',
	[   3 ] = 'again',
	[   4 ] = 'again',
	[   5 ] = 'again',
	[   6 ] = 'again',
	[  10 ] = 'again',
	[  11 ] = 'again',
	[  12 ] = 'again',
	[  14 ] = 'again',
	[  20 ] = 'again',
	[  21 ] = 'again',
	[  22 ] = 'again',

	-- partial transfers are ok, since Lsyncd has registered the event that
	-- caused the transfer to be partial and will recall rsync.
	[  23 ] = 'ok',
	[  24 ] = 'ok',

	[  25 ] = 'again',
	[  30 ] = 'again',
	[  35 ] = 'again',

	[ 255 ] = 'again'
  	}
}
