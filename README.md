# syncosync — secure peer to peer backup synchronization

syncosync is a software for running a backup appliance, normally a
Raspberry Pi4 with a USB3 connected hard disk drive. The main function
is to receive backups from other computers on the local network and
synchronize these backups to a similar system at a different location.
The device on the other side of the network does the same, so
two devices on two sides of the network are always in sync.<br />
**You don't lose anything if something happens at either end**

Copylefted libre software, licensed [AGPLv3+](http://www.gnu.org/licenses/agpl.html).<br />
_Use, inspect, change, and share at will; with all_.

[Website](https://syncosync.org/) (Main entry point)

[Manuals](https://manuals.syncosync.org/) (manuals)

[Forum](https://forum.syncosync.org/) (help by the team and others)

[Downloads](https://downloads.syncosync.org/) (RPi, BPi images)

![Screenshot](https://syncosync.org/images/main_screen.png)

This is the source code repository of syncosync.
