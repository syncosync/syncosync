#!/usr/bin/env python3
"""
sysstat script. This is for checking and changing the state of the system
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse

# following imports are all necessary for cli
from soscore import soshelpers, sysstate
from sosmodel import sosconstants
from sosmodel.sos_enums import SwitchAction, SystemMode, SystemStatus
from sosmodel.sysstate import SysStateResult
from sosutils.logging import init_logging
from sosutils.runtime_args import parser_log_args


def main():
    switch_action = SwitchAction.DEFAULT

    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="state check and management",
        epilog=sosconstants.EPILOG,
    )
    parser.add_argument(
        "-g",
        "--get",
        help="get system state (before and after switching)",
        action="store_true",
    )
    parser.add_argument("-r", "--result", help="get lastet result", action="store_true")
    parser.add_argument(
        "--setup_disks", help="switch to Disk Setup mode", action="store_true"
    )
    parser.add_argument(
        "--setup_volumes", help="switch to Volume Setup mode", action="store_true"
    )
    parser.add_argument(
        "--resize_volumes", help="switch to Resize Volumes mode", action="store_true"
    )
    parser.add_argument("--default", help="switch to Default mode", action="store_true")
    parser.add_argument(
        "--persistent",
        help="make system running in Disastor Recovery Mode persistent",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "--default_no_sync", help="switch to Default state", action="store_true"
    )
    parser.add_argument(
        "--disaster_swap", help="switch to Disaster Swap state", action="store_true"
    )
    parser.add_argument(
        "--disk_error", help="switch to Disk Error (for testing purposes)", action="store_true"
    )
    parser.add_argument(
        "--remote_recovery", help="switch to Remote Recovery state", action="store_true"
    )
    parser.add_argument(
        "--shutdown", help="switch to Shutdown state", action="store_true"
    )
    parser.add_argument("-c", "--config", help="configuration backup file")
    parser.add_argument("-a", "--auth", help="backupfile password")
    parser.add_argument(
        "-p", "--pretty", help="formats json output", action="store_true", default=False
    )

    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)

    mystate = sysstate.SysState()

    if args.get:
        mystatestate = mystate.get()
        print(f"State (numeric): {mystatestate.sysstate}\nDecoded:")
        for line in SystemStatus.message(mystatestate.sysstate):
            print(line)
        print(f"Actual Mode: {SystemMode.message(mystatestate.actmode)}")
        print(f"Stored Mode: {SystemMode.message(mystatestate.storedmode)}")
        print(soshelpers.pretty_print(mystatestate.to_json(), args.pretty))

    if args.result:
        myresult = mystate.get_latest_result()
        print(soshelpers.pretty_print(myresult.to_json(), args.pretty))

    req_mode = None

    if args.setup_disks:
        req_mode = SystemMode.SETUP_DISKS
    if args.setup_volumes:
        req_mode = SystemMode.SETUP_VOLUMES
    if args.resize_volumes:
        req_mode = SystemMode.RESIZE_VOLUMES
    elif args.default:
        req_mode = SystemMode.DEFAULT
    elif args.default_no_sync:
        req_mode = SystemMode.DEFAULT_NO_SYNC
    elif args.disaster_swap:
        req_mode = SystemMode.DISASTER_SWAP
    elif args.disk_error:
        req_mode = SystemMode.DISK_ERROR
    elif args.remote_recovery:
        req_mode = SystemMode.REMOTE_RECOVERY
    elif args.shutdown:
        req_mode = SystemMode.SHUTDOWN

    if args.persistent:
        switch_action = SwitchAction.DISASTER_SWAP_PERSISTENT

    if req_mode is not None:
        result: SysStateResult = mystate.changemode(
            req_mode,
            cfg_backup_filepath=args.config,
            config_pwd=args.auth,
            switch_action=switch_action,
        )

        if result.mode == req_mode:
            print(f"Switching to {SystemMode.message(req_mode)} successful")
        else:
            print(
                f"Switching to {SystemMode.message(req_mode)} failed, will stay in {SystemMode.message(result.mode)}"
            )
        print("Report:")
        print("\n".join(result.report))

        if args.get:
            mystatestate = mystate.get()
            print(f"State (numeric): {mystatestate.sysstate}\nDecoded:")
            for line in SystemStatus.message(mystatestate.sysstate):
                print(line)
            print(f"Actual Mode: {SystemMode.message(mystatestate.actmode)}")
            print(f"Stored Mode: {SystemMode.message(mystatestate.storedmode)}")
            print(soshelpers.pretty_print(mystatestate.to_json(), args.pretty))


if __name__ == "__main__":
    main()
