#!/usr/bin/env python3
"""
The Script to generate a chroot environment
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse

from soscore import gen_chroot_env
from sosmodel import sosconstants
from sosutils.logging import init_logging
from sosutils.runtime_args import parser_log_args


def main():
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="Generate Chroot Environment",
        epilog=sosconstants.EPILOG,
    )
    parser.add_argument("-g", "--generate", help="generate", action="store_true")
    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)

    if args.generate:
        gen_chroot_env.gen_chroot_env(["bash", "rsync"])


if __name__ == "__main__":
    main()
