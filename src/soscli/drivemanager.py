#!/usr/bin/env python3
"""
This is script for managing and showing the drives
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import json

from soscore import (
    drivemanager,
    remotehost_systemsetupdata,
    soshelpers,
    sshdmanager,
    sysstate,
    systemsetupdata,
)
from sosmodel import sos_enums, sosconstants
from sosmodel.drivesetupDriveConfiguration import DrivesetupDriveConfiguration
from sosmodel.sos_enums import SsdAction, SystemMode
from sosmodel.systemsetupdata import PartitioningData, SystemSetupDataModel
from sosutils.logging import init_logging
from sosutils.runtime_args import parser_log_args


def main():
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="management of syncosync drives",
        epilog=sosconstants.EPILOG,
    )
    parser.add_argument("-f", "--format", help="format all drives", action="store_true")
    parser.add_argument(
        "-e",
        "--expand",
        help="expand existing sos vg with all possible drives. The json needs the password for the configuration backup, the selected_vg_uuid is not the already used vg_uuid on this box"
        'e.g. \'{ "selected_vg_uuid": "anZovn-3ITP-SOl4-nERm-7QLQ-8DBq-fNKzTS"}\'',
    )
    parser.add_argument(
        "-c",
        "--create_volumes",
        help="json representation of volumes: "
        'e.g. \'{ "local": "100", "remote":"100"}\'',
    )
    parser.add_argument(
        "-r",
        "--resize_volumes",
        help="json representation of volumes: "
        'e.g. \'{ "local": "100", "remote":"100"}\'',
    )
    parser.add_argument(
        "-y", "--yes", help="confirmation for formatting", action="store_true"
    )
    parser.add_argument(
        "-g", "--get", help="get actual situation as json", action="store_true"
    )
    parser.add_argument(
        "-l", "--latest_ssd", help="show last received remote ssd", action="store_true"
    )
    parser.add_argument("-o", "--own_ssd", help="show own ssd", action="store_true")
    parser.add_argument(
        "--common_ssd",
        help="show common ssd which is minimum of both ssd",
        action="store_true",
    )
    parser.add_argument(
        "-p", "--pretty", help="formats json output", action="store_true", default=False
    )
    parser.add_argument(
        "-m", "--mount", help="mount syncosync volumes (if possible)", action="store_true", default=False
    )
    parser.add_argument(
        "-u", "--umount", help="unmount syncosync volumes (if possible)", action="store_true", default=False
    )
    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)

    mydrives = drivemanager.Drives()
    mydrives.generate()
    if args.get:
        print_drives(mydrives)
        exit()
    if args.mount:
        mount(mydrives)
        exit()
    if args.umount:
        umount(mydrives)
        exit()
    if args.format:
        mystate = sysstate.SysState()
        mystatestate = mystate.get()
        if mystatestate.actmode != SystemMode.SETUP_DISKS:
            print("Format drives only in SETUP_DISKS mode")
        drives = mydrives.get_drivelist()
        s = ", "
        driveliststring = s.join(drives)
        if args.yes:
            format_drives(driveliststring, drives, mydrives)
        else:
            print("add -y to create sos volumes on drives " + driveliststring)
    if args.expand:
        mystate = sysstate.SysState()
        mystatestate = mystate.get()
        if mystatestate.actmode != SystemMode.SETUP_DISKS:
            print("Expand drives only in SETUP_DISKS mode")
        drives = mydrives.get_drivelist()
        s = ", "
        driveliststring = s.join(drives)
        if args.yes:
            expand_drives(args, driveliststring, drives, mydrives)
        else:
            print("add -y to create sos volumes on drives " + driveliststring)
    if args.create_volumes:
        if args.yes:
            print("Creating local and remote partitions")
            partitioning = PartitioningData.from_json(args.create_volumes)
            result = mydrives.create_volumes(partitioning)
            if result:
                print("Success!")
            else:
                print("Error")
        else:
            print("add -y to create volumes")

    if args.resize_volumes:
        print("Resizing local and remote partitions")
        partitioning = PartitioningData.from_json(args.resize_volumes)
        result = mydrives.resize_volumes(partitioning)
        if result:
            print("Success!")
        else:
            print("Error")

    if args.latest_ssd:
        remote_ssd: SystemSetupDataModel = (
            remotehost_systemsetupdata.RemoteHostSystemSetupData().get()
        )
        print(soshelpers.pretty_print(remote_ssd.to_json(), args.pretty))
        print("Action: ", SsdAction.to_readable(remote_ssd.action))

    if args.own_ssd:
        own_ssd: SystemSetupDataModel = systemsetupdata.get_system_ssd()
        print(soshelpers.pretty_print(own_ssd.to_json(), args.pretty))

    if args.common_ssd:
        common_ssd: SystemSetupDataModel = systemsetupdata.get_common_ssd()
        print(soshelpers.pretty_print(common_ssd.to_json(), args.pretty))


def expand_drives(args, driveliststring, drives, mydrives):
    print("Creating sos volume group on drives " + driveliststring)
    driveconfiguration = DrivesetupDriveConfiguration.from_json(args.expand)
    result = mydrives.expand_sos(driveconfiguration, drives)
    if result != -1:
        print(f"Success, free space {result} extends.")
        # some calculation for a proposal...
        system = int(result / 10)
        if system > sosconstants.SYSTEM_RECOMMEND_SIZE:
            system = sosconstants.SYSTEM_RECOMMEND_SIZE
        localremote = int((result - system) / 2)
        print(
            'you could create volumes with \'{"local":',
            localremote,
            ', "remote":',
            localremote,
            "}'",
        )


def format_drives(driveliststring, drives, mydrives):
    print("Creating sos volume group on drives " + driveliststring)
    result = mydrives.create_sos(drives)
    if result != -1:
        print(f"Success, free space {result} extends.")
        # some calculation for a proposal...
        localremote = int(result / 2)
        print(
            'you could create volumes with \'{"local":',
            localremote,
            ', "remote":',
            localremote,
            "}'",
        )


def mount(mydrives):
    success = mydrives.mount_sos_volumes()
    if success:
        print(
            "All sos volumes mounted - attention - this may not correspondent with the system state! "
        )
    else:
        print("could not mount sos volumes")


def print_drives(mydrives):
    report = mydrives.to_json()
    print(json.dumps(json.loads(report), indent=4, sort_keys=True))


def umount(mydrives):
    my_intranet = sshdmanager.get(sos_enums.Sshd.INTRANET)
    my_extranet = sshdmanager.get(sos_enums.Sshd.EXTRANET)
    if (my_intranet.pid != -1 and my_intranet.access == sos_enums.SshdAccess.OPEN) or (
        my_extranet.pid != -1 and my_extranet.access == sos_enums.SshdAccess.OPEN
    ):
        print("Won't unmount while sshds are in open state. Close them first.")
        exit()
    success = mydrives.umount_sos_volumes()
    if success:
        print(
            "All sos volumes unmounted - attention - this may not correspondent with the system state! "
        )
    else:
        print("could not unmount sos volumes")


if __name__ == "__main__":
    main()
