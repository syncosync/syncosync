#!/usr/bin/env python3

"""
This is the managing program for managing syncosync users. This is also to be seen as a blueprint for ui etc.
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse

from soscore import sosaccounts, soshelpers
from sosmodel import sosconstants
from sosmodel.serializerbase import SerializerBaseJsonEncoder
from sosmodel.sosaccount import SosAccountAddModel, SosAccountModel
from sosutils.logging import init_logging
from sosutils.runtime_args import parser_log_args


def main():
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="Manage syncosync accounts"
        "Hint: to load arguments from a file foo use something like"
        '-a "$(< foo)',
        epilog=sosconstants.EPILOG,
    )

    parser.add_argument(
        "-l", "--list", help="list accounts, output is always json", action="store_true"
    )
    parser.add_argument(
        "-m", "--mod", help="modify an account, input is json, see add how it works."
    )
    parser.add_argument(
        "-a",
        "--add",
        help='add an account, input is json e.g. {"name": "sostst",'
        '"rlongname": "sos Test Account",'
        ' "mail_address": "sostst@example.com",'
        '"backup_period": 7, "remind_duration": 22, "info_period": 7,'
        '"mail_info_level": "2", "max_space_allowed": 100,'
        '"password": "<cryptpw>", "ssh_pub_key": ""}'
        "\npassword has to be already crypted e.h. openssl passwd -6 mypasswd",
    )
    parser.add_argument("-g", "--get", help="get account by account name")
    parser.add_argument(
        "-d",
        "--delete",
        help="delete account, input is json, but could be as easy as only the name",
    )
    parser.add_argument(
        "-e",
        "--erase",
        help="erase data of account, input is json, but could be as easy as only the name",
    )
    parser.add_argument(
        "-p", "--pretty", help="formats json output", action="store_true", default=False
    )
    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)

    my_accounts = sosaccounts.SosAccounts()

    if args.add:
        new_account = SosAccountAddModel.from_json(args.add)
        my_accounts.add(new_account)

    if args.mod:
        if not my_accounts.mod_from_json(args.mod):
            print("Could not modify account")

    if args.list:
        accountlist = my_accounts.get()
        accountjson = SerializerBaseJsonEncoder.to_json(accountlist)
        print(soshelpers.pretty_print(accountjson, args.pretty))
        exit()

    if args.get:
        my_account: SosAccountModel = my_accounts.get_by_name(args.get)
        if my_account is not None:
            print(soshelpers.pretty_print(my_account.to_json(), args.pretty))
        else:
            print(f"Account {args.get} not found")

    if args.delete:
        del_account = SosAccountAddModel.from_json(args.delete)
        my_accounts.delete(del_account)

    if args.erase:
        erase_account = SosAccountAddModel.from_json(args.erase)
        my_accounts.erase_data(erase_account)


if __name__ == "__main__":
    main()
