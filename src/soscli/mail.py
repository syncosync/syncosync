#!/usr/bin/env python3
"""
The Script to test and configure the email module
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import argparse

from soscore import mail, soshelpers
from sosmodel import sosconstants
from sosutils.logging import init_logging
from sosutils.runtime_args import parser_log_args


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="syncosync mail configuration",
        epilog=sosconstants.EPILOG,
    )
    parser.add_argument(
        "-s",
        "--set",
        dest="set",
        help="Input json to modify mail setting variables, for example: "
        '{"admin_mail_address":"example@test.com", '
        '"remote_admin_mail_address":"example@test.com", "admin_info_period":7,'
        '"smtp_host":"smtp.gmail.com", "smtp_user":"myaddress@grmail.com",'
        '"smtp_passwd":"1234", "smtp_port":587, "system_mails":True}',
    )
    parser.add_argument(
        "-t",
        "--test",
        dest="test",
        help="Send an test eMail to the given mail "
        "you have to configure the mail settings first!",
    )
    parser.add_argument(
        "-p", "--pretty", help="formats json output", action="store_true", default=False
    )
    parser.add_argument(
        "-g", "--get", action="store_true", help="get mail settings as json"
    )

    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)

    mymail = mail.Mail()

    if args.test:
        if mail.send_test_mail(args.test):
            print("The mail was successfully sent!")
        else:
            print("There was a problem sending the mail")

    if args.get:
        print(soshelpers.pretty_print(mymail.get().to_json(), args.pretty))

    if args.set:
        mymail.set_from_json(args.set)


if __name__ == "__main__":
    main()
