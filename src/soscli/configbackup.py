#!/usr/bin/env python3

"""
This is the commandline access to configbackup functions
"""

import argparse

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import json

# following imports are all necessary for cli
import os

from soscore import configbackup, soshelpers
from sosmodel import sos_enums, sosconstants
from sosmodel.serializerbase import SerializerBaseJsonEncoder
from sosutils.logging import init_logging
from sosutils.runtime_args import parser_log_args


def main():  # noqa: C901
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="backup and restore of syncosync configuration",
        epilog=sosconstants.EPILOG,
    )
    parser.add_argument(
        "-b", "--backup", help="backup active configuration", action="store_true"
    )
    parser.add_argument(
        "-r", "--restore", help="restore configuration from filename given"
    )
    parser.add_argument(
        "-g",
        "--get",
        help="get json metadata of active configuration",
        action="store_true",
    )
    parser.add_argument(
        "-l",
        "--list",
        help="show json metadata all stored configuration backups",
        action="store_true",
    )
    parser.add_argument(
        "--latest",
        help="show json metadata of latest local configuration backup",
        action="store_true",
    )
    parser.add_argument(
        "--latest_remote",
        help="show json metadata of latest remote configuration backup",
        action="store_true",
    )
    parser.add_argument(
        "--remote",
        help="show json metadata of all remote configuration backups",
        action="store_true",
    )
    parser.add_argument("-o", "--other", help="show json metadata of backup file")
    parser.add_argument(
        "-c",
        "--check",
        help="check if backupfile could be unpacked with passwd (see -a)",
    )
    parser.add_argument(
        "-i",
        "--info",
        help="get info from backupfile, which will be unpacked with passwd (see -a)",
    )
    parser.add_argument(
        "-a", "--auth", help="backupfile password (for check and restore)"
    )
    parser.add_argument(
        "-p", "--pretty", help="formats json output", action="store_true", default=False
    )

    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)

    if args.backup:
        result = configbackup.backup()
        if result != "":
            print(f"backup successful to file {result}")
        else:
            print("backup not successful, see logs")

    if args.restore:
        if os.path.isfile(args.restore):
            result, report = configbackup.restore(args.restore, args.auth)
            if not result:
                print("Not successful")
            else:
                print("Successful")
            print(report)
        else:
            print("config file " + args.restore + " does not exist")
            exit(1)

    if args.check:
        if os.path.isfile(args.check):
            destdir = configbackup.unpack(args.check, args.auth)
            print(
                f"backupfile successfull unpacked to {destdir}, take care of removing it later"
            )
        else:
            print("config file " + args.check + " does not exist")
            exit(1)

    if args.info:
        if os.path.isfile(args.info):
            my_data = configbackup.get_data(args.info, args.auth)
            if my_data is not None:
                print(soshelpers.pretty_print(my_data.to_json(), args.pretty))
            else:
                print("Could not unpack (Wrong password?)")
        else:
            print("config file " + args.info + " does not exist")

    if args.get:
        config = configbackup.get()
        print(soshelpers.pretty_print(config.to_json(), args.pretty))

    if args.list:
        configlist = configbackup.get_stored()
        configlistjson = json.dumps(
            configlist, cls=SerializerBaseJsonEncoder, sort_keys=True, indent=4
        )
        print(soshelpers.pretty_print(configlistjson, args.pretty))

    if args.latest:
        config = configbackup.get_latest()
        print(soshelpers.pretty_print(config.to_json(), args.pretty))

    if args.latest_remote:
        config = configbackup.get_latest(
            os.path.join(
                sosconstants.MOUNT[sos_enums.Partition.REMOTE],
                sosconstants.SOS_CONF_BACKUP_PATH,
            )
        )
        print(soshelpers.pretty_print(config.to_json(), args.pretty))

    if args.remote:
        configlist = configbackup.get_stored(
            os.path.join(
                sosconstants.MOUNT[sos_enums.Partition.REMOTE],
                sosconstants.SOS_CONF_BACKUP_PATH,
            )
        )
        configlistjson = json.dumps(
            configlist, cls=SerializerBaseJsonEncoder, sort_keys=True, indent=4
        )
        print(soshelpers.pretty_print(configlistjson, args.pretty))

    if args.other:
        if os.path.isfile(args.other):
            config = configbackup.get_metadata_from_configbackup(args.other)
            if config is not None:
                print(soshelpers.pretty_print(config.to_json(), args.pretty))
            else:
                print(f"Could not get metadata from {args.other}, see log.")
        else:
            print("config file " + args.restore + " does not exist")
            exit(1)


if __name__ == "__main__":
    main()
