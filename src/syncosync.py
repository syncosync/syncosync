#!/usr/bin/env python3
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import grp
import os
import pwd
import signal
import sys
import time
from multiprocessing import Event
from typing import Optional

from soscore import sshdmanager
from soscore.soscoreManager import SoscoreManager, SoscoreManagerManager
from sosmodel.sos_enums import Sshd, SshdAccess
from sosui.sosui import Sosui
from sosutils.logging import LoggerCategory, get_logger, init_logging
from sosutils.runtime_args import parse_args


class Syncosync:
    def __init__(self, application_args):
        SoscoreManagerManager.register("SoscoreManager", SoscoreManager)
        self.__soscore_manager_manager: SoscoreManagerManager = SoscoreManagerManager()
        self.__soscore_manager: Optional[SoscoreManager] = None
        self.__sosui: Optional[Sosui] = None
        self.__application_args = application_args

    def start(self):
        if self.__sosui is not None or self.__soscore_manager is not None:
            logger = get_logger(LoggerCategory.soscore)
            logger.error("Cannot start sosui or core twice")
            return
        self.__soscore_manager_manager.start()
        self.__soscore_manager: SoscoreManager = (
            self.__soscore_manager_manager.SoscoreManager()
        )
        self.__start_sosui()

    def __start_sosui(self):
        self.__sosui = Sosui(
            self.__soscore_manager,
            self.__application_args.root_path,
            self.__application_args.debug,
        )

        uid = pwd.getpwnam("root").pw_uid  # should always be 0, but you never know
        if os.getuid() == uid:
            logger = get_logger(LoggerCategory.soscore)
            logger.debug("Running as root")
            uid_to_run_sosui_with = pwd.getpwnam("www-data").pw_uid
            gid_sosui_is_run_with = grp.getgrnam("www-data").gr_gid

            # TODO: pass as config parameter
            fullname = self.__soscore_manager_manager.address
            try:
                dirname = os.path.dirname(fullname)
                os.chown(dirname, 0, gid_sosui_is_run_with)
                os.chmod(dirname, 0o770)

                os.chown(fullname, 0, gid_sosui_is_run_with)
                os.chmod(fullname, 0o770)
            except Exception as e:  # TODO: check all possible exceptions
                logger.warning(f"Could not change ownership of socket: {e}")
                pass

            self.__sosui.start(uid_to_run_sosui_with, gid_sosui_is_run_with)
        else:
            # TODO: only call this while developing with intelliJ/angular!
            self.__sosui.start()

    def shutdown(self):
        logger = get_logger(LoggerCategory.soscore)
        logger.info("syncosync shutting down")
        # Place any code to run in soscore before the shutdown!
        self.__soscore_manager.shutdown_soscore()
        self.__sosui.stop_sosui()
        logger.debug("Stopping sosaccountacces")
        os.system("service sosaccountaccess stop")
        logger.debug("Stopping lsyncd")
        os.system("service lsyncd stop")
        # No funny business afterwards
        logger.info("Stopping soscore")
        self.__soscore_manager_manager.shutdown()
        self.__sosui.terminate()
        logger.info("Joining sosui")
        self.__sosui.join()


def main():
    args = parse_args()
    init_logging(args)
    logger = get_logger(LoggerCategory.soscore)

    stop_event: Event = Event()
    main_pid: int = os.getpid()
    logger.debug("Starting syncosync main with PID: " + str(main_pid))

    logger.debug("Disabling ssh server")
    os.system("systemctl disable ssh")

    def signal_handler(sig, frame):
        if os.getpid() != main_pid:
            logger.warning("Signal in subprocess")
            if sig == signal.SIGTERM and stop_event.is_set():
                logger.info("Received SIGTERM and syncosync is to be stopped")
                signal.signal(signal.SIGTERM, signal.SIG_DFL)
                os.kill(os.getpid(), signal.SIGTERM)
        logger.info(f"Received signal: {sig}")
        if not stop_event.is_set():
            logger.info("Setting stop signal")
        stop_event.set()

    # catches signal in all subprocesses for a proper shutdown
    signal.signal(signal.SIGINT, signal_handler)

    syncosync: Syncosync = Syncosync(args)
    syncosync.start()

    # SIGTERM should only be handled by the main process, subprocesses cannot be stopped otherwise (except for
    # cases where we propagate the SIGTERM. Since adding individual SIGTERM handlers each time is not what we want,
    # we will just stick to this way of handling it for now
    signal.signal(signal.SIGTERM, signal_handler)
    try:
        while not stop_event.is_set():
            time.sleep(1)
    except KeyboardInterrupt or Exception:
        logger.info("Received (keyboard) interrupt")
    finally:
        syncosync.shutdown()
        sshdmanager.change_access(Sshd.INTRANET, SshdAccess.OFF)
        sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OFF)
        logger.debug("Stopping sosaccountacces in main")
        os.system("service sosaccountaccess stop")
        logger.debug("Stopping lsyncd in main")
        os.system("service lsyncd stop")
        logger.debug("Enabling ssh server again")
        os.system("systemctl enable ssh")
        sys.exit(0)


if __name__ == "__main__":
    main()
