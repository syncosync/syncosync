# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import Optional

from flask import Flask, request
from soscore.soscoreManager import SoscoreManager
from sosui.sosui_endpoint import SosuiEndpoint
from sosui.status_codes import StatusCodes
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.sosui)


class SosuiRootEndpoint(SosuiEndpoint):
    """
    In its core, this endpoint serves / and catchall.
    Thus it is supposed to serve index.html and any static files as needed.
    """

    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        super().__init__(flask_app, soscore_manager)

    def _action(self, path: Optional[str] = None):
        if request.method == "GET":
            iso_language_str = self._get_language_str()
            if path and "." in path:
                if path.startswith(iso_language_str):
                    response = self._flask_app.send_static_file("ui/" + path)
                elif path.startswith("files"):
                    response = self._flask_app.send_static_file(path)
                else:
                    response = self._flask_app.send_static_file(
                        "ui/" + iso_language_str + "/" + path
                    )
            else:
                response = self._flask_app.send_static_file(
                    "ui/" + iso_language_str + "/index.html"
                )
            return response
        else:
            return SosuiEndpoint.get_response(
                response_status=StatusCodes.METHOD_NOT_ALLOWED
            )
