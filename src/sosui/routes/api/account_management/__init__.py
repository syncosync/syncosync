# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask
from soscore.soscoreManager import SoscoreManager
from sosui.routes.api.account_management.account_list_endpoint import (
    AccountListEndpoint,
)
from sosui.routes.api.account_management.add_account_endpoint import AddAccountEndpoint
from sosui.routes.api.account_management.delete_account_endpoint import (
    DeleteAccountEndpoint,
)
from sosui.routes.api.account_management.erase_data_account_endpoint import (
    EraseDataAccountEndpoint,
)
from sosui.routes.api.account_management.edit_account_endpoint import (
    EditAccountEndpoint,
)


def register_account_management_endpoints(
    flask_app: Flask, soscore_manager: SoscoreManager
):
    flask_app.add_url_rule(
        "/api/account_management/account_list",
        "account_list",
        AccountListEndpoint(flask_app, soscore_manager),
        methods=["GET"],
    )
    flask_app.add_url_rule(
        "/api/account_management/add_account",
        "add_account",
        AddAccountEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
    flask_app.add_url_rule(
        "/api/account_management/edit_account",
        "edit_account",
        EditAccountEndpoint(flask_app, soscore_manager),
        methods=["PUT"],
    )
    flask_app.add_url_rule(
        "/api/account_management/delete_account",
        "delete_account",
        DeleteAccountEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
    flask_app.add_url_rule(
        "/api/account_management/erase_data_account",
        "erase_data_account",
        EraseDataAccountEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
