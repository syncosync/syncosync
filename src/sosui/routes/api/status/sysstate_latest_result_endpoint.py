# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from typing import Optional

from flask import Flask, Response
from soscore.soscoreManager import SoscoreManager
from sosmodel.sysstate import SysStateResult
from sosui.sosui_endpoint import SosuiEndpoint, allowed_methods
from sosui.status_codes import StatusCodes


class SysstateLatestResultEndpoint(SosuiEndpoint):
    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        super().__init__(flask_app, soscore_manager)

    @allowed_methods(["GET"])
    def _action(
        self, path: Optional[str] = None, lang: Optional[str] = None
    ) -> Response:
        result: SysStateResult = self._soscore_manager.get_latest_result()
        return SosuiEndpoint.get_response(
            response_status=StatusCodes.OK, payload=result.to_json()
        )
