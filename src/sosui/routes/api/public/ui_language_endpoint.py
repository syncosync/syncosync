# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import Optional

from flask import Flask, request
from soscore.soscoreManager import SoscoreManager
from sosmodel.sosconstants import COOKIE_LANGUAGE
from sosmodel.uilanguage import UiLanguage
from sosui.sosui_endpoint import SosuiEndpoint, allowed_methods
from sosui.status_codes import StatusCodes


class UiLanguageEndpoint(SosuiEndpoint):
    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        super().__init__(flask_app, soscore_manager)

    @allowed_methods(["GET", "POST"])
    def _action(self, path: Optional[str] = None, lang: Optional[str] = None):
        if request.method == "GET":
            return self._get()
        elif request.method == "POST":
            return self._post()
        else:
            return SosuiEndpoint.get_response(
                response_status=StatusCodes.METHOD_NOT_ALLOWED
            )

    def _post(self):
        ui_language_to_set: UiLanguage = UiLanguage.from_json(request.data)
        response = SosuiEndpoint.get_response(
            response_status=StatusCodes.OK, payload=ui_language_to_set.to_json()
        )
        response.set_cookie(COOKIE_LANGUAGE, ui_language_to_set.to_json())
        derp = ui_language_to_set.to_json()
        derp.strip()
        return response

    def _get(self):
        language_set = request.cookies.get(COOKIE_LANGUAGE, None)
        if language_set:
            ui_language: UiLanguage = UiLanguage.from_json(language_set)
        else:
            # use systemlanguage
            ui_language: UiLanguage = self._soscore_manager.get_systemlanguage()
        return SosuiEndpoint.get_response(
            response_status=StatusCodes.OK, payload=ui_language.to_json()
        )
