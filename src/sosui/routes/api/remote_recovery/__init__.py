# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from flask import Flask
from soscore.soscoreManager import SoscoreManager
from sosui.routes.api.remote_recovery.cancel_endpoint import CancelEndpoint
from sosui.routes.api.remote_recovery.progress_endpoint import ProgressEndpoint
from sosui.routes.api.remote_recovery.sysstate_rr_result_endpoint import (
    SysstateRRResultEndpoint,
)


def register_remote_recovery_endpoints(
    flask_app: Flask, soscore_manager: SoscoreManager
):
    flask_app.add_url_rule(
        "/api/remote_recovery/progress",
        "remote_recovery/progress",
        ProgressEndpoint(flask_app, soscore_manager),
        methods=["GET"],
    )

    flask_app.add_url_rule(
        "/api/remote_recovery/rr_result",
        "remote_recovery/rr_result",
        SysstateRRResultEndpoint(flask_app, soscore_manager),
        methods=["GET"],
    )

    flask_app.add_url_rule(
        "/api/remote_recovery/cancel",
        "remote_recovery/cancel",
        CancelEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
