# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask
from soscore.soscoreManager import SoscoreManager
from sosui.routes.api.setup.admin_password_endpoint import AdminPasswordEndpoint
from sosui.routes.api.setup.available_timezones_endpoint import (
    AvailableTimezoneEndpoint,
)
from sosui.routes.api.setup.download_configbackup_endpoint import (
    DownloadConfigbackupEndpoint,
)
from sosui.routes.api.setup.drivesetup_endpoint import DrivesetupEndpoint
from sosui.routes.api.setup.factory_reset_endpoint import FactoryResetEndpoint
from sosui.routes.api.setup.hostname_endpoint import HostnameEndpoint
from sosui.routes.api.setup.is_leader_endpoint import IsLeaderEndpoint
from sosui.routes.api.setup.key_exchange_cancel import KeyExchangeCancelEndpoint
from sosui.routes.api.setup.key_exchange_follow import KeyExchangeFollowEndpoint
from sosui.routes.api.setup.key_exchange_lead import KeyExchangeLeadEndpoint
from sosui.routes.api.setup.local_key_endpoint import LocalKeyEndpoint
from sosui.routes.api.setup.mail_config_endpoint import MailConfigEndpoint
from sosui.routes.api.setup.mail_test_endpoint import MailTestEndpoint
from sosui.routes.api.setup.mode_endpoint import ModeEndpoint
from sosui.routes.api.setup.nic_config_endpoint import NicConfigEndpoint
from sosui.routes.api.setup.partition_drives_endpoint import PartitionDrivesEndpoint
from sosui.routes.api.setup.partitioning_lead import PartitioningLeadEndpoint
from sosui.routes.api.setup.partitioning_status_endpoint import (
    PartitioningStatusEndpoint,
)
from sosui.routes.api.setup.reboot_system_endpoint import RebootSystemEndpoint
from sosui.routes.api.setup.remote_host_endpoint import RemoteHostEndpoint
from sosui.routes.api.setup.remote_key_endpoint import RemoteKeyEndpoint
from sosui.routes.api.setup.resolv_config_endpoint import ResolvConfigEndpoint
from sosui.routes.api.setup.shutdown_system_endpoint import ShutdownSystemEndpoint
from sosui.routes.api.setup.sos_config_endpoint import SosConfigEndpoint
from sosui.routes.api.setup.ssh_config_endpoint import SshConfigEndpoint
from sosui.routes.api.setup.sync_ssd_lead_endpoint import SyncSsdLeadEndpoint
from sosui.routes.api.setup.timezone_endpoint import TimezoneEndpoint
from sosui.routes.api.setup.trafficshape_endpoint import TrafficshapeEndpoint
from sosui.routes.api.setup.trafficshape_mode_endpoint import TrafficshapeModeEndpoint


def register_setup_endpoints(flask_app: Flask, soscore_manager: SoscoreManager):
    flask_app.add_url_rule(
        "/api/setup/hostname",
        "hostname",
        HostnameEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/nic_config",
        "nic_config",
        NicConfigEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/resolv_config",
        "resolv_config",
        ResolvConfigEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/ssh_config",
        "ssh_config",
        SshConfigEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/local_key",
        "local_key",
        LocalKeyEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/remote_host",
        "remote_host",
        RemoteHostEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/remote_key",
        "remote_key",
        RemoteKeyEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST", "DELETE"],
    )
    flask_app.add_url_rule(
        "/api/setup/key_exchange_cancel",
        "key_exchange_cancel",
        KeyExchangeCancelEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/key_exchange_lead",
        "key_exchange_lead",
        KeyExchangeLeadEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/key_exchange_follow",
        "key_exchange_follow",
        KeyExchangeFollowEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/admin_password",
        "admin_password",
        AdminPasswordEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/mode",
        "mode",
        ModeEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/drivesetup",
        "drivesetup",
        DrivesetupEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/mail_setup",
        "mail_setup",
        MailConfigEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/mail_test",
        "mail_test",
        MailTestEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/sos_config",
        "sos_config",
        SosConfigEndpoint(flask_app, soscore_manager),
        methods=["GET"],
    )
    flask_app.add_url_rule(
        "/api/setup/shutdown_system",
        "shutdown_system",
        ShutdownSystemEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/partition_drives",
        "partition_drives",
        PartitionDrivesEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/is_leader",
        "is_leader",
        IsLeaderEndpoint(flask_app, soscore_manager),
        methods=["GET"],
    )
    flask_app.add_url_rule(
        "/api/setup/partitioning_lead",
        "partitioning_data",
        PartitioningLeadEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/timezone",
        "timezone_setup",
        TimezoneEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/available_timezone",
        "available_timezone",
        AvailableTimezoneEndpoint(flask_app, soscore_manager),
        methods=["GET"],
    )
    flask_app.add_url_rule(
        "/api/setup/trafficshape",
        "trafficshape",
        TrafficshapeEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/trafficshape_mode",
        "trafficshape_mode",
        TrafficshapeModeEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/factory_reset",
        "factory_reset",
        FactoryResetEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/reboot_system",
        "reboot_system",
        RebootSystemEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/download_configbackup",
        "download_configbackup",
        DownloadConfigbackupEndpoint(flask_app, soscore_manager),
        methods=["GET"],
    )
    flask_app.add_url_rule(
        "/api/setup/sync_ssd_lead",
        "sync_ssd_lead",
        SyncSsdLeadEndpoint(flask_app, soscore_manager),
        methods=["POST"],
    )
    flask_app.add_url_rule(
        "/api/setup/partitioning_status",
        "partitioning_status",
        PartitioningStatusEndpoint(flask_app, soscore_manager),
        methods=["GET", "POST"],
    )
