# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import Optional

from flask import Flask, Response, request
from soscore.drivemanager import Drives
from soscore.soscoreManager import SoscoreManager
from sosmodel.drivesetupDriveConfiguration import DrivesetupDriveConfiguration
from sosmodel.genericUiResponse import GenericUiResponse
from sosmodel.localizedMessages import LocalizedMessage
from sosui.sosui_endpoint import SosuiEndpoint, allowed_methods, requires_authentication
from sosui.status_codes import StatusCodes


class DrivesetupEndpoint(SosuiEndpoint):
    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        super().__init__(flask_app, soscore_manager)

    @requires_authentication()
    @allowed_methods(["GET", "POST"])
    def _action(self, path: Optional[str] = None, lang: Optional[str] = None):
        if request.method == "GET":
            return self.get(path, lang)
        elif request.method == "POST":
            return self.post(path, lang)

    def get(self, path: Optional[str] = None, lang: Optional[str] = None) -> Response:
        drives: Drives = self._soscore_manager.get_drives()
        return SosuiEndpoint.get_response(
            response_status=StatusCodes.OK, payload=drives.to_json()
        )

    def post(self, path: Optional[str] = None, lang: Optional[str] = None) -> Response:
        drive_config: DrivesetupDriveConfiguration = (
            DrivesetupDriveConfiguration.from_json(request.data)
        )
        response_body: GenericUiResponse = GenericUiResponse()
        self._soscore_manager.format_drives(drive_config)
        # TODO: Adjust additional content and localized message
        response_body.localized_reason = LocalizedMessage.DRIVESETUP_DISK_FORMAT_DONE
        return SosuiEndpoint.get_response(
            response_status=StatusCodes.OK, payload=response_body.to_json()
        )
