# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import Optional

from flask import Flask, request
from soscore.soscoreManager import SoscoreManager
from sosmodel.genericUiResponse import GenericUiResponse
from sosmodel.soskey import SosKeyModel
from sosui.sosui_endpoint import SosuiEndpoint, allowed_methods, requires_authentication
from sosui.status_codes import StatusCodes


class RemoteKeyEndpoint(SosuiEndpoint):
    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        super().__init__(flask_app, soscore_manager)

    @requires_authentication()
    @allowed_methods(["GET", "POST", "DELETE"])
    def _action(self, path: Optional[str] = None, lang: Optional[str] = None):
        if request.method == "GET":
            return self._get()
        elif request.method == "POST":
            return self._post()
        elif request.method == "DELETE":
            return self._delete()
        else:
            return SosuiEndpoint.get_response(
                response_status=StatusCodes.METHOD_NOT_ALLOWED
            )

    def _delete(self):
        # delete the remote key we accepted, thus terminating sync
        self._soscore_manager.delete_remote_key()
        response: SosKeyModel = self._soscore_manager.get_remote_key()
        return SosuiEndpoint.get_response(
            response_status=StatusCodes.OK, payload=response.to_json()
        )

    def _post(self):
        # Accept the key we last received during key exchange
        response: GenericUiResponse = GenericUiResponse()
        self._soscore_manager.accept_remote_key()
        return SosuiEndpoint.get_response(
            response_status=StatusCodes.OK, payload=response.to_json()
        )

    def _get(self):
        # Simply fetch the remote key we accepted
        response: SosKeyModel = self._soscore_manager.get_remote_key()
        return SosuiEndpoint.get_response(
            response_status=StatusCodes.OK, payload=response.to_json()
        )
