# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import Optional

from flask import Flask, request
from soscore.soscoreManager import SoscoreManager
from soscore.soserrors import ConfigBackupFileDecryptionError
from sosmodel.sysstate import SysStateResult, SysStateTransportModel
from sosui.sosui_endpoint import SosuiEndpoint, allowed_methods, requires_authentication
from sosui.status_codes import StatusCodes
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.sosui)


class ModeEndpoint(SosuiEndpoint):
    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        super().__init__(flask_app, soscore_manager)

    @requires_authentication()
    @allowed_methods(["GET", "POST"])
    def _action(self, path: Optional[str] = None, lang: Optional[str] = None):
        if request.method == "GET":
            return self._get()
        elif request.method == "POST":
            return self._post()
        else:
            return SosuiEndpoint.get_response(
                response_status=StatusCodes.METHOD_NOT_ALLOWED
            )

    def _get(self):
        sysstate: SysStateTransportModel = self._soscore_manager.get_system_state()
        return SosuiEndpoint.get_response(
            response_status=StatusCodes.OK, payload=sysstate.to_json()
        )

    def _post(self):
        sysstate: SysStateTransportModel = SysStateTransportModel.from_json(
            request.data
        )
        try:
            logger.debug(f"Switch Action: {sysstate.switch_action}")
            res: SysStateResult = self._soscore_manager.set_system_state(sysstate)
        except ConfigBackupFileDecryptionError as e:
            logger.warning(
                "Failed decrypting config backup while trying to set sysstate to {}",
                sysstate.state_desired,
            )
            raise e

        return SosuiEndpoint.get_response(
            response_status=StatusCodes.OK, payload=res.to_json()
        )
