# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import json
from typing import Optional

from flask import Flask, request
from soscore.soscoreManager import SoscoreManager
from sosui.sosui_endpoint import SosuiEndpoint, allowed_methods
from sosui.status_codes import StatusCodes
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.sosui)


class LoginEndpoint(SosuiEndpoint):
    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        super().__init__(flask_app, soscore_manager)

    @allowed_methods(["POST"])
    def _action(self, path: Optional[str] = None, lang: Optional[str] = None):
        if request.method == "POST":
            # Login attempt!
            try:
                payload = json.loads(request.data)
                password_entered = payload.get("password", None)
                if password_entered is None:
                    return SosuiEndpoint.get_response(
                        response_status=StatusCodes.UNAUTHORIZED
                    )
                token = self._soscore_manager.login(password_entered)
                if token is None:
                    return SosuiEndpoint.get_response(
                        response_status=StatusCodes.UNAUTHORIZED
                    )

                # we got a token we can return...
                retval = {"token": str(token)}
                return SosuiEndpoint.get_response(
                    response_status=StatusCodes.OK, payload=json.dumps(retval)
                )
            except Exception as e:
                logger.exception(e)
                return SosuiEndpoint.get_response(
                    response_status=StatusCodes.INTERNAL_SERVER_ERROR
                )
