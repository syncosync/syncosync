# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from abc import ABC, abstractmethod
from datetime import datetime
from functools import wraps
from typing import List, Optional

from flask import Flask, Response, request
from soscore.soscoreManager import SoscoreManager
from soscore.soserrors import SosError
from sosmodel.genericUiResponse import GenericUiResponse
from sosmodel.localizedMessages import LocalizedMessage
from sosmodel.sos_enums import GenericUiResponseStatus, SupportedLanguage
from sosmodel.sosconstants import COOKIE_LANGUAGE
from sosmodel.uilanguage import UiLanguage
from sosui.status_codes import StatusCodes
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.sosui)


def allowed_methods(methods):
    def wrapper(f):
        @wraps(f)
        def wrapped(self: SosuiEndpoint, *f_args) -> Response:
            if request.method not in methods:
                logger.error(
                    f"Method {str(request.method)} not allowed for endpoint with allowed methods {str(methods)}"
                )
                return SosuiEndpoint.get_response(
                    response_status=StatusCodes.METHOD_NOT_ALLOWED
                )
            return f(self, *f_args)

        return wrapped

    return wrapper


def requires_authentication():
    def wrapper(f):
        @wraps(f)
        def wrapped(self: SosuiEndpoint, *f_args) -> Response:
            token = request.headers.get("Authorization", None)
            if (
                token is None
                or not token.startswith("Bearer ")
                or not len(token.split()) == 2
                or not self._soscore_manager.validate_token(token.split()[1])
            ):
                logger.error(f"Authorization header '{token}' is invalid")
                return SosuiEndpoint.get_response(
                    response_status=StatusCodes.UNAUTHORIZED
                )
            return f(self, *f_args)

        return wrapped

    return wrapper


class SosuiEndpoint(ABC):
    """
    SosuiEndpoint represents a simple endpoint executing an action responding
    with an empty response payload
    """

    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        self._flask_app: Flask = flask_app
        self._soscore_manager: SoscoreManager = soscore_manager
        self._response: Response = Response(status=StatusCodes.OK)

    def __call__(self, *args, path: Optional[str] = None) -> Response:
        # noinspection PyBroadException
        response: Response
        response_body: Optional[GenericUiResponse] = None
        try:
            response = self._action(path)
            response.headers["Last-Modified"] = int(datetime.now().timestamp())
            response.headers[
                "Cache-Control"
            ] = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0"
            response.headers["Pragma"] = "no-cache"
            response.headers["Expires"] = "-1"
        except SosError as e:
            response_body = GenericUiResponse()
            response_body.status = GenericUiResponseStatus.ERROR
            response_body.localized_reason = e.localized_message
            response_body.additional_information = e.message
            response = SosuiEndpoint.get_response(
                response_status=StatusCodes.INTERNAL_SERVER_ERROR,
                payload=response_body.to_json(),
            )
        except Exception as e:
            # TODO: Proper logging!
            logger.error(
                f"Exception occurred during the handling of a request({path}): {str(e)}"
            )
            logger.exception(e)
            response_body = GenericUiResponse()
            response_body.status = GenericUiResponseStatus.ERROR
            # TODO: This should be LocalizedMessage[e.args[0]]... but it would break to much atm
            response_body.localized_reason = LocalizedMessage.UNKNOWN_ERROR
            response_body.additional_information = str(e)
            response = SosuiEndpoint.get_response(
                response_status=StatusCodes.INTERNAL_SERVER_ERROR,
                payload=response_body.to_json(),
            )
        return response

    @staticmethod
    def get_response(response_status: StatusCodes, payload: str = "{}") -> Response:
        """
        Method to get a new response with a status code and a simple json payload.
        """
        response = Response(status=response_status.value)
        response.data = payload
        return response

    @abstractmethod
    def _action(self, path: Optional[str] = None) -> Response:
        """
        Defines the action the endpoint represents.
        If self._set_response is not called, the default Status 200 (OK) will be returned.
        """

    def _get_language_str(self):
        language_set = request.cookies.get(COOKIE_LANGUAGE, None)
        iso_language_str = "en-US"
        if language_set:
            ui_language: UiLanguage = UiLanguage.from_json(language_set)
            if ui_language.language == SupportedLanguage.UNSET:
                # serve best browser match
                supported_languages: List[str] = [
                    SupportedLanguage.to_iso(lang_enum_val)
                    for lang_enum_val in SupportedLanguage
                    if lang_enum_val != SupportedLanguage.UNSET
                ]
                iso_language_str = request.accept_languages.best_match(
                    supported_languages
                )
            else:
                # serve matching language
                iso_language_str = SupportedLanguage.to_iso(ui_language.language)
        else:
            # use systemlanguage
            ui_language: UiLanguage = self._soscore_manager.get_systemlanguage()
            iso_language_str = SupportedLanguage.to_iso(ui_language.language)
            if iso_language_str == "":
                supported_languages: List[str] = [
                    SupportedLanguage.to_iso(lang_enum_val)
                    for lang_enum_val in SupportedLanguage
                    if lang_enum_val != SupportedLanguage.UNSET
                ]
                iso_language_str = request.accept_languages.best_match(
                    supported_languages
                )
        return iso_language_str
