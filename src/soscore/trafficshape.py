"""
Shaping traffic to/from remotehost
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from datetime import datetime
from typing import Optional

from soscore import configbase, remotehost, soshelpers_file, soshelpers_shape, sysstate
from sosmodel.sos_enums import SystemMode, TrafficShapeSwitch
from sosmodel.sosconstants import SOS_CONF_TRAFFICSHAPE, TRAFFICSHAPE_SPECIFIC_CACHE
from sosmodel.sysstate import SysStateResult
from sosmodel.trafficshape import (
    TrafficShapeModel,
    TrafficShapeSpecificModel,
    TrafficShapeSwitchModel,
)
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def get_cached_json():
    if os.path.isfile(TRAFFICSHAPE_SPECIFIC_CACHE):
        with open(TRAFFICSHAPE_SPECIFIC_CACHE) as json_file:
            return json_file.read()
    return None


def get_cached_settings() -> Optional[TrafficShapeSpecificModel]:
    data = get_cached_json()
    if data is not None:
        return TrafficShapeSpecificModel.from_json(data)
    else:
        return None


class TrafficShape(configbase.ConfigBase):
    """
    Traffic Shaping
    """

    def __init__(self):
        super().__init__(TrafficShapeModel(), SOS_CONF_TRAFFICSHAPE)

    def provide_model(self):
        return TrafficShapeModel()

    def is_it_day(self) -> bool:
        """
        checks, if the actual time is in between day and night borders
        :return: True if yes
        """
        self.config: TrafficShapeModel
        minutes_of_day = datetime.now().minute + 60 * datetime.now().hour
        is_it_day = self.config.day <= minutes_of_day <= self.config.night
        if self.config.day >= self.config.night:
            is_it_day = not is_it_day
        return is_it_day

    @staticmethod
    def _set_and_cache(newshape: TrafficShapeSpecificModel):
        has_changed = True
        if os.path.isfile(TRAFFICSHAPE_SPECIFIC_CACHE):
            with open(TRAFFICSHAPE_SPECIFIC_CACHE) as json_file:
                if newshape.to_json() == json_file.read():
                    has_changed = False
        if has_changed:
            soshelpers_shape.shape_mod_bw(newshape.outgoing, newshape.incoming)
            soshelpers_file.write_to_file(
                TRAFFICSHAPE_SPECIFIC_CACHE, newshape.to_json()
            )

    def set_specific(self):
        self.config: TrafficShapeModel
        newshape = TrafficShapeSpecificModel()
        logger.debug(f"Checking specific against: {self.config.to_json()}")
        newshape.day = self.is_it_day()
        if newshape.day:
            newshape.outgoing = self.config.out_day
            newshape.incoming = self.config.in_day
        else:
            newshape.day = False
            newshape.outgoing = self.config.out_night
            newshape.incoming = self.config.in_night
        self._set_and_cache(newshape)
        return True

    def set_at_time(self):
        self.config: TrafficShapeModel
        minutes_of_day = datetime.now().minute + 60 * datetime.now().hour
        newshape = TrafficShapeSpecificModel()

        if minutes_of_day == self.config.day:
            newshape.outgoing = self.config.out_day
            newshape.incoming = self.config.in_day
            self._set_and_cache(newshape)

        if minutes_of_day == self.config.night:
            newshape.day = False
            newshape.outgoing = self.config.out_night
            newshape.incoming = self.config.in_night
            self._set_and_cache(newshape)

    def toggle(self):
        """
        switches from day to night mode and vice versa
        """
        self.config: TrafficShapeModel
        newshape = TrafficShapeSpecificModel()
        oldshape: TrafficShapeSpecificModel = get_cached_settings()
        if oldshape is not None and oldshape.day is not True:
            # set to day
            newshape.outgoing = self.config.out_day
            newshape.incoming = self.config.in_day
            self._set_and_cache(newshape)
        else:
            newshape.day = False
            newshape.outgoing = self.config.out_night
            newshape.incoming = self.config.in_night
            self._set_and_cache(newshape)

    def get_mode(self) -> TrafficShapeSwitchModel:
        """returns the actual mode"""
        out_mode = TrafficShapeSwitchModel()
        actshape = get_cached_settings()
        mystate = sysstate.SysState()
        mystatestate = mystate.get()
        if mystatestate.actmode == SystemMode.DEFAULT_NO_SYNC:
            out_mode.mode = TrafficShapeSwitch.OFF
        else:
            if actshape.day:
                out_mode.mode = TrafficShapeSwitch.DAY
            else:
                out_mode.mode = TrafficShapeSwitch.NIGHT
        return out_mode

    def force_mode(self, mode: TrafficShapeSwitchModel):
        """
        sets the new mode on request...
        """
        # first handle shaping
        oldshape: TrafficShapeSpecificModel = get_cached_settings()
        if (
            oldshape is not None
            and oldshape.day is True
            and mode.mode == TrafficShapeSwitch.NIGHT
        ) or (oldshape is None and mode.mode == TrafficShapeSwitch.NIGHT):
            # ok set to night
            newshape = TrafficShapeSpecificModel()
            newshape.day = False
            newshape.outgoing = self.config.out_night
            newshape.incoming = self.config.in_night
            self._set_and_cache(newshape)
        elif (
            oldshape is not None
            and oldshape.day is False
            and mode.mode == TrafficShapeSwitch.DAY
        ) or (oldshape is None and mode.mode == TrafficShapeSwitch.DAY):
            # ok set to day
            newshape = TrafficShapeSpecificModel()
            newshape.day = True
            newshape.outgoing = self.config.out_day
            newshape.incoming = self.config.in_day
            self._set_and_cache(newshape)
        # now handle sync / no sync
        mystate = sysstate.SysState()
        mystatestate = mystate.get()
        result: SysStateResult
        if mode.mode == TrafficShapeSwitch.OFF:
            if mystatestate.actmode == SystemMode.DEFAULT_NO_SYNC:
                logger.debug("Mode is already DEFAULT_NO_SYNC. No modechange")
                return
            if mystatestate.actmode == SystemMode.DEFAULT:
                result = mystate.changemode(SystemMode.DEFAULT_NO_SYNC)
                if result.mode != SystemMode.DEFAULT_NO_SYNC:
                    logger.error(
                        "Could not switch to DEFAULT_NO_SYNC mode from traffic shape"
                    )
            else:
                logger.warning(
                    f"Mode change to DEFAULT_NO_SYNC requested with wrong mode: {SystemMode.message(mystatestate.actmode)}"
                )
        else:
            if mystatestate.actmode == SystemMode.DEFAULT:
                logger.debug("Mode is already DEFAULT. No modechange")
                return
            if mystatestate.actmode == SystemMode.DEFAULT_NO_SYNC:
                result = mystate.changemode(SystemMode.DEFAULT)
                if result.mode != SystemMode.DEFAULT:
                    logger.error("Could not switch to DEFAULT mode from traffic shape")
            else:
                logger.warning(
                    f"Mode change to DEFAULT requested with wrong mode: {SystemMode.message(mystatestate.actmode)}"
                )

    def start(self):
        """
        starts shaping
        """
        self.config: TrafficShapeModel
        if not soshelpers_shape.is_shape_on():
            myremotehost = remotehost.RemoteHost().get()
            soshelpers_shape.shape_on(myremotehost.port)
            self.set_specific()
        else:
            logger.warning("Shaping should already be on. Please check manually")

    @staticmethod
    def stop():
        """Stops shaping"""
        soshelpers_shape.shape_off()
        if os.path.isfile(TRAFFICSHAPE_SPECIFIC_CACHE):
            os.remove(TRAFFICSHAPE_SPECIFIC_CACHE)
