import glob
import json
import os
import socket
import spwd
import sys
import tarfile
import tempfile
import time

import gnupg
from soscore import sosaccounts, soshelpers, soshelpers_file
from sosmodel import sos_enums, sosconstants
from sosmodel.configbackup import ConfigBackupManifestModel, ConfigBackupModel
from sosmodel.serializerbase import SerializerBaseJsonEncoder
from sosmodel.sosaccount import SosAccountAddModel
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def keep_max_number():
    """
    deletes all config backups which exceed SOS_CONF_BACKUP_MAX
    :return:
    """
    files = sorted(
        glob.glob(
            os.path.join(
                sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                sosconstants.SOS_CONF_BACKUP_PATH,
            )
            + "*"
        ),
        reverse=True,
    )
    # print(len(files), files)
    for i in range(sosconstants.SOS_CONF_BACKUP_MAX + 1, len(files)):
        try:
            os.remove(files[i])
            logger.debug(f"Deleted odd config backup: {files[i]}")
        except FileNotFoundError:
            logger.error(f"Could not delete odd config backup: {files[i]}")


def create_archive():
    """
    creates a config backup tar gz as a temp file without deleting it

    :return: the filepath of the archive
    """
    # ok so, we have some place, where we can backup the configuration, let's go
    start_dir = os.getcwd()
    with tempfile.TemporaryDirectory() as target_dir:
        os.chdir(target_dir)
        # now get the sosusers
        mysosaccounts = sosaccounts.SosAccounts()
        accountlist = mysosaccounts.get()
        saveaccountlist = []
        # accountjson = "["
        for account in accountlist:
            saveaccount = SosAccountAddModel.from_json(account.to_json())
            #  add the password
            saveaccount._password = spwd.getspnam(account.name)[1]
            # add the ssh key
            if os.path.isfile(
                os.path.join(sosconstants.ACCOUNT_KEYFILE_DIR, saveaccount.name)
            ):
                # ok, we have a file. Should we check it or is it ok, just to add it?
                with open(
                    os.path.join(sosconstants.ACCOUNT_KEYFILE_DIR, saveaccount.name)
                ) as keyfile:
                    saveaccount.ssh_pub_key = keyfile.read()
            saveaccountlist.append(saveaccount)
        sosadminaccount = SosAccountAddModel.from_json('{"name":"sosadmin"}')
        sosadminaccount.password = spwd.getspnam("sosadmin")[1]
        saveaccountlist.append(sosadminaccount)
        # accountjson = accountjson + "]"
        accountjson = SerializerBaseJsonEncoder.to_json(saveaccountlist)
        soshelpers_file.write_to_file(target_dir + "/sosaccounts.json", accountjson)
        with tempfile.NamedTemporaryFile("wb", suffix=".tgz", delete=False) as f:
            with tarfile.open(fileobj=f, mode="w:gz") as tar:
                for name in sosconstants.CONFIGBACKUP_ITEMS:
                    if os.path.isfile(name) or os.path.isdir(name):
                        tar.add(name)
                    else:
                        logger.warning(f"file {name} not found for backup")
                tar.close()
            f.close()
    os.chdir(start_dir)
    return f.name


def backup():
    configbackup = ConfigBackupModel()
    """
    backups the configuration to /mnt/local.. and sets metadata
    :return: destfilepath of backed up config, "" if not successful
    """
    logger.debug(
        f"backup of backup config called from {sys._getframe(1).f_code.co_name}"
    )
    local_mountpath, local_params = soshelpers_file.get_mountpoint(
        sosconstants.MOUNTDEV[sos_enums.Partition.LOCAL]
    )
    if local_mountpath != sosconstants.MOUNT[sos_enums.Partition.LOCAL]:
        logger.error(
            "Local volume not mounted locally. No configuration backup possible"
        )
        return ""
    else:
        configbackup.timestamp = int(time.time())
        configbackup.hostname = (
            socket.gethostname()
        )  # TODO: should we get this from hostname core?
        soshelpers_file.write_to_file(
            sosconstants.SOS_CONF_TIMESTAMP_FILEPATH,
            json.dumps({"timestamp": configbackup.timestamp}),
        )
        archive_filepath = create_archive()
        configbackup.md5sum = soshelpers.md5Checksum(archive_filepath)

        # this is the timestamp for the filename
        file_timestamp = time.strftime(
            "%Y%m%d%H%M%S", time.gmtime(configbackup.timestamp)
        )

        # now, we know already, that the local volume is mounted, but we should also check if the destination
        # directory exists
        if not os.path.isdir(
            os.path.join(
                sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                sosconstants.SOS_CONF_BACKUP_PATH,
            )
        ):
            os.makedirs(
                os.path.join(
                    sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                    sosconstants.SOS_CONF_BACKUP_PATH,
                )
            )

        start_dir = os.getcwd()
        with tempfile.TemporaryDirectory() as target_dir:
            os.chdir(target_dir)

            # encrypt the archive
            gpg = gnupg.GPG()
            # now get the password information for encryption
            mypwdict = {}
            mypwdict = soshelpers.parse_shadowpwd_data(spwd.getspnam("sosadmin")[1])
            if mypwdict is None or "salt" not in mypwdict:
                logger.error(
                    "sosadmin passwordhash has no salt!? Aborting backupconfig",
                )
                return ""

            base64_salt = soshelpers.str2base64(mypwdict["salt"])

            manifest = ConfigBackupManifestModel()
            manifest.set(
                timestamp=file_timestamp,
                md5sum=configbackup.md5sum,
                hostname=configbackup.hostname,
                salt=base64_salt,
            )
            destfile = "content.tar.gz.gpg"

            destbackupfilepath = (
                os.path.join(
                    sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                    sosconstants.SOS_CONF_BACKUP_PATH,
                )
                + "sosconf_"
                + file_timestamp
                + "_"
                + configbackup.hostname
                + ".tgz"
            )
            soshelpers_file.write_to_file("manifest.json", manifest.to_json())
            with open(archive_filepath, "rb") as f:
                passphrase = mypwdict["salt"] + "$" + mypwdict["pwhash"]
                gpg.encrypt_file(
                    f,
                    recipients=None,
                    symmetric="AES256",
                    output=destfile,
                    passphrase=passphrase,
                    armor=False,
                )
            with open(destbackupfilepath, "wb") as f:
                with tarfile.open(fileobj=f, mode="w:gz") as tar:
                    tar.add("manifest.json")
                    tar.add(destfile)
                    tar.close()
                f.close()

            logger.info(f"written configuration backup to {destbackupfilepath}")
            os.remove(archive_filepath)
            os.remove(destfile)
            os.remove("manifest.json")
            os.chdir(start_dir)

        akt_dir = os.getcwd()
        os.chdir(
            os.path.join(
                sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                sosconstants.SOS_CONF_BACKUP_PATH,
            )
        )
        os.chdir(akt_dir)
        # some housekeeping
        keep_max_number()
        # store the metadata of the last configbackup
        soshelpers_file.write_to_file(
            sosconstants.SOS_CONF_BACKUP_METADATA, configbackup.to_json()
        )
        soshelpers_file.unset_configbackup_flag()
        return destbackupfilepath
