"""
configbackup tests
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest

from soscore import soshelpers

shadowentry = "$6$F6Q9gSJwkfuIxRGs$tTEQB5J0LPBqhRifDxJOv8GLKuRq0/IxF8rS8nRJy9n8jtVi/qvWgvnnmzfpDK/pAXJpNc2m2NKjsF4f8.feL/"


class Test_ConfigBackup(unittest.TestCase):
    def test_shadowpwp_parsing(self):
        myshadowdict = soshelpers.parse_shadowpwd_data(shadowentry)
        print("salt:", myshadowdict["salt"])
        print("pwhash:", myshadowdict["pwhash"])
        print("base64(salt):", soshelpers.str2base64(myshadowdict["salt"]))


if "__main__" == __name__:
    unittest.main()
