"""
managing all around drives, volumes etc.
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2024  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import math
import os
import re
import shutil
import stat
import subprocess
import time
import collections
from subprocess import PIPE, STDOUT, Popen, run
from typing import Dict, List, Optional, Tuple

import parted
import psutil
from soscore import configbackup, sosconfig, soserrors, soshelpers_file, sysstate
from sosmodel import sosconstants
from sosmodel.drivesetupDriveConfiguration import DrivesetupDriveConfiguration
from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import Partition, SystemMode, VolMount
from sosmodel.sosconstants import MOUNT, MOUNTDEV, MOUNTOPTS, RUNDIR
from sosmodel.syncstat import DriveSpaceModel
from sosmodel.systemsetupdata import PartitioningData
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def check_drive_check(drive):
    """
    checks if a drive could be checked at all
    :param drive: just the device without any path, e.g. sda
    :return: True if drive is checkable
    """
    result_read = os.popen("hdparm -C /dev/" + drive).read()
    m = re.search(
        ".*unknown.*", result_read
    )  # let's assume, if a drive is unknown, we should not ask for it anymore
    if m:
        return False
    else:
        return True


def start_hdidle(hdidleargs: List[str] = []):
    """starts or restarts hd-idle

    Args:
        hdidleargs: arguments for hd-idle: e.g. ['-i', '0', '-a', 'sda', '-i', '300']
    """
    cmdline = ["/usr/sbin/hd-idle"] + hdidleargs
    # Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if "hd-idle".lower() in proc.name().lower():
                if proc.cmdline() != cmdline:
                    proccmdlinestr = " ".join(proc.cmdline())
                    cmdlinestr = " ".join(cmdline)
                    logger.debug(
                        f"Killing hd-idle as arguments do not match: {proccmdlinestr} - {cmdlinestr}"
                    )
                    proc.kill()
                else:
                    return
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    # now, we have either not found a running hd-idle or we have stopped it, because it
    # has different arguments
    cmdlinestr = " ".join(cmdline)
    try:
        subprocess.run(
            "start-stop-daemon --start --quiet --oknodo --exec /usr/sbin/hd-idle -- "
            + " ".join(hdidleargs),
            shell=True,
        )
    except Exception as e:

        logger.error(f"starting {cmdlinestr} not possible: {e}")
    logger.debug(f"started {cmdlinestr}")


def call_e2fsck(devicepath) -> bool:
    logger.debug(f"running e2fsck on {devicepath}")
    try:
        run_result = run(
            ["/usr/sbin/e2fsck", "-p", "-f", devicepath],
            shell=False,
            check=False,
            capture_output=True,
        )
        if run_result.returncode != 0:
            logger.error(f"e2fsck on {devicepath} returned {run_result.returncode}: {run_result.stdout}")
            return False
        return True
    except Exception as e:
        logger.warning(f"Something went wrong, calling e2fsck -p -f {devicepath}: {e}")
        return False

def call_resize2fs(devicepath: str, param: str, mounted=False) -> str:
    """
    returns the output of the resize2fs command

    Args:
        devicepath (str): full path of volume (e.g. /dev/sos/local)
        param (str): parameter for resize2fs
        mounted (boolean): if the device is mounted or not

    Returns:
        str: output from resize2fs
    """
    try:
        cmd = "resize2fs " + param + " " + devicepath
        # logger.debug(f"cmd for resizefs is {cmd}")

        p = Popen(
            cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True
        )
        command_read = ""
        if p.stdout is not None:
            command_read = p.stdout.read().decode("utf-8")
            if ("Please run 'e2fsck -f " + devicepath + "' first.") in command_read:
                if not call_e2fsck(devicepath):
                    return "fserror" 
                p = Popen(
                    cmd,
                    shell=True,
                    stdin=PIPE,
                    stdout=PIPE,
                    stderr=STDOUT,
                    close_fds=True,
                )
                if p.returncode != 0:
                    logger.error(
                    f"resize2fs {param} {devicepath}: returned != 0: {command_read}"
                    )
                    return "resize2fs error"
                if p.stdout is not None:
                    command_read = p.stdout.read().decode("utf-8")
    except Exception as e:
        logger.error(
            f"Something went wrong, calling resize2fs {param} {devicepath}:{str(e)}"
        )
        return "resize2fs error"
    # logger.debug(f"command read from resizefs {param} is {command_read}")
    return command_read


def get_minmal_size(devicepath: str, mounted=False) -> int:
    """
    returns the minimal size a volume could be reduced to in extends (blocks of 4MB)

    Args:
        devicepath (str): full path of volume (e.g. /dev/sos/local)
        mounted (boolean): if the device is mounted or not

    Returns:
        int: minimal size in blocks
        -1 if error in filesystem
    """
    command_read = call_resize2fs(devicepath, "-P", mounted)
    if command_read == "resize2fs error" or command_read == "fserror":
        return -1
    if command_read != "":
        m = re.search(
            r".*Estimated minimum size of the filesystem: ([0-9]*).*", command_read
        )
        if m:
            return math.ceil(int(m.group(1)) / 4096)
        else:
            return 0
    else:
        return 0


def set_minimal_size(devicepath: str, mounted=False) -> int:
    command_read = call_resize2fs(devicepath, "-M", mounted)
    if command_read == "resize2fs error" or command_read == "fserror":
        return -1
    if command_read != "":
        m = re.search(
            r".* The filesystem on .* is now ([0-9]*) (1k) blocks long.*", command_read
        )
        if m:
            return math.ceil(int(m.group(1)) / 4096)
        else:
            return 0
    else:
        return 0


def set_maximal_size(devicepath: str, mounted=False) -> int:
    command_read = call_resize2fs(devicepath, "", mounted)
    if command_read == "resize2fs error" or command_read == "fserror":
        return -1
    if command_read != "":
        m = re.search(
            r".* The filesystem on .* is now ([0-9]*) (1k) blocks long.*", command_read
        )
        if m:
            return math.ceil(int(m.group(1)) / 4096)
        else:
            return 0
    else:
        return 0


def update_local_size():
    """
    checks and updates the local size cache
    :return True if has changed, false else:
    """
    mount, options = soshelpers_file.get_mountpoint(MOUNTDEV[Partition.LOCAL])
    if mount == MOUNT[Partition.LOCAL]:
        statvfs = os.statvfs(sosconstants.MOUNT[Partition.LOCAL])
        new_drivespace = DriveSpaceModel()
        new_drivespace.total_size = statvfs.f_frsize * statvfs.f_blocks
        new_drivespace.free_space = statvfs.f_bfree * statvfs.f_frsize
        new_drivespace.iused = statvfs.f_files - statvfs.f_ffree
        data = soshelpers_file.read_from_file(sosconstants.DRIVESPACE_CACHE)
        if data is not None:
            old_drivespace = DriveSpaceModel.from_json(data)
            if old_drivespace == new_drivespace:
                return False
        soshelpers_file.write_to_file(
            sosconstants.DRIVESPACE_CACHE, new_drivespace.to_json()
        )
        return True
    return False


def set_local_ownership():
    os.chown(os.path.join(sosconstants.MOUNT[Partition.LOCAL], "."), 0, 0)
    try:
        os.system(
            f"chown -R root:root {os.path.join(sosconstants.MOUNT[Partition.LOCAL], 'lost+found')}"
        )
    except Exception as e:
        logger.error(
            f"Could not change owner root:root of /mnt/local/lost+found: {str(e)}"
        )
    try:
        os.system(
            f"chown -R root:root {os.path.join(sosconstants.MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, 'syncosync')}"
        )
    except Exception as e:
        logger.error(
            f"Could not change owner root:root of /mnt/local/syncosync/syncosync: {str(e)}"
        )


def set_remote_ownership():
    try:
        os.system(
            f"chown -R syncosync:syncosync {os.path.join(sosconstants.MOUNT[Partition.REMOTE], '.')}"
        )
    except Exception as e:
        logger.error(
            f"Could not change owner syncosync:syncosync of /mnt/rjail/remote: {str(e)}"
        )


def get_lvname(device: Partition, vg_uuid) -> str:
    """returns the lvname

    Args:
        device (Partition): see enum, e.g. Partition.SYSTEM
        vg_uuid ([type]): sos vg_uuid, e.g. thrrV6-UwBg-c4f8-A6G2-n1Mm-JrJB-JoMXbG

    Returns:
        str: lvname e.g. sos-thrrV6-UwBg-c4f8-A6G2-n1Mm-JrJB-JoMXbG/system
    """
    lvname = f"sos-{vg_uuid}/{MOUNTDEV[device]}"
    return lvname


def get_blkdevice(device: Partition, vg_uuid) -> str:
    """returns the blkdevice string

    Args:
        device (Partition): see enum, e.g. Partition.SYSTEM
        vg_uuid ([type]): sos vg_uuid, e.g. thrrV6-UwBg-c4f8-A6G2-n1Mm-JrJB-JoMXbG

    Returns:
        str: blkdevice e.g. /dev/mapper/sos--thrrV6--UwBg--c4f8--A6G2--n1Mm--JrJB--JoMXbG-system
    """
    strange_vg_uuid = re.sub(r"-", "--", vg_uuid)
    blkdevice = f"/dev/mapper/sos--{strange_vg_uuid}-{MOUNTDEV[device]}"
    return blkdevice

def compare_params(expected, actual) -> bool:
    if 'rw' in expected and 'ro' in actual:
        return False
    if 'ro' in expected and 'rw' in actual:
        return False
    return True

def mount_drive(
    device: Partition, vg_uuid: str, destination: Partition, params
) -> bool:
    """
    mounts a drive
    :param device: enum of partition
    :param vg_uuid: vg_uuid
    :param destination: enum of partition
    :param params: list of mount params (see man mount)
    :return: True if success
    """
    blkdevice = get_blkdevice(device, vg_uuid)
    try:
        blk_exists = stat.S_ISBLK(os.stat(blkdevice).st_mode)
    except FileNotFoundError:
        blk_exists = False
    if blk_exists:

        deststr = MOUNT[destination]
        print("params:", params)
        logger.debug(f"Mounting {blkdevice} to {deststr} with opts: {','.join(params)}")
        try:
            run_result = subprocess.run(
                f"mount -o '{','.join(params)}' {blkdevice} {deststr}",
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            if run_result.returncode != 0:
                logger.error(f"Was not able to mount {blkdevice}: {run_result.returncode}")
                return False
        except Exception as e:
            logger.error(f"Was not able to mount {blkdevice}: {str(e)}")
            return False
        actual_mountpoint, actual_params = soshelpers_file.get_mountpoint(MOUNTDEV[device])
        if not compare_params(params, actual_params):
            logger.error(f"Mounting resulted in different params: {','.join(actual_params)}")
            return False
        if device == Partition.LOCAL and destination == Partition.LOCAL:
            # this is a good situation to update the drive size
            update_local_size()
            # and to restore last access
            if os.path.isfile(sosconstants.SOSACCOUNTACCESS_PERSISTENT):
                shutil.copy2(
                    sosconstants.SOSACCOUNTACCESS_PERSISTENT,
                    sosconstants.SOSACCOUNTACCESS_CACHE,
                )
            if os.path.isfile(sosconstants.SOSACCOUNTMAIL_PERSISTENT):
                shutil.copy2(
                    sosconstants.SOSACCOUNTMAIL_PERSISTENT,
                    sosconstants.SOSACCOUNTMAIL_CACHE,
                )
        return True
    return False


def umount_volume(device, vg_uuid) -> bool:
    """
    unmounts a single volume
    :param volume: enum which defines a volume (e.g. sos_enum.Partition.LOCAL)
    :return: True if success
    """
    result = True
    mountpoint, options = soshelpers_file.get_mountpoint(MOUNTDEV[device])
    if mountpoint is not None:
        try:
            if mountpoint == MOUNT[Partition.LOCAL]:
                os.system("service sosaccountaccess stop")
            logger.debug(
                f"Unmounting volume {mountpoint} - {get_blkdevice(device, vg_uuid)}"
            )
            run_result = subprocess.run(
                f"umount {get_blkdevice(device, vg_uuid)}",
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            if run_result.returncode != 0:
                logger.warning(f"umount {get_blkdevice(device, vg_uuid)} not successfull: {run_result.returncode}:{run_result.stdout}")
        except Exception as e:
            logger.error(
                f"Was not able to unmount {get_blkdevice(device, vg_uuid)}: {str(e)}"
            )
            result = False
    return result


def stop_quota():
    subprocess.run(f"quotaoff {MOUNT[Partition.LOCAL]}", shell=True)


def start_quota():
    subprocess.run(f"quotaon {MOUNT[Partition.LOCAL]}", shell=True)


class PartitionInfo(SerializerBase):
    """info about a partition on a drive"""

    def __init__(
        self, number: int, name: str, length: int, fstype: Optional[str] = None
    ):
        self.number: int = number
        self.name: str = name
        self.length: int = length
        self.fstype: Optional[str] = fstype


class Drive(SerializerBase):
    """status of a drive at boot(?) time"""

    def __init__(self):
        self.id: Optional[str] = None
        self.product: Optional[str] = None
        self.revision: Optional[str] = None
        self.device: Optional[str] = None  # e.g. "sda"
        self.checkable: bool = False  # could be checked if spin up
        self.sata_size: int = 0
        self.vg_uuid: Optional[str] = None
        self.vg_name: Optional[str] = None
        self.vg_state: Optional[str] = None
        self.partitions: List[PartitionInfo] = []

    def generate(self, lsscsi_line):
        self.id = lsscsi_line[0:9]
        self.product = lsscsi_line[30:47]
        self.revision = lsscsi_line[47:51]
        self.device = lsscsi_line[58:61]
        try:
            with open("/sys/class/block/" + self.device + "/size") as f:
                self.sata_size = int(f.read()) * 512
        except OSError:
            self.sata_size = 0
        self.checkable = check_drive_check(self.device)
        return self.__get_details()

    def __get_details(self):
        """
        tries to find out, what is on a hdd
        :return:
        True if the drive was readable in any way
        Fals else
        """
        try:
            device = parted.getDevice("/dev/" + self.device)
        except Exception as e:
            logger.error(f"Could not get details from drive {self.device}: {str(e)}")

            return False
        try:
            disk = parted.newDisk(device)
        except Exception:
            # the partition is not readable. This is also the case for sos drives,
            # so we do not report it as an issue
            return True
        # ok, this disk should be readable
        for partition in disk.partitions:
            filesystem = partition.fileSystem
            if filesystem:
                fstype = filesystem.type
            else:
                fstype = None
            temp_partition_name = ""
            if partition.type != 0:
                try:
                    temp_partition_name = partition.name
                except Exception as e:
                    logger.warning(f"Could not get partition name: {str(e)}")
                    temp_partition_name = ""
            new_partition = PartitionInfo(
                partition.number, temp_partition_name, partition.getLength(), fstype
            )
            self.partitions.append(new_partition)
        return True

    def initialise(self):
        """
        initialises a drive
        :return: True if everything is fine, False else
        """
        if self.vg_uuid is not None and self.vg_uuid != "":
            subprocess.run(
                "vgrename -f -y " + self.vg_uuid + " deleteme",
                check=False,
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            subprocess.run(
                "vgremove -f -y deleteme",
                check=False,
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            subprocess.run(
                "vgscan",
                check=False,
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            subprocess.run(
                "pvremove -f -y /dev/" + self.device,
                check=False,
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            subprocess.run(
                "vgchange -ay",
                check=False,
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )

        subprocess.run(
            "wipefs -af /dev/" + self.device,
            shell=True,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
        time.sleep(3)
        subprocess.run(
            "pvcreate -ffy /dev/" + self.device,
            shell=True,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
        logger.info("Drive " + self.device + " initialised")
        return True


class PhysicalVolumeGroup(SerializerBase):
    def __init__(self):
        self.pv_dev: Optional[str] = None
        self.vg_uuid: Optional[str] = None
        self.vg_name: Optional[str] = None

    def generate(self, line):
        fields = line.split(",")
        self.pv_dev = fields[0][7:]
        self.vg_uuid = fields[1]
        self.vg_name = fields[2]


class PhysicalVolumeGroups(SerializerBase):
    def __init__(self):
        self.pv: List[PhysicalVolumeGroup] = []
        self.update()

    def update(self):
        self.pv = []
        pvs_output = os.popen(
            'pvs -o pv_name,vg_uuid,vg_name --noheadings --separator=","'
        ).readlines()
        for line in pvs_output:
            line = line.rstrip()
            mypv = PhysicalVolumeGroup()
            mypv.generate(line)
            self.pv.append(mypv)


class VolumeGroup(SerializerBase):
    def __init__(self):
        self.vg_uuid: Optional[str] = None
        self.vg_extent_size: Optional[str] = None
        self.vg_extent_count: Optional[str] = None
        self.vg_free: Optional[str] = None
        self.vg_status: Optional[str] = None
        self.vg_name: Optional[str] = None

    def generate(self, line):
        fields = line.split(",")
        self.vg_uuid = fields[0][2:]
        self.vg_name = fields[1]
        self.vg_extent_size = fields[2]
        self.vg_extent_count = fields[3]
        self.vg_free = fields[4]
        self.vg_status = fields[5][3]


class LogicalVolume(SerializerBase):
    """
    holding information about one logical volume
    """

    # lv_name, lv_uuid, lv_full_name, lv_path, lv_active, lv_size, vg_uuid, vg_name

    def __init__(self):
        self.lv_name: Optional[str] = None
        self.lv_uuid: Optional[str] = None
        self.lv_full_name: Optional[str] = None
        self.lv_path: Optional[str] = None
        self.lv_active: Optional[str] = None
        self.lv_size: int = 0
        self.lv_used_size: int = 0
        self.vg_uuid: Optional[str] = None
        self.vg_name: Optional[str] = None

    #    self.mountpoint: Optional[str] = None

    def generate(self, line):
        line = line.rstrip()
        fields = line.split(",")
        self.lv_name = fields[0][2:]
        self.lv_uuid = fields[1]
        self.lv_full_name = fields[2]
        self.lv_path = fields[3]
        self.lv_active = fields[4]
        self.lv_size = int(float(fields[5]))
        self.vg_uuid = fields[6]
        self.vg_name = fields[7]
        #    self.mountpoint = soshelpers_file.get_mountpoint(self.lv_path)
        self.lv_used_size = get_minmal_size(self.lv_path)


class LogicalVolumes(SerializerBase):
    """
    holding all logical volumes detected
    """

    def __init__(self):
        self.lv: Dict[str, LogicalVolume] = dict()
        # If false, there is a lv from vg sos which is not active ...
        self.active: bool = True
        self.update()

    def update(self):
        lvs_output = os.popen(
            'lvs -o lv_name,lv_uuid,lv_full_name,lv_path,lv_active,lv_size,vg_uuid,vg_name --units 4m --nosuffix --noheadings --separator=","'
        ).readlines()
        for line in lvs_output:
            mylv = LogicalVolume()
            mylv.generate(line)

            # when at least one volume is not active, set the whole vg state to inactive
            m = re.search("sos-(.*)", mylv.vg_name)
            if m:
                if mylv.vg_name == "sos" and mylv.lv_active != "active":
                    self.active = False
                self.lv[mylv.lv_full_name] = mylv
        # also if there is no lv at all
        if not self.lv:
            self.active = False


class VolumeGroups(SerializerBase):
    """
    holding all volumegroups detected. Should be only one: sos
    """

    def __init__(self):
        self.vg: Dict[str, VolumeGroup] = dict()
        self.bad: bool = False
        self.multiple_sos: bool = False
        self.sos_found: bool = False
        self.cfgmd5sum: str = ""
        self.cfgdate: float = 0
        self.cfghostname: str = ""
        self.vg_uuid: str = (
            ""  # this is, where we store the vg_uuid, if we have only one.
        )
        self.update()

    def update(self):
        """update volume groups from system"""
        vgs_output = os.popen(
            'vgs -o vg_uuid,vg_name,vg_extent_size,vg_extent_count,vg_free,vg_attr --units 4m --nosuffix --noheadings --separator=","'
        ).readlines()
        for line in vgs_output:
            myvg = VolumeGroup()
            myvg.generate(line)
            # check if the volume group is good or bad
            if myvg.vg_status == "p":
                self.bad = True
            # check and set if there are multiple sos groups
            m = re.search("sos-(.*)", myvg.vg_name)
            if m:
                this_uuid = m.group(1)
                if this_uuid != myvg.vg_uuid:
                    logger.warning(
                        f"{myvg.vg_name} has not the same uuid {myvg.vg_uuid}, skipping"
                    )
                else:
                    self.vg_uuid = myvg.vg_uuid
                    if self.sos_found:
                        self.multiple_sos = True
                        self.vg_uuid = ""
                    self.sos_found = True
                    # let's check for a config backup
                    self.cache_configbackup(this_uuid)

            self.vg[myvg.vg_uuid] = myvg  # add this group

    def cache_configbackup(self, vg_uuid):
        # test if the special device exists
        strange_vg_uuid = re.sub(r"-", "--", vg_uuid)
        blkdevice = f"/dev/mapper/sos--{strange_vg_uuid}-local"
        try:
            blk_exists = stat.S_ISBLK(os.stat(blkdevice).st_mode)
        except FileNotFoundError:
            blk_exists = False
        if blk_exists:
            # now check first if the /dev/mapper/sos-local is already mounted
            was_mounted = True
            if not os.path.ismount(sosconstants.MOUNT[Partition.LOCAL]):
                was_mounted = False
                if not mount_drive(Partition.LOCAL, vg_uuid, Partition.LOCAL, ["ro"]):
                    logger.error("could not read configuration as /mnt/local is not mounted")
                    return
            config = configbackup.get_latest()
            if config is not None:
                self.cfgmd5sum = config.md5sum
                self.cfgdate = config.timestamp
                self.cfghostname = config.hostname
                # also copy this file to a place where it could be used later for restoring it
                srcpath = configbackup.get_latest_filepath()
                if srcpath != "":
                    dstpath = os.path.join(RUNDIR, vg_uuid)
                    if not os.path.isdir(dstpath):
                        os.makedirs(dstpath)
                    else:
                        # delete all existing files
                        for filename in os.listdir(dstpath):
                            os.remove(os.path.join(dstpath, filename))
                    shutil.copy(srcpath, dstpath, follow_symlinks=True)
                else:
                    logger.warning("No configuration backup found")

            if not was_mounted:
                umount_volume(Partition.LOCAL, vg_uuid)

    def get_free_sos_extents(self):
        if self.multiple_sos or not self.sos_found:
            return -1
        else:
            myvg = list(self.vg.values())[0]
            return int(float(myvg.vg_free))

    def __getitem__(self, item):
        return self.vg[item]


class Drives(SerializerBase):
    """reading semistatic information drivelist"""

    def __init__(self):
        # True if at least a single non-SOS drive is present
        self.non_sos_drive: bool = False
        self.errorneous_drive: bool = False
        self.drives: List[Drive] = []
        self.pvs: Optional[PhysicalVolumeGroups] = None
        self.vgs: Optional[VolumeGroups] = None
        self.lvs: Optional[LogicalVolumes] = None
        self.sos_vol_mount = VolMount.NONE

    def read_cache(self):
        """
        reads boot status from cache, this should not really happen.
        :return:
        """

        if os.path.isfile(sosconstants.DRIVESTATUS_CACHE):
            with open(sosconstants.DRIVESTATUS_CACHE) as json_file:
                file_data = json.load(json_file)
                self.drives.clear()
                for drive in file_data["drive"]:
                    newdrive = Drive()
                    newdrive.__dict__ = drive
                    self.drives.append(newdrive)
            return True
        else:
            return False

    def generate(self):
        """
        this should be done once at boot time
        :return:
            True if it is a startable situation
            False else
        """
        logger.debug("Generating drive status")
        hdidleargs: List[str] = ["-i", "0"]
        myconfigcaller = sosconfig.SosConfig()
        myconfig = myconfigcaller.get_system_description()
        drive_order = myconfig.drives
        drive_found = False
        lsscsi_output = os.popen("/usr/bin/lsscsi").readlines()
        self.drives.clear()
        for line in lsscsi_output:
            drive = Drive()
            if drive.generate(line):
                # now find at which entry in the array, this drive should show up
                for i in range(0, len(drive_order)):
                    # print("id: "+drive.id+" order: "+drive_order[i])
                    if drive.id == drive_order[i]:
                        # print("drive added:"+drive.device)
                        self.drives.append(drive)
                        hdidleargs.extend(["-a", drive.device, "-i", "600"])
                        drive_found = True
            else:
                self.errorneous_drive = True
        # now, read the whole LVM setup
        self.vgs = VolumeGroups()
        self.pvs = PhysicalVolumeGroups()
        self.lvs = LogicalVolumes()
        for pv in self.pvs.pv:
            for i in self.drives:
                if i and i.device == pv.pv_dev[0:3]:
                    i.vg_uuid = pv.vg_uuid
                    i.vg_name = pv.vg_name
                    #    self.vgs[pv.vg_uuid].vg_name = pv.vg_name
                    if pv.vg_uuid:
                        i.vg_state = self.vgs[pv.vg_uuid].vg_status
        self.non_sos_drive = False
        for drive in self.drives:
            if drive and drive.vg_name is not None:
                m = re.search("sos-(.*)", drive.vg_name)
                if not m:
                    self.non_sos_drive = True
            else:
                self.non_sos_drive = True
        if not self.vgs.multiple_sos and self.vgs.sos_found:
            # ok, there is only one sos vg, now check, if all drives are mounted
            local_mountpoint, local_params = soshelpers_file.get_mountpoint(
                MOUNTDEV[Partition.LOCAL]
            )
            system_mountpoint, system_params = soshelpers_file.get_mountpoint(
                MOUNTDEV[Partition.SYSTEM]
            )
            remote_mountpoint, remote_params = soshelpers_file.get_mountpoint(
                MOUNTDEV[Partition.REMOTE]
            )
            if (
                local_mountpoint is None
                and system_mountpoint is None
                and remote_mountpoint is None
            ):
                self.sos_vol_mount = VolMount.NONE
            elif (
                local_mountpoint == MOUNT[Partition.LOCAL]
                and system_mountpoint == MOUNT[Partition.SYSTEM]
                and remote_mountpoint == MOUNT[Partition.REMOTE]
            ):
                self.sos_vol_mount = VolMount.NORMAL
            elif (
                local_mountpoint == MOUNT[Partition.REMOTE]
                and system_mountpoint == MOUNT[Partition.SYSTEM]
                and remote_mountpoint == MOUNT[Partition.LOCAL]
            ):
                self.sos_vol_mount = VolMount.SWAP
            else:
                self.sos_vol_mount = VolMount.INCOMPLETE
        start_hdidle(hdidleargs)
        return not (self.errorneous_drive or self.non_sos_drive or self.vgs.bad or self.vgs.multiple_sos)

    def get_free_data(self) -> int:
        """
        outputs available free extents
        :return:
        """
        if self.vgs is not None:
            return self.vgs.get_free_sos_extents()
        else:
            return 0

    def get_partitioning(self) -> Tuple[int, int]:
        """
        outputs local and remote volume size in 4MB blocks
        :return:
        int: local
        int: remote
        """
        pass

    def get_drivelist(self):
        """
        returns array with device filenames e.g. ['sda','sdb']
        """
        drivelist = []
        for drive in self.drives:
            if drive:
                drivelist.append(drive.device)
        return drivelist

    def get_checklist(self):
        """
        returns array with device filenames of disks, which could be checked e.g. ['sda','sdb']
        """
        drivelist = []
        for drive in self.drives:
            if drive and drive.checkable:
                drivelist.append(drive.device)
        return drivelist

    def get_drive_by_device(self, device):
        """
        returns drive selected by device (e.g. sda)
        :param device: e.g. sda
        :return: drive object if found
        """
        for drive in self.drives:
            if drive and drive.device == device:
                return drive
        return None

    def format_drives(
        self, drivesetup_drive_config: DrivesetupDriveConfiguration
    ) -> int:
        """Handling all actions about formmating drives: expanding, restoring from a different vg_uuid, expanding
        Automatic detection based on data in drivesetup_drive_config

        Args:
            drivesetup_drive_config (DrivesetupDriveConfiguration): [description]

        Returns:
            int: [description]
        """
        thisstate = sysstate.SysState()
        drives = self.get_drivelist()
        if drivesetup_drive_config.selected_vg_uuid is None:
            # Erase all drives and create a "fresh" volume group with ALL drives attached
            extents = self.create_sos(drives)
            thisstate.changemode(SystemMode.SETUP_VOLUMES)
            return extents
        else:
            # ok, we have a vg_uuid selected, now we have to check if it is the configured one
            sosconfigcall = sosconfig.SosConfig()
            myconfig = sosconfigcall.get()
            logger.debug(f"Actual vguuid: {myconfig.vg_sos_uuid} selected vguuid: {drivesetup_drive_config.selected_vg_uuid}")
            # we have to expand the drives before, so we can switch to default mode with no issues later
            extents = self.expand_sos(drivesetup_drive_config, drives)
            if myconfig.vg_sos_uuid != drivesetup_drive_config.selected_vg_uuid:
                # we have to restore the configuration first
                cfg_backup_path = os.path.join(
                    RUNDIR,
                    drivesetup_drive_config.selected_vg_uuid,
                    os.listdir(
                        os.path.join(RUNDIR, drivesetup_drive_config.selected_vg_uuid)
                    )[0],
                )
                logger.debug(f"configbackup from {cfg_backup_path} will be restored.")
                if drivesetup_drive_config.password_confirmation:
                    thisstate.changemode(
                        SystemMode.DEFAULT,
                        cfg_backup_path,
                        drivesetup_drive_config.password_confirmation,
                    )
                else:
                    raise soserrors.SosError
                myconfig = sosconfigcall.get()
                myconfig.vg_sos_uuid = drivesetup_drive_config.selected_vg_uuid
                sosconfigcall.set()
            else:
                thisstate.changemode(SystemMode.DEFAULT)

            # Use the selected_vg_uuid to expand the given vg uuid (IF VG is complete) by all the drives that are
            # not part of the VG
            return extents

    def expand_sos(
        self, drivesetup_drive_config: DrivesetupDriveConfiguration, drivelist
    ) -> int:
        """Expands an existiting sos vg with the drives from the list

        Args:
            drivesetup_drive_config: DrivesetupDriveConfiguration the selected_vg_uuid must not be None
            drivelist (list of str): array of devicenames e.g. ["sdb","sdc","sdd"]

        Returns:
            int: free extents of the complete sos vg
        """

        devices = []
        if self.vgs is None:
            logger.error(
                "expand_sos with no existing volume groups called. This may not happen"
            )
            raise soserrors.SosError
        else:
            for drivestr in drivelist:
                drive = self.get_drive_by_device(drivestr)
                if drive and drive.vg_uuid != drivesetup_drive_config.selected_vg_uuid:
                    drive.initialise()
                    devices.append("/dev/" + drive.device)
            if devices:
                s = " "
                driveliststr = s.join(devices)
                result=subprocess.run(
                    "vgextend -fy sos-" + drivesetup_drive_config.selected_vg_uuid + " " + driveliststr,
                    shell=True,
                    capture_output=True
                )
                logger.info(f"Expanded VG group sos with {driveliststr}: {result.stdout} - {result.stderr}")
                self.generate()
            else:
                logger.warning("Empty drivelist for expanding sos")
            return self.vgs.get_free_sos_extents()

    def reset_drives(self, drivelist) -> List[str]:
        """
        resets drives (delete them from vg and so on)
        :param drivelist:  array of devicenames e.g. ["sdb","sdc","sdd"]
        :return: list of complete devices ["/dev/sdb","/dev/sdc","/dev/sdd"]
        """
        logger.debug(f'Request to reset drives:{",".join(drivelist)}')
        for volume in sosconstants.SOS_VOLUMES:
            if os.path.ismount(sosconstants.MOUNT[volume]):
                raise soserrors.SosError(
                    f"{sosconstants.MOUNT[volume]} is still mounted. "
                    f"Stop sos services and unmount"
                )
        # now, the rest is really up to the risk of the caller
        if self.vgs is not None and self.vgs.vg_uuid != "":
            subprocess.run(
                "vgremove -fy sos-" + self.vgs.vg_uuid,
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
        for volume in sosconstants.SOS_VOLUMES:
            try:
                blk_exists = stat.S_ISBLK(
                    os.stat(f"{sosconstants.MOUNTDEV[volume]}").st_mode
                )
            except FileNotFoundError:
                blk_exists = False
            if blk_exists:
                cp = subprocess.run(
                    f"dmsetup remove -f sos-{volume}",
                    shell=True,
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                )
                if cp.returncode != 0:
                    logger.error(f"Could not remove volume {volume}")
                else:
                    logger.error(f"Volume {volume} removed")
        devices = []
        for drivestr in drivelist:
            drive = self.get_drive_by_device(drivestr)
            if drive:
                drive.initialise()
                devices.append("/dev/" + drive.device)
        return devices

    def create_sos(self, drivelist) -> int:
        """
        creates sos volumegroup
        :param drivelist:  array of devicenames e.g. ["sdb","sdc","sdd"]
        :return: available extents
        """
        devices = self.reset_drives(drivelist)
        if devices:
            s = " "
            driveliststr = s.join(devices)
            subprocess.run(
                "vgcreate -fy sos " + driveliststr,
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            self.generate()
            if self.vgs is None:
                logger.error("No volume groups after create sos")
                raise soserrors.SosError
            else:
                vg_uuid = list(self.vgs.vg)[0]
            subprocess.run(
                "vgrename sos sos-" + vg_uuid,
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            self.generate()
            logger.info(f"Created VG group sos-{vg_uuid} with " + driveliststr)
            # adjust system_size boundaries
            system_size = int(self.vgs.get_free_sos_extents() / 10)
            if system_size > sosconstants.SYSTEM_RECOMMEND_SIZE:
                system_size = sosconstants.SYSTEM_RECOMMEND_SIZE
            if system_size < sosconstants.SYSTEM_MIN_SIZE:
                system_size = sosconstants.SYSTEM_MIN_SIZE
            cp = subprocess.run(
                f"lvcreate -y --name system -l {system_size} sos-{vg_uuid}",
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            if cp.returncode != 0:
                logger.error(
                    f"Cannot create system volume with a size of {system_size} extents."
                )
                return False
            self.generate()
            if self.vgs is not None:
                sosconfigcall = sosconfig.SosConfig()
                myconfig = sosconfigcall.get()
                myconfig.vg_sos_uuid = self.vgs.vg_uuid
                myconfig.local = None
                myconfig.remote = None
                sosconfigcall.set()
            else:
                logger.error("No volume groups after create sos")
                raise soserrors.SosError
        else:
            logger.error("Empty drivelist for creating sos")
            return False
        return self.vgs.get_free_sos_extents()

    def mount_sos_volumes(  # noqa: C901
        self, swap=False, local_ro=False, remote_ro=False, sosaccount=True
    ) -> bool:
        """
        mounts all sos volumes (local, remote, system)
        :param swap: if True mount reverse -> see disaster swap
        :param local_ro: mount to local mountpoint read only
        :param remote_ro: mount to remote mountpoint read only
        :return: True if the mounting situation has changed, False else
        TODO: should we throw more detailed exceptions?
        """
        logger.debug(f"Mounting all sos volumes with swap mode: {swap}")
        result = False
        if (
            not (self.non_sos_drive or self.vgs.bad or self.vgs.multiple_sos)
            and self.vgs.sos_found
        ):
            # we can do only something if there is at least something to mount
            local_mountpoint, actual_local_params = soshelpers_file.get_mountpoint(
                MOUNTDEV[Partition.LOCAL]
            )
            system_mountpoint, actual_system_params = soshelpers_file.get_mountpoint(
                MOUNTDEV[Partition.SYSTEM]
            )
            remote_mountpoint, actual_remote_params = soshelpers_file.get_mountpoint(
                MOUNTDEV[Partition.REMOTE]
            )

            logger.debug(
                f"Mountpoints: Local:{local_mountpoint}, Remote:{remote_mountpoint}, System:{system_mountpoint}"
            )

            vg_uuid = list(self.vgs.vg)[0]

            # First, check, if the partitions should swap - this means, we have to unmount them both
            if local_mountpoint is not None:
                if (
                    not swap
                    and local_mountpoint == MOUNT[Partition.REMOTE]
                    or (swap and local_mountpoint == MOUNT[Partition.LOCAL])
                ):
                    run_result = subprocess.run(f"quotaoff {MOUNT[Partition.LOCAL]}", shell=True)
                    if run_result.returncode != 0:
                        logger.warning(f"quoatoff failed: {run_result.returncode}:{run_result.stdout}")
                    umount_volume(Partition.LOCAL, vg_uuid)
                    local_mountpoint = None
            if remote_mountpoint is not None:
                if (
                    swap
                    and remote_mountpoint == MOUNT[Partition.REMOTE]
                    or (not swap and remote_mountpoint == MOUNT[Partition.LOCAL])
                ):
                    umount_volume(Partition.REMOTE, vg_uuid)
                    remote_mountpoint = None

            # Now mount everything according the requests:
            # remount if mode change (ro <-> rw)
            # system_mountpoint is always mounted rw
            if system_mountpoint is None:
                if not mount_drive(
                        Partition.SYSTEM,
                        vg_uuid,
                        Partition.SYSTEM,
                        MOUNTOPTS[Partition.SYSTEM],
                    ):
                        raise soserrors.SosError
                result = True

            # swap or not?
            if swap:
                local_device = Partition.REMOTE
                remote_device = Partition.LOCAL
            else:
                local_device = Partition.LOCAL
                remote_device = Partition.REMOTE

            local_params = MOUNTOPTS[Partition.LOCAL].copy()

            if local_ro:
                local_params.append("ro")
            #    subprocess.run(f"quotaon {MOUNT[Partition.LOCAL]}", shell=True)
            else:
                local_params.append("rw")

            if local_mountpoint is None:
                # this means, no remount necessary
                if not mount_drive(local_device, vg_uuid, Partition.LOCAL, local_params):
                    raise soserrors.SosError
                if sosaccount:
                    os.system("service sosaccountaccess start")

            else:
                if (local_ro and "ro" not in local_params) or (
                    not local_ro and "ro" in local_params
                ):
                    local_params.append("remount")
                    os.system("service sosaccountaccess stop")
                    if not mount_drive(local_device, vg_uuid, Partition.LOCAL, local_params):
                        raise soserrors.SosError
                    if sosaccount:
                        os.system("service sosaccountaccess start")
                    result = True

            if not local_ro or swap:
                if not os.path.isfile(
                    os.path.join(MOUNT[Partition.LOCAL], "aquota.user")
                ):
                    logger.warning(
                        f"{MOUNT[Partition.LOCAL]}/aquota.user is missing and will be generated, "
                        "this should happen only once after formatting"
                    )
                    run_result = subprocess.run(
                        f"quotacheck -c {MOUNT[Partition.LOCAL]}", shell=True
                    )
                    if run_result.returncode != 0:
                        logger.warning(f"quoatacheck {MOUNT[Partition.LOCAL]} failed: {run_result.returncode}:{run_result.stdout}")
                    # TODO: should we also reset the quota per user here? Basically yes.
                    run_result = subprocess.run(
                        f"quotaon {MOUNT[Partition.LOCAL]}",
                        shell=True,
                    )
                    if run_result.returncode != 0:
                        logger.warning(f"quoaton {MOUNT[Partition.LOCAL]} failed: {run_result.returncode}:{run_result.stdout}")
                else:
                    run_result = subprocess.run(f"quotacheck {MOUNT[Partition.LOCAL]}", shell=True)
                    if run_result.returncode != 0:
                        logger.warning(f"quoatacheck {MOUNT[Partition.LOCAL]} failed: {run_result.returncode}:{run_result.stdout}")
                    run_result = subprocess.run(f"quotaon {MOUNT[Partition.LOCAL]}", shell=True)
                    if run_result.returncode != 0:
                        logger.warning(f"quoatacheck {MOUNT[Partition.LOCAL]} failed: {run_result.returncode}:{run_result.stdout}")
            # now the remote mount
            remote_params = MOUNTOPTS[Partition.REMOTE].copy()
            if remote_ro:
                remote_params.append("ro")
            else:
                remote_params.append("rw")

            if remote_mountpoint is None:
                # this means, no remount necessary
                if not mount_drive(remote_device, vg_uuid, Partition.REMOTE, remote_params):
                    raise soserrors.SosError
                result = True
            else:
                if (remote_ro and "ro" not in remote_params) or (
                    not remote_ro and "ro" in remote_params
                ):
                    remote_params.append("remount")
                    if not mount_drive(remote_device, vg_uuid, Partition.REMOTE, remote_params):
                        raise soserrors.SosError
                    result = True

            if not remote_ro:
                run_result = subprocess.run(
                    f"chown -R syncosync:syncosync {MOUNT[Partition.REMOTE]}",
                    shell=True,
                )
                if run_result.returncode != 0:
                    logger.warning(f"chown -R syncosync:syncosync {MOUNT[Partition.REMOTE]} not successfull: {run_result.returncode}:{run_result.stdout}")       

        else:
            logger.error(
                f"Drive status does not allow mounting sos volumes: "
                f"non_sos_drive: {self.non_sos_drive}, "
                f"vgs_bad: {self.vgs.bad}, "
                f"vgs.multiple_sos: {self.vgs.multiple_sos}"
            )
            raise soserrors.SosError
        return result

    def swap_sos_volumes(self):
        subprocess.run(
            f"lvrename sos-{self.vgs.vg_uuid} local temp",
            shell=True,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
        subprocess.run(
            f"lvrename sos-{self.vgs.vg_uuid} remote local",
            shell=True,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
        subprocess.run(
            f"lvrename sos-{self.vgs.vg_uuid} temp remote",
            shell=True,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
        return None

    def umount_sos_volumes(self, partitions=sosconstants.SOS_VOLUMES) -> bool:
        """
        unmount a list of volumes (see above)
        :param partitions:
        :return:
        """
        """ just unmount the volumes in the list"""
        if self.vgs is None:
            return False
        result = True
        for partition in partitions:
            result = result and umount_volume(partition, self.vgs.vg_uuid)
        return result

    def create_volumes(self, volumes: PartitioningData):
        """
        creates or resizes local and remote volumes on the sos group
        :param volumes: PartitioningData model, only local and remote are used
        :return: True if successful
        """
        if self.vgs is None or self.vgs.vg == {}:
            raise soserrors.DriveNotFormatted
        if self.lvs is None or len(self.lvs.lv) <= 1:
            return self.init_volumes(volumes)
        else:
            return self.resize_volumes(volumes)

    def init_volumes(self, volumes: PartitioningData):
        """
        creates local and remote volumes on the sos group
        :param volumes: PartitioningData model, only local and remote are used
        :return: True if successful
        """
        extents = {}
        if self.vgs is None:
            return False
        available_extents = self.vgs.get_free_sos_extents()
        logger.debug(f"Intialising Volumes: Available extents: {available_extents}")
        extents["local"] = int(volumes.local)
        extents["remote"] = int(volumes.remote)
        if (extents["local"] + extents["remote"]) > available_extents:
            logger.error(
                f"Requested volume sizes of {extents['local']}(local) / {extents['remote']}(remote) exceeds free size of sos volume group of {available_extents}"
            )
            return False
        for volume in ["local", "remote"]:
            cp = subprocess.run(
                f"lvcreate -y --name {volume} -l {extents[volume]} sos-{self.vgs.vg_uuid}",
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            if cp.returncode != 0:
                logger.error(
                    f"Cannot create {volume} volume with a size of {extents[volume]} extents."
                )
                return False
            else:
                logger.info(
                    f"Created {volume} volume with a size of {extents[volume]} extents."
                )
        for partition in sosconstants.SOS_VOLUMES:
            cp = subprocess.run(
                f"mkfs.ext4 -F -F -O extent {get_blkdevice(partition, self.vgs.vg_uuid)}",
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            if cp.returncode != 0:
                logger.error(
                    f"Cannot create ext4 filesystem on {get_blkdevice(partition, self.vgs.vg_uuid)}"
                )
                return False
            else:
                logger.info(
                    f"Created ext4 filesystem on {get_blkdevice(partition, self.vgs.vg_uuid)}"
                )
            cp = subprocess.run(
                f"tune2fs -r 0 {get_blkdevice(partition, self.vgs.vg_uuid)}",
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            if cp.returncode != 0:
                logger.error(
                    f"Can't tune2fs {get_blkdevice(partition, self.vgs.vg_uuid)}"
                )
                return False
        self.generate()
        # now, as these volumes are empty. We should store a configuration on them
        # therefore, we have to mount them
        self.mount_sos_volumes(sosaccount=False)
        # create the base directories
        os.mkdir(os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR))
        os.mkdir(os.path.join(MOUNT[Partition.REMOTE], sosconstants.HDDSUBDIR))
        sosconfigcall = sosconfig.SosConfig()
        myconfig = sosconfigcall.get()
        myconfig.local = extents["local"]
        myconfig.remote = extents["remote"]
        sosconfigcall.set()
        self.umount_sos_volumes()
        return True

    def resize_volumes(self, volumes: PartitioningData):
        """
        resizes local and remote volumes on the sos group
        :param volumes: PartitioningData model, only local and remote are used
        :return: True if successful
        """
        extents = {}
        if self.vgs is None or self.lvs is None:
            return False
        available_extents = self.vgs.get_free_sos_extents()
        logger.debug(f"Resizing Volumes: Available extents: {available_extents} request: local:{volumes.local} remote: {volumes.remote}")

        act_local = self.lvs.lv["sos-" + self.vgs.vg_uuid + "/local"].lv_size
        # min_local = self.lvs.lv["sos-" + self.vgs.vg_uuid + "/local"].lv_used_size
        act_remote = self.lvs.lv["sos-" + self.vgs.vg_uuid + "/remote"].lv_size
        # min_remote = self.lvs.lv["sos-" + self.vgs.vg_uuid + "/remote"].lv_used_size
        extents["local"] = int(volumes.local)
        extents["remote"] = int(volumes.remote)
        if (
            extents["local"] + extents["remote"]
        ) > act_local + act_remote + available_extents:
            raise soserrors.VolumeSizeNotPossible

        mysystate = sysstate.SysState()
        thisstate = mysystate.get()
        myresult = mysystate.changemode(SystemMode.RESIZE_VOLUMES)
        if myresult.mode != SystemMode.RESIZE_VOLUMES:
            logger.error("Cannot switch to RESIZE_VOLUMES mode for resizing volumes.")
            raise soserrors.ModeSwitchFailed

        # order of resizing depends on which will grow and which will shrink
        if act_local >= extents["local"]:
            volumeorder = ["local", "remote"]
        else:
            volumeorder = ["remote", "local"]
        for volume in volumeorder:
            logger.debug(
                f"resizing volume /dev/sos-{self.vgs.vg_uuid}/{volume} to {extents[volume]} extents"
            )
            cp = subprocess.run(
                f"lvresize -y -r -l {extents[volume]} /dev/sos-{self.vgs.vg_uuid}/{volume}",
                shell=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            if cp.returncode != 0:
                logger.error(
                    f"Cannot create {volume} volume with a size of {extents[volume]} extents."
                )
                myresult = mysystate.changemode(thisstate.actmode)
                raise soserrors.VolumeResizeError
            else:
                logger.info(
                    f"Created {volume} volume with a size of {extents[volume]} extents."
                )
        self.generate()
        # now, as these volumes are empty. We should store a configuration on them
        # therefore, we have to mount them
        self.mount_sos_volumes()
        sosconfigcall = sosconfig.SosConfig()
        myconfig = sosconfigcall.get()
        myconfig.local = extents["local"]
        myconfig.remote = extents["remote"]
        sosconfigcall.set()
        # TODO: we could directly do a configbackup here
        soshelpers_file.set_configbackup_flag()
        self.umount_sos_volumes()
        myresult = mysystate.changemode(thisstate.actmode)
        if myresult.mode != thisstate.actmode:
            logger.error(
                f"Cannot reset to mode {thisstate.actmode} after resizing volumes."
            )
            raise soserrors.ModeSwitchFailed
        return True
