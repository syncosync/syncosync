"""
This is the sos housekeeper core part
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import time

from soscore import hostname, remotehost, sosaccounts, soshelpers_file, syncstat
from soscore.configbackup import backup, get_latest_filepath
from soscore.drivemanager import set_local_ownership, set_remote_ownership
from soscore.mail import Mail, get_html_mail
from soscore.sosconfig import SosConfig
from soscore.soshelpers_i18n import translate_msg
from sosmodel import sosconstants
from sosmodel.mailconfig import MailConfigModel
from sosmodel.remotehost import RemoteHostModel
from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import MailInfoLevels, RemoteUpStatus, SystemMode
from sosmodel.sosaccount import SosAccountModel
from sosmodel.sosconfig import SosConfigModel
from sosmodel.soshousekeeper import HousekeeperModel, MailModel
from sosmodel.syncstat import SyncStatModel
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


class Mails(SerializerBase):
    def __init__(self):
        self.accounts = {}

    def get_by_name(self, name) -> MailModel:
        return self.accounts.get(name)

    def add_line_by_name(self, name, mailaddress, line):
        account = self.get_by_name(name)
        if account is not None:
            account.add_line(line)
        else:
            new_account_mail = MailModel()
            new_account_mail.name = name
            new_account_mail.address = mailaddress
            new_account_mail.add_line(line)
            self.accounts[name] = new_account_mail

    def add_attachment_by_name(self, name, mailaddress, attachment):
        account = self.get_by_name(name)
        if account is not None:
            account.add_attachment(attachment)
        else:
            new_account_mail = MailModel()
            new_account_mail.name = name
            new_account_mail.address = mailaddress
            new_account_mail.add_attachment(attachment)
            self.accounts[name] = new_account_mail

    def get_names(self):
        return self.accounts.keys()

    def to_json(self) -> str:
        output = ""
        for name in self.accounts.keys():
            if output != "":
                output += ", "
            output += self.accounts.get(name).to_json()
        return '{ "mails": [ ' + output + " ] }"


class Housekeeper:
    """Housekeeper class to handle housekeeper tasks

    The main functions provided are checking and notifying accounts if they should
    back up again, alerting the admin if the remote host is not reachable and
    sending the admin an email that contains the past events since the last
    mail, for example if any users were notified to do an overdue backup"""

    def __init__(self):
        self.values = HousekeeperModel()
        self.mailconfig: MailConfigModel = Mail().get()
        self.mysosconfig: SosConfigModel = SosConfig().get()
        self.hostname = hostname.HostName().get().hostname
        self.mail_from = self.mailconfig.mail_from
        self.admin_mail_content = []
        self.admin_info_period = self.mailconfig.admin_info_period
        self.remote_admin_info_period = self.mailconfig.remote_admin_info_period
        self.admin_mail_address = self.mailconfig.admin_mail_address
        self.force_admin_mail = False
        self.mails = Mails()
        self.my_accounts = sosaccounts.SosAccounts()
        self.mysyncstat: SyncStatModel = syncstat.SyncStatReader().get()

        # Last seen is set to the time of initialization to prevent an to early mail if the system was down for example
        self.rm_last_seen = time.time()

        logger.debug(
            "Init admin values: sender {} mail_address: {} mail_info_period: {}".format(
                self.mail_from, self.admin_mail_address, str(self.admin_info_period)
            ),
        )

    def check_all(self, dryrun=False):
        """
        check and process housekeeper actions
        :param dryrun: if true, return mails as json and do nothing
        :return: json for dryrun
        """
        mysyncstat: SyncStatModel = syncstat.SyncStatReader().get()
        if mysyncstat.actmode == SystemMode.DEFAULT:
            self.check_remotehost()
        if (
            mysyncstat.actmode == SystemMode.DEFAULT
            or mysyncstat.actmode == SystemMode.DEFAULT_NO_SYNC
        ):
            logger.debug("Check all")
            self.check_accounts()
            self.check_disk()
            if (
                self.mails.get_by_name(sosconstants.SOSADMIN_USER) is not None
                or self.mailconfig.mail_info_level == MailInfoLevels.INFO
            ):
                self.gen_admin_info()
            # now do some sanity stuff
            set_local_ownership()
            set_remote_ownership()
            # restart sosaccountacces service
            os.system("service sosaccountaccess stop")
            os.system("service sosaccountaccess start")
            # no check, if a configbackup is necessary
            logger.debug(
                f"Checking config backup flag: {soshelpers_file.check_configbackup_flag()}"
            )
            if soshelpers_file.check_configbackup_flag():
                backup()
            filepath = get_latest_filepath()
            if filepath == "":
                backup()
            filepath = get_latest_filepath()
            if filepath != "":
                self.mails.add_attachment_by_name(
                    sosconstants.SOSADMIN_USER,
                    self.mailconfig.admin_mail_address,
                    filepath,
                )
                self.mails.add_line_by_name(
                    sosconstants.SOSADMIN_USER,
                    self.mailconfig.admin_mail_address,
                    translate_msg(
                        "config_backup_attached",
                        {},
                        self.mysosconfig.ui_language.language,
                    ),
                )
                logger.debug(f"Attaching backupfile {filepath} to sosadmin mail")
        else:
            self.mails.add_line_by_name(
                sosconstants.SOSADMIN_USER,
                self.mailconfig.admin_mail_address,
                translate_msg(
                    "system_is_in_strange_state",
                    {"STATE": SystemMode.message(mysyncstat.actmode)},
                    self.mysosconfig.ui_language.language,
                ),
            )
        if dryrun:
            return self.mails.to_json()
        else:
            self.process_account_mails()

    def process_account_mails(self):
        logger.debug("Processing mails")
        mailhandler = Mail()
        now = int(time.time())
        for name in self.mails.get_names():
            if (name != sosconstants.SOSADMIN_USER) and (name != "_rsosadmin"):
                account: SosAccountModel = self.my_accounts.get_by_name(name)
                mailaccount = self.mails.get_by_name(name)
                # check if info period is exceeded
                # logger.debug(f"Mail for {name}<{account.mail_address}> info_period:{account.info_period} last_mail:{account.last_mail} now:{now} ")
                if (
                    account.info_period != 0
                    and self.in_days(now - account.last_mail) >= account.info_period
                ):
                    if account.mail_address == "":
                        logger.warning(
                            f"No mail address for sending mail for account {name}"
                        )
                    else:
                        subject = translate_msg(
                            "account_mail_subject",
                            {"ACCOUNT_NAME": name},
                            self.mysosconfig.ui_language.language,
                        )
                        headline = translate_msg(
                            "account_mail_hl",
                            {"ACCOUNT_NAME": name},
                            self.mysosconfig.ui_language.language,
                        )
                        mail_content = "<br/><br/>".join(mailaccount.lines)
                        if mailhandler.send_mail(
                            account.mail_address,
                            subject,
                            get_html_mail(headline, mail_content, self.hostname),
                            headline + "\n" + "\n".join(mailaccount.lines),
                            mailaccount.attachments,
                        ):
                            account.last_mail = now
        # now process the mail to sosadmin
        mailaccount = self.mails.get_by_name(sosconstants.SOSADMIN_USER)
        if mailaccount is not None:
            if self.mailconfig.admin_mail_address == "":
                logger.warning("Empty admin mail address, check mail settings")
            else:
                if self.mailconfig.admin_info_period != 0 and (
                    (
                        self.in_days(now - self.my_accounts.sosadmin_last_mail)
                        >= self.mailconfig.admin_info_period
                    )
                    or self.force_admin_mail
                ):
                    subject = translate_msg(
                        "admin_subject",
                        {"HOSTNAME": self.hostname},
                        self.mysosconfig.ui_language.language,
                    )
                    headline = translate_msg(
                        "admin_headline",
                        {"HOSTNAME": self.hostname},
                        self.mysosconfig.ui_language.language,
                    )
                    mail_content = "<br/><br/>".join(mailaccount.lines)
                    if mailhandler.send_mail(
                        mailaccount.address,
                        subject,
                        get_html_mail(headline, mail_content, self.hostname),
                        headline + "\n" + "\n".join(mailaccount.lines),
                        mailaccount.attachments,
                    ):
                        self.my_accounts.sosadmin_last_mail = now
        # now process the mail to remote sosadmin
        mailaccount = self.mails.get_by_name("_rsosadmin")
        if mailaccount is not None:
            if self.mailconfig.remote_admin_mail_address == "":
                logger.warning("Empty remote admin mail address, check mail settings")
            else:
                if self.mailconfig.remote_admin_info_period != 0 and (
                    self.in_days(now - self.my_accounts.remote_sosadmin_last_mail)
                    >= self.mailconfig.remote_admin_info_period
                ):
                    subject = translate_msg(
                        "remote_admin_subject",
                        {"HOSTNAME": self.hostname},
                        self.mysosconfig.ui_language.language,
                    )
                    headline = translate_msg(
                        "remote_admin_headline",
                        {"HOSTNAME": self.hostname},
                        self.mysosconfig.ui_language.language,
                    )
                    mail_content = "<br/><br/>".join(mailaccount.lines)
                    if mailhandler.send_mail(
                        mailaccount.address,
                        subject,
                        get_html_mail(headline, mail_content, self.hostname),
                        headline + "\n" + "\n".join(mailaccount.lines),
                        mailaccount.attachments,
                    ):
                        self.my_accounts.remote_sosadmin_last_mail = now
        self.my_accounts.store_last_mail()
        mailhandler.close_server()

    def check_disk(self):
        """
        check status of disks
        :return:
        """
        try:
            # avoid math errors
            percent = int(
                100
                * (self.mysyncstat.total_size - self.mysyncstat.free_space)
                / self.mysyncstat.total_size
            )
            if percent >= sosconstants.DU_WARN_LEVEL:
                admin_msg = translate_msg(
                    "disk_warn_admin_msg",
                    {"PERCENT": percent},
                    self.mysosconfig.ui_language.language,
                )
                self.mails.add_line_by_name(
                    sosconstants.SOSADMIN_USER,
                    self.mailconfig.admin_mail_address,
                    admin_msg,
                )
        except ZeroDivisionError:
            logger.warning("Totalsize of 0 read")
            pass

    def check_accounts(self):
        """
        Check accounts, if the backup is overdue or the quota is exceeded

        :rtype: None"""

        for account in self.my_accounts.get():
            account_info_period = int(account.info_period)
            account_remind_duration = int(account.remind_duration)

            logger.debug(
                f"checking: {account.name}, mail_address: {account.mail_address}, "
                f"mail_info_level: {MailInfoLevels.levelname(account.mail_info_level)},"
                f" info_period: {account_info_period}, last_access: {account.last_access},"
                f" last mail: {account.last_mail}"
            )

            time_diff = int(time.time() - account.last_access)
            overdue_days = self.in_days(time_diff)
            overdue_hours = int(self.in_hours(time_diff) - 24 * overdue_days)

            logger.debug(
                f"overdue for {account.name}: {overdue_days} days, {overdue_hours} hr, time_diff: {time_diff}"
            )
            if overdue_days >= account_remind_duration:
                if account.last_access == 0:
                    admin_msg = translate_msg(
                        "backup_never_admin_msg", {"ACCOUNT_NAME": account.name}
                    )
                    account_mail_content = translate_msg("backup_never_account_msg", {})
                else:
                    # Set admin mail (append new message)
                    admin_msg = translate_msg(
                        "backup_overdue_admin_msg",
                        {
                            "ACCOUNT_NAME": account.name,
                            "OVERDUE_DAYS": str(overdue_days),
                            "OVERDUE_HOURS": str(overdue_hours),
                            "LAST_ACCESS_DATE": time.strftime(
                                "%d/%m/%Y", time.localtime(account.last_access)
                            ),
                            "LAST_ACCESS_TIME": time.strftime(
                                "%H:%M:%S", time.localtime(account.last_access)
                            ),
                        },
                        self.mysosconfig.ui_language.language,
                    )
                    # Set user mail
                    account_mail_content = translate_msg(
                        "backup_overdue_account_msg",
                        {
                            "OVERDUE_DAYS": str(overdue_days),
                            "OVERDUE_HOURS": str(overdue_hours),
                            "LAST_ACCESS_DATE": time.strftime(
                                "%d/%m/%Y", time.localtime(account.last_access)
                            ),
                            "LAST_ACCESS_TIME": time.strftime(
                                "%H:%M:%S", time.localtime(account.last_access)
                            ),
                        },
                        self.mysosconfig.ui_language.language,
                    )
                self.mails.add_line_by_name(
                    sosconstants.SOSADMIN_USER,
                    self.mailconfig.admin_mail_address,
                    admin_msg,
                )
                self.mails.add_line_by_name(
                    account.name, account.mail_address, account_mail_content
                )
            try:
                percent_used = int(
                    account.space_used
                    * 10000
                    / (self.mysyncstat.total_size * account.max_space_allowed)
                )
                if (
                    percent_used >= sosconstants.DU_WARN_LEVEL
                    and account.mail_info_level == MailInfoLevels.WARNING
                ) or (
                    percent_used >= sosconstants.DU_ERR_LEVEL
                    and account.mail_info_level == MailInfoLevels.ERROR
                ):
                    account_mail_content = translate_msg(
                        "quota_full_account_msg",
                        {
                            "SPACE_USED": self.sizeof_fmt(account.space_used),
                            "PERCENT": percent_used,
                        },
                        self.mysosconfig.ui_language.language,
                    )
                    self.mails.add_line_by_name(
                        account.name, account.mail_address, account_mail_content
                    )
                if account.mail_info_level == MailInfoLevels.INFO:
                    account_mail_content = translate_msg(
                        "quota_info_account_msg",
                        {
                            "SPACE_USED": self.sizeof_fmt(account.space_used),
                            "PERCENT": percent_used,
                        },
                        self.mysosconfig.ui_language.language,
                    )
                    self.mails.add_line_by_name(
                        account.name, account.mail_address, account_mail_content
                    )
                if percent_used >= sosconstants.DU_ERR_LEVEL:
                    admin_msg = translate_msg(
                        "quota_full_admin_msg",
                        {"ACCOUNT_NAME": account.name, "PERCENT": percent_used},
                        self.mysosconfig.ui_language.language,
                    )
                    self.mails.add_line_by_name(
                        sosconstants.SOSADMIN_USER,
                        self.mailconfig.admin_mail_address,
                        admin_msg,
                    )
            except ZeroDivisionError:
                logger.warning(
                    "Account Max Space or total size is zero, Account: {}", account.name
                )
                pass

    def gen_admin_info(self):
        """
        generates system info for admin
        :return:
        """
        try:
            # avoid math errors
            percent = int(
                100
                * (self.mysyncstat.total_size - self.mysyncstat.free_space)
                / self.mysyncstat.total_size
            )
            admin_msg = translate_msg(
                "disk_info_admin_msg",
                {
                    "TOTAL_SIZE": self.sizeof_fmt(self.mysyncstat.total_size),
                    "USED_SIZE": self.sizeof_fmt(
                        self.mysyncstat.total_size - self.mysyncstat.free_space
                    ),
                    "PERCENT": percent,
                },
                self.mysosconfig.ui_language.language,
            )
            self.mails.add_line_by_name(
                sosconstants.SOSADMIN_USER,
                self.mailconfig.admin_mail_address,
                admin_msg,
            )
        except ZeroDivisionError:
            logger.warning("Total size is 0")
            pass
        admin_msg = translate_msg(
            "account_info",
            {"NUM_ACCOUNTS": len(self.my_accounts.get())},
            self.mysosconfig.ui_language.language,
        )
        self.mails.add_line_by_name(
            sosconstants.SOSADMIN_USER, self.mailconfig.admin_mail_address, admin_msg
        )

    def check_remotehost(self):
        """This function generates the mail part if remotehost is not reachable"""
        rm_host: RemoteHostModel = remotehost.RemoteHost().get()
        # we should check and work on remote host status only, if it is set
        if (
            self.mysyncstat.remote_up != RemoteUpStatus.NOT_AVAILABLE
            and self.mysyncstat.remote_up != RemoteUpStatus.UP
        ):
            # Send mail if rmhost is down for longer then an hour
            if self.in_hours(time.time() - self.mysyncstat.remote_down_since) >= 1:
                self.mails.add_line_by_name(
                    sosconstants.SOSADMIN_USER,
                    self.mailconfig.admin_mail_address,
                    translate_msg(
                        "remote_host_down_admin_msg",
                        {
                            "REMOTE_DOWN_DATE": time.strftime(
                                "%d/%m/%Y",
                                time.localtime(self.mysyncstat.remote_down_since),
                            ),
                            "REMOTE_DOWN_TIME": time.strftime(
                                "%H:%M:%S",
                                time.localtime(self.mysyncstat.remote_down_since),
                            ),
                            "LSYNCD_PORT": rm_host.port,
                            "LSYNCD_HOST": rm_host.hostname,
                        },
                        self.mysosconfig.ui_language.language,
                    ),
                )
                self.mails.add_line_by_name(
                    "_rsosadmin",
                    self.mailconfig.remote_admin_mail_address,
                    translate_msg(
                        "remote_host_down_remote_admin_msg",
                        {
                            "REMOTE_DOWN_DATE": time.strftime(
                                "%d/%m/%Y",
                                time.localtime(self.mysyncstat.remote_down_since),
                            ),
                            "REMOTE_DOWN_TIME": time.strftime(
                                "%H:%M:%S",
                                time.localtime(self.mysyncstat.remote_down_since),
                            ),
                            "LSYNCD_PORT": rm_host.port,
                            "LSYNCD_HOST": rm_host.hostname,
                        },
                        self.mysosconfig.ui_language.language,
                    ),
                )
            else:
                logger.debug(
                    "admin will not be notified because the remote host has not been down for an hour"
                )

    @staticmethod
    def in_days(value):
        """Returns giving value in days

        :param value: value being converted
        :type value: float
        :return: given float value in days
        :rtype: int
        """
        return int(value / (60 * 60 * 24))

    @staticmethod
    def in_hours(value):
        """Returns giving value in hours

        :param value: value being converted
        :type value: float
        :return: given float value in hours
        :rtype: int
        """
        return int(value / (60 * 60))

    @staticmethod
    def sizeof_fmt(num, suffix="B"):
        for unit in ["", "K", "M", "G", "T", "P", "E", "Z"]:
            if abs(num) < 1024.0:
                return "%3.1f%s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f%s%s" % (num, "Y", suffix)
