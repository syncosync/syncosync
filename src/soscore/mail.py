"""
Module to send mails
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import smtplib
import time
from email import utils
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os.path import basename

from soscore import configbase
from soscore.hostname import HostName
from soscore.soshelpers_file import del_file_if_exits, write_to_file
from soscore.soshelpers_i18n import translate_msg
from sosmodel.mailconfig import MailConfigModel
from sosmodel.sos_enums import MailSslType
from sosmodel.sosconstants import HTML_MAIL_FILE, SOS_CONF_MAILCONFIG, get_version
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def send_test_mail(receiver):
    """Sends a test mail to the given receiver

    :param receiver: receiver of the testmail
    :type receiver: str
    :return: True if sent False if not
    :rtype: bool
    """
    mail = Mail()
    hostname = HostName().get().hostname
    now = time.time()
    data = {
        "HOSTNAME": hostname,
        "LAST_ACCESS_DATE": time.strftime("%d/%m/%Y", time.localtime(now)),
        "LAST_ACCESS_TIME": time.strftime("%H:%M:%S", time.localtime(now)),
    }
    msg = translate_msg("testmail_content", data)
    hl = translate_msg("testmail_hl")
    body = get_html_mail(hl, msg, hostname)
    textbody = "\n".join([hl, msg, hostname])
    return mail.send_mail(
        receiver,
        translate_msg("testmail_subject", {"HOSTNAME": hostname}),
        body,
        textbody,
        [],
    )


def get_html_mail(headline, content, hostname):
    """This function returns html mail with given content and headline

    :param headline: headline of eMail
    :type headline: str
    :param content: content of the eMail
    :type content: str
    :param hostname: hostname for mail signature
    :type hostname: str
    :return: mail ready to be sent
    :rtype: str
    """
    try:
        f = open(HTML_MAIL_FILE, "r")
        html = f.read()
        f.close()
    except PermissionError:
        logger.warning("Html frame could not be opened, sending e-mail without html")
        html = ""
    except OSError:
        logger.error("Html frame could not be found! Sending mail without html")
        html = ""

    if html:
        html = html.replace("HOSTNAME", hostname)
        html = html.replace("Headline", headline)
        html = html.replace("Content_Mail", content)
        html = html.replace("Version", get_version())
        return html
    else:
        return f"<h1>{headline}</h1>\n{content}"


class Mail(configbase.ConfigBase):
    """
    remote host getter, setter and controls
    """

    def __init__(self):
        super().__init__(MailConfigModel(), SOS_CONF_MAILCONFIG)
        self.server = None
        self.server_open = False

    @staticmethod
    def provide_model():
        return MailConfigModel()

    def set_specific(self):
        self.config: MailConfigModel
        # there is nothing to do for the mail sending from soscore, but we are also adjusting nullmailer, so the admin
        # gets all system mails
        write_to_file("/etc/nullmailer/adminaddr", self.config.admin_mail_address)
        write_to_file("/etc/nullmailer/adminaddr", "")
        write_to_file("/etc/nullmailer/allmailfrom", self.config.smtp_user)
        # generate the content for remotes
        remoteline = (
            self.config.smtp_host
            + " smtp port="
            + str(self.config.smtp_port)
            + " "
            + MailSslType.typename(self.config.ssl_type)
            + "user='"
            + self.config.smtp_user
            + "' pass='"
            + self.config.smtp_passwd
            + "'"
        )
        if(self.config.system_mails):
            write_to_file("/etc/nullmailer/remotes", remoteline)
        else:
            del_file_if_exits("/etc/nullmailer/remotes")

        return True

    def open_server(self):
        smtp_settings: MailConfigModel = self.get()
        if smtp_settings.smtp_host == "":
            logger.warning("Empty host, check mail settings")
            return False
        if smtp_settings.ssl_type == MailSslType.SSL_TLS:
            self.server = smtplib.SMTP_SSL(
                host=smtp_settings.smtp_host, port=smtp_settings.smtp_port
            )
        else:
            self.server = smtplib.SMTP(
                host=smtp_settings.smtp_host, port=smtp_settings.smtp_port
            )

        # if logger.getEffectiveLevel() == logging.DEBUG:
        #    self.server.set_debuglevel(1)

        if smtp_settings.ssl_type == MailSslType.STARTTLS:
            self.server.starttls()
        password = smtp_settings.smtp_passwd
        sender = smtp_settings.smtp_user
        if sender == "" or password == "":
            logger.warning("Empty password or user name, check mail settings")
            return False
        try:
            if len(password) > 0:
                result = self.server.login(sender, password)
            else:
                return False
            logger.debug(f"Successfully connected to mail server {smtp_settings.smtp_host}")
            self.server_open = True
            return True
        except (
            smtplib.SMTPAuthenticationError,
            smtplib.SMTPHeloError,
            smtplib.SMTPNotSupportedError,
            smtplib.SMTPException,
        ) as e:
            logger.error(
                f"An error occurred while trying to login into the server: {str(e)}"
            )
            return False

    def send_mail(self, TO, SUBJECT, HTMLBODY, TEXTBODY, attachments):
        """This function sends an html email

        :param TO: receiver of the mail
        :type TO: str
        :param SUBJECT: subject of the mail
        :type SUBJECT: str
        :param HTMLBODY: body of the mail as html
        :type HTMLBODY: str
        :param TEXTBODY: body of the mail as text
        :type TEXTBODY: str
        :param attachments: list of attachments (full path)
        :type attachments: list
        :return: True is mail was sent, false if not
        :rtype: bool"""

        config: MailConfigModel = self.get()
        # Create message container
        message = MIMEMultipart()
        message["subject"] = SUBJECT
        message["To"] = TO
        message["From"] = f"{config.sender_name} <{config.mail_from}>"
        message["Date"] = utils.formatdate(localtime=1)
        message["Message-ID"] = utils.make_msgid()
        message.preamble = """Your mail reader does not support the report format."""

        html_body = MIMEText(HTMLBODY, "html")
        message.attach(html_body)
        # text_body = MIMEText(TEXTBODY, 'plain')
        # message.attach(text_body)
        # now process the attachments
        for attachment in attachments:
            with open(attachment, "rb") as fil:
                part = MIMEApplication(fil.read(), Name=basename(attachment))
            # After the file is closed
            part["Content-Disposition"] = 'attachment; filename="%s"' % basename(
                attachment
            )
            message.attach(part)
            logger.debug(f"Attached file {basename(attachment)}")
        if self.server is None:
            if not self.open_server():
                return False
        try:
            self.server.sendmail(message["From"], [TO], message.as_string())
        except Exception:
            return False
        logger.debug(f"Successfully sent mail to {TO}")
        return True

    def close_server(self):
        """Close the connection to the server"""
        if self.server is not None and self.server_open:
            self.server.quit()

    @staticmethod
    def factory_reset_specific():
        del_file_if_exits("/etc/nullmailer/remotes")
        del_file_if_exits("/etc/nullmailer/allmailfrom")
        del_file_if_exits("/etc/nullmailer/adminaddr")
