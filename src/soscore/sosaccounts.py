"""
This code is handling sos accounts
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import copy
import glob
import grp
import json
import os
import pwd
import shutil
import subprocess

import dirsync
from soscore import configbackup_backup, soserrors, soshelpers, soshelpers_file
from sosmodel import sosconstants
from sosmodel.sosaccount import SosAccountAddModel, SosAccountModel
from sosmodel.sosconstants import MOUNT, MOUNTDEV, Partition
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def create_account_home(name):
    if not os.path.isdir(os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR)):
        logger.error("No subdir /mnt/local/syncosync, is a disk mounted to /mnt/local?")
        return
    if not os.path.isdir(
        os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, name)
    ):
        try:
            os.makedirs(
                os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, name)
            )
        except OSError:
            logger.error(
                f"Could not create account dir "
                f"{os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, name)}"
            )
            raise soserrors.AccountCreationError
        else:
            logger.debug(
                f"Account dir '{os.path.join(MOUNT[Partition.LOCAL], name)}' created."
            )
    try:
        # for jail reasons, the base dir is owned by root
        os.chown(
            os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, name), 0, 0
        )
    except Exception as e:
        logger.error(
            f"Could not change owner "
            f"{os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, name)}: {str(e)}"
        )
    # install chroot environment
    _copy_chroot(name)
    if not os.path.isdir(
        os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, name, "data")
    ):
        try:
            os.makedirs(
                os.path.join(
                    MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, name, "data"
                )
            )
        except OSError:
            logger.error(
                f"Could not create dir "
                f"{os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, name, 'data')}"
            )
    # we do this with os.system as there is no recursive way in python...
    try:
        os.system(
            f"chown -R {name}:{name} {os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, name, 'data')}"
        )
    except Exception as e:
        logger.error(
            f"Could not change owner of "
            f"{os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, name, 'data')}: {str(e)}"
        )


def _copy_chroot(name):
    """
    copy the chroot environment to the named account
    :param name: account name - this directory should exist alreeady
    :return:
    """
    logger.debug(
        f"Installing chroot env from {sosconstants.CHROOT_GEN_ENV} to /mnt/local/{sosconstants.HDDSUBDIR}/{name}"
    )
    chroot_items = glob.glob(os.path.join(sosconstants.CHROOT_GEN_ENV, "*"))
    for dirpath in chroot_items:
        dirname = os.path.basename(dirpath)
        if not os.path.isdir(
            os.path.join(
                sosconstants.MOUNT[sosconstants.Partition.LOCAL],
                sosconstants.HDDSUBDIR,
                name,
                dirname,
            )
        ):
            os.makedirs(
                os.path.join(
                    sosconstants.MOUNT[sosconstants.Partition.LOCAL],
                    sosconstants.HDDSUBDIR,
                    name,
                    dirname,
                )
            )
        dirsync.sync(
            os.path.join(sosconstants.CHROOT_GEN_ENV, dirname),
            os.path.join(
                sosconstants.MOUNT[sosconstants.Partition.LOCAL],
                sosconstants.HDDSUBDIR,
                name,
                dirname,
            ),
            "sync",
            create=True,
            purge=True,
            logger=logger,
        )
        # reconstruct the ownership of this environment
        os.system(f"chown -R root:root {dirpath}")


def set_quota(name, max_space_allowed):
    """
    sets the quota for an account by defining a % of the complete available space
    :param name: account name
    :param max_space_allowed: number between 1 and 100: 100 means virtually no quota.
    TODO: check if 100 should not end in not setting up any quota :return:
    :return True if no error, False else.
    """
    with os.popen("df /mnt/local | tail -1 | awk '{ print $2 }'") as f:
        blocks = int(f.read())
    quotablocks = int(max_space_allowed * (blocks / 100))
    command = (
        "setquota -u "
        + name
        + " "
        + str(quotablocks)
        + " "
        + str(quotablocks)
        + " 0 0 -a"
    )
    logger.debug(f"Setting account quota with '{command}'")
    if os.system(command):
        logger.error(f"Could not set quota for account '{name}'")
        return False
    return True


def getsosaccount_names():
    """
    :return: list with all sosaccount names from /etc/passwd
    """
    try:
        sosagrp = grp.getgrnam(sosconstants.SOSACCOUNT_GROUP_NAME)
        sosaccountlist = sosagrp.gr_mem
    except KeyError:
        sosaccountlist = []
    return sosaccountlist


def __del_homedir(name: str) -> bool:
    """
    Deletes an account's homedir
    :param name:
    :return: True if success
    """
    retval = True
    # First delete the account's home directory
    dir_path = os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, name)
    try:
        shutil.rmtree(dir_path)
    except OSError as e:
        logger.error(f"Could not delete {dir_path}: {e.strerror}")
        retval = False
    return retval

def __erase_datadir(name: str) -> bool:
    """
    Erase all data an account's data
    :param name:
    :return: True if success
    """
    retval = True
    dir_path = os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, name,'data')
    for filename in os.listdir(dir_path):
        file_path = os.path.join(dir_path, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            logger.error(f"Failed to delete {file_path}. Reason: {str(e)}")
            retval = False
    return retval


def kill_account_processes(name: str):
    """Kills all processes owned by an account, to secure, that nothing will be written anymore

    Args:
        name (str): account name (unix account also)
    """
    # kill all sessions of the account
    subprocess.run(
        f"killall -u {name}",
        check=False,
        shell=True,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    subprocess.run(
        f"killall -9 -u {name}",
        check=False,
        shell=True,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )


def __del_account_only(name) -> bool:
    """This deletes only the account on the SD card not on the hdd!

    Args:
        name (str): account name

    Returns:
        bool: True if not error
    """
    kill_account_processes(name)
    # remove the pub key, if there is one
    if os.path.isfile(os.path.join(sosconstants.ACCOUNT_KEYFILE_DIR, name)):
        os.remove(os.path.join(sosconstants.ACCOUNT_KEYFILE_DIR, name))
    # now delete the account
    if os.system(f"userdel {name}"):
        logger.critical(f"Could not delete account ¸'{name}' with userdel")
        return False
    return True


def del_by_name(name: str):
    """
    Deletes an account defined by it's name, which is unique. This function is the core of deleting an account
    it is defined outside the SosAccounts class to enable rollback and housekeeping for data which is not found
    in regular account access methods - for what reason ever. e.g. after changing hdd etc.
    The function does not raise any exceptions as there is no other chance than to try to delete everything.
    :param name: the unique account name
    :return: True, if every step was successful, false else, see logging then.
    """
    retval = __del_homedir(name)
    retval = __del_account_only(name) and retval
    configbackup_backup.backup()
    logger.info(f"Account '{name}' deleted")
    return retval

def erase_data_by_name(name: str):
    """
    Erase everything in names data directory
    :param name: the unique account name
    :return: True, if every step was successful, false else, see logging then.
    """
    retval = __erase_datadir(name)
    logger.info(f"Account's '{name}' data erased")
    return retval


def delete_all_accounts():
    """
    deletes all accounts, without removi
    :return:
    """
    logger.debug("Deleting all sos accounts")
    sosaccountlist = getsosaccount_names()
    for name in sosaccountlist:
        __del_account_only(name)


def is_password_set(name: str) -> bool:
    # Check if a password is set
    p = subprocess.run(["passwd", "-S", name], capture_output=True)
    content = p.stdout.decode()
    if name + " P" in content:
        return True
    else:
        return False


class SosAccounts:
    """
    This class is for managing accounts in the sos system.
    Most the account information itself is sored in SosAccountModel objects
    """

    def __init__(self):
        """
        initialise the account arrays and for the first time last_access and space_used
        """
        self.last_system_pwd = 0
        self.last_shadow_pwd = 0
        self.last_access_read = 0
        self.last_mail_read = 0
        self.space_used_read = 0
        self.sosadmin_last_mail = 0
        self.remote_sosadmin_last_mail = 0
        self.sosaccounts = {}
        self.last_access = (
            {}
        )  # this dict will be filled with the json produced by sosaccountaccessd
        self.__update_from_system_pwd()
        self.__update_passwd_info()

    def __read_access_data(self, filename):
        with open(filename) as json_file:
            try:
                self.last_access = json.load(json_file)
                for laname in self.last_access.keys():
                    if laname in self.sosaccounts:
                        self.sosaccounts.get(laname).last_access = self.last_access[
                            laname
                        ]
            except Exception as e:
                logger.warning(f"Error while reading access data: {str(e)}")
                pass

    def __read_mail_data(self, filename):
        last_mail = {}
        with open(filename) as json_file:
            try:
                last_mail = json.load(json_file)
                for laname in last_mail.keys():
                    if laname == sosconstants.SOSADMIN_USER:
                        self.sosadmin_last_mail = last_mail[laname]
                    if laname == "_rsosadmin":
                        self.remote_sosadmin_last_mail = last_mail[laname]
                    if laname in self.sosaccounts:
                        self.sosaccounts.get(laname).last_mail = last_mail[laname]
            except Exception as e:
                logger.warning(f"Error while reading mail data: {str(e)}")
                pass

    def __read_space_data(self, filename):
        with open(filename) as json_file:
            try:
                self.space_used = json.load(json_file)
                for laname in self.space_used.keys():
                    if laname in self.sosaccounts:
                        self.sosaccounts.get(laname).space_used = self.space_used[
                            laname
                        ]
            except Exception as e:
                logger.warning(f"Error while reading mail data: {str(e)}")
                pass
        return

    def __update_passwd_info(self):
        if int(os.stat("/etc/shadow").st_mtime) > self.last_shadow_pwd:
            for name in self.sosaccounts.keys():
                self.sosaccounts.get(name).is_password_set = is_password_set(name)
            self.last_shadow_pwd = int(os.stat("/etc/shadow").st_mtime)

    def __update_from_system_pwd(self):
        """
        get all sosaccount group members from system
        """
        if int(os.stat("/etc/passwd").st_mtime) > self.last_system_pwd:
            try:
                sosagrp = grp.getgrnam(sosconstants.SOSACCOUNT_GROUP_NAME)
                sosaccounts = sosagrp.gr_mem
            except KeyError:
                sosaccounts = []
                self.sosaccounts = {}

            pwd.getpwall()
            for name in sosaccounts:
                u = pwd.getpwnam(name)
                key_fingerprint = soshelpers.sshfilefingerprint(
                    os.path.join(sosconstants.ACCOUNT_KEYFILE_DIR, name)
                )
                (
                    rlongname,
                    mail_address,
                    backup_period,
                    remind_duration,
                    info_period,
                    mail_info_level,
                    max_space_allowed,
                ) = u.pw_gecos.split(",")
                if name not in self.sosaccounts:
                    self.sosaccounts[name] = SosAccountModel()
                self.sosaccounts[name].set(
                    name=name,
                    rlongname=rlongname,
                    mail_address=mail_address,
                    backup_period=int(backup_period),
                    remind_duration=int(remind_duration),
                    info_period=int(info_period),
                    mail_info_level=int(mail_info_level),
                    max_space_allowed=int(max_space_allowed),
                    is_password_set=is_password_set(name),
                    key_fingerprint=key_fingerprint,
                )
            for name in list(self.sosaccounts.keys()):
                if not (name in sosaccounts):
                    del self.sosaccounts[name]
            self.last_system_pwd = int(os.stat("/etc/passwd").st_mtime)

    def __update_last_access(self):
        """
        update last_access attributes by reading the cache file
        """
        if (
            os.path.isfile(sosconstants.SOSACCOUNTACCESS_CACHE)
            and int(os.stat(sosconstants.SOSACCOUNTACCESS_CACHE).st_mtime)
            > self.last_access_read
        ):
            self.__read_access_data(sosconstants.SOSACCOUNTACCESS_CACHE)
            self.last_access_read = int(
                os.stat(sosconstants.SOSACCOUNTACCESS_CACHE).st_mtime
            )

    def __update_space_used(self):
        """
        update space_used attributes by reading the cache file
        """
        if (
            os.path.isfile(sosconstants.SOSACCOUNTSPACE_CACHE)
            and int(os.stat(sosconstants.SOSACCOUNTSPACE_CACHE).st_mtime)
            > self.space_used_read
        ):
            self.__read_space_data(sosconstants.SOSACCOUNTSPACE_CACHE)
            self.space_used_read = int(
                os.stat(sosconstants.SOSACCOUNTSPACE_CACHE).st_mtime
            )

    def __update_last_mail(self):
        """
        update last_mail attributes by reading the cache file
        """
        if (
            os.path.isfile(sosconstants.SOSACCOUNTMAIL_CACHE)
            and int(os.stat(sosconstants.SOSACCOUNTMAIL_CACHE).st_mtime)
            > self.last_mail_read
        ):
            self.__read_mail_data(sosconstants.SOSACCOUNTMAIL_CACHE)
            self.last_mail_read = int(
                os.stat(sosconstants.SOSACCOUNTMAIL_CACHE).st_mtime
            )

    def update_data(self):
        """
        generally update all attributes
        """
        self.__update_from_system_pwd()
        self.__update_passwd_info()
        self.__update_last_access()
        self.__update_last_mail()
        self.__update_space_used()

    def get(self):
        """
        the main entry to get a list of all sos accounts
        :return: list of SosAccountModel objects
        """
        self.update_data()
        return self.sosaccounts.values()

    def get_by_name(self, name) -> SosAccountModel:
        """
        get an account by it's unique name
        :param name: the account name
        :return: a SosAccountModel object, None if no account
        """
        self.update_data()
        return self.sosaccounts.get(name)

    def add(self, account, no_backup=False):
        """
        add a new sos account to the system
        :param account: a SosAccountAddModel object
        :param no_backup: if True no config backup will be made (triggered from restore)
        :return true if successful, false else
        """
        retval = True
        logger.debug(f"Adding account '{account.name}' requested")
        # is the account already existing?
        # print("account:", account.name)
        if self.get_by_name(account.name):
            raise soserrors.AccountAlreadyExistingError

        # now check, if there is maybe a system account with that name, not possible either
        # this is not really performing, but does not happen often
        try:
            accountfound = pwd.getpwnam(account.name)
        except KeyError:
            accountfound = False
        if accountfound and account.name not in getsosaccount_names():
            raise soserrors.AccountAlreadyExistingError

        # the data is already checked in the account init method, so we can add it at full risk
        # first start with adding the account
        if account.password is not None and account.password != "":
            command = (
                'useradd -M -d /data -c "'
                + account.rlongname
                + ","
                + account.mail_address
                + ","
                + str(account.backup_period)
                + ","
                + str(account.remind_duration)
                + ","
                + str(account.info_period)
                + ","
                + str(int(account.mail_info_level))
                + ","
                + str(account.max_space_allowed)
                + '"'
                + " -p '"
                + str(account.password)
                + "'"
                + " -s /bin/bash "
                + account.name
            )
            account.is_password_set = True

        else:
            command = (
                'useradd -M -d /data -c "'
                + account.rlongname
                + ","
                + account.mail_address
                + ","
                + str(account.backup_period)
                + ","
                + str(account.remind_duration)
                + ","
                + str(account.info_period)
                + ","
                + str(int(account.mail_info_level))
                + ","
                + str(account.max_space_allowed)
                + '"'
                + " -p '!'"
                + " -s /bin/bash "
                + account.name
            )
            account.is_password_set = False

        if os.system(command):
            logger.critical(f"Could not add account with useradd: '{account.name}'")
            raise soserrors.AccountCreationError

        logger.debug(f"Adding '{account.name}' to sosaccount group")
        if os.system("usermod -G sosaccount " + account.name):
            logger.error(f"Could not add account {account.name} to sosaccount group ")

        # set ssh_pub_key
        fingerprint = soshelpers.set_ssh_pub_key(
            os.path.join(sosconstants.ACCOUNT_KEYFILE_DIR, account.name),
            account.ssh_pub_key,
        )

        # now create/refresh the file structure for the account
        create_account_home(account.name)
        # ok, now go for the quota settings, another hard piece of work
        set_quota(account.name, account.max_space_allowed)

        # now finally: add the account also to the array
        self.sosaccounts[account.name] = SosAccountModel()
        self.sosaccounts[account.name].set(
            name=account.name,
            rlongname=account.rlongname,
            mail_address=account.mail_address,
            backup_period=account.backup_period,
            remind_duration=account.remind_duration,
            info_period=account.info_period,
            mail_info_level=account.mail_info_level,
            max_space_allowed=account.max_space_allowed,
            is_password_set=account.is_password_set,
            key_fingerprint=fingerprint,
        )
        if not no_backup:
            configbackup_backup.backup()
        logger.info(f"Account: '{account.name}' added.")
        return retval

    def mod(self, account: SosAccountAddModel, no_backup=False):
        """
        modifies an existing account
        :param account:
        :param no_backup: if True no config backup will be made (triggered from restore)
        :return: True if everything ok, False else
        """
        # ok, now check every item.
        logger.debug(f"Modifying account '{account.name}' requested")
        changed = False
        to_mod: SosAccountModel = self.get_by_name(account.name)
        if to_mod is None:
            logger.error(
                f"There is no account with name '{account.name}'. This should not happen."
            )
            return False
        if (
            to_mod.rlongname != account.rlongname
            or to_mod.mail_address != account.mail_address
            or to_mod.backup_period != account.backup_period
            or to_mod.remind_duration != account.remind_duration
            or to_mod.info_period != account.info_period
            or to_mod.mail_info_level != account.mail_info_level
            or to_mod.max_space_allowed != account.max_space_allowed
        ):
            command = (
                'usermod -c "'
                + account.rlongname
                + ","
                + account.mail_address
                + ","
                + str(account.backup_period)
                + ","
                + str(account.remind_duration)
                + ","
                + str(account.info_period)
                + ","
                + str(int(account.mail_info_level))
                + ","
                + str(account.max_space_allowed)
                + '" '
                + account.name
            )
            if os.system(command):
                logger.error(f"Could not modify account '{account.name}'")
            logger.debug("Unix user was modified")
            changed = True
        # should we change the quota?
        if to_mod.max_space_allowed != account.max_space_allowed:
            logger.debug("Quota was changed")
            set_quota(account.name, account.max_space_allowed)
        # check if we should change/delete the password
        if account.password == "":
            if os.system(f"passwd -d {account.name}"):
                logger.error(f"Could not modify account '{account.name}'")
            logger.debug("Password deleted")
            account.is_password_set = False
            changed = True
        else:
            if account.password is not None:
                # ok, we have to change the password
                # newpass = crypt.crypt(account.password)
                command = "usermod -p '" + account.password + "' " + account.name
                if os.system(command):
                    logger.error(f"Could not change password for {account.name}")
                logger.debug("Password changed")
                account.is_password_set = True
                changed = True

        if account.ssh_pub_key is not None:
            # process the pub key
            fingerprint = soshelpers.set_ssh_pub_key(
                os.path.join(sosconstants.ACCOUNT_KEYFILE_DIR, account.name),
                account.ssh_pub_key,
            )
            logger.debug("ssh key was changed")
            changed = True
        else:
            fingerprint = account.key_fingerprint

        # now finally: mod the account also in the array
        self.sosaccounts[account.name].set(
            name=account.name,
            rlongname=account.rlongname,
            mail_address=account.mail_address,
            backup_period=account.backup_period,
            remind_duration=account.remind_duration,
            info_period=account.info_period,
            mail_info_level=account.mail_info_level,
            max_space_allowed=account.max_space_allowed,
            is_password_set=account.is_password_set,
            key_fingerprint=fingerprint,
        )
        if changed:
            if not no_backup:
                configbackup_backup.backup()
        return True

    def mod_from_json(self, json_str):
        """
        modfies an account from a json string.
        Note: if an entry is Null (aka None aka not existing),
        it is not modified.
        :param json_str:
        :return: True if modification was successful, False else
        """
        account: SosAccountAddModel = SosAccountAddModel.from_json(json_str)
        orig = self.get_by_name(account.name)
        if orig is None:
            logger.error(
                f"There is no account with name '{account.name}'. This should not happen."
            )
            return False
        to_mod = copy.deepcopy(orig)
        to_mod.update_from_json(json_str)
        mod_add_account = SosAccountAddModel.from_json(to_mod.to_json())
        mod_add_account.password = account.password
        mod_add_account.ssh_pub_key = account.ssh_pub_key
        return self.mod(mod_add_account)

    def delete(self, account: SosAccountModel):
        """
        Deletes an account
        :param account: SosAccountModel
        :return: True, if everything ok, False else
        """
        to_del: SosAccountModel = self.get_by_name(account.name)
        if to_del is None:
            logger.error(
                f"There is no account with name '{account.name}'. This should not happen."
            )
            return False
        return del_by_name(to_del.name)

    def erase_data(self, account: SosAccountModel):
        """
        Erase all data from an account
        :param account: SosAccountModel
        :return: True, if everything ok, False else
        """
        to_erase: SosAccountModel = self.get_by_name(account.name)
        if to_erase is None:
            logger.error(
                f"There is no account with name '{account.name}'. This should not happen."
            )
            return False
        retval = erase_data_by_name(to_erase.name)
        self.__update_space_used()
        return retval

    def store_last_mail(self):
        last_mail_store = {}
        for name in self.sosaccounts.keys():
            last_mail_store[name] = self.get_by_name(name).last_mail
        last_mail_store[sosconstants.SOSADMIN_USER] = self.sosadmin_last_mail
        last_mail_store["_rsosadmin"] = self.remote_sosadmin_last_mail
        soshelpers_file.write_to_file(
            sosconstants.SOSACCOUNTMAIL_CACHE, json.dumps(last_mail_store)
        )
        destination, mountopts = soshelpers_file.get_mountpoint(
            MOUNTDEV[Partition.LOCAL]
        )
        if destination == MOUNT[Partition.LOCAL] and "ro" not in mountopts:
            shutil.copy2(
                sosconstants.SOSACCOUNTMAIL_CACHE,
                sosconstants.SOSACCOUNTMAIL_PERSISTENT,
            )
            logger.debug(f"Stored {sosconstants.SOSACCOUNTMAIL_PERSISTENT}")

    def set_last_mail(self, name, last_mail):
        """
        sets last mail value to the time given (reset is by setting to 0)
        :param name: account name
        :param last_mail: time.time() stamp
        :return: True, if has changed
        """
        account: SosAccountModel = self.get_by_name(name)
        if account is None:
            return True
        old_last_mail = account.last_mail
        if old_last_mail != last_mail:
            account.last_mail = last_mail
            self.store_last_mail()
            return True
        return False
