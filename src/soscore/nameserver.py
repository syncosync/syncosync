# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
For managing nameservers
"""
import os
from typing import Optional

from soscore import configbase, soshelpers_file
from sosmodel.nameserver import NameServerModel
from sosmodel.sosconstants import SOS_CONF_NAMESERVER
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


class NameServer(configbase.ConfigBase):
    def __init__(self):
        super().__init__(NameServerModel(), SOS_CONF_NAMESERVER)

    @staticmethod
    def provide_model():
        return NameServerModel()

    @staticmethod
    def get_specific() -> Optional[NameServerModel]:
        """
        requests service specific configuration and returns this to the get call in the parent.
        :return: NameServerModel or None if error
        """
        nameserver = NameServerModel()
        resolvers = []
        if os.path.isfile("/etc/resolv.conf"):
            try:
                with open("/etc/resolv.conf", "r") as resolvconf:
                    for line in resolvconf.readlines():
                        line = line.split("#", 1)[0]
                        line = line.rstrip()
                        if "nameserver" in line:
                            resolvers.append(line.split()[1])
            except IOError as error:
                logger.error(f"Could not analyse resolv.conf:{error}")
                return None
            if len(resolvers) >= 1 and resolvers[0]:
                nameserver.first_nameserver = resolvers[0]
            if len(resolvers) >= 2 and resolvers[1]:
                nameserver.second_nameserver = resolvers[1]
            if len(resolvers) >= 3 and resolvers[2]:
                nameserver.third_nameserver = resolvers[2]
        return nameserver

    def set_specific(self) -> bool:
        output = []
        self.config: NameServerModel
        if self.config.first_nameserver != "":
            output.append(f"nameserver {self.config.first_nameserver}")
        if self.config.second_nameserver != "":
            output.append(f"nameserver {self.config.second_nameserver}")
        if self.config.third_nameserver != "":
            output.append(f"nameserver {self.config.third_nameserver}")
        if output != []:
            try:
                with open("/etc/resolv.conf", "w") as outfile:
                    outfile.write("\n".join(output))
                    outfile.write("\n")
            except OSError:
                logger.error("not able to output nameserver config to resolv.conf")
        else:
            soshelpers_file.del_file_if_exits("/etc/resolv.conf")
        return True

    @staticmethod
    def factory_reset_specific():
        soshelpers_file.del_file_if_exits("/etc/resolv.conf")
