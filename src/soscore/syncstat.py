"""
syncstat, is used by the syncstat deamon (or by the sos manager)
"""
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2023  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import glob
import os
import pwd
import socket
import time

import psutil
import soscore.soshelpers_file
import soscore.soshelpers_hardware
import soscore.soshelpers_shape
from soscore import (
    drivemanager,
    remotehost,
    sosconfig,
    soshelpers,
    sshdmanager,
    trafficshape,
)
from soscore.soskeyx import SosKeyExchange
from sosmodel import sosconstants
from sosmodel.sos_enums import (
    RemoteUpStatus,
    Sshd,
    SshdAccess,
    SyncstatStatus,
    SyncStatus,
    SystemMode,
)
from sosmodel.sostocheck import SosToCheck
from sosmodel.syncstat import BandWidthModel, DriveSpaceModel, SyncStatModel
from sosmodel.sysstate import SysStateModel
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def check_process_running(process_name: str) -> bool:
    """checks if a process with process_name is running

    Args:
        process_name (str): name of process

    Returns:
        bool: True if yes, False else
    """
    # Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if process_name.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False


class SyncStatUpdater:
    """ "
    Actively updates all status and stores this also, should normally be only called from
    one process on the system
    """

    def __init__(self):
        self.last_checked = 0
        self.syncstat: SyncStatModel = SyncStatModel()
        self.syncstat.date = 0
        # self.last_stat: int = 0  # this is when we got the last stat
        # last time sysstate was read
        self._last_sysstate = 0
        self.old_remote_up: RemoteUpStatus = (
            RemoteUpStatus.NOT_AVAILABLE
        )  # this stores the state
        # if the remote partner is reachable
        self.remotehostconfig = remotehost.RemoteHost()
        self.remotehostip = ""
        self.oldremotehostip = ""
        self.mylsyncd = self.remotehostconfig.get()
        self.mysosconfig = sosconfig.SosConfig()
        self.mounthealth = True
        self.aktconfig = self.mysosconfig.get()
        self.aktsystemdescription = self.mysosconfig.get_system_description()
        self.old_syncup: SyncStatus = SyncStatus.NOT_AVAILABLE
        self.old_syncdown: SyncStatus = SyncStatus.NOT_AVAILABLE
        self.oldtx, self.oldrx = soscore.soshelpers_shape.get_traffic()
        self.oldbw_up = 0
        self.oldbw_down = 0
        self.uuid = ""
        self.old_uuid = ""
        self.oldtime = time.time()
        self.drivestatus = drivemanager.Drives()
        self.drivestatus.generate()
        self.last_sostocheck_read = 0
        self.last_drivespace_read = 0
        self.oldconfigbackup_needed = False
        self.day = False
        # this is the ringbuffer for bandwidth values
        self.bw_buffer = []
        # fill the buffer with 0
        akttime = int(time.time())
        for i in range(sosconstants.BW_BUFFER_SIZE):
            self.bw_buffer.append(
                BandWidthModel(
                    0,
                    0,
                    akttime
                    - (sosconstants.MAX_BW - i) * sosconstants.SYNCSTAT_REFRESH_TIME,
                )
            )

    def update_data(self):
        """
        updates the structure
        :return: True, if data has changed (to issue a get afterwards)
        """
        has_changed = False
        if self.sysstatecheck():
            has_changed = True
        if self.remotecheck():
            has_changed = True
        if self.syncsyncstat():
            has_changed = True
        if self.bandwidth():
            has_changed = True
        if self.drivespin():
            has_changed = True
        if self.update_mounthealth():
            has_changed = True
        if self.sostocheck():
            has_changed = True
        if self.diskspace():
            has_changed = True
        if (
            soshelpers.soshelpers_file.check_configbackup_flag()
            and not self.oldconfigbackup_needed
        ):
            self.syncstat.configbackup_needed = True
            self.oldconfigbackup_needed = True
            has_changed = True
        elif (
            not soshelpers.soshelpers_file.check_configbackup_flag()
            and self.oldconfigbackup_needed
        ):
            self.syncstat.configbackup_needed = False
            self.oldconfigbackup_needed = False
            has_changed = True

        mytrafficshape = trafficshape.get_cached_settings()
        actday = mytrafficshape.day
        if actday != self.syncstat.day:
            self.day = actday
            self.syncstat.day = actday
            has_changed = True

        if has_changed:
            self.syncstat.date = int(time.time())
            self.syncstat.syncstat_status = SyncstatStatus.AVAILABLE
            soscore.soshelpers_file.write_to_file(
                sosconstants.SYNCSTAT_CACHE, self.syncstat.to_json()
            )
            # here we could also setup external hardware indicators as LEDS
            soscore.soshelpers_hardware.set_hw_outputs(self.aktconfig, self.syncstat)
        return has_changed

    def update_mounthealth(self) -> bool:
        """It seems, that, if a disks fails, the OS will remount a drive as ro. 

        Returns:
            bool: true, if health has changed
        """
        my_mounthealth = True
        if self.syncstat.actmode == SystemMode.DEFAULT or self.syncstat.actmode == SystemMode.DEFAULT_NO_SYNC or self.syncstat.actmode == SystemMode.REMOTE_RECOVERY or self.syncstat.actmode == SystemMode.RR_FINAL:
            # check mounts:
            local_mountpoint, local_params = soscore.soshelpers_file.get_mountpoint(
                sosconstants.MOUNTDEV[sosconstants.Partition.LOCAL]
            )
            if local_mountpoint != sosconstants.MOUNT[sosconstants.Partition.LOCAL] or "rw" not in local_params:
                my_mounthealth = False
            system_mountpoint, system_params = soscore.soshelpers_file.get_mountpoint(
                sosconstants.MOUNTDEV[sosconstants.Partition.SYSTEM]
            )
            if system_mountpoint != sosconstants.MOUNT[sosconstants.Partition.SYSTEM] or "rw" not in system_params:
                my_mounthealth = False
            remote_mountpoint, remote_params = soscore.soshelpers_file.get_mountpoint(
                sosconstants.MOUNTDEV[sosconstants.Partition.REMOTE]
            )
            if remote_mountpoint != sosconstants.MOUNT[sosconstants.Partition.REMOTE] or "rw" not in remote_params:
                my_mounthealth = False
        if self.syncstat.actmode == SystemMode.DISASTER_SWAP:
            # check mounts for disaster swap
            system_mountpoint, system_params = soscore.soshelpers_file.get_mountpoint(
                sosconstants.MOUNTDEV[sosconstants.Partition.SYSTEM]
            )
            if system_mountpoint != sosconstants.MOUNT[sosconstants.Partition.SYSTEM] or "rw" not in system_params:
                my_mounthealth = False
            remote_mountpoint, remote_params = soscore.soshelpers_file.get_mountpoint(
                sosconstants.MOUNTDEV[sosconstants.Partition.REMOTE]
            )
            if remote_mountpoint != sosconstants.MOUNT[sosconstants.Partition.LOCAL] or "rw" not in remote_params:
                my_mounthealth = False
        # logger.debug(f"mounthealth called in mode '{SystemMode.message(self.syncstat.actmode)}' is: {my_mounthealth}")
        if my_mounthealth != self.mounthealth:
            self.mounthealth = my_mounthealth
            self.syncstat.mounthealth = my_mounthealth
            return True
        else:
            return False


    def sostocheck(self):
        """
        gets the lsyncinfo from the sosrsyncwrapper script, which outputs it's results in a cache file
        :return: True if data has changed
        """
        has_changed = False
        if os.path.isfile(sosconstants.SOSTOCHECK_CACHE):
            if (
                int(os.stat(sosconstants.SOSTOCHECK_CACHE).st_mtime)
                > self.last_sostocheck_read
            ):
                with open(sosconstants.SOSTOCHECK_CACHE) as json_file:
                    try:
                        sostocheck = SosToCheck.from_json(json_file.read())
                        self.syncstat.to_check = sostocheck.to_check
                        self.syncstat.percent = sostocheck.percent
                        has_changed = True
                        self.last_sostocheck_read = int(
                            os.stat(sosconstants.SOSTOCHECK_CACHE).st_mtime
                        )
                    except Exception as e:
                        logger.warning(
                            f"Could not decode cache file {sosconstants.SOSTOCHECK_CACHE}: {str(e)}"
                        )
        return has_changed

    def diskspace(self):
        """
        gets used space of mounted (aka hdd) volumes, but only checks if volumes are in use,
        to avoid spin up of drives.
        :return: True if dis
        """
        has_changed = False
        if os.path.isfile(sosconstants.DRIVESPACE_CACHE):
            if (
                int(os.stat(sosconstants.DRIVESPACE_CACHE).st_mtime)
                > self.last_drivespace_read
            ):
                with open(sosconstants.DRIVESPACE_CACHE) as json_file:
                    try:
                        drivespace = DriveSpaceModel.from_json(json_file.read())
                        self.syncstat.free_space = drivespace.free_space
                        self.syncstat.total_size = drivespace.total_size
                        self.syncstat.iused = drivespace.iused
                        has_changed = True
                        self.last_drivespace_read = int(
                            os.stat(sosconstants.DRIVESPACE_CACHE).st_mtime
                        )
                    except Exception as e:
                        logger.warning(
                            f"Could not decode cache file {sosconstants.DRIVESPACE_CACHE}: {str(e)}"
                        )
        return has_changed

    def drivespin(self):
        """
        gets spin status of all drives
        :return: True if spin status has changed
        """
        has_changed = False
        for (
            drive
        ) in self.drivestatus.get_checklist():  # only check drives, which are checkable
            if soshelpers.check_drive(drive):
                if drive in self.syncstat.driveup:
                    logger.info(f"drive spin down: {drive}")
                    has_changed = True
                    self.syncstat.driveup.remove(drive)
            else:
                if drive not in self.syncstat.driveup:
                    logger.info(f"drive spin up: {drive}")
                    has_changed = True
                    self.syncstat.driveup.append(drive)
        return has_changed

    def bandwidth(self):
        """
        update bandwidth information
        :return: True if bandwidth has changed
        """
        tx, rx = soscore.soshelpers_shape.get_traffic()
        # calculate the bandwidth
        nowtime = time.time()
        timerange = nowtime - self.oldtime
        self.oldtime = nowtime
        has_changed = False
        if timerange <= 0:
            # did this really happen?
            self.syncstat.bw_down = 0
            self.syncstat.bw_up = 0
        else:
            self.syncstat.bw_up = round(
                ((tx - self.oldtx) * 8 / timerange) / 1000000, 2
            )
            self.syncstat.bw_down = round(
                ((rx - self.oldrx) * 8 / timerange) / 1000000, 2
            )
            # ok, store this tuple also in the bw history
            bw_info = BandWidthModel(
                self.syncstat.bw_up, self.syncstat.bw_down, int(nowtime)
            )
            if len(self.bw_buffer) >= sosconstants.BW_BUFFER_SIZE:
                self.bw_buffer.pop(0)
            self.bw_buffer.append(bw_info)
            if (
                self.oldbw_down != self.syncstat.bw_down
                or self.oldbw_up != self.syncstat.bw_up
            ):
                self.oldbw_down = self.syncstat.bw_down
                self.oldbw_up = self.syncstat.bw_up
                has_changed = True
        self.oldtx = tx
        self.oldrx = rx
        return has_changed

    def remotecheck(self):
        """
        checks in time boundaries from sosconstants, if remote host is reachable.
        Note: this is the only area, where lsyncd is started - and to avoid in any way to lose data it should be
        :return: True if status has changed
        """
        akttime = int(time.time())
        # we have to refresh the remotehost
        self.mylsyncd = self.remotehostconfig.get()
        has_changed = False

        # first check, if the remote host is available at all and reachable and so on
        # check the remote host only, if there is a reason to do so
        # logger.debug(
        #     f"Remotecheck with mode {self.syncstat.actmode} and remote status {self.old_remote_up} "
        #     f"time left if good: {akttime - (self.last_checked+sosconstants.CHECK_TIME_TRUE)} "
        #     f"bad: {akttime - (self.last_checked+sosconstants.CHECK_TIME_FALSE)}"
        # )
        if (
            self.syncstat.actmode == SystemMode.DEFAULT
            or self.syncstat.actmode == SystemMode.REMOTE_RECOVERY
        ) and self.mylsyncd.hostname != "":
            if (
                self.last_checked + sosconstants.CHECK_TIME_TRUE < akttime
                and self.old_remote_up == RemoteUpStatus.UP
            ) or (
                self.last_checked + sosconstants.CHECK_TIME_FALSE < akttime
                and self.old_remote_up != RemoteUpStatus.UP
            ):
                self.last_checked = akttime
                (
                    self.syncstat.remote_up,
                    self.uuid,
                ) = self.remotehostconfig.check_host()
                try:
                    self.remotehostip = socket.gethostbyname(self.mylsyncd.hostname)
                except Exception as error:
                    logger.error(
                        f"Could not resolve host {self.mylsyncd.hostname}: {str(error)}"
                    )
                logger.debug(
                    f"Checked remotehost {self.mylsyncd.hostname} ({self.remotehostip}) with result: {RemoteUpStatus.message(self.syncstat.remote_up)}"
                )
        else:
            if self.mylsyncd.hostname == "":
                self.syncstat.remote_up = RemoteUpStatus.NOT_SET
                logger.debug("Will not check remote host, as remote hostname is empty")
            else:
                self.syncstat.remote_up = RemoteUpStatus.NOT_AVAILABLE

        # check if remote state has changed and change the timers
        if self.syncstat.remote_up != self.old_remote_up:
            has_changed = True
            self.old_remote_up = self.syncstat.remote_up
            if self.syncstat.remote_up != RemoteUpStatus.UP:
                self.syncstat.remote_down_since = int(time.time())

        # now process lsyncd and also sshd
        if (
            self.syncstat.remote_up == RemoteUpStatus.UP
            and self.syncstat.actmode == SystemMode.DEFAULT
        ):
            # we process the state, even if it is unchanged, so we are sure, lsyncd is in the right state
            if self.oldremotehostip != "" and (
                self.remotehostip != self.oldremotehostip
            ):
                logger.info("IP address has changed, lsyncd will be restarted")
                remotehost.check_and_start_lsyncd(restart=True)
                self.oldremotehostip = self.remotehostip
            elif self.old_uuid != "" and (self.uuid != self.old_uuid):
                logger.info("Remote uuid has changed, lsyncd will be restarted")
                remotehost.check_and_start_lsyncd(restart=True)
                self.old_uuid = self.uuid
            else:
                remotehost.check_and_start_lsyncd()
                self.old_uuid = self.uuid
                self.oldremotehostip = self.remotehostip
        else:
            remotehost.stop_lsyncd()
        # process external sshd
        # to make this clear: if the remote pubkey is not here, this means, no remote host could access our sshd so
        # it can be closed. This is _not_ related to our local key!
        remote_key = SosKeyExchange.get_remote_key()
        if remote_key.fingerprint != "" and self.syncstat.actmode == SystemMode.DEFAULT:
            sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OPEN)
        else:
            sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OFF)

        if has_changed:
            logger.info(
                f"Remote check status to {self.mylsyncd.hostname} has changed to: '{RemoteUpStatus.message(self.syncstat.remote_up)}'"
            )
        return has_changed

    def syncsyncstat(self):
        """
        tries to find out if there is any incoming or outgoing sync
        :return: True if status has changed
        """
        has_changed = False
        if self.syncstat.remote_up == RemoteUpStatus.UP:
            # now check there is an
            if check_process_running("sosrsyncwrapper"):
                self.syncstat.syncup = SyncStatus.ACTIVE
            else:
                self.syncstat.syncup = SyncStatus.IDLE
            # check if there is any active download, we do this by checking, if there is rsync running
            # on syncosync's user id
            try:
                self.syncstat.syncdown = SyncStatus.IDLE
                pwd.getpwnam("syncosync")
                if not os.system("ps -u syncosync -U syncosync | grep -q rsync"):
                    self.syncstat.syncdown = SyncStatus.ACTIVE
            except KeyError:
                logger.critical("There is no syncosync user in the system!?")
        else:
            self.syncstat.syncup = SyncStatus.NOT_AVAILABLE
            self.syncstat.syncdown = SyncStatus.NOT_AVAILABLE
        if self.old_syncup != self.syncstat.syncup:
            has_changed = True
            self.syncstat.syncup_change_since = int(time.time())
            self.old_syncup = self.syncstat.syncup
        if self.old_syncdown != self.syncstat.syncdown:
            has_changed = True
            self.syncstat.syncdown_change_since = int(time.time())
            self.old_syncdown = self.syncstat.syncdown
        return has_changed

    def sysstatecheck(self):
        """
        gets the actual system state from the cache file
        :return: True if status has changed
        """
        if (
            os.path.isfile(sosconstants.ACTSTATE_FILE)
            and int(os.stat(sosconstants.ACTSTATE_FILE).st_mtime) > self._last_sysstate
        ):
            with open(sosconstants.ACTSTATE_FILE) as json_file:
                try:
                    thissysstate = SysStateModel()
                    content = json_file.read()
                    thissysstate.update_from_json(content)
                except Exception:
                    pass
            self._last_sysstate = int(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
            self.syncstat.actmode = thissysstate.actmode
            self.syncstat.sysstate = thissysstate.sysstate
            return True
        return False

    def get(self):
        """
        the standard getter.
        :return: SyncStatModel object
        """
        return self.syncstat

    def get_bw_history(self):
        return self.bw_buffer


class SyncStatReader:
    """
    For clients which will just present the actual status from cached data. Dunno if there is any need for it, but it
    does not cost so much performance to write out the status as a json in tmp
    """

    def __init__(self):
        self.syncstat = SyncStatModel()
        self.last_syncstat_read = 0

    def update_data(self):
        has_changed = False
        if os.path.isfile(sosconstants.SYNCSTAT_CACHE):
            if (
                int(os.stat(sosconstants.SYNCSTAT_CACHE).st_mtime)
                > self.last_syncstat_read
            ):
                with open(sosconstants.SYNCSTAT_CACHE) as json_file:
                    data = json_file.read()
                    mysyncstat = SyncStatModel()
                    mysyncstat.update_from_json(data)
                    self.syncstat = mysyncstat
                    self.last_syncstat_read = int(
                        os.stat(sosconstants.SYNCSTAT_CACHE).st_mtime
                    )
                    has_changed = True
        else:
            self.syncstat.syncstat_status = SyncstatStatus.NOT_AVAILABLE
        return has_changed

    def get(self):
        """
        the standard getter.
        :return: SyncStatModel object
        """
        self.update_data()
        return self.syncstat
