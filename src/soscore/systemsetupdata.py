"""
Handling of systemsetupdata json to exchange between sos buddies
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from soscore import drivemanager, hostname, remotehost_systemsetupdata, soskey
from sosmodel.sos_enums import Partition, PartitioningState, SsdAction
from sosmodel.systemsetupdata import SystemSetupDataModel


def get_system_ssd():
    """collect the data from different areas"""
    myssd = SystemSetupDataModel()
    mykey = soskey.SosKey()
    mykeykey = mykey.get()
    myssd.pub_key = mykeykey.pub_key
    mydrive = drivemanager.Drives()
    mydrive.generate()
    myssd.hostname = hostname.HostName().get().hostname
    myssd.partition_data.free = mydrive.get_free_data()
    myssd.vg_sos_uuid = mydrive.vgs.vg_uuid
    try:
        myssd.partition_data.local = mydrive.lvs.lv[
            drivemanager.get_lvname(Partition.LOCAL, myssd.vg_sos_uuid)
        ].lv_size
    except KeyError:
        myssd.partition_data.local = 0
    try:
        myssd.partition_data.remote = mydrive.lvs.lv[
            drivemanager.get_lvname(Partition.REMOTE, myssd.vg_sos_uuid)
        ].lv_size
    except KeyError:
        myssd.partition_data.remote = 0
    try:
        myssd.partition_data.local_min = mydrive.lvs.lv[
            drivemanager.get_lvname(Partition.LOCAL, myssd.vg_sos_uuid)
        ].lv_used_size
    except KeyError:
        myssd.partition_data.local_min = 0
    try:
        myssd.partition_data.remote_min = mydrive.lvs.lv[
            drivemanager.get_lvname(Partition.REMOTE, myssd.vg_sos_uuid)
        ].lv_used_size
    except KeyError:
        myssd.partition_data.remote_min = 0
    return myssd


def get_common_ssd():
    """calculate a minimal ssd out of system_ssd and remote_ssd"""
    system_ssd = get_system_ssd()
    remote_ssd: SystemSetupDataModel = (
        remotehost_systemsetupdata.RemoteHostSystemSetupData().get()
    )
    if system_ssd.vg_sos_uuid == "" or remote_ssd.vg_sos_uuid == "":
        return system_ssd
    common_ssd = SystemSetupDataModel()
    system_extends = (
        system_ssd.partition_data.free
        + system_ssd.partition_data.local
        + system_ssd.partition_data.remote
    )
    remote_extends = (
        remote_ssd.partition_data.free
        + remote_ssd.partition_data.local
        + remote_ssd.partition_data.remote
    )
    common_ssd.partition_data.local_min = max(
        system_ssd.partition_data.local_min, remote_ssd.partition_data.remote_min
    )
    common_ssd.partition_data.remote_min = max(
        system_ssd.partition_data.remote_min, remote_ssd.partition_data.local_min
    )
    if system_extends > remote_extends:
        # ok, we have to go for the remote extends as this vg seems to be smaller
        common_ssd.partition_data.local = remote_ssd.partition_data.remote
        common_ssd.partition_data.remote = remote_ssd.partition_data.local
        common_ssd.partition_data.free = remote_ssd.partition_data.free
    else:
        common_ssd.partition_data.local = system_ssd.partition_data.local
        common_ssd.partition_data.remote = system_ssd.partition_data.remote
        common_ssd.partition_data.free = system_ssd.partition_data.free
    return common_ssd


def get_partioning_status() -> PartitioningState:
    system_ssd = get_system_ssd()
    remote_ssd: SystemSetupDataModel = (
        remotehost_systemsetupdata.RemoteHostSystemSetupData().get()
    )
    if not system_ssd.vg_sos_uuid or not remote_ssd.vg_sos_uuid:
        # No volume group on one of the two parties, we cannot resize partitions here
        return PartitioningState.RESIZE_NOT_POSSIBLE
    elif not SsdAction.is_leader(remote_ssd.action):
        # The system is leading partitioning -> dictating the partitioning
        return PartitioningState.OK

    common_ssd = SystemSetupDataModel()
    system_extends = (
        system_ssd.partition_data.free
        + system_ssd.partition_data.local
        + system_ssd.partition_data.remote
    )
    remote_extends = (
        remote_ssd.partition_data.free
        + remote_ssd.partition_data.local
        + remote_ssd.partition_data.remote
    )
    common_ssd.partition_data.local_min = max(
        system_ssd.partition_data.local_min, remote_ssd.partition_data.remote_min
    )
    common_ssd.partition_data.remote_min = max(
        system_ssd.partition_data.remote_min, remote_ssd.partition_data.local_min
    )

    if (
        common_ssd.partition_data.local_min + common_ssd.partition_data.remote_min
    ) > system_extends or (
        common_ssd.partition_data.local_min + common_ssd.partition_data.remote_min
    ) > remote_extends:
        return PartitioningState.RESIZE_NOT_POSSIBLE
    elif (
        system_ssd.partition_data.local == remote_ssd.partition_data.remote
        and system_ssd.partition_data.remote == remote_ssd.partition_data.local
    ):
        # Local and remote partitioning matches on both sides
        return PartitioningState.OK
    else:
        return PartitioningState.RESIZE_NEEDED
