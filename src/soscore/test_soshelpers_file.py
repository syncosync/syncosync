import logging
import os
import sys

import soscore.sysstate
from soscore.logged_test_case import LoggedTestCase
from sosmodel import sysstate
from sosmodel.sos_enums import SystemMode

REPORTFILE = "/tmp/sosreport.json"

logger = logging.getLogger()
logger.level = logging.DEBUG
stream_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(stream_handler)


class TestSosHelpersFile(LoggedTestCase):
    def test_01_init(self):
        # clean up stuff
        if os.path.isfile(REPORTFILE):
            os.remove(REPORTFILE)

    def test_02_init_file(self):
        mymodel = sysstate.SysStateResult(SystemMode.REMOTE_RECOVERY, ["1st line"])
        soscore.sysstate.write_result_file(REPORTFILE, mymodel)
        assert os.path.exists(REPORTFILE)

    def test_03_read_file(self):
        mymodel = soscore.sysstate.read_result_file(REPORTFILE)
        self.assertEqual(mymodel.mode, SystemMode.REMOTE_RECOVERY, "test good")
        self.assertEqual(mymodel.report, ["1st line"], "test good")

    def test_04_append_file(self):
        soscore.sysstate.result_file_append(REPORTFILE, SystemMode.DEFAULT, "2nd line")
        mymodel = soscore.sysstate.read_result_file(REPORTFILE)
        self.assertEqual(mymodel.mode, SystemMode.DEFAULT, "test good")
        self.assertEqual(mymodel.report, ["1st line", "2nd line"], "test good")
