"""
Parent class for configurations to be stored
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import copy
import os
import time
from abc import ABC, abstractmethod

from soscore import configbackup_backup
from soscore.soshelpers_file import read_from_file, write_to_file
from sosmodel.configbasemodel import ConfigBaseModel
from sosmodel.serializerbase import SerializerBase
from sosmodel.sosconstants import FACTORY_CONF_DIR, SOS_CONF_DIR, SPECIFIC_GET_TIME
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


class ConfigBase(ABC):
    """Handling of config related methods"""

    def __init__(self, config, configfile):
        self.configfile: str = configfile
        self.config: ConfigBaseModel = config
        self.oldconfig = ConfigBaseModel()
        self.last_read = 0  # last time the config was refreshed from it's file. Should be the timestamp of the file
        self.last_get_specific = (
            0  # last time the config was re-read from it's real service config
        )
        self.get()

    @abstractmethod
    def provide_model(self):
        return SerializerBase()

    def set_specific(self):
        return True

    def get_specific(self):
        return None

    def factory_reset_specific(self):
        return None

    def _get_child_model(self):
        config = self.provide_model()  # pylint: disable=no-member
        return config

    def write_config(self, backup=True):
        """
        writes a config to SOS_CONF_DIR
        :param backup: if set, config backup will also be issued
        :return:
        """
        write_to_file(
            os.path.join(SOS_CONF_DIR, self.configfile), self.config.to_json()
        )
        self.last_read = int(
            os.stat(os.path.join(SOS_CONF_DIR, self.configfile)).st_mtime
        )
        if backup:
            configbackup_backup.backup()

    #            # set_configbackup_flag()

    def set_from_file(self, basepath=SOS_CONF_DIR, backup=True) -> bool:
        """
        sets a config either from some other path (e.g. for swap state) or from SOS_CONF_DIR,
        if SOS_CONF_DIR, last_read is also checked and set
        :param backup: If true, will set backup flag
        :param basepath: if set, the path from which the file will be read
        :return: True if read (and changed), False else
        """
        logger.debug(f"Setting from file {basepath}/{self.configfile}")
        if os.path.isfile(os.path.join(basepath, self.configfile)):
            with open(os.path.join(basepath, self.configfile)) as json_file:
                data = json_file.read()
                self.config.update_from_json(data)
                logger.debug(f"Updating config with {data}")
                self.oldconfig = copy.deepcopy(self.config)
                self.set_specific()
                if basepath == SOS_CONF_DIR:
                    self.last_read = int(
                        os.stat(os.path.join(SOS_CONF_DIR, self.configfile)).st_mtime
                    )
                else:
                    self.write_config(backup=backup)
            return True
        logger.warning(
            f"{basepath}/{self.configfile} is not existing. This should not happen"
        )
        return False

    def _refresh_from_file(self):
        if os.path.isfile(os.path.join(SOS_CONF_DIR, self.configfile)):
            if self.last_read < int(
                os.stat(os.path.join(SOS_CONF_DIR, self.configfile)).st_mtime
            ):
                with open(os.path.join(SOS_CONF_DIR, self.configfile)) as json_file:
                    data = json_file.read()
                    self.config.update_from_json(data)
                    self.oldconfig = copy.deepcopy(self.config)
                    self.last_read = int(
                        os.stat(os.path.join(SOS_CONF_DIR, self.configfile)).st_mtime
                    )

    def get_from_file(self, filepath):
        """
        reads a configuration from a file
        :param filepath:
        :return: the configuration, None else
        """
        if os.path.isfile(filepath):
            data = read_from_file(filepath)
            if data is not None:
                config = self._get_child_model()
                return config.from_json(data)
            else:
                return None
        else:
            return None

    def set_from_config(self, new_config, backup=False):
        if new_config is not None:
            new_config: ConfigBaseModel
            logger.debug(f"Set_from_config requested with {new_config.to_json()}")
            self.config.update_from_json(new_config.to_json())
            return self.set(backup)
        else:
            return False

    def set(self, backup=True):
        """
        checks if config has changed and writes the config to SOS_CONF_DIR
        :param backup: if True, will also backup
        :return: True if has changed, False else
        """
        logger.debug(f"Set requested is {self.config.to_json()}")
        if self.oldconfig.to_json() != self.config.to_json():
            result = self.set_specific()
            if result:
                self.write_config(backup=backup)
                self.oldconfig = copy.deepcopy(self.config)
            return result
        else:
            return False

    def set_from_json(self, update_json, backup=True):
        """
        sets a config from json
        :param update_json: (possible partial) json string
        :param backup: if True, will backup change
        :return: True, if config has changed
        """
        self.config.update_from_json(update_json)
        return self.set(backup)

    def check_specific(self):
        """
        checks, if perhaps the specific configuration has been changed from outside. Altough this should not happen,
        it is checked every 60 seconds (asynchronous with the next get after +60s).
        :return:
        """
        act_time = int(time.time())
        if (self.last_get_specific + SPECIFIC_GET_TIME) < act_time:
            self.last_get_specific = act_time
            specific_config = self.get_specific()
            if specific_config is None:
                return  # Nothing to be handled
            if specific_config.to_json() != self.config.to_json():
                logger.warning(
                    f"Specific service {self.config.configname} has different "
                    f"configuration set externally: {specific_config.to_json()}. Will write back"
                )
                self.config = copy.deepcopy(specific_config)
                self.oldconfig = copy.deepcopy(self.config)
                self.write_config(backup=False)  # at this point, we should not backup

    def get(self):
        """
        returns a the actual configuration
        :return: configmodel
        """
        self._refresh_from_file()
        self.check_specific()
        return self.config

    def factory_reset(self):
        """resets the configuration to factory defaults.
        This can be either by removing any configuration or by resetting the configuration file
        from the static part
        """
        if os.path.exists(os.path.join(FACTORY_CONF_DIR, self.configfile)):
            self.set_from_file(
                os.path.join(FACTORY_CONF_DIR, self.configfile), backup=False
            )
        else:
            self.config = self._get_child_model()
            self.write_config(backup=False)
        self.factory_reset_specific()
