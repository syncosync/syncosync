"""
Hardware supports
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import re
import subprocess

from soscore import soshelpers_file
from sosmodel import sos_enums
from sosmodel.sosconfig import SosConfigModel
from sosmodel.syncstat import SyncStatModel
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def bpi_m1_setled(num: int, state: bool):
    """sets GPIO Leds for Bananapi M1

    Args:
        num (int): GPIOnum (0-2)
        state (bool): true = on

        Pinout to GPIO group:

        3V3   1    2 5V
        PB21  3    4 5V
        PB20  5    6 GND
        PI03  7    8 PH00
        GND   9   10 PH01
        PI19 11   12 PH02
        PI18 13   14 GND
        PI17 15   16 PH20
        3V3  17   18 PH21
        PI12 19   20 GND
        PI13 21   22 PI16
        PI11 23   24 PI10
        GND  25   26 PI14

        And:

        https://linux-sunxi.org/A20/PIO#PA00_.280.29

        0 = Pin 11 = PI19 = 275
        1 = Pin 12 = PH02 = 226
        2 = Pin 13 = PI18 = 274

    """

    logger.debug(f"Setting LED status on BananaPi M1: Led {num} State: {state}")

    sysfs_led = [275, 226, 274]

    state_num: int = 0

    if state:
        state_num = 1

    if num >= 0 and num < len(sysfs_led):
        subprocess.run(
            [f"echo {sysfs_led[num]} > /sys/class/gpio/export"],
            shell=True,
            check=False,
            capture_output=True,
        )
        subprocess.run(
            [f"echo out > /sys/class/gpio/gpio{sysfs_led[num]}/direction"],
            shell=True,
            check=False,
            capture_output=True,
        )
        subprocess.run(
            [f"echo {state_num} > /sys/class/gpio/gpio{sysfs_led[num]}/value"],
            shell=True,
            check=False,
            capture_output=True,
        )


def rpi4_setled(num: int, action: str):
    """sets onboard led for rpi4

    Args:
        num (int): 0 = green led, 1 = red led
        action (str): "default-on", "none", "heartbeat"
    """
    subprocess.run(
        [f"echo {action} > /sys/class/leds/led{str(num)}/trigger"],
        shell=True,
        check=False,
        capture_output=True,
    )


def set_hw_outputs(sosconfig: SosConfigModel, reason: SyncStatModel):
    if sosconfig.systype == sos_enums.SysType.RPI4:
        logger.debug("Setting LED status on RPI4")
        # set the leds, led1 is red, led0 is green
        if reason.actmode == sos_enums.SystemMode.SHUTDOWN:
            rpi4_setled(1, "default-on")
            rpi4_setled(0, "none")
        elif (
            reason.actmode == sos_enums.SystemMode.DEFAULT
            or reason.actmode == sos_enums.SystemMode.UNKNOWN
        ):
            if (
                reason.syncdown == sos_enums.SyncStatus.ACTIVE
                or reason.syncup == sos_enums.SyncStatus.ACTIVE
            ):
                rpi4_setled(0, "heartbeat")
            else:
                rpi4_setled(0, "default-on")
            if reason.remote_up != sos_enums.RemoteUpStatus.UP:
                rpi4_setled(1, "heartbeat")
            else:
                rpi4_setled(1, "none")
        elif reason.actmode == sos_enums.SystemMode.DEFAULT_NO_SYNC:
            rpi4_setled(0, "default-on")
            rpi4_setled(1, "default-on")
        else:
            rpi4_setled(0, "heartbeat")
            rpi4_setled(1, "heartbeat")

    if sosconfig.systype == sos_enums.SysType.BPI:
        if (
            reason.actmode == sos_enums.SystemMode.DEFAULT
            or reason.actmode == sos_enums.SystemMode.UNKNOWN
        ):
            if reason.syncdown == sos_enums.SyncStatus.ACTIVE:
                bpi_m1_setled(sos_enums.BpiLed.DOWN, True)
            else:
                bpi_m1_setled(sos_enums.BpiLed.DOWN, False)

            if reason.syncup == sos_enums.SyncStatus.ACTIVE:
                bpi_m1_setled(sos_enums.BpiLed.UP, True)
            else:
                bpi_m1_setled(sos_enums.BpiLed.UP, False)

            if reason.remote_up != sos_enums.RemoteUpStatus.UP:
                bpi_m1_setled(sos_enums.BpiLed.ERROR, True)
            else:
                bpi_m1_setled(sos_enums.BpiLed.ERROR, False)
        else:
            bpi_m1_setled(sos_enums.BpiLed.ERROR, True)
            bpi_m1_setled(sos_enums.BpiLed.UP, False)
            bpi_m1_setled(sos_enums.BpiLed.DOWN, False)


def get_systype():
    # we are guessing the type with most sophisticated AI
    # first check in cpuinfo for bpi and rpi
    model = ""
    # cpuinfo_content = soshelpers_file.read_from_file("/proc/cpuinfo")
    # if cpuinfo_content is not None:
    #     m = re.search(r"Hardware\s*: (.*)\s*", cpuinfo_content)
    #     if m:
    #         hardware = m.group(1)
    #         if hardware == "BCM2711" or hardware == "BCM2835":
    #             return sos_enums.SysType.RPI4
    #         if "sun7i" in hardware:
    #             return sos_enums.SysType.BPI
    model = soshelpers_file.read_from_file("/proc/device-tree/model")
    if model is not None:
        if "Raspberry Pi 4" in model:
            return sos_enums.SysType.RPI4
        if "Banana Pi" in model:
            return sos_enums.SysType.BPI
    # now check with dmidecode for qemu
    try:
        p = subprocess.run("dmidecode", capture_output=True)
        content = p.stdout.decode()
        m = re.search(r"\s*Manufacturer\s*: (.*)\s*", content)
        if m:
            manufacturer = m.group(1)
            if manufacturer == "QEMU":
                return sos_enums.SysType.QEMU
    except Exception as e:
        logger.warning("dmidecode was not successful:", str(e))
    return sos_enums.SysType.GENERIC
