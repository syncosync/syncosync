#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import base64
import os
import time
from datetime import datetime
from multiprocessing.managers import SyncManager
from threading import Event, Thread
from typing import List, Optional

import soscore.sshdmanager as sshdmanager
from soscore import (
    remotehost_systemsetupdata,
    sosadmin_passwd,
    systemsetupdata,
    timezone,
)
from soscore.authmanager import AuthManager
from soscore.drivemanager import Drives
from soscore.factory_reset import run_rull_factory_reset
from soscore.hostname import HostName
from soscore.mail import Mail, send_test_mail
from soscore.nameserver import NameServer
from soscore.nicconfig import NicConfig
from soscore.remotehost import RemoteHost
from soscore.sosaccounts import SosAccounts
from soscore.sosconfig import SosConfig
from soscore.soserrors import SosError
from soscore.soskey import SosKey
from soscore.soskeyx import SosKeyExchange
from soscore.sosversion import SyncosyncVersion
from soscore.syncstat import SyncStatUpdater
from soscore.sysstate import SysState
from soscore.trafficshape import TrafficShape
from sosmodel import sosconstants
from sosmodel.drivesetupDriveConfiguration import DrivesetupDriveConfiguration
from sosmodel.factory_reset import FactoryResetModel
from sosmodel.hostname import HostNameModel
from sosmodel.mailconfig import MailConfigModel
from sosmodel.nameserver import NameServerModel
from sosmodel.nicconfig import NicConfigModel
from sosmodel.partitioning_status import PartitioningStatus
from sosmodel.remotehost import RemoteHostModel
from sosmodel.sos_enums import SsdAction, Sshd, SystemMode
from sosmodel.sosaccount import SosAccountAddModel, SosAccountModel
from sosmodel.sosadmin_passwd import SosAdminPassWordModel
from sosmodel.sosconfig import SosConfigModel
from sosmodel.soskey import SosKeyModel
from sosmodel.sostocheck import SosToCheck
from sosmodel.sosversion import SyncosyncVersionModel
from sosmodel.sshdmanager import SshdConfig
from sosmodel.syncstat import BandWidthModel, SyncStatModel
from sosmodel.sysstate import SysStateResult, SysStateTransportModel
from sosmodel.systemsetupdata import PartitioningData, SystemSetupDataModel
from sosmodel.timezone import TimeZoneModel
from sosmodel.timezone_list import TimezoneListModel
from sosmodel.trafficshape import TrafficShapeModel, TrafficShapeSwitchModel
from sosmodel.uilanguage import UiLanguage
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


class SoscoreManagerManager(SyncManager):
    pass


class SoscoreManager:
    def __init__(self):
        self.__sosuser_list: SosAccounts = SosAccounts()
        self.__sos_config: SosConfig = SosConfig()
        self.__mail_config: Mail = Mail()
        self.__syncstat: SyncStatUpdater = SyncStatUpdater()
        self.__sysstate: SysState = SysState()
        self.__local_sos_key: SosKey = SosKey()
        self.__remote_host: RemoteHost = RemoteHost()
        self.__traffic_shape: TrafficShape = TrafficShape()
        self._hostname_controller: HostName = HostName()
        self._nameserver_controller: NameServer = NameServer()
        self._nic_controller: NicConfig = NicConfig()
        self._auth_manager: AuthManager = AuthManager()
        self.__timezone_manager = timezone.TimeZone()
        self._sosversion = SyncosyncVersion()
        self.__stop_signal: Event = Event()
        self.__soscore_maintainer: Thread = Thread(
            name="soscore_main",
            target=self.__soscore_main,
        )
        self.__soscore_maintainer.daemon = False
        self.__soscore_maintainer.start()
        self.__drives: Drives = Drives()
        self.__last_rrcache_read = 0
        self.__last_rrcache: Optional[SosToCheck] = None

    def __soscore_main(self) -> None:
        # start traffic shaping
        self.__traffic_shape.start()
        minutes = datetime.now().minute
        reset_mode = self.__sysstate.reset_mode()
        logger.info(f"Started system with mode: {SystemMode.message(reset_mode)}")
        try:
            self.__drives.generate()
            while not self.__stop_signal.is_set():
                # self.__report_ids("Soscore Main")
                time.sleep(sosconstants.SYNCSTAT_REFRESH_TIME)
                if self.__syncstat.update_data():
                    # ok status has changed. maybe mounthealth is bad?
                    if not self.__syncstat.mounthealth:
                        self.__sysstate.changemode(SystemMode.DISK_ERROR)
                new_minutes = datetime.now().minute
                if new_minutes != minutes:
                    self.__traffic_shape.get()
                    self.__traffic_shape.set_at_time()
                    minutes = new_minutes
        except Exception as e:
            logger.warning("Unhandled exception in soscore main!")
            logger.exception(e)
        finally:
            logger.info("soscore stopping")
            # stop traffic shaping
            self.__traffic_shape.stop()

    def shutdown_soscore(self):
        logger.debug("soscore will be shut down")
        self.__sysstate.changemode(SystemMode.SHUTDOWN)
        self.__stop_signal.set()
        self.__soscore_maintainer.join()

    def get_userlist(self) -> List[SosAccountModel]:
        return self.__sosuser_list.get()

    def get_syncstat(self) -> SyncStatModel:
        return self.__syncstat.get()

    def get_bandwidth_history(self) -> List[BandWidthModel]:
        return self.__syncstat.get_bw_history()

    def account_add(self, new_user: SosAccountAddModel):
        # TODO: Catch errors!
        self.__sosuser_list.add(new_user)

    def account_mod(self, edit_user: SosAccountAddModel):
        # TODO: use boolean return or try/catch
        self.__sosuser_list.mod(edit_user)

    def account_delete(self, user_to_delete):
        self.__sosuser_list.delete(user_to_delete)

    def account_erase_data(self, user_to_erase_data):
        self.__sosuser_list.erase_data(user_to_erase_data)

    # actual usage of controllers

    # Language getter/setter for the entire system
    def get_systemlanguage(self) -> UiLanguage:
        return self.__sos_config.get().ui_language

    def set_systemlanguage(self, language: UiLanguage) -> None:
        self.__sos_config.get().ui_language = language
        self.__sos_config.set()

    # Hostname controller
    def get_hostname(self) -> HostNameModel:
        return self._hostname_controller.get()

    def set_hostname(self, hostname: HostNameModel) -> None:
        # TODO: catch error/read result/whatever
        self._hostname_controller.set_from_config(hostname)

    # nameserver controller
    def get_nameservers(self) -> NameServerModel:
        return self._nameserver_controller.get()

    def set_nameservers(self, nameservers: NameServerModel) -> None:
        self._nameserver_controller.set_from_config(nameservers)

    # nic config controller
    def get_nic_configs(self) -> List[NicConfigModel]:
        return self._nic_controller.get()

    def set_nic_configs(self, nics: List[NicConfigModel]) -> None:
        self._nic_controller.set(nics)

    # SosConfig related methods
    def get_mail_config(self) -> MailConfigModel:
        return self.__mail_config.get()

    def set_mail_config(self, mail_config_model: MailConfigModel) -> bool:
        self.__mail_config.config = mail_config_model
        return self.__mail_config.set()

    def mail_send_test(self) -> bool:
        mail_settings: MailConfigModel = self.__mail_config.get()
        return send_test_mail(mail_settings.admin_mail_address)

    # Auth related
    def login(self, password: str) -> Optional[str]:
        return self._auth_manager.login(password)

    def validate_token(self, token: str) -> bool:
        return self._auth_manager.validate_token(token)

    # Sync setup
    def generate_local_key(self) -> SosKeyModel:
        if not self.__local_sos_key.generate():
            raise ValueError("Failed generating sos key")
            # TODO: throw specific exception for the UI
        else:
            return self.__local_sos_key.get()

    def get_local_key(self) -> SosKeyModel:
        return self.__local_sos_key.get()

    def get_remote_host(self) -> RemoteHostModel:
        return self.__remote_host.get()

    def set_remote_host(self, new_remote_host) -> None:
        self.__remote_host.set_from_config(new_remote_host)

    def delete_remote_key(self) -> None:
        SosKeyExchange.remove_remote_key()

    def get_remote_key(self) -> SosKeyModel:
        return SosKeyExchange.get_remote_key()

    def accept_remote_key(self) -> None:
        SosKeyExchange.accept_key()

    def key_exchange_lead(self) -> SystemSetupDataModel:
        return SosKeyExchange.send_ssd(SsdAction.LEADER_SEND_KEY)

    def sync_ssd_lead(self) -> SystemSetupDataModel:
        return SosKeyExchange.send_ssd(SsdAction.LEADER_REQUEST_DATA)

    def partitioning_lead(
        self, partition_data: PartitioningData
    ) -> SystemSetupDataModel:
        common_ssd: SystemSetupDataModel = systemsetupdata.get_common_ssd()
        common_ssd.partition_data = partition_data
        if common_ssd.partition_data.local + common_ssd.partition_data.remote > 0:
            return SosKeyExchange.send_ssd(SsdAction.LEADER_RESIZE_PARTITIONS)
        else:
            ssd_result = SosKeyExchange.send_ssd(SsdAction.LEADER_INIT_PARTITIONS)
            return ssd_result

    def get_partitioning_status(self) -> PartitioningStatus:
        partitioning_status: PartitioningStatus = PartitioningStatus()
        partitioning_status.overall_status = systemsetupdata.get_partioning_status()
        return partitioning_status

    def auto_resize_partitions(self) -> None:
        self.__drives.resize_volumes(self.get_reverse_remote_partitioning())

    def key_exchange_follow(self) -> Optional[SystemSetupDataModel]:
        # SosKeyExchange.receive_cancel_signal.clear()
        SosKeyExchange.receive_cancel_signal = False
        return SosKeyExchange.receive_ssd()

    def key_exchange_cancel(self) -> None:
        logger.debug("key exchange canceled from soscoreManager")
        # SosKeyExchange.receive_cancel_signal.set()
        SosKeyExchange.receive_cancel_signal = True

    def set_admin_password(self, new_password: SosAdminPassWordModel) -> bool:
        return sosadmin_passwd.set(new_password)

    def get_drives(self) -> Drives:
        drives: Drives = Drives()
        drives.generate()
        return drives

    @staticmethod
    def __report_ids(msg):
        logger.debug(f"uid, gid = {os.getuid()},{os.getgid()}: {msg}")

    @staticmethod
    def get_foo():
        return "foo"

    def get_system_state(self) -> SysStateTransportModel:
        current_state: SysStateTransportModel = SysStateTransportModel()
        current_state.status = self.__sysstate.get()
        return current_state

    def set_system_state(self, new_state: SysStateTransportModel) -> SysStateResult:
        path_to_config_backup_file: Optional[str] = None
        if not new_state or not new_state.state_desired:
            raise SosError("Invalid new state passed")
        elif (
            new_state.config_backup_encoded
            and new_state.config_backup_name
            and new_state.config_backup_password
        ):
            path_to_config_backup_file = "/tmp/" + new_state.config_backup_name
            # Now decode base64 received and store to file
            decoded = base64.decodebytes(
                new_state.config_backup_encoded.encode("ascii")
            )
            with open(path_to_config_backup_file, "wb") as temp_file_to_write_to:
                temp_file_to_write_to.write(decoded)

        result: SysStateResult = self.__sysstate.changemode(
            new_state.state_desired,
            path_to_config_backup_file,
            new_state.config_backup_password,
            new_state.switch_action,
        )
        logger.info(
            f"System mode switched to '{SystemMode.message(new_state.state_desired)}' with result '{SystemMode.message(result.mode)}' "
            f"and report: {result.report}"
        )
        return result

    def get_sos_config(self) -> SosConfigModel:
        return self.__sos_config.get()

    def shutdown_system(self) -> None:
        os.system("shutdown -h now")

    def reboot_system(self):
        os.system("shutdown -r now")

    def get_remote_sync_ssh_config(self) -> SshdConfig:
        return sshdmanager.get(Sshd.EXTRANET)

    def set_remote_sync_ssh_config(self, new_config: SshdConfig) -> None:
        sshdmanager.set(Sshd.EXTRANET, new_config)

    def format_drives(self, drive_config: DrivesetupDriveConfiguration) -> int:
        try:
            result = self.__drives.format_drives(drive_config)
            if result != -1:
                # self.__sysstate.update_sysstate()
                mystate = self.__sysstate.get()
                logger.debug(f"sysmode: {SystemMode.message(mystate.actmode)}")
            return result
        except Exception as e:
            logger.error(f"Unable to format drive: {str(e)}")
            raise e

    def get_partitioning(self) -> PartitioningData:
        common_ssd: SystemSetupDataModel = systemsetupdata.get_common_ssd()
        return common_ssd.partition_data

    def get_reverse_remote_partitioning(self) -> PartitioningData:
        remote_ssd: SystemSetupDataModel = (
            remotehost_systemsetupdata.RemoteHostSystemSetupData().get()
        )
        reverse_partitioning = PartitioningData()
        reverse_partitioning.local = remote_ssd.partition_data.remote
        reverse_partitioning.remote = remote_ssd.partition_data.local
        return reverse_partitioning

    def partition_drives(self, partition_data: PartitioningData) -> bool:
        partitioning_result = self.__drives.create_volumes(partition_data)
        self.__sysstate.changemode(SystemMode.DEFAULT)
        return partitioning_result

    def is_local_system_leader(self) -> bool:
        remote_ssd: SystemSetupDataModel = (
            remotehost_systemsetupdata.RemoteHostSystemSetupData().get()
        )
        return not SsdAction.is_leader(remote_ssd.action)

    def get_timezone(self) -> TimeZoneModel:
        return self.__timezone_manager.get()

    def set_timezone(self, new_timezone: TimeZoneModel) -> bool:
        return self.__timezone_manager.set_from_config(new_timezone)

    def get_available_timezones(self) -> TimezoneListModel:
        available_timezones = TimezoneListModel()
        available_timezones.available_timezones = (
            self.__timezone_manager.get_zone_list()
        )
        return available_timezones

    def set_trafficshape(self, new_trafficshape: TrafficShapeModel) -> bool:
        return self.__traffic_shape.set_from_config(new_trafficshape)

    def get_trafficshape(self) -> TrafficShapeModel:
        return self.__traffic_shape.get()

    def get_trafficshape_switch(self) -> TrafficShapeSwitchModel:
        return self.__traffic_shape.get_mode()

    def set_trafficshape_switch(
        self, new_trafficshape_switch: TrafficShapeSwitchModel
    ) -> None:
        self.__traffic_shape.force_mode(new_trafficshape_switch)

    def get_sosversion(self) -> SyncosyncVersionModel:
        return self._sosversion.get()

    def get_rrtocheck(self) -> Optional[SosToCheck]:
        if os.path.isfile(sosconstants.RRTOCHECK_CACHE):
            if (
                int(os.stat(sosconstants.RRTOCHECK_CACHE).st_mtime)
                > self.__last_rrcache_read
            ):
                self.__last_rrcache_read = int(
                    os.stat(sosconstants.RRTOCHECK_CACHE).st_mtime
                )
                with open(sosconstants.RRTOCHECK_CACHE) as file:
                    sostocheck = SosToCheck.from_json(file.read())
                    self.__last_rrcache = sostocheck
        else:
            self.__last_rrcache = None
        return self.__last_rrcache

    def get_rr_result(self) -> SysStateResult:
        return self.__sysstate.get_rr_result()

    def get_latest_result(self) -> SysStateResult:
        return self.__sysstate.get_latest_result()

    def is_password_valid(self, password: SosAdminPassWordModel) -> bool:
        return self._auth_manager.is_valid_password(password.password)

    def factory_reset(self, reset_config: FactoryResetModel):
        # TODO: Consider something other than remote recovery?
        # TODO: Update all instances used
        # TODO: Use reset_config
        run_rull_factory_reset(reset_config)
