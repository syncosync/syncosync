"""
management of sshd settings and the deamons themselves
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os
import re
import shutil
import subprocess
import time
import uuid

import psutil
from soscore import sosaccounts, soshelpers_file
from sosmodel import sos_enums, sosconstants, sshdmanager
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def shutdown(p: psutil.Process):
    try:
        p.terminate()
    except Exception as e1:
        logger.error(
            f"Could not graceful terminate sshd with pid {p.pid}, try to kill: {e1}"
        )
        try:
            p.kill()
        except Exception as e2:
            logger.error(
                f"Could not even kill sshd on pid {p.pid}, go help yourself: {e2}"
            )
            return False


def get_port_from_cfg(mode: sos_enums.Sshd) -> int:
    """
    returns the port which is set in the configuration
    :param mode: intranet or extranet
    :return: port (-1 if some error)
    """
    if not os.path.isfile(sosconstants.SSHD_CFG[mode, sos_enums.SshdAccess.OPEN]):
        factory_reset()
    input_file = open(sosconstants.SSHD_CFG[mode, sos_enums.SshdAccess.OPEN], "r")
    file_content = input_file.read()
    m = re.search(r"Port ([0-9]*).*", file_content)
    if m:
        pid = int(m.group(1))
    else:
        pid = -1
    return pid


def set_port_in_cfg_path(filepath: str, new_port):
    """
    sets the port in a sshd config file
    :param filepath:
    :return:
    """
    input_file = open(filepath, "r")
    file_content = input_file.read()
    input_file.close()
    m = re.compile("(Port )[0-9]*(.*)")
    res = m.sub(r"\g<1>" + str(new_port) + r"\g<2>", file_content)
    file_content = res
    f = open(filepath, "w")
    f.write(file_content)
    f.close()
    return


def get(mode: sos_enums.Sshd) -> sshdmanager.SshdConfig:
    """
    The usual to get a SsshdConfig Model
    :param mode:
    :return:
    """
    mysshdconfig = sshdmanager.SshdConfig()
    mysshdconfig.port = get_port_from_cfg(mode)
    mysshdconfig.pid, mysshdconfig.access = status(mode)
    return mysshdconfig


def set(mode: sos_enums.Sshd, new_config: sshdmanager.SshdConfig):
    """
    to set a new config (only port)
    :param mode:
    :param new_config:
    :return:
    """
    old_port = get_port_from_cfg(mode)
    if old_port != new_config.port:
        # ok, port has changed
        set_port_in_cfg_path(
            sosconstants.SSHD_CFG[mode, sos_enums.SshdAccess.OPEN], new_config.port
        )
        set_port_in_cfg_path(
            sosconstants.SSHD_CFG[mode, sos_enums.SshdAccess.CLOSE], new_config.port
        )
        soshelpers_file.set_configbackup_flag()
        restart_sshd(mode)
    return


def stop_sshd(
    status_pid: int, mode: sos_enums.Sshd, status_access: sos_enums.SshdAccess
):
    """stop a sshd and - if necessary - it's childs
       information about the sshd could be get via status before
    Args:
        status_pid (int): pid of the sshd
        mode (sos_enums.Sshd): INTRANET or EXTRANET
        status_access (sos_enums.SshdAccess): access tipe (OPEN, CLOSE ...)
    """
    # ok, this means, we have to stop and start the sshd to activate it
    p = psutil.Process(status_pid)
    # store in between the children
    children = p.children(recursive=True)
    shutdown(p)
    # and wait some graceful time
    time.sleep(1)
    logger.debug(f"sshd with mode {mode} on pid {status_pid} stopped")
    # ok kill the clients depending on the mode
    if status_access == sos_enums.SshdAccess.OPEN and mode == sos_enums.Sshd.EXTRANET:
        # on the extranet we could simply kill all childs
        for child in children:
            logger.debug(f"shutdown sshd child {child.pid}")
            shutdown(child)
    # on the intranet we just kill all processes owned by sosaccount users
    if status_access == sos_enums.SshdAccess.OPEN and mode == sos_enums.Sshd.EXTRANET:
        accounts = sosaccounts.getsosaccount_names()
        for name in accounts:
            sosaccounts.kill_account_processes(name)
            os.system(f"killall -u {name}")
            os.system(f"killall -9 -u {name}")


def change_access(mode: sos_enums.Sshd, access) -> int:
    """
    starts the sshd with mode and access
    :return: pid of sshd, -1 if access == OFF
    """
    # first have a look, what is already there
    status_pid, status_access = status(mode)
    if status_pid != -1:
        if status_access != access:
            stop_sshd(status_pid, mode, status_access)
        else:
            # there is no reason to start or restart, this is already the running configuration
            return status_pid
    else:
        if mode == sos_enums.Sshd.INTRANET and access != sos_enums.SshdAccess.OFF:
            # TODO: this is far from clean, but this could give us best access to the machine
            subprocess.call(["service", "ssh", "stop"])
            if not os.path.isdir("/var/run/sshd"):
                os.makedirs("/var/run/sshd")
            logger.debug("stopped systemd ssh service")
            # and wait some graceful time
            time.sleep(1)
    return start_sshd(mode, access)


def start_sshd(mode: sos_enums.Sshd, access: sos_enums.SshdAccess) -> int:
    """starts the sshd of given mode and access

    Args:
        mode (sos_enums.Sshd): INTRANET, EXTRANET
        access (sos_enums.SshdAccess): OFF, OPEN, CLOSE

    Returns:
        int: pid of sshd process
    """
    if access != sos_enums.SshdAccess.OFF:
        if not os.path.isfile(sosconstants.SSHD_CFG[mode, sos_enums.SshdAccess.OPEN]):
            # this should happen only once, we copy the cfg file at the right position
            logger.warning(
                f"{sosconstants.SSHD_CFG[mode, sos_enums.SshdAccess.OPEN]} not found, will copy it from defaults."
            )
            factory_reset()
        if mode == sos_enums.Sshd.EXTRANET:
            data = str(uuid.uuid4())
            soshelpers_file.write_to_file(
                sosconstants.SSH_EXTERNAL_UUID, "echo " + data
            )
            logger.debug(f"Generated new external uuid: {data}")
        try:
            proc = subprocess.Popen(
                ["/usr/sbin/sshd", "-f", sosconstants.SSHD_CFG[mode, access]]
            )
        except Exception as e:
            logger.error(
                f"Could not start sshd with cfg {sosconstants.SSHD_CFG[mode, access]}: {str(e)}"
            )
            return -1
        pid = proc.pid
        logger.debug(f"Started sshd with cfg {sosconstants.SSHD_CFG[mode, access]}")
        # should we wait for graceful start?
        startloop = time.time()
        while True:
            pid, access_result = status(mode)
            if pid != -1:
                return pid
            if time.time() - startloop > 5:
                logger.error(f"Timeout while waiting for sshd process start_{mode}")
                return -1
            time.sleep(0.5)
    else:
        # if this is intranet, and access is off, start system sshd
        if mode == sos_enums.Sshd.INTRANET:
            # TODO: this is far from clean, but this could give us best access to the machine
            subprocess.call(["service", "ssh", "start"])
            logger.debug("started systemd ssh service")
        return -1


def restart_sshd(mode):
    """restarts the sshd with a new configuration (port) or does nothing, if not started

    Args:
        mode ([type]): [description]
    """
    status_pid, status_access = status(mode)
    if status_pid != -1:
        stop_sshd(status_pid, mode, status_access)
        start_sshd(mode, status_access)


def status(mode):
    """
    find the status of the sshd process and it's arguments
    :return: process id, -1 if not found, access mode
    """
    for p in psutil.process_iter():
        name = ""
        cfgpath = ""
        try:
            name = p.name()
            cmdline = p.cmdline()
        except (psutil.AccessDenied, psutil.ZombieProcess):
            continue
        except psutil.NoSuchProcess:
            continue
        # logger.debug(f"name: {name} p.cmdline:{p.cmdline()}")
        if len(cmdline) > 0 and name == "sshd":
            if (
                psutil.__version__ >= "5.8.0"
            ):  # at least since then, there is one more field in the cmdline
                if cmdline[1] == "/usr/sbin/sshd":
                    if len(cmdline) >= 4:
                        cfgpath = cmdline[3]
            else:
                if cmdline[0] == "/usr/sbin/sshd":
                    if len(cmdline) >= 3:
                        cfgpath = cmdline[2]

            if cfgpath == sosconstants.SSHD_CFG[mode, sos_enums.SshdAccess.OPEN]:
                access = sos_enums.SshdAccess.OPEN
                return p.pid, access
            if cfgpath == sosconstants.SSHD_CFG[mode, sos_enums.SshdAccess.CLOSE]:
                access = sos_enums.SshdAccess.CLOSE
                return p.pid, access
    # if we not have found a suiting process, return -1
    return -1, sos_enums.SshdAccess.OFF


def factory_reset():
    """copy all initial files from shared config to sos conf dir"""
    for sshd_cfg_file in sosconstants.SSHD_CFG.values():
        shutil.copy(
            os.path.join(
                sosconstants.FACTORY_CONF_DIR, os.path.basename(sshd_cfg_file)
            ),
            sosconstants.SOS_CONF_DIR,
        )
