"""
factory reset the whole system
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from soscore import (
    drivemanager,
    hostname,
    mail,
    nameserver,
    nicconfig,
    remotehost,
    sosaccounts,
    sosconfig,
    sshdmanager,
    sysstate,
    timezone,
    trafficshape,
)
from soscore.soshelpers_file import del_file_if_exits
from sosmodel import sosconstants
from sosmodel.factory_reset import FactoryResetModel
from sosmodel.sos_enums import EraseDiskLevel, SystemMode
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def run_rull_factory_reset(reset_config: FactoryResetModel):
    if reset_config.erase_disk == EraseDiskLevel.QUICK_ERASE:
        factory_reset_disks()
    factory_reset(reset_config.reset_networking)
    os.system("reboot")


def factory_reset_disks():
    """initialise all drives. All data gets lost!"""
    mystate = sysstate.SysState()
    mystate.changemode(SystemMode.SETUP_DISKS)
    mydrives = drivemanager.Drives()
    mydrives.generate()
    drives = mydrives.get_drivelist()
    s = ", "
    driveliststring = s.join(drives)
    mydrives.reset_drives(driveliststring)


def factory_reset(network: bool) -> bool:
    # check sysstate first

    sosaccounts.delete_all_accounts()
    hostname.HostName().factory_reset()
    nameserver.NameServer().factory_reset()
    remotehost.RemoteHost().factory_reset()
    sosconfig.SosConfig().factory_reset()
    sshdmanager.factory_reset()
    mail.Mail().factory_reset()
    timezone.TimeZone().factory_reset()
    trafficshape.TrafficShape().factory_reset()
    # delete some files
    del_file_if_exits(
        os.path.join(sosconstants.SOS_CONF_DIR, sosconstants.SOS_CONF_REMOTEHOST_SSD)
    )
    del_file_if_exits(sosconstants.SOS_KEYFILE)
    del_file_if_exits(sosconstants.SOS_KEYFILE_PUB)
    del_file_if_exits(sosconstants.ACTSTATE_FILE)
    del_file_if_exits(sosconstants.SOS_CONF_STOREDMODE)
    del_file_if_exits(sosconstants.REMOTE_KEYFILE_PUB)
    if network:
        nicconfig.factory_reset()
    return True
