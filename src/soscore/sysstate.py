"""
checking which state and mode the system is in
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os
import time
from typing import Optional
import inspect

import soscore.sosaccounts
import soscore.soshelpers_file
import soscore.soshelpers_i18n
from soscore import (
    configbackup,
    drivemanager,
    remotehost,
    sosconfig,
    soserrors,
    soshelpers_file,
    soshelpers_keys,
    soskey,
    sshdmanager,
)
from soscore.soshelpers_file import read_from_file, write_to_file
from sosmodel import sosconstants
from sosmodel.sos_enums import (
    Partition,
    Sshd,
    SshdAccess,
    SwitchAction,
    SystemMode,
    SystemStatus,
    VolMount,
)
from sosmodel.sosconfig import SosConfigModel
from sosmodel.sysstate import SysModeModel, SysStateModel, SysStateResult
from sosmodel.systemsetupdata import PartitioningData
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def read_mode_from_file(filepath=sosconstants.SOS_CONF_STOREDMODE) -> SysModeModel:
    if os.path.isfile(filepath):
        with open(filepath) as json_file:
            my_mode = SysModeModel.from_json(json_file.read())
            return my_mode
    else:
        # this should be more robust to try to switch to DEFAULT if there is no stored mode
        my_mode = SysModeModel(SystemMode.DEFAULT)
        return my_mode


def read_result_file(filepath) -> SysStateResult:
    data = read_from_file(filepath)
    if data is not None:
        result = SysStateResult.from_json(data)
        return result
    else:
        result = SysStateResult()
        return result


def write_result_file(filepath, result: SysStateResult):
    result.date = int(time.time())
    write_to_file(filepath, result.to_json())


def result_file_append(filepath, mode, line: str):
    """
    extends or creates a result file
    :param filepath: name of result file
    :param mode: new mode
    :param line: string with additional report line
    """
    result = read_result_file(filepath)
    if result is not None:
        result.append(line)
        result.mode = mode
    else:
        result = SysStateResult(mode, line)
    write_result_file(filepath, result)


class SysState:
    def __init__(self):
        if os.path.isfile(sosconstants.ACTSTATE_FILE):
            with open(sosconstants.ACTSTATE_FILE) as json_file:
                self.sysstate = SysStateModel.from_json(json_file.read())
        else:
            self.sysstate = SysStateModel(
                SystemStatus.NOTHING, SystemMode.UNKNOWN, SystemMode.UNKNOWN
            )

        my_storedmode = read_mode_from_file(sosconstants.SOS_CONF_STOREDMODE)
        self.sysstate.storedmode = my_storedmode.mode
        self.mydrives = drivemanager.Drives()
        self.mycfg_backup = configbackup
        self.myconfigbackup = None
        self.mysoskey = soskey.SosKey()
        self.mysoskeykey = None
        self.myremotehost = remotehost.RemoteHost()
        self.myremotehostoutput = None
        self.myintrasshdstatus = None
        self.myextrasshdstatus = None
        self.last_update = 0
        self.update_sysstate()
        self.__last_rr_result = None
        self.__last_rr_result_read = 0

    def update_sysstate(self):
        """
        updates sysstate by checking all possible stuff
        :return: True if state has changed, False else
        """
        newstate = SysStateModel()
        newstate.sysstate = SystemStatus.NOTHING
        # we do not check the modes here
        newstate.storedmode = self.sysstate.storedmode
        newstate.actmode = self.sysstate.actmode
        self.mydrives.generate()
        self.myconfigbackup = self.mycfg_backup.get()
        self.mysoskeykey = self.mysoskey.get()
        self.myremotehostoutput = self.myremotehost.get()
        self.myintrasshdstatus = sshdmanager.get(Sshd.INTRANET)
        self.myextrasshdstatus = sshdmanager.get(Sshd.EXTRANET)
        new_sosconfig = sosconfig.SosConfig().get()
        # now analyze the situation
        if (
            self.mydrives.sos_vol_mount == VolMount.NONE
            or self.mydrives.sos_vol_mount == VolMount.INCOMPLETE
        ):
            newstate.sysstate = newstate.sysstate + SystemStatus.SOS_VOLUMES_NOT_MOUNTED
        if new_sosconfig.vg_sos_uuid != self.mydrives.vgs.vg_uuid:
            newstate.sysstate = newstate.sysstate + SystemStatus.VG_UUID_NOT_MATCH
        if self.mydrives.errorneous_drive:
            newstate.sysstate = newstate.sysstate + SystemStatus.DISK_ERROR
        if (
            self.mydrives.non_sos_drive
            or self.mydrives.vgs.bad
            or self.mydrives.vgs.multiple_sos
            or not self.mydrives.vgs.sos_found
            or (
                self.sysstate.storedmode != SystemMode.DISASTER_SWAP
                and new_sosconfig.vg_sos_uuid != self.mydrives.vgs.vg_uuid
            )
        ):
            newstate.sysstate = newstate.sysstate + SystemStatus.DISK_SETUP_NEED
        else:
            if (
                f"sos-{self.mydrives.vgs.vg_uuid}/local" not in self.mydrives.lvs.lv
                or f"sos-{self.mydrives.vgs.vg_uuid}/remote" not in self.mydrives.lvs.lv
            ):
                newstate.sysstate = newstate.sysstate + SystemStatus.PARTITIONING_NEED
        if self.myconfigbackup.md5sum == "":
            newstate.sysstate = (
                newstate.sysstate + SystemStatus.SOS_CONF_BACKUP_NOT_DONE
            )
        if self.mydrives.vgs.cfgmd5sum == "":
            newstate.sysstate = newstate.sysstate + SystemStatus.NO_CONF_BACKUP_ON_VG
        if (
            self.myconfigbackup.md5sum != ""
            and self.mydrives.vgs.cfgmd5sum != ""
            and (self.myconfigbackup.md5sum != self.mydrives.vgs.cfgmd5sum)
        ):
            logger.debug(
                f"config md5sum: {self.myconfigbackup.md5sum} vs. {self.mydrives.vgs.cfgmd5sum} on drive"
            )
            newstate.sysstate = (
                newstate.sysstate + SystemStatus.SOS_CONF_BACKUP_NOT_MATCH
            )
        if self.mysoskeykey.fingerprint is None or self.mysoskeykey.fingerprint == "":
            newstate.sysstate = newstate.sysstate + SystemStatus.SOS_KEY_NOT_GENERATED
        if self.myremotehostoutput.hostname == "":
            newstate.sysstate = (
                newstate.sysstate + SystemStatus.REMOTE_HOST_NOT_CONFIGURED
            )
        remote_key = soshelpers_keys.get_remote_key()
        if remote_key.fingerprint == "":
            newstate.sysstate = newstate.sysstate + SystemStatus.REMOTE_KEY_NOT_SET
        if self.myintrasshdstatus.pid == -1:
            newstate.sysstate = newstate.sysstate + SystemStatus.INTRANET_SSHD_DOWN
        else:
            if self.myintrasshdstatus.access == SshdAccess.CLOSE:
                newstate.sysstate = newstate.sysstate + SystemStatus.INTRANET_SSHD_CLOSE
        if self.myextrasshdstatus.pid == -1:
            newstate.sysstate = newstate.sysstate + SystemStatus.EXTRANET_SSHD_DOWN
        else:
            if self.myextrasshdstatus.access == SshdAccess.CLOSE:
                newstate.sysstate = newstate.sysstate + SystemStatus.EXTRANET_SSHD_CLOSE
        if newstate != self.sysstate:
            self.sysstate = newstate
            soscore.soshelpers_file.write_to_file(
                sosconstants.ACTSTATE_FILE, self.sysstate.to_json()
            )
            self.last_update = int(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
            return True
        else:
            return False

    def get(self):
        if not os.path.isfile(sosconstants.ACTSTATE_FILE):
            self.update_sysstate()
        elif int(os.stat(sosconstants.ACTSTATE_FILE).st_mtime) > self.last_update:
            with open(sosconstants.ACTSTATE_FILE) as json_file:
                try:
                    content = json_file.read()
                    self.sysstate.update_from_json(content)
                except Exception as e:
                    logger.error(f"{str(e)}")
                    pass
            self.last_update = int(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
        return self.sysstate

    def get_latest_result(self) -> SysStateResult:
        """
        returns the latest result (actmode and report)
        :return: SysStateResult
        """
        if self.sysstate.actmode == SystemMode.REMOTE_RECOVERY:
            result = read_result_file(sosconstants.RR_RESULT)
        else:
            result = read_result_file(sosconstants.LAST_RESULT)
        return result

    def get_rr_result(self) -> SysStateResult:
        if os.path.isfile(sosconstants.RR_RESULT):
            if (
                int(os.stat(sosconstants.RR_RESULT).st_mtime)
                > self.__last_rr_result_read
            ):
                self.__last_rr_result_read = int(
                    os.stat(sosconstants.RR_RESULT).st_mtime
                )
                with open(sosconstants.RR_RESULT) as file:
                    result = SysStateResult.from_json(file.read())
                    self.__last_rr_result = result
        else:
            logger.warning("Request for last_rr_result with no file found")
            self.__last_rr_result = None
        return self.__last_rr_result

    def setactmode(self, newmode):
        logger.debug(f"setactmode is called with {SystemMode.message(newmode)}")
        if newmode != self.sysstate.actmode:
            self.sysstate.actmode = newmode
            soscore.soshelpers_file.write_to_file(
                sosconstants.ACTSTATE_FILE, self.sysstate.to_json()
            )
            self.last_update = int(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)

    def reset_mode(self) -> SystemMode:
        """starts the stored mode (or default, if there was no stored mode)
        :return: The mode, which has been started (as an SysModeModel object)
        """
        my_storedmode = read_mode_from_file(sosconstants.SOS_CONF_STOREDMODE)
        trymode = my_storedmode.mode
        logger.info(f"Try to reset mode to {SystemMode.message(trymode)}")
        result = self.changemode(mode=trymode)
        if result.mode != trymode:
            logger.warning(
                f"Setting to mode '{SystemMode.message(trymode)}' failed: {result.report}. Set to '{SystemMode.message(result.mode)}' instead."
            )
        return result.mode

    def changemode(  # noqa: C901
        self,
        mode: SystemMode,
        cfg_backup_filepath: Optional[str] = "",
        config_pwd: Optional[str] = "",
        switch_action: Optional[SwitchAction] = SwitchAction.DEFAULT,
    ) -> SysStateResult:
        """
        This is basically a FSM. If a new mode is applicable to set, this will happen
        :param mode: New Mode
        :param cfg_backup_filepath: path to configuration backup (will be needed for some modes)
        :param config_pwd: configuration backup password (will be needed for some modes)
        :param switch_action: decides what to do when switching (only necessary for making disaster swap persistent)
        :return: Mode, which really as been set, result as an array of lines
        """
        self.update_sysstate()
        this_mode = self.sysstate.actmode
        # cfgrestoredir = ""  # this is, where we the unpacked cfg backup goes to.
        result = SysStateResult(mode=this_mode, report=[])
        logger.debug(
            f"Mode '{SystemMode.message(mode)}' has been requested, "
            f"actual mode is: '{SystemMode.message(this_mode)}'"
            f" switch_action is:'{switch_action}' caller is {inspect.stack()[1][3]}"
        )
        # read result from local drive and append the rest to that if coming from REMOTE_RECOVERY
        if this_mode == SystemMode.REMOTE_RECOVERY:
            result = read_result_file(sosconstants.RR_RESULT)

        # First handle possible transitions
        """
        disk setup need AND mode not in (setup disks, default) -> true
        or
        vg uuid match AND mode default AND cfg filepath -> true
        """
        if self.sysstate.sysstate & SystemStatus.DISK_ERROR:
            mode = SystemMode.DISK_ERROR
        if self.sysstate.sysstate & SystemStatus.DISK_SETUP_NEED and (
            (mode not in (SystemMode.SETUP_DISKS, SystemMode.DEFAULT, SystemMode.DISK_ERROR))
            or (
                mode == SystemMode.DEFAULT
                and (
                    not self.sysstate.sysstate & SystemStatus.VG_UUID_NOT_MATCH
                    or not cfg_backup_filepath
                    or not config_pwd
                )
            )
        ):
            logger.debug(
                f"Can not switch to '{SystemMode.message(mode)}' as disk setup is needed"
            )
            result.append(
                f"Can not switch to '{SystemMode.message(mode)}' as disk setup is needed"
            )
            mode = SystemMode.SETUP_DISKS
        if (
            self.sysstate.sysstate & SystemStatus.PARTITIONING_NEED
            and mode != SystemMode.SETUP_VOLUMES
            and mode != SystemMode.REMOTE_RECOVERY
        ):
            logger.debug(
                f"Can not switch to '{SystemMode.message(mode)}' as volume setup is needed"
            )
            result.append(
                f"Can not switch to '{SystemMode.message(mode)}' as volume setup is needed"
            )
            mode = SystemMode.SETUP_VOLUMES

        if (
            this_mode != SystemMode.SETUP_VOLUMES
            and this_mode != SystemMode.UNKNOWN
            and this_mode != SystemMode.REMOTE_RECOVERY
            and mode == SystemMode.REMOTE_RECOVERY
        ):
            logger.warning(
                "Remote Recovery Switch only allowed from volume setup, unknown or remote_recovery"
            )
            result.append(
                "Remote Recovery Switch only allowed from volume setup, unknown or remote_recovery"
            )
            write_result_file(sosconstants.LAST_RESULT, result)
            return result

        self.setactmode(SystemMode.INTERMEDIATE)  # while in mode switch, don't do anything in syncstat

        # now find out, if it is a transition where we need a configuration backup and set it automagically
        if (
            mode == SystemMode.DEFAULT or mode == SystemMode.DEFAULT_NO_SYNC
        ) and self.sysstate.storedmode == SystemMode.DISASTER_SWAP:
            if not cfg_backup_filepath or cfg_backup_filepath == "":
                if switch_action != SwitchAction.DISASTER_SWAP_PERSISTENT:
                    # to make it persistent, we just have to backup the configuration
                    # default means: we are switching back from DISASTER_SWAP to DEFAULT
                    cfg_backup_filepath = configbackup.get_latest_filepath(
                        os.path.join(
                            sosconstants.MOUNT[sosconstants.Partition.REMOTE],
                            sosconstants.SOS_CONF_BACKUP_PATH,
                        )
                    )
                    if cfg_backup_filepath == "":
                        logger.warning("No config backup found")
                        result.append("No config backup found")
                        write_result_file(sosconstants.LAST_RESULT, result)
                        return result
                    else:
                        logger.debug(
                            f"Using config backup {cfg_backup_filepath} for mode '{SystemMode.message(mode)}'"
                        )

        # same for transition to DISASTER_SWAP
        if mode == SystemMode.DISASTER_SWAP and (
            self.sysstate.storedmode == SystemMode.DEFAULT
            or self.sysstate.storedmode == SystemMode.DEFAULT_NO_SYNC
        ):
            if not cfg_backup_filepath:
                # default means: we are switching back from DISASTER_SWAP to DEFAULT
                cfg_backup_filepath = configbackup.get_latest_filepath(
                    os.path.join(
                        sosconstants.MOUNT[sosconstants.Partition.REMOTE],
                        sosconstants.SOS_CONF_BACKUP_PATH,
                    )
                )
                if cfg_backup_filepath == "":
                    logger.warning("No config backup found")
                    result.append("No config backup found")
                    write_result_file(sosconstants.LAST_RESULT, result)
                    return result
                else:
                    logger.debug(
                        f"Using config backup {cfg_backup_filepath} for mode '{SystemMode.message(mode)}'"
                    )

        # stop lsyncd if not in default mode
        if mode != SystemMode.DEFAULT:
            if not remotehost.stop_lsyncd():
                logger.critical("Could not stop lsyncd: data loss is unavoidable")

        if (
            (
                (mode == SystemMode.DEFAULT or mode == SystemMode.DEFAULT_NO_SYNC)
                and self.sysstate.storedmode == SystemMode.DISASTER_SWAP
                and switch_action != SwitchAction.DISASTER_SWAP_PERSISTENT
            )
            or (
                mode == SystemMode.REMOTE_RECOVERY
                and this_mode == SystemMode.SETUP_VOLUMES
            )
            or (
                mode == SystemMode.DISASTER_SWAP
                and (
                    self.sysstate.storedmode == SystemMode.DEFAULT
                    or self.sysstate.storedmode == SystemMode.DEFAULT_NO_SYNC
                )
            )
        ):
            if config_pwd is None or config_pwd == "" or not cfg_backup_filepath:
                logger.error(
                    f"Requested switch to '{SystemMode.message(mode)}' with backup restore with no "
                    f"or empty password or empty file path"
                )
                result.append(
                    f"Requested switch to '{SystemMode.message(mode)}' with backup restore with no "
                    f"or empty password or empty file path"
                )
                # report.append(soshelpers.translate_msg('no_cfg_backup_provided'))
                write_result_file(sosconstants.LAST_RESULT, result)
                self.setactmode(this_mode)
                return result
            logger.debug("Switch from Disaster Swap to Default mode requested")

        if cfg_backup_filepath and (
            mode == SystemMode.DEFAULT_NO_SYNC
            or mode == SystemMode.REMOTE_RECOVERY
            or mode == SystemMode.DISASTER_SWAP
            or mode == SystemMode.DEFAULT
        ):
            # ok this is for any config restore - either return from DISASTER_SWAP or config restore in DEFAULT_NO_SYNC
            # or REMOTE_RECOVERY
            try:
                src_dir = configbackup.unpack(cfg_backup_filepath, config_pwd)
            except soserrors.ConfigBackupFilenameError as e:
                result.append(
                    "Could not use filename for decryption. Is this really a configuration backup file?"
                )
                self.setactmode(
                    this_mode
                )  # just set the old mode. Nothing did happen yet.
                write_result_file(sosconstants.LAST_RESULT, result)
                raise e

            except soserrors.ConfigBackupFileDecryptionError as e:
                result.append(
                    "Could not decrypt configuration backup file. Wrong password?"
                )
                self.setactmode(
                    this_mode
                )  # just set the old mode. Nothing did happen yet.
                write_result_file(sosconstants.LAST_RESULT, result)
                raise e

            if src_dir == "":
                self.setactmode(
                    this_mode
                )  # just set the old mode. Nothing did happen yet.
                logger.error(f"cannot decrypt {cfg_backup_filepath}")
                result.append(
                    "Could not decrypt configuration backup file. Wrong password?"
                )
                write_result_file(sosconstants.LAST_RESULT, result)
                return result
            # Close all sshd access for this time
            sshdmanager.change_access(Sshd.INTRANET, SshdAccess.CLOSE)
            sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.CLOSE)
            # at this point, we need to handle the partitioning for remote_recovery
            if mode == SystemMode.REMOTE_RECOVERY:
                sosconfig_rr = sosconfig.SosConfig()
                sosconfig_rr.set_from_file(
                    basepath=os.path.join(src_dir, sosconstants.SOS_CONF_DIR_RELATIVE),
                    backup=False,
                )
                sosconfig_rr_data: SosConfigModel = sosconfig_rr.get()
                partcfg = PartitioningData.from_json(sosconfig_rr_data.to_json())
                # print(f"Free space:{self.mydrives.get_free_data()}, request local: {sosconfig_rr_data.local}"
                #      f", request remote: {sosconfig_rr_data.remote}")
                # let's calculate, if everything is fine.
                if not self.mydrives.create_volumes(partcfg):
                    # uh, we have to break here, find out, how.
                    result.append(
                        f"Was not possible to create volumes with local size {partcfg.local},"
                        f"remote size {partcfg.remote} "
                        f"on free space {self.mydrives.get_free_data()}"
                    )
                    # right now, we will just return, this should be fine, as we are in SETUP_DISKS mode anywhere
                    write_result_file(sosconstants.LAST_RESULT, result)
                    return result
            # it should not change anything to delete all accounts first.
            soscore.sosaccounts.delete_all_accounts()
            if mode == SystemMode.DISASTER_SWAP:
                # swap the drives, first the local part writeable to reset the ownership
                self.mydrives.mount_sos_volumes(
                    swap=True, local_ro=False, remote_ro=True
                )
            else:
                # mount the drives in the usual way
                self.mydrives.mount_sos_volumes()
            # now restore the config
            success, cfg_bu_report = configbackup.restore_from_dir(src_dir, mode)
            # as this may be a configuration from another vg-uuid, we have to reset the vg-uuid in the config,
            # except it is DISASTER_SWAP, where we should not change it.
            if mode != SystemMode.DISASTER_SWAP:
                new_sosconfig = sosconfig.SosConfig().get()
                new_sosconfig.vg_sos_uuid = self.mydrives.vgs.vg_uuid
                sosconfig.SosConfig().set_from_config(new_sosconfig, backup=True)
            result.extend(cfg_bu_report)
            if not success:
                logger.error("Was not able to restore configuration")
                result.append("Was not able to restore configuration")
                # right now, we switch to Setup mode, as we should be able to fix everything from there
                # Save the state, if it has changed
                write_result_file(sosconstants.LAST_RESULT, result)
                return result
            else:
                # ownership per account should have been already fixed in configbackup.restore
                # still we have to look for lost+found, syncosync and .
                drivemanager.set_local_ownership()
                if mode != SystemMode.DISASTER_SWAP:
                    drivemanager.set_remote_ownership()
                logger.info(f"Successful configuration restore {cfg_backup_filepath}")
                result.append("Configuration restore successful")
                # update the sysstate again, so we are sure everything is up to date
                self.update_sysstate()
            if mode == SystemMode.DEFAULT or mode == SystemMode.DEFAULT_NO_SYNC:
                result.extend(configbackup.create_orphaned_accounts())
                result.extend(configbackup.restabilize_accounts())
                # to collect the sizes right
                os.system("service sosaccountaccess restart")
            if mode == SystemMode.REMOTE_RECOVERY:
                soscore.soshelpers_file.write_to_file(
                    sosconstants.RR_RESULT, result.to_json()
                )
            # remove the unpacked dir finally
            # shutil.rmtree(src_dir)

        # clean up dirs, if coming back from remote recovery
        if mode == SystemMode.RR_FINAL:
            result.extend(configbackup.create_orphaned_accounts())
            result.extend(configbackup.restabilize_accounts())
            drivemanager.set_local_ownership()
            # to collect the sizes right
            os.system("service sosaccountaccess restart")

        if (
            (mode == SystemMode.DEFAULT or mode == SystemMode.DEFAULT_NO_SYNC)
            and self.sysstate.storedmode == SystemMode.DISASTER_SWAP
            and switch_action == SwitchAction.DISASTER_SWAP_PERSISTENT
        ):
            # we have to unmount and swap
            self.mydrives.umount_sos_volumes()
            self.mydrives.swap_sos_volumes()
            new_sosconfig = sosconfig.SosConfig().get()
            new_sosconfig.vg_sos_uuid = self.mydrives.vgs.vg_uuid
            sosconfig.SosConfig().set_from_config(new_sosconfig, backup=True)

        # now check for mounts
        if (
            mode == SystemMode.DEFAULT
            or mode == SystemMode.DEFAULT_NO_SYNC
            or mode == SystemMode.REMOTE_RECOVERY
            or mode == SystemMode.RR_FINAL
        ):
            try:
                self.mydrives.mount_sos_volumes()
                result.extend(configbackup.create_orphaned_accounts())
                result.extend(configbackup.restabilize_accounts())
            except:
                mode = SystemMode.DISK_ERROR

            if mode == SystemMode.REMOTE_RECOVERY:
                drivemanager.stop_quota()
            else:
                drivemanager.start_quota()
        elif mode == SystemMode.DISASTER_SWAP:
            try:
                self.mydrives.mount_sos_volumes(
                swap=True, local_ro=False, remote_ro=True
                )
                result.extend(configbackup.create_orphaned_accounts())
                result.extend(configbackup.restabilize_accounts())
            except:
                mode = SystemMode.DISK_ERROR

        if (
            (mode == SystemMode.DEFAULT or mode == SystemMode.DEFAULT_NO_SYNC)
            and self.sysstate.storedmode == SystemMode.DISASTER_SWAP
            and switch_action == SwitchAction.DISASTER_SWAP_PERSISTENT
        ):
            # now finalize making the Disaster Swap Persistent
            drivemanager.set_remote_ownership()
            self.mycfg_backup.backup()

        # check to do a configbackup
        if mode == SystemMode.DEFAULT or mode == SystemMode.DEFAULT_NO_SYNC:
            if self.sysstate.sysstate & SystemStatus.SOS_CONF_BACKUP_NOT_DONE:
                logger.info(
                    "First time configuration backup while starting default mode"
                )
                self.mycfg_backup.backup()
                self.sysstate.sysstate = (
                    self.sysstate.sysstate - SystemStatus.SOS_CONF_BACKUP_NOT_DONE
                )

        # intranet access
        if (
            mode == SystemMode.DEFAULT
            or mode == SystemMode.DEFAULT_NO_SYNC
            or mode == SystemMode.DISASTER_SWAP
        ):
            # we should open the sshs only when mounted
            destination_a, mountopts = soshelpers_file.get_mountpoint(
                sosconstants.MOUNTDEV[Partition.LOCAL]
            )
            destination_b, mountopts = soshelpers_file.get_mountpoint(
                sosconstants.MOUNTDEV[Partition.REMOTE]
            )
            if (
                destination_a == sosconstants.MOUNT[Partition.LOCAL]
                or destination_b == sosconstants.MOUNT[Partition.LOCAL]
            ):
                sshdmanager.change_access(Sshd.INTRANET, SshdAccess.OPEN)
            else:
                sshdmanager.change_access(Sshd.INTRANET, SshdAccess.CLOSE)
        else:
            if mode == SystemMode.SHUTDOWN:
                sshdmanager.change_access(Sshd.INTRANET, SshdAccess.OFF)
            else:
                sshdmanager.change_access(Sshd.INTRANET, SshdAccess.CLOSE)

        # extranet access
        logger.debug(f"Checking Remotehost status: {self.sysstate.sysstate}")
        if mode == SystemMode.DEFAULT:
            if self.sysstate.sysstate & SystemStatus.REMOTE_KEY_NOT_SET:
                logger.debug(
                    "Remote Key is not set, external ssh access is not started"
                )
                result.append(
                    "Remote Key is not set, external ssh access is not started"
                )
                sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OFF)
            else:
                # we should open the sshs only when mounted
                # this is for security again
                destination_a, mountopts = soshelpers_file.get_mountpoint(
                    sosconstants.MOUNTDEV[Partition.REMOTE]
                )
                if destination_a == sosconstants.MOUNT[Partition.REMOTE]:
                    sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OPEN)
                else:
                    logger.error(
                        "Could not open extranet access as remote partition is not mounted!"
                    )
                    sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OFF)
        else:
            sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OFF)

        # for remote recovery it is now the time to start the remote recovery service
        if mode == SystemMode.REMOTE_RECOVERY:
            os.system("service remote_recovery start")
            result.append("started file transfer for remote recovery")

        # now  finalize for all modes

        # Save the state, if it has changed and then, we are done
        if (
            mode == SystemMode.DEFAULT
            or mode == SystemMode.DEFAULT_NO_SYNC
            or mode == SystemMode.DISASTER_SWAP
            or mode == SystemMode.REMOTE_RECOVERY
            or mode == SystemMode.RR_FINAL
        ):
            if self.sysstate.storedmode != mode:
                modeset = SysModeModel(mode)
                soscore.soshelpers_file.write_to_file(
                    sosconstants.SOS_CONF_STOREDMODE, modeset.to_json()
                )
                self.sysstate.storedmode = mode
            self.setactmode(mode)
            result.mode = self.sysstate.actmode
            write_result_file(sosconstants.LAST_RESULT, result)
            logger.debug(f"Switch to '{SystemMode.message(mode)}' complete.")
            return result

        if mode == SystemMode.SHUTDOWN:
            # set the sshds according
            sshdmanager.change_access(Sshd.INTRANET, SshdAccess.OFF)
            sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OFF)
            self.mydrives.umount_sos_volumes()
            self.setactmode(mode)
            result.mode = self.sysstate.actmode
            write_result_file(sosconstants.LAST_RESULT, result)
            return result

        if (
            (
                mode == SystemMode.SETUP_DISKS
                or (self.sysstate.sysstate & SystemStatus.DISK_SETUP_NEED)
            )
            or (
                mode == SystemMode.SETUP_VOLUMES
                or (self.sysstate.sysstate & SystemStatus.PARTITIONING_NEED)
            )
            or (mode == SystemMode.RESIZE_VOLUMES)
            or (mode == SystemMode.DISK_ERROR)
        ):
            # set the sshds according
            logger.debug("Stopping sosaccountaccess")
            sshdmanager.change_access(Sshd.INTRANET, SshdAccess.CLOSE)
            sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OFF)
            self.mydrives.umount_sos_volumes()
            self.setactmode(mode)
            result.mode = self.sysstate.actmode
            write_result_file(sosconstants.LAST_RESULT, result)
            return result

        logger.error(
            f"Requested to switch to unimplemented mode '{SystemMode.message(mode)}'"
            f"Will stay in mode '{SystemMode.message(self.sysstate.actmode)}'"
        )
        # in every case, open sshd for admin access
        sshdmanager.change_access(Sshd.INTRANET, SshdAccess.CLOSE)
        write_result_file(sosconstants.LAST_RESULT, result)
        return result
