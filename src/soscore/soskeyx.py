#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import io
import os
import shutil
import socket
import time
from typing import Optional

import paramiko
from paramiko.sftp import SFTP_OP_UNSUPPORTED
from paramiko.sftp_attr import SFTPAttributes
from paramiko.sftp_client import SFTPClient
from paramiko.sftp_handle import SFTPHandle
from paramiko.sftp_server import SFTPServer
from paramiko.sftp_si import SFTPServerInterface
from paramiko.transport import Transport
from soscore import (
    remotehost,
    soserrors,
    soshelpers,
    soshelpers_file,
    soskey,
    sshdmanager,
    sysstate,
    systemsetupdata,
)
from soscore.remotehost_systemsetupdata import RemoteHostSystemSetupData
from sosmodel import sosconstants
from sosmodel.sos_enums import SsdAction, Sshd, SshdAccess, SystemMode
from sosmodel.soskey import SosKeyModel
from sosmodel.sysstate import SysStateResult
from sosmodel.systemsetupdata import SystemSetupDataModel
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


class SosKeyExchange:
    """
    Class handling the receiving/sending/accepting of keys during key exchange as well as get/delete remote key
    """

    _ssd_in = b""
    receiver_start_time = 0
    sender_start_time = 0
    # receive_cancel_signal: Event = Event()
    receive_cancel_signal = False
    actmode: SystemMode = SystemMode.UNKNOWN

    class SsdInFileHandle(SFTPHandle):
        def __init__(self, path, flags=0):
            super().__init__(flags)
            self.writefile = io.BytesIO(b"")
            self.readfile = self.writefile
            self.path = path

        def close(self):
            if self.writefile.tell() > 0:
                SosKeyExchange._ssd_in = self.writefile.getvalue()
            super().close()

        def stat(self):
            attr = SFTPAttributes()
            attr.st_mode = 644
            attr.st_uid = 0
            attr.st_gid = 0
            attr.st_size = 0
            return attr

    class SsdOutFileHandle(SFTPHandle):
        def __init__(self, path, flags=0):
            super().__init__(flags)
            # Get SSD data here
            myssd = systemsetupdata.get_system_ssd()
            myssd.action = SsdAction.FOLLOWER
            ssd_json = bytes(myssd.to_json(), "UTF-8")
            self.len = len(ssd_json)
            self.readfile = io.BytesIO(ssd_json)
            self.path = path

        def close(self):
            super().close()

        def stat(self):
            attr = SFTPAttributes()
            attr.st_size = self.len
            attr.st_mode = 644
            attr.st_uid = 0
            attr.st_gid = 0
            attr.st_atime = 0
            attr.st_mtime = 0
            attr.filename = self.path
            return attr

    class SSHKeyxSI(SFTPServerInterface):
        def open(self, path, flags, attr):
            logger.debug(f"Open {path}")
            try:
                if path == sosconstants.SOS_KEYX_SSDPATH_IN:
                    logger.debug(f"Incoming file:{path}")
                    return SosKeyExchange.SsdInFileHandle(path, flags)
                elif path == sosconstants.SOS_KEYX_SSDPATH_OUT:
                    logger.debug(f"Outgoing file:{path}")
                    return SosKeyExchange.SsdOutFileHandle(path, flags)
                else:
                    logger.warning(f"Opening filename not supported: {path}")
                    return SFTP_OP_UNSUPPORTED

            except Exception as e:
                logger.error(f"In SSHKEyxSI: {str(e)}")
                return SFTP_OP_UNSUPPORTED

        def stat(self, path):
            return SFTPAttributes()

    class SSHKeyxServer(paramiko.ServerInterface):
        def check_auth_publickey(self, username, key):
            # TODO: Check pubkey against own keyx fingerprint
            return paramiko.AUTH_SUCCESSFUL

        def get_allowed_auths(self, username):
            return "publickey"

        def check_channel_request(self, kind, chanid):
            # TODO: Only allow relevant channel to be opened?
            return paramiko.OPEN_SUCCEEDED

    @staticmethod
    def accept_key(checksum=None):
        if checksum:
            logger.error("Not checking key checksum!")
        if not os.path.isfile(sosconstants.SOS_KEYX_PUBKEY):
            raise soserrors.SosError("No key received")
        try:
            shutil.copyfile(
                sosconstants.SOS_KEYX_PUBKEY, sosconstants.REMOTE_KEYFILE_PUB
            )
            logger.info(
                f"Accepted new remote key with fp {soshelpers.sshfilefingerprint(sosconstants.SOS_KEYX_PUBKEY)}"
            )
            soshelpers_file.set_configbackup_flag()
        except Exception as e:
            raise soserrors.SosError(f"Could not copy key: {str(e)}")

    @staticmethod
    def save_mode_and_switch():
        mystate = sysstate.SysState()
        mystatestate = mystate.get()
        SosKeyExchange.actmode = mystatestate.actmode
        if SosKeyExchange.actmode == SystemMode.DEFAULT:
            result: SysStateResult = mystate.changemode(SystemMode.DEFAULT_NO_SYNC)
            if result.mode != SystemMode.DEFAULT_NO_SYNC:
                raise soserrors.ModeSwitchFailed
        elif (
            SosKeyExchange.actmode == SystemMode.RR_FINAL
            or SosKeyExchange.actmode == SystemMode.REMOTE_RECOVERY
        ):
            raise soserrors.ModeDoesNotAllowKeyExchange

    @staticmethod
    def restore_mode():
        mystate = sysstate.SysState()
        if SosKeyExchange.actmode == SystemMode.DEFAULT:
            result: SysStateResult = mystate.changemode(SystemMode.DEFAULT)
            if result.mode != SystemMode.DEFAULT:
                raise soserrors.ModeSwitchFailed

    @staticmethod
    def send_ssd(action=SsdAction.LEADER_SEND_KEY) -> SystemSetupDataModel:
        SosKeyExchange.save_mode_and_switch()
        SosKeyExchange.receive_cancel_signal = False
        # collect the data from different areas
        myssd = systemsetupdata.get_system_ssd()
        myssd.action = action
        ssd_json = myssd.to_json()
        ssd_to_send: io.BytesIO = io.BytesIO(bytes(ssd_json, "UTF-8"))

        remotecfg = remotehost.RemoteHost()
        remote = remotecfg.get()
        # print(f"json:{ssd_to_send.getvalue()}")
        try:
            with paramiko.SSHClient() as client:
                client.set_missing_host_key_policy(paramiko.WarningPolicy())
                SosKeyExchange.sender_start_time = int(time.time())
                SosKeyExchange.__connect_client(client, remote)
                transport: Optional[Transport] = client.get_transport()
                if transport:
                    with transport:
                        sftp = paramiko.SFTPClient.from_transport(transport)
                        if sftp:
                            with sftp:
                                # TODO: Should we check for timeout also here? I guess so...
                                ssdreceived: SystemSetupDataModel = (
                                    SosKeyExchange.__lead_ssd_exchange_fetch_file_sftp(
                                        sftp, ssd_to_send
                                    )
                                )
                        # maybe we should move this to some higher layer
                        SosKeyExchange.__write_pub_key(ssdreceived)
        except soserrors.SosError as e:
            # passthrough SosError since those are thrown intentionally
            raise e
        except Exception as e:
            logger.exception(e)
        finally:
            # restore ssh status
            SosKeyExchange.restore_mode()
        return ssdreceived

    @staticmethod
    def __write_pub_key(ssdreceived):
        with open(sosconstants.SOS_KEYX_PUBKEY, "wb") as keyx:
            keyx.write(bytes(ssdreceived.pub_key, "UTF-8"))
            keyx.write(bytes("\n", "UTF-8"))

    @staticmethod
    def __lead_ssd_exchange_fetch_file_sftp(
        sftp: SFTPClient, ssd_to_send: io.BytesIO
    ) -> SystemSetupDataModel:
        sftp.putfo(ssd_to_send, sosconstants.SOS_KEYX_SSDPATH_IN, confirm=False)
        with io.BytesIO() as fo:
            sftp.getfo(sosconstants.SOS_KEYX_SSDPATH_OUT, fo)
            fo.seek(0)
            ssdreceived = SystemSetupDataModel()
            logger.debug("Got ssd from remote:", fo.getvalue())
            fo.seek(0)
            ssdreceived.update_from_json(fo.getvalue())
            myremotehost_ssd_manager = RemoteHostSystemSetupData()
            myremotehost_ssd_manager.set_from_json(fo.getvalue())
        return ssdreceived

    @staticmethod
    def __connect_client(client, remote):
        connected: bool = False
        while (
            not SosKeyExchange.receive_cancel_signal
            and not connected
            and time.time()
            <= SosKeyExchange.sender_start_time + sosconstants.SOS_KEYX_TIMEOUT
        ):
            try:
                client.connect(
                    socket.gethostbyname(remote.hostname),
                    remote.port,
                    key_filename=sosconstants.SOSKEYX_KEYFILE,
                )
                connected = True
            except Exception as e:
                logger.warning(f"Error while connecting {str(e)}")
                pass
        if not connected:
            # TODO: client.close()? No idea if actually required if no connection was established
            # or if it may even cause undefined behaviour
            if SosKeyExchange.receive_cancel_signal:
                logger.debug("key exchange cancelled")
                raise soserrors.SosError("key exchange cancelled")
            else:
                logger.warning("Timeout while ssd exchange")
                raise soserrors.SosError("Timeout while ssd exchange")

    @staticmethod
    def receive_ssd() -> Optional[SystemSetupDataModel]:
        SosKeyExchange.save_mode_and_switch()
        SosKeyExchange._ssd_in = b""
        SosKeyExchange.receive_cancel_signal = False
        sshd = sshdmanager.get(Sshd.EXTRANET)
        SosKeyExchange.receiver_start_time = int(time.time())
        myssdin: Optional[SystemSetupDataModel] = None
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                sock.bind(("0.0.0.0", sshd.port))
                sock.listen(1)
                # TODO: cancel ability?
                sock.settimeout(1)
                myssdin = SosKeyExchange.__try_to_receive_ssd(sock)
        except soserrors.SosError as e:
            raise e
        except Exception as e:
            logger.exception(e)
        finally:
            SosKeyExchange.restore_mode()
        return myssdin

    @staticmethod
    def __try_to_receive_ssd(sock: socket.socket) -> Optional[SystemSetupDataModel]:
        myssdin = None
        while not SosKeyExchange.receive_cancel_signal and not myssdin:
            try:
                client, _addr = sock.accept()
                myssdin = SosKeyExchange.__receive_ssd_using_socket(client)
            except socket.timeout:
                if (
                    time.time()
                    > SosKeyExchange.receiver_start_time + sosconstants.SOS_KEYX_TIMEOUT
                ):
                    logger.warning("Timeout while ssd exchange")
                    raise soserrors.SosError("Timeout while ssd exchange")
        return myssdin

    @staticmethod
    def __receive_ssd_using_socket(client) -> SystemSetupDataModel:
        with paramiko.Transport(client) as transport:
            # Get the ssh keyx key and
            key = paramiko.RSAKey.from_private_key_file(sosconstants.SOSKEYX_KEYFILE)
            transport.add_server_key(key)
            transport.set_subsystem_handler(
                "sftp", SFTPServer, SosKeyExchange.SSHKeyxSI
            )
            transport.start_server(server=SosKeyExchange.SSHKeyxServer())

            SosKeyExchange.__wait_for_key_to_be_written()
            logger.debug("Got ssd from remote: ", SosKeyExchange._ssd_in)
            myssdin = SystemSetupDataModel()
            myssdin.update_from_json(SosKeyExchange._ssd_in)
            myremotehost_ssd_manager = RemoteHostSystemSetupData()
            myremotehost_ssd_manager.set_from_json(SosKeyExchange._ssd_in)
            SosKeyExchange.__write_pub_key(myssdin)

            while (
                transport.is_active()
                and time.time()
                < SosKeyExchange.receiver_start_time + sosconstants.SOS_KEYX_TIMEOUT
            ):
                time.sleep(2)
        return myssdin

    @staticmethod
    def __wait_for_key_to_be_written():
        while len(SosKeyExchange._ssd_in) == 0:
            if (
                time.time()
                > SosKeyExchange.receiver_start_time + sosconstants.SOS_KEYX_TIMEOUT
            ):
                raise soserrors.SosError("Timeout while receiving key")
            # print(f"waiting {time.time()}")
            time.sleep(0.2)

    @staticmethod
    def remove_remote_key():
        # in any way stop external sshd
        sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OFF)
        if not os.path.isfile(sosconstants.REMOTE_KEYFILE_PUB):
            raise soserrors.SosError(
                f"No key present to remove: {sosconstants.REMOTE_KEYFILE_PUB}"
            )
        try:
            os.remove(sosconstants.REMOTE_KEYFILE_PUB)
            soshelpers_file.set_configbackup_flag()
        except Exception as e:
            raise soserrors.SosError("Could not remove key:", str(e))

    @staticmethod
    def get_remote_key():
        """
        get remote pub key and fingerprint
        :return: remote key
        """
        remote_key = SosKeyModel()
        if os.path.isfile(sosconstants.REMOTE_KEYFILE_PUB):
            with open(sosconstants.REMOTE_KEYFILE_PUB, "r") as f:
                remote_key.pub_key = f.read().rstrip()
                remote_key.fingerprint = soshelpers.sshfingerprint(remote_key.pub_key)
        else:
            # logger.debug(
            #     "Requested fingerprint for remote key but no key file is existing"
            # )
            soskey.fingerprint = None
        return remote_key
