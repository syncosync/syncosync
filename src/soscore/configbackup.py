"""
classes and functions for handling backup of sos configuration
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import crypt
import glob
import json
import os
import re
import shutil
import socket
import spwd
import subprocess
import sys
import tarfile
import tempfile
import time
from typing import List, Optional, Tuple, Union

import gnupg
import soscore.soshelpers_file
from soscore import (
    hostname,
    mail,
    nameserver,
    remotehost,
    sosaccounts,
    soserrors,
    soshelpers,
    soshelpers_file,
)
from sosmodel import sos_enums, sosconstants
from sosmodel.configbackup import (
    ConfigBackupContentModel,
    ConfigBackupManifestModel,
    ConfigBackupModel,
)
from sosmodel.hostname import HostNameModel
from sosmodel.localizedMessages import LocalizedMessage
from sosmodel.serializerbase import SerializerBaseJsonDecoder, SerializerBaseJsonEncoder
from sosmodel.sos_enums import Partition, SystemMode
from sosmodel.sosaccount import SosAccountAddModel
from sosmodel.sosconstants import MOUNT
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def keep_max_number():
    """
    deletes all config backups which exceed SOS_CONF_BACKUP_MAX
    :return:
    """
    files = sorted(
        glob.glob(
            os.path.join(
                sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                sosconstants.SOS_CONF_BACKUP_PATH,
            )
            + "*"
        ),
        reverse=True,
    )
    # print(len(files), files)
    for i in range(sosconstants.SOS_CONF_BACKUP_MAX + 1, len(files)):
        try:
            os.remove(files[i])
            logger.debug(f"Deleted odd config backup: {files[i]}")
        except FileNotFoundError:
            logger.error(f"Could not delete odd config backup: {files[i]}")


def parse_filepath(filepath):
    """
    parses filepath for hostname and date
    :param filepath:
    :return: { "hostname": hostname, "date": date }
    """
    m = re.search(
        r".*sosconf_(?P<date>\d*)_(?P<hostname>[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.tgz",
        filepath,
    )
    if m:
        return m.groupdict()
    else:
        return None


def get_metadata_from_configbackup(filepath):
    """
    reads the metadata from a config backup file by parsing the filname

    :param filepath: the path to the config file
    :return: ConfigBackupModel with metadata set from file
    """
    configdata = ConfigBackupModel()
    # first unpack the archive containg the crypted file and the manifest
    try:
        (manifest, target_dir) = unpack_main_file(filepath)
    except soserrors.ConfigBackupFileDecryptionError:
        return None
    if manifest is None:
        return None
    configdata.hostname = manifest.hostname
    configdata.timestamp = manifest.timestamp
    configdata.md5sum = manifest.md5sum
    configdata.filepath = filepath
    # delete manifest
    os.remove(os.path.join(target_dir, "manifest.json"))
    # delete encyrpted file
    os.remove(os.path.join(target_dir, "content.tar.gz.gpg"))
    os.rmdir(target_dir)  # remove the dir to keep stuff clean
    return configdata


def get_filelist(
    dirpath=os.path.join(
        sosconstants.MOUNT[sos_enums.Partition.LOCAL], sosconstants.SOS_CONF_BACKUP_PATH
    )
):
    """
    returns a list of backup config files which are stored
    on the path

    :return:  list of files
    """
    if os.path.isdir(dirpath):
        return sorted(glob.glob(dirpath + "sosconf_[0-9]*.tgz"), reverse=True)
    else:
        return []


def get_stored(
    dirpath=os.path.join(
        sosconstants.MOUNT[sos_enums.Partition.LOCAL], sosconstants.SOS_CONF_BACKUP_PATH
    )
):
    """
    returns a sorted list backup configs which are stored
    on the path

    :return:  list of ConfigBackupModel objects
    """
    stored_list = []
    for file in get_filelist(dirpath):
        stored_list.append(get_metadata_from_configbackup(file))
    logger.debug(f"found {len(stored_list)} configuration backups")
    return stored_list


def get_latest_filepath(
    dirpath=os.path.join(
        sosconstants.MOUNT[sos_enums.Partition.LOCAL], sosconstants.SOS_CONF_BACKUP_PATH
    )
):
    """
    returns the latest config as a filepath (filename with full path)
    :param dirpath:
    :return: string
    """
    filelist = get_filelist(dirpath)
    if filelist != []:
        return filelist[0]
    else:
        return ""


def get_latest(
    dirpath=os.path.join(
        sosconstants.MOUNT[sos_enums.Partition.LOCAL], sosconstants.SOS_CONF_BACKUP_PATH
    )
):
    """
    returns the latest config as a ConfigBackupModel
    :param dirpath:
    :return: ConfigBackupModel if existing. None, else
    """
    filepath = get_latest_filepath(dirpath)
    if filepath != "":
        return get_metadata_from_configbackup(filepath)
    else:
        return None


def decrypt(
    filepath: str, md5sum: str, password: Optional[str], base64salt: Optional[str]
) -> str:
    """
    decrypts and unpacks a backup file to a tmp directory
    check for exceptions raised.
    :param filepath: full path to a backup file
    :param password: password if /etc/shadow is not present or does not match the one of the backups
    :return: full path to directory of unpacked file, "" if not successful
    """
    if os.path.islink(filepath):
        filepath = os.readlink(filepath)

    if not password or not base64salt:
        mypwdict = soshelpers.parse_shadowpwd_data(spwd.getspnam("sosadmin")[1])
        if not mypwdict or "salt" not in mypwdict:
            logger.error(
                "no pwhash nor salt for sosadmin in shadowpwd?",
            )
            raise soserrors.ConfigBackupFileDecryptionError(
                "no pwhash nor salt for sosadmin in shadowpwd "
            )
        pwhash = mypwdict["salt"] + "$" + mypwdict["pwhash"]
    else:
        pwhash = crypt.crypt(password, salt=soshelpers.base642str(base64salt))

    gpg = gnupg.GPG()
    # this trick should give you a tmpfilename, for the decrypt output. Do we need more?
    mytmpfile = tempfile.NamedTemporaryFile(delete=False)
    mytmpfile.write(b"foo")
    outname = mytmpfile.name
    mytmpfile.close()
    logger.debug(
        f"Trying to decrypt: {filepath} with passphrase {pwhash} to file {outname}"
    )
    with open(filepath, "rb") as f:
        gpg.decrypt_file(f, passphrase=pwhash, output=outname)
        outname_checksum = soshelpers.md5Checksum(outname)
        logger.debug(f"checking decrypted checksum {outname_checksum} against {md5sum}")
        if soshelpers.md5Checksum(outname) != md5sum:
            raise soserrors.ConfigBackupFileDecryptionError(
                f"Failed to decrypt config backup {filepath}.",
                LocalizedMessage.CONFIGBACKUP_DECRYPT_FAILED,
            )
    return outname


def unpack_main_file(filepath) -> Tuple[ConfigBackupManifestModel, str]:
    try:
        with tarfile.open(filepath) as tar:
            target_dir = tempfile.mkdtemp()
            logger.debug(f"target_dir:{target_dir}")
            tar.extractall(path=target_dir)
        logger.debug(f"unpacked successful {filepath} to {target_dir}")
        if not os.path.isfile(os.path.join(target_dir, "manifest.json")):
            logger.error("no manifest file in config backup")
            raise soserrors.ConfigBackupFileDecryptionError(
                "no manifest file in config backup"
            )
        manifest = ConfigBackupManifestModel.from_json_file(
            os.path.join(target_dir, "manifest.json")
        )
        return (manifest, target_dir)
    except:
        return (None, "")


def unpack(filepath, password) -> str:
    """
    decrypts and unpacks a backup file to a tmp directory
    check for exceptions raised.
    :param filepath: full path to a backup file
    :param password: password
    :return: full path to directory of unpacked file, "" if not successful
    """
    if os.path.islink(filepath):
        filepath = os.readlink(filepath)
    if not os.path.isfile(filepath):
        logger.error(f"config backup file {filepath} is not existing, cannot unpack")
        return ""

    # first unpack the archive containg the crypted file and the manifest
    (manifest, target_dir) = unpack_main_file(filepath)

    if manifest is None:
        return ""

    outname = decrypt(
        filepath=os.path.join(target_dir, "content.tar.gz.gpg"),
        md5sum=manifest.md5sum,
        password=password,
        base64salt=manifest.salt,
    )

    if outname == "":
        return ""
    logger.debug(f"got {outname} as tar file")
    with tarfile.open(outname) as tar:
        configbackup_target_dir = tempfile.mkdtemp()
        logger.debug(f"target_dir:{configbackup_target_dir}")
        tar.extractall(path=configbackup_target_dir)
    logger.debug(f"unpacked successful {filepath} to {configbackup_target_dir}")
    # delete decrypted file
    os.remove(outname)
    # delete manifest
    os.remove(os.path.join(target_dir, "manifest.json"))
    # delete encyrpted file
    os.remove(os.path.join(target_dir, "content.tar.gz.gpg"))
    # delete the dir at all
    os.rmdir(target_dir)
    return configbackup_target_dir


def get_data(filepath, password) -> Optional[ConfigBackupContentModel]:
    """
    get data from a dedicated file with password
    :param filepath:
    :param password:
    :return: ConfigBackupContentModel
    """
    my_output = ConfigBackupContentModel()
    if os.path.islink(filepath):
        filepath = os.readlink(filepath)
    src_dir = unpack(filepath, password)
    if src_dir == "":
        logger.warning("Could not unpack config backup file")
        return None
    else:
        my_output.sosaccounts = []
        if os.path.isfile(os.path.join(src_dir, "sosaccounts.json")):
            with open(os.path.join(src_dir, "sosaccounts.json")) as f:
                accounts_to_restore_json = f.read()
                tmpsosaccounts_deserialized = SerializerBaseJsonDecoder.decode(
                    SosAccountAddModel, accounts_to_restore_json
                )
                if isinstance(tmpsosaccounts_deserialized, SosAccountAddModel):
                    raise soserrors.SosError("Invalid sos accounts")
                my_output.sosaccounts = tmpsosaccounts_deserialized
                for account in my_output.sosaccounts:
                    if account.name == "sosadmin":
                        my_output.sosaccounts.remove(account)
        if os.path.isfile(os.path.join(src_dir, sosconstants.SOS_CONF_HOSTNAME)):
            with open(
                os.path.join(src_dir, sosconstants.SOS_CONF_HOSTNAME)
            ) as json_file:
                config = HostNameModel()
                data = json_file.read()
                config.from_json(data)
                my_output.hostname = config.hostname
        else:
            logger.warning(
                f"Config backup file {filepath} has no hostname configuration"
            )
        my_output.remotehost = remotehost.read_from_file(
            os.path.join(src_dir, "etc/lsyncd/lsyncd.conf.lua")
        )
        shutil.rmtree(src_dir)
        return my_output


def create_orphaned_accounts():
    report = []
    my_accounts = sosaccounts.SosAccounts()
    # now create accounts which have a home dir but no account. This should also not happen
    accountlist = sosaccounts.getsosaccount_names()
    for itempath in glob.glob(
        os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, "*")
    ):
        item = os.path.basename(itempath)
        logger.debug(f"Checking for local directory {item}")
        if (
            os.path.isdir(
                os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, item)
            )
            and item != "syncosync"
            and item not in accountlist
        ):
            # we should try to add an account
            logger.debug(f"Creating account for orphaned dir: {item}")
            newaccount = SosAccountAddModel()
            try:
                newaccount.name = item
            except ValueError:
                logger.warning(
                    f"{item} is not an allowed account name. No account created."
                )
                report.append(
                    f"{item} is not an allowed account name. No account created."
                )
            newaccount.rlongname = "Created from config restore"
            try:
                my_accounts.add(newaccount, no_backup=True)
                logger.info(f"Created account for orphaned dir: {item}")
                report.append(f"Created account for orphaned dir: {item}")
            except soserrors.AccountCreationError:
                logger.warning(f"Could not create an account for {item}")
                report.append(f"Could not create an account for {item}")
    return report


def restabilize_accounts():
    report = []
    my_accounts = sosaccounts.SosAccounts()
    accountlist = my_accounts.get()
    for account in accountlist:
        # lets create the home again, to be sure, all stuff is there and ownership is set right
        sosaccounts.create_account_home(account.name)
        # recreate quota
        sosaccounts.set_quota(account.name, account.max_space_allowed)
    # finally sync everything
    os.sync()
    return report


def remove_empty_accounts():
    # first remove accounts which have no home dir. This should not happen
    report = []
    my_accounts = sosaccounts.SosAccounts()
    accountlist = my_accounts.get()
    for account in accountlist:
        if not os.path.isdir(
            os.path.join(MOUNT[Partition.LOCAL], sosconstants.HDDSUBDIR, account.name)
        ):
            logger.warning(f"Account {account.name} removed: no home directory")
            report.append(f"Account {account.name} removed: no home directory")
            sosaccounts.del_by_name(account.name)
    return report


def restore(filepath, password):
    """
    restores a configuration backup to the actual configuration.
    Handle with care!

    :param filepath: the path and filename of the configuration backup
    :param password: password

    :return: True if everything went fine, false else.
    """
    report = []
    if os.path.islink(filepath):
        filepath = os.readlink(filepath)
    src_dir = unpack(filepath, password)
    if src_dir == "":
        report.append("Could not unpack restore file for unknown reason")
        return False, report
    result, report = restore_from_dir(src_dir)
    # remove the unpacked dir finally
    shutil.rmtree(src_dir)
    return result, report


def restore_from_dir(src_dir, mode: SystemMode = SystemMode.DEFAULT):
    """
    restore from an already unpacked configuration backup
    the src_dir will not be removed after restore!
    :param src_dir: the dir of the unpacked configuration
    :param mode: SystemMode, to decide, what should be restored and what not
    :return: result -> bool, report -> list of strings
    """
    report: List[str] = []
    # we also do not restore if sshd is open - there could be glitches
    # let's restore the usual files, this is easy
    # but switch off quota first to be able to restore also aquota.user
    logger.debug(f"restoring configuration backup from dir {src_dir}")
    subprocess.run(f"quotaoff {MOUNT[Partition.LOCAL]}", shell=True)
    if os.path.isfile(os.path.join(MOUNT[Partition.LOCAL], "aquota.user")):
        os.remove(os.path.join(MOUNT[Partition.LOCAL], "aquota.user"))
    basepath = os.path.join(src_dir, sosconstants.SOS_CONF_DIR_RELATIVE)
    remotehost.RemoteHost().set_from_file(basepath=basepath, backup=False)
    hostname.HostName().set_from_file(basepath=basepath, backup=False)
    nameserver.NameServer().set_from_file(basepath=basepath, backup=False)
    mail.Mail().set_from_file(basepath=basepath, backup=False)
    for restorefile in sosconstants.CONFIGRESTORE_ITEMS:
        src = os.path.join(src_dir, restorefile[1:])
        dest = restorefile
        if os.path.isdir(src):
            if os.path.isdir(dest):
                shutil.rmtree(dest)
            shutil.copytree(src, dest)
            logger.debug(f"Restored directory {src} -> {dest}")
        elif os.path.isfile(src):
            shutil.copy(src, dest)
            logger.debug(f"Restored file {src} -> {dest}")
        else:
            logger.warning(f"Item {restorefile} not found in archive to restore")
    # check quotas and restart quota
    subprocess.run(f"quotacheck -c {MOUNT[Partition.LOCAL]}", shell=True)
    subprocess.run(f"quotaon {MOUNT[Partition.LOCAL]}", shell=True)
    # now the more complicated part: restoring accounts
    accounts_to_restore: Union[SosAccountAddModel, List[SosAccountAddModel]] = []
    if os.path.isfile(os.path.join(src_dir, "sosaccounts.json")):
        with open(os.path.join(src_dir, "sosaccounts.json")) as accountsfile:
            accounts_to_restore_json = accountsfile.read()
            accounts_to_restore = SerializerBaseJsonDecoder.decode(
                SosAccountAddModel, accounts_to_restore_json
            )
    else:
        logger.warning("No sosaccounts.json file to restore. Is that correct?")
        return False, report
    if not isinstance(accounts_to_restore, list):
        logger.warning("Accounts to restore are not in list-format.")
        return False, report
    # now check if accounts have to be modified or added
    my_accounts = sosaccounts.SosAccounts()
    for account in accounts_to_restore:
        if account.name != "sosadmin":
            logger.debug(f"Restoring account {account.name}")
            my_account = my_accounts.get_by_name(account.name)
            if my_account is not None:
                # this account is already existing and should me modified
                # first we transform it to an AddModel
                mod_account = SosAccountAddModel.from_json(my_account.to_json())
                # print(f"mod_account1: {mod_account.to_json()}")
                # then we mod json with the to restored data
                mod_account.update_from_json(account.to_json())
                # print(f"mod_account2: {mod_account.to_json()}")
                # then we mod it. Voilá!
                my_accounts.mod(mod_account, no_backup=True)
                logger.info(f"Modified account: {account.name}")
            else:
                # this account does not exist
                my_accounts.add(account, no_backup=True)
                logger.info(f"Restored account: {account.name}")
                report.append(
                    f"Restored account: {account.name}"
                )  # if anything but DISASTER_SWAP: flag a config backup
    if mode != SystemMode.DISASTER_SWAP:
        soscore.soshelpers_file.set_configbackup_flag()
    return True, report


def create_archive():
    """
    creates a config backup tar gz as a temp file without deleting it

    :return: the filepath of the archive
    """
    # ok so, we have some place, where we can backup the configuration, let's go
    start_dir = os.getcwd()
    with tempfile.TemporaryDirectory() as target_dir:
        os.chdir(target_dir)
        # now get the sosusers
        mysosaccounts = sosaccounts.SosAccounts()
        accountlist = mysosaccounts.get()
        saveaccountlist = []
        # accountjson = "["
        for account in accountlist:
            saveaccount = SosAccountAddModel.from_json(account.to_json())
            #  add the password
            saveaccount._password = spwd.getspnam(account.name)[1]
            # add the ssh key
            if os.path.isfile(
                os.path.join(sosconstants.ACCOUNT_KEYFILE_DIR, saveaccount.name)
            ):
                # ok, we have a file. Should we check it or is it ok, just to add it?
                with open(
                    os.path.join(sosconstants.ACCOUNT_KEYFILE_DIR, saveaccount.name)
                ) as keyfile:
                    saveaccount.ssh_pub_key = keyfile.read()
            saveaccountlist.append(saveaccount)
        sosadminaccount = SosAccountAddModel.from_json('{"name":"sosadmin"}')
        sosadminaccount.password = spwd.getspnam("sosadmin")[1]
        saveaccountlist.append(sosadminaccount)
        # accountjson = accountjson + "]"
        accountjson = SerializerBaseJsonEncoder.to_json(saveaccountlist)
        soscore.soshelpers_file.write_to_file(
            target_dir + "/sosaccounts.json", accountjson
        )
        with tempfile.NamedTemporaryFile("wb", suffix=".tgz", delete=False) as f:
            with tarfile.open(fileobj=f, mode="w:gz") as tar:
                for name in sosconstants.CONFIGBACKUP_ITEMS:
                    if os.path.isfile(name) or os.path.isdir(name):
                        tar.add(name)
                    else:
                        logger.warning(f"file {name} not found for backup")
                tar.close()
            f.close()
    os.chdir(start_dir)
    return f.name


def backup():
    configbackup = ConfigBackupModel()
    """
    backups the configuration to /mnt/local.. and sets metadata
    :return: destfilepath of backed up config, "" if not successful
    """
    logger.debug(
        f"backup of backup config called from {sys._getframe(1).f_code.co_name}"
    )
    local_mountpath, local_params = soshelpers_file.get_mountpoint(
        sosconstants.MOUNTDEV[sos_enums.Partition.LOCAL]
    )
    if local_mountpath != sosconstants.MOUNT[sos_enums.Partition.LOCAL]:
        logger.error(
            "Local volume not mounted locally. No configuration backup possible"
        )
        return ""
    else:
        configbackup.timestamp = int(time.time())
        configbackup.hostname = (
            socket.gethostname()
        )  # TODO: should we get this from hostname core?
        soscore.soshelpers_file.write_to_file(
            sosconstants.SOS_CONF_TIMESTAMP_FILEPATH,
            json.dumps({"timestamp": configbackup.timestamp}),
        )
        archive_filepath = create_archive()
        configbackup.md5sum = soshelpers.md5Checksum(archive_filepath)

        # this is the timestamp for the filename
        file_timestamp = time.strftime(
            "%Y%m%d%H%M%S", time.gmtime(configbackup.timestamp)
        )

        # now, we know already, that the local volume is mounted, but we should also check if the destination
        # directory exists
        if not os.path.isdir(
            os.path.join(
                sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                sosconstants.SOS_CONF_BACKUP_PATH,
            )
        ):
            os.makedirs(
                os.path.join(
                    sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                    sosconstants.SOS_CONF_BACKUP_PATH,
                )
            )

        start_dir = os.getcwd()
        with tempfile.TemporaryDirectory() as target_dir:
            os.chdir(target_dir)

            # encrypt the archive
            gpg = gnupg.GPG()
            # now get the password information for encryption
            mypwdict = {}
            mypwdict = soshelpers.parse_shadowpwd_data(spwd.getspnam("sosadmin")[1])
            if mypwdict is None or "salt" not in mypwdict:
                logger.error(
                    "sosadmin passwordhash has no salt!? Aborting backupconfig",
                )
                return ""

            base64_salt = soshelpers.str2base64(mypwdict["salt"])

            manifest = ConfigBackupManifestModel()
            manifest.set(
                timestamp=file_timestamp,
                md5sum=configbackup.md5sum,
                hostname=configbackup.hostname,
                salt=base64_salt,
            )
            destfile = "content.tar.gz.gpg"

            destbackupfilepath = (
                os.path.join(
                    sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                    sosconstants.SOS_CONF_BACKUP_PATH,
                )
                + "sosconf_"
                + file_timestamp
                + "_"
                + configbackup.hostname
                + ".tgz"
            )
            soshelpers_file.write_to_file("manifest.json", manifest.to_json())
            with open(archive_filepath, "rb") as f:
                passphrase = mypwdict["salt"] + "$" + mypwdict["pwhash"]
                gpg.encrypt_file(
                    f,
                    recipients=None,
                    symmetric="AES256",
                    output=destfile,
                    passphrase=passphrase,
                    armor=False,
                )

            with open(destbackupfilepath, "wb") as f:
                with tarfile.open(fileobj=f, mode="w:gz") as tar:
                    tar.add("manifest.json")
                    tar.add(destfile)
                    tar.close()
                f.close()

            logger.info(f"written configuration backup to {destbackupfilepath}")
            os.remove(archive_filepath)
            os.remove(destfile)
            os.remove("manifest.json")
            os.chdir(start_dir)

        akt_dir = os.getcwd()
        os.chdir(
            os.path.join(
                sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                sosconstants.SOS_CONF_BACKUP_PATH,
            )
        )
        os.chdir(akt_dir)
        # some housekeeping
        keep_max_number()
        # store the metadata of the last configbackup
        soscore.soshelpers_file.write_to_file(
            sosconstants.SOS_CONF_BACKUP_METADATA, configbackup.to_json()
        )
        soshelpers_file.unset_configbackup_flag()
        return destbackupfilepath


def get():
    """
    provides actual configuration

    :return: ConfigBackupModel Note: if timestamp is 0, this means, that the configuration was not
    backupped. This is mainly because there was no local volume mounted yet.
    """
    configbackup = ConfigBackupModel()
    if configbackup.timestamp == 0:
        # ok, lets get the metadata from the actual configuration, hope there is a backup
        if os.path.isfile(sosconstants.SOS_CONF_BACKUP_METADATA):
            with open(sosconstants.SOS_CONF_BACKUP_METADATA) as file:
                configbackup = ConfigBackupModel.from_json(file.read())
                logger.debug(
                    f"found a configuration backup with md5sum: {configbackup.md5sum}"
                )
    return configbackup


def factory_reset():
    pass


class ConfigBackup:
    """
    This class handles all actions about the _actual_ sos configuration.
    The model is described in sosmodel.configbackup
    """

    def __init__(self):
        self.configbackup = ConfigBackupModel()
