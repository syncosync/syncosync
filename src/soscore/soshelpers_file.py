"""
helper functions for file io
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2022  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os
import re
from pathlib import Path
from typing import Tuple, List

from sosmodel import sosconstants
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def write_to_file(filename, data):
    """
    writes data to file
    :param filename:
    :param data:
    :return:
    """
    destpath = os.path.dirname(filename)
    if destpath != "" and not os.path.isdir(destpath):
        os.makedirs(destpath)
        logger.info(f"Created dir: {destpath}")
    filehndl = open(filename, "w")
    filehndl.write(data)
    filehndl.close()
    logger.debug(f"File {filename} written with: {data[:50]}")
    return


def del_file_if_exits(filepath):
    """deletes a file only if it exists

    Args:
        filepath (path): full path to file
    """
    if os.path.isfile(filepath):
        os.remove(filepath)


def read_from_file(filepath):
    """
    reads data from file
    :param filepath:
    :return: data read from file
    """
    if os.path.isfile(filepath):
        with open(filepath) as file:
            data = file.read()
            return data
    else:
        logger.warning(f"File '{filepath}' is not existing")
        return None


def set_configbackup_flag():
    """
    just sets a flag, that a config backup would be necessary
    :return:
    """
    if not os.path.isdir(sosconstants.RUNDIR):
        os.mkdir(sosconstants.RUNDIR)
    if not os.path.isfile(sosconstants.CONFIGBACKUP_FLAG):
        Path(sosconstants.CONFIGBACKUP_FLAG).touch()
    logger.debug("Configbackup flag is set")


def unset_configbackup_flag():
    """
    deletes the configbackup flag file
    :return:
    """
    if os.path.isfile(sosconstants.CONFIGBACKUP_FLAG):
        os.remove(sosconstants.CONFIGBACKUP_FLAG)


def check_configbackup_flag():
    """
    checks if a configbackup flag is set
    :return: True if set, False else
    """
    return os.path.isfile(sosconstants.CONFIGBACKUP_FLAG)


def get_mountpoint(subdev: str) -> Tuple[str, List[str]]:
    """Check mountpoint of the subdev

    Args:
        subdev (str): e.g. MOUNTDEV[Partition.LOCAL]

    Returns:
        Tuple[str, List[str]]: mountpoint (e.g. /mnt/local), mount options (e.g. rw)
    """
    with open("/proc/mounts") as fp:
        for line in fp:
            if line[0] == "/":
                line = line.split()
                m = re.search(f"/dev/mapper/sos-.*-{subdev}", line[0])
                if m:
                    arglist = []
                    if line[3] != "":
                        arglist = line[3].split(",")
                    return line[1], arglist
        return None, []
