import logging
import os
import sys

from soscore import configbase, soshelpers_file
from soscore.logged_test_case import LoggedTestCase
from sosmodel import sosconstants
from sosmodel.configbasemodel import ConfigBaseModel

SOS_CONF_TESTHOST = "testhost.json"
SPECIFIC_FILE = "/tmp/syncosync/testhost_specific.json"
SETBASE = "/tmp/syncosync"

logger = logging.getLogger()
logger.level = logging.DEBUG
stream_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(stream_handler)


class TestHostModel(ConfigBaseModel):
    """
    Represents a host name and a port
    """

    def __init__(self):
        self.configname = "testhost"
        self.hostname = "thostname"
        self.port = 1234
        super().__init__()


class TestHost(configbase.ConfigBase):
    def __init__(self):
        super().__init__(TestHostModel(), "testhost.json")
        self.specific_set = False

    def provide_model(self):
        return TestHostModel()

    def get_specific(self):
        config = TestHostModel().from_json(
            soshelpers_file.read_from_file(SPECIFIC_FILE)
        )
        return config

    def set_specific(self) -> bool:
        self.config: TestHostModel
        self.specific_set = True  # this is for the unit test only
        soshelpers_file.write_to_file(SPECIFIC_FILE, self.config.to_json())
        return True  # This is important


class TestHostNoStatic(configbase.ConfigBase):
    def __init__(self):
        super().__init__(TestHostModel(), "testhost.json")
        self.specific_set = False

    @staticmethod
    def provide_model():
        return TestHostModel()


class TestConfigBase(LoggedTestCase):
    def test_01_init(self):
        # clean up stuff
        if os.path.isfile("/etc/syncosync/testhost.json"):
            os.remove("/etc/syncosync/testhost.json")
        if os.path.isfile(SPECIFIC_FILE):
            os.remove(SPECIFIC_FILE)
        if not os.path.isdir("/tmp/syncosync"):
            os.mkdir("/tmp/syncosync")
        assert not os.path.exists("/etc/syncosync/testhost.json")
        newconfig = TestHostModel()
        newconfig.hostname = "foobar"
        newconfig.port = 9999
        soshelpers_file.write_to_file(
            os.path.join(SETBASE, SOS_CONF_TESTHOST), newconfig.to_json()
        )

    def test_02_get_initial(self):
        thmanager = TestHost()
        thostname = thmanager.get()
        self.assertEqual(thostname.hostname, "thostname")
        self.assertEqual(thostname.port, 1234)
        self.assertEqual(thmanager.specific_set, False)

    def test_03_set_from_json(self):
        thmanager = TestHost()
        thmanager.set_from_json('{"port":4444}')
        self.assertEqual(thmanager.specific_set, True)
        assert os.path.exists("/etc/syncosync/testhost.json")

    def test_04_get_first_set(self):
        thmanager = TestHost()
        thost = thmanager.get()
        self.assertEqual(thost.hostname, "thostname")
        self.assertEqual(thost.port, 4444)
        thost.update_from_json('{"hostname":"alice"}')
        thmanager.specific_set = False
        thmanager.set(backup=False)
        self.assertEqual(thost.hostname, "alice")
        self.assertEqual(thost.port, 4444)
        self.assertEqual(thmanager.specific_set, True)
        thost = thmanager.get()
        thmanager.set(backup=False)
        self.assertEqual(thost.hostname, "alice")

    def test_05_read_from_config(self):
        thmanager = TestHost()
        thost = thmanager.get()
        self.assertEqual(thost.hostname, "alice")
        self.assertEqual(thost.port, 4444)

    def test_06_set_from_file(self):
        thmanager = TestHost()
        thmanager.set_from_file(basepath=SETBASE)
        thost = thmanager.get()
        self.assertEqual(thost.hostname, "foobar")
        self.assertEqual(thost.port, 9999)

    def test_07_set_from_file_sosconf(self):
        newconfig = TestHostModel()
        newconfig.hostname = "joshua"
        newconfig.port = 7777
        thmanager = TestHost()
        soshelpers_file.write_to_file(
            os.path.join(sosconstants.SOS_CONF_DIR, SOS_CONF_TESTHOST),
            newconfig.to_json(),
        )
        thmanager.set_from_file()
        thost = thmanager.get()
        self.assertEqual(thost.hostname, "joshua")
        self.assertEqual(thost.port, 7777)
        diffconfig = thmanager.get_from_file(SPECIFIC_FILE)
        self.assertEqual(diffconfig.to_json(), thost.to_json())

    def test_08_change_specific_and_check(self):
        newconfig = TestHostModel()
        newconfig.hostname = "guenther"
        newconfig.port = 8888
        soshelpers_file.write_to_file(SPECIFIC_FILE, newconfig.to_json())
        thmanager = TestHost()
        thost = thmanager.get()
        self.assertEqual(thost.hostname, "guenther")
        self.assertEqual(thost.port, 8888)


class TestConfigBaseNoStatic(LoggedTestCase):
    def test_01_init(self):
        # clean up stuff
        if os.path.isfile("/etc/syncosync/testhost.json"):
            os.remove("/etc/syncosync/testhost.json")
        if os.path.isfile(SPECIFIC_FILE):
            os.remove(SPECIFIC_FILE)
        if not os.path.isdir("/tmp/syncosync"):
            os.mkdir("/tmp/syncosync")
        assert not os.path.exists("/etc/syncosync/testhost.json")
        newconfig = TestHostModel()
        newconfig.hostname = "foobar"
        newconfig.port = 9999
        soshelpers_file.write_to_file(
            os.path.join(SETBASE, SOS_CONF_TESTHOST), newconfig.to_json()
        )

    def test_02_get_initial(self):
        thmanager = TestHostNoStatic()
        thostname = thmanager.get()
        self.assertEqual(thostname.hostname, "thostname")
        self.assertEqual(thostname.port, 1234)
        self.assertEqual(thmanager.specific_set, False)

    def test_03_set_from_json(self):
        thmanager = TestHostNoStatic()
        thmanager.set_from_json('{"port":4444}')
        assert os.path.exists("/etc/syncosync/testhost.json")

    def test_04_get_first_set(self):
        thmanager = TestHostNoStatic()
        thost = thmanager.get()
        self.assertEqual(thost.hostname, "thostname")
        self.assertEqual(thost.port, 4444)
        thost.update_from_json('{"hostname":"alice"}')
        thmanager.specific_set = False
        thmanager.set(backup=False)
        self.assertEqual(thost.hostname, "alice")
        self.assertEqual(thost.port, 4444)
        thost = thmanager.get()
        thmanager.set(backup=False)
        self.assertEqual(thost.hostname, "alice")

    def test_05_read_from_config(self):
        thmanager = TestHostNoStatic()
        thost = thmanager.get()
        self.assertEqual(thost.hostname, "alice")
        self.assertEqual(thost.port, 4444)

    def test_06_set_from_file(self):
        thmanager = TestHost()
        thmanager.set_from_file(basepath=SETBASE)
        thost = thmanager.get()
        self.assertEqual(thost.hostname, "foobar")
        self.assertEqual(thost.port, 9999)

    def test_07_set_from_file_sosconf(self):
        newconfig = TestHostModel()
        newconfig.hostname = "joshua"
        newconfig.port = 7777
        thmanager = TestHostNoStatic()
        soshelpers_file.write_to_file(
            os.path.join(sosconstants.SOS_CONF_DIR, SOS_CONF_TESTHOST),
            newconfig.to_json(),
        )
        thmanager.set_from_file()
        thost = thmanager.get()
        self.assertEqual(thost.hostname, "joshua")
        self.assertEqual(thost.port, 7777)
