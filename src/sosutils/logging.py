import logging
import os
import sys
from enum import Enum
from functools import wraps
from typing import Callable, Optional

from loguru import logger
from sosmodel.sosconstants import AMOUNT_OF_ARCHIVES, LOG_ARCHIVE, LOG_ARCHIVE_BASENAME


class LoggerCategory(Enum):
    unknown = "unknown"
    soscore = "soscore"
    sosui = "sosui"
    soscli = "soscli"
    utils = "utils"
    werkzeug = "werkzeug"
    paramiko = "paramiko"


def init_logging(args):
    log_console_level_label, log_console_level_val = log_level(args.console_log_level)
    _, log_file_level = log_level(args.log_file_level)
    log_trace = log_console_level_val <= 20
    log_file_trace = log_file_level <= 10
    colorize = not args.no_log_colors

    log_fmt_time_c = "[<cyan>{time:HH:mm:ss.SS}</cyan>]"
    log_fmt_time_fs = "[<cyan>{time:MM-DD HH:mm:ss.SS}</cyan>]"
    log_fmt_id = "[<cyan>{extra[name]: >17}</cyan>]"
    log_fmt_mod_c = "[<cyan>{module: >19.19}:{line: <4}</cyan>]"
    log_fmt_mod_fs = "[<cyan>{module: >19}:{line: <4}</cyan>]"
    log_fmt_level = "[<lvl>{level: >1.1}</lvl>]"
    log_fmt_msg = "<level>{message}</level>"

    log_format_c = [
        log_fmt_time_c,
        log_fmt_id,
        log_fmt_mod_c,
        log_fmt_level,
        log_fmt_msg,
    ]
    log_format_fs = [
        log_fmt_time_fs,
        log_fmt_id,
        log_fmt_mod_fs,
        log_fmt_level,
        log_fmt_msg,
    ]
    if not args.disable_file_logging:
        log_format_c[log_format_c.index(log_fmt_time_c)] = log_fmt_time_fs
        if not log_trace:
            log_format_c.remove(log_fmt_mod_c)
        else:
            log_format_c[log_format_c.index(log_fmt_mod_c)] = log_fmt_mod_fs
    fs_log_format = " ".join(log_format_fs)
    log_format_console = " ".join(log_format_c)
    logconfig = {
        "handlers": [
            {
                "sink": sys.stdout,
                "format": log_format_console,
                "colorize": colorize,
                "level": log_console_level_val,
                "enqueue": True,
                "filter": filter_errors,
            },
            {
                "sink": sys.stderr,
                "format": log_format_console,
                "colorize": colorize,
                "level": "ERROR",
                "diagnose": log_trace,
                "backtrace": True,
                "enqueue": True,
            },
        ],
        "extra": {"name": "unknown", "identifier": ""},
    }

    # In case multiple files are to be logged to, simply append more sinks with filters accordingly
    file_logs = [
        {
            "sink": args.log_path,
            "format": fs_log_format,
            "level": log_file_level,
            "backtrace": True,
            "diagnose": log_file_trace,
            "enqueue": True,
            "encoding": "UTF-8",
        }
    ]
    log_file_retention = str(args.log_file_retention) + " days"
    for log in file_logs:
        log["retention"] = log_file_retention
        log["rotation"] = str(args.log_file_rotation)
        log["compression"] = custom_compress
    logconfig["handlers"].extend(file_logs)
    try:
        logger.configure(**logconfig)
    except ValueError:
        logger.critical("Logging parameters/configuration is invalid.")
        sys.exit(1)
    try:
        init_custom(logger)
    except Exception:
        pass
    # logger.info("Setting log level to {} ({}).", str(log_level_val), log_level_label)
    setup_loggers()


def custom_compress(filepath_to_compress: str) -> None:
    import zipfile

    archive_extension = "zip"

    rotate_old_archives(
        archive_directory=LOG_ARCHIVE,
        filename_base=LOG_ARCHIVE_BASENAME,
        extension=archive_extension,
    )

    archive_filename = "{}.{}".format(LOG_ARCHIVE_BASENAME, archive_extension)
    destination = os.path.join(LOG_ARCHIVE, archive_filename)
    # Compress to path desired
    with zipfile.ZipFile(
        destination, mode="w", compression=zipfile.ZIP_DEFLATED
    ) as f_comp:
        f_comp.write(destination, os.path.basename(filepath_to_compress))
    os.remove(filepath_to_compress)


def rotate_old_archives(archive_directory, filename_base, extension):
    if not os.path.exists(archive_directory):
        os.mkdir(archive_directory)
    # Now depending on whether the path already exists, rename the file(s) down from maximum amount
    # of records to be held.
    filename = f"{filename_base}.{extension}"
    path_out = os.path.join(archive_directory, filename)
    if not os.path.exists(path_out):
        # File not present, job done
        return
    # Rotate old compressed zip files
    rotated_count = AMOUNT_OF_ARCHIVES
    while rotated_count > 0:
        full_path = f"{path_out}.{rotated_count}"
        if rotated_count == AMOUNT_OF_ARCHIVES and os.path.exists(full_path):
            # Remove oldest file
            os.remove(full_path)
        elif 10 > rotated_count > 0:
            # rename rotated_count - 1 to rotated_count
            if rotated_count == 1:
                full_path_previous = path_out
            else:
                full_path_previous = f"{path_out}.{rotated_count - 1}"
            if os.path.exists(full_path_previous):
                os.rename(full_path_previous, full_path)
        rotated_count -= 1


def setup_loggers():
    logging.getLogger("werkzeug").setLevel(logging.DEBUG)
    logging.getLogger("werkzeug").addHandler(
        InterceptHandler(log_section=LoggerCategory.werkzeug)
    )
    logging.getLogger("paramiko").setLevel(logging.CRITICAL)
    logging.getLogger("paramiko").addHandler(
        InterceptHandler(log_section=LoggerCategory.werkzeug)
    )


def log_level(arg_log_level):
    # List has an order, dict doesn't. We need the guaranteed order to
    # determine debug level based on arg_debug_level.
    verbosity_levels = [
        ("TRACE", 5),
        ("DEBUG5", 6),
        ("DEBUG4", 7),
        ("DEBUG3", 8),
        ("DEBUG2", 9),
        ("DEBUG", 10),
        ("INFO", 20),
        ("SUCCESS", 25),
        ("WARNING", 30),
        ("ERROR", 40),
        ("CRITICAL", 50),
    ]
    # Case insensitive.
    arg_log_level = arg_log_level.upper() if arg_log_level else None
    # Easy label->level lookup.
    verbosity_map = {k.upper(): v for k, v in verbosity_levels}

    # Log level by label.
    forced_log_level = verbosity_map.get(arg_log_level, None)
    if forced_log_level:
        return arg_log_level, forced_log_level
    else:
        # Default log level if no level was specified
        return "WARNING", verbosity_map.get("WARNING")


def apply_custom(func):
    @wraps(func)
    def decorated(self, *args, **kwargs):
        log = func(self, *args, **kwargs)
        try:
            init_custom(log)
        finally:
            return log

    return decorated


def init_custom(log_out: logger):
    log_out.level("DEBUG2", no=9, color="<blue>")
    log_out.level("DEBUG3", no=8, color="<blue>")
    log_out.level("DEBUG4", no=7, color="<blue>")
    log_out.level("DEBUG5", no=6, color="<blue>")
    log_out.debug2 = lambda message, *args, **kwargs: log_out.opt(depth=1).log(
        "DEBUG2", message, *args, **kwargs
    )
    log_out.debug3 = lambda message, *args, **kwargs: log_out.opt(depth=1).log(
        "DEBUG3", message, *args, **kwargs
    )
    log_out.debug4 = lambda message, *args, **kwargs: log_out.opt(depth=1).log(
        "DEBUG4", message, *args, **kwargs
    )
    log_out.debug5 = lambda message, *args, **kwargs: log_out.opt(depth=1).log(
        "DEBUG5", message, *args, **kwargs
    )


def filter_errors(record):
    return record["level"].no < logger.level("ERROR").no


def get_bind_name(logger_type: LoggerCategory, name: Optional[str] = None) -> str:
    """Translates the logger_type into the identifier for the log message.  Specifying name forces the identifier
    to that value
    """
    if name:
        return name
    else:
        return logger_type.value


@apply_custom
def get_logger(
    logger_type: LoggerCategory,
    name: Optional[str] = None,
    filter_func: Optional[Callable] = None,
) -> logger:
    try:
        if isinstance(logger_type, LoggerCategory):
            log_id = logger_type
        else:
            log_id = LoggerCategory.unknown
    except ValueError:
        log_id = LoggerCategory.unknown
    parsed_name = get_bind_name(log_id, name)
    new_logger = logger.bind(name=parsed_name)
    if filter_func:
        new_logger.patch(filter_func)
    return new_logger


# this is being used to intercept standard python logging to loguru
class InterceptHandler(logging.Handler):
    def __init__(self, *args, **kwargs):
        try:
            self.log_section = kwargs["log_section"]
            del kwargs["log_section"]
        except KeyError:
            self.log_section = LoggerCategory.unknown
        try:
            self.log_identifier = kwargs["log_identifier"]
            del kwargs["log_identifier"]
        except KeyError:
            self.log_identifier = LoggerCategory.unknown.value
        super().__init__(*args, **kwargs)
        self.log_identifier = get_bind_name(self.log_section, None)

    def emit(self, record):
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back
            depth += 1
        with logger.contextualize(name=self.log_identifier):
            lvl: str = "DEBUG"
            if self.log_section == LoggerCategory.werkzeug:
                lvl: str = "DEBUG2"
            logger.opt(depth=depth, exception=record.exc_info).log(
                lvl, record.getMessage()
            )
