# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import EraseDiskLevel
from sosmodel.sosadmin_passwd import SosAdminPassWordModel


class FactoryResetModel(SerializerBase):
    """
    Passed back for lead/follow determination of UI elements
    """

    # In order to make sure the current administrator as well as the intention of really factory resetting the device
    _password_check: SosAdminPassWordModel

    # Whether or not networking (i.e., a static configuration) should be reset as well
    _reset_networking: bool

    # Whether disks should be erased or not - and if so, what security level of erasure is needed
    _erase_disk: EraseDiskLevel

    def __init__(self):
        super().__init__()
        self._password_check = SosAdminPassWordModel()
        self._reset_networking = False
        self._erase_disk: EraseDiskLevel = EraseDiskLevel.NO_ERASE

    @property
    def reset_networking(self) -> bool:
        return self._reset_networking

    @reset_networking.setter
    def reset_networking(self, value: bool) -> None:
        self._reset_networking = value

    @property
    def password_check(self) -> SosAdminPassWordModel:
        return self._password_check

    @password_check.setter
    def password_check(self, value: SosAdminPassWordModel) -> None:
        self._password_check = value

    @property
    def erase_disk(self) -> EraseDiskLevel:
        return self._erase_disk

    @erase_disk.setter
    def erase_disk(self, value: EraseDiskLevel) -> None:
        self._erase_disk = value
