"""
Model for synchronisation state (and mucho more)
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from typing import List

from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import (
    RemoteUpStatus,
    SyncstatStatus,
    SyncStatus,
    SystemMode,
    SystemStatus,
)


class DriveSpaceModel(SerializerBase):
    """
    Holding information about local drive
    """

    total_size: int
    free_space: int
    iused: int

    def __init__(self, total_size: int = 0, free_space: int = 0, iused: int = 0):
        # total size of local volume in bytes
        self.total_size = total_size
        # free space on local volume in bytes
        self.free_space = free_space
        # inodes used (dunno why this is necessary, I think for calculation of new to sync files)
        self.iused = iused

    def __eq__(self, other):
        return (
            self.total_size == other.total_size
            and self.free_space == other.free_space
            and self.iused == other.iused
        )


class SyncStatModel(SerializerBase):
    """
    The model holding all necessary information to show the actual sync status
    """

    # Overall status
    syncstat_status: SyncstatStatus
    # System status
    sysstate: SystemStatus
    # Actual mode
    actmode: SystemMode
    # is the remote host reachable or not?
    remote_up: RemoteUpStatus
    # and, since when it is down?
    remote_down_since: int
    # is there any data uploaded to remote host?
    syncup: SyncStatus
    # and when has the state changed?
    syncup_change_since: int
    # is there any data arriving from remote host?
    syncdown: SyncStatus
    # and when has this state changed last time?
    syncdown_change_since: int
    # Bandwidth upstream (to remote host) in bytes/sec
    bw_up: float
    # array of drives, which are up and running ("sda", "sdb")
    driveup: List[str]
    # Bandwidth downstream (from remote host) in bytes/sec
    bw_down: float
    # the timestamp (as of int(time.time())) of this status
    date: int
    # total size of local volume in bytes
    total_size: int
    # free space on local volume in bytes
    free_space: int
    # inodes used (dunno why this is necessary, I think for calculation of new to sync files)
    iused: int
    # files which have to be checked for rsync
    to_check: int
    # file name of actual file (informative)
    akt_file: str
    # how much of the akt_file is transferred in %
    percent: int
    # time stamp, when the last admin mail was set
    admin_last_mail: int
    # is a config backup necessary?
    configbackup_needed: bool
    # is traffic shaping on (day mode)?
    day: bool
    # are the drives correctly mounted
    mounthealth: bool

    def __init__(self):
        self.syncstat_status = SyncstatStatus.NOT_AVAILABLE
        self.sysstate = SystemStatus.NOTHING
        self.actmode = SystemMode.STARTUP
        self.remote_up = RemoteUpStatus.NOT_AVAILABLE
        self.remote_down_since = 0
        self.syncup = SyncStatus.NOT_AVAILABLE
        self.syncup_change_since = 0
        self.syncdown = SyncStatus.NOT_AVAILABLE
        self.syncdown_change_since = 0
        self.bw_up = 0
        self.driveup = []
        self.bw_down = 0
        self.date = 0
        self.total_size = 0
        self.free_space = 0
        self.iused = 0
        self.to_check = 0
        self.akt_file = ""
        self.percent = 0
        self.admin_last_mail = 0
        self.configbackup_needed = False
        self.day = False
        self.mounthealth = True


class BandWidthModel(SerializerBase):
    """
    The model holding bandwidth information at a dedicated point of time.
    """

    # Bandwidth upstream (to remote host) in bytes/sec
    bw_up: float
    # Bandwidth downstream (from remote host) in bytes/sec
    bw_down: float
    # the timestamp (as of int(time.time())) of this status
    date: int

    def __init__(self, bw_up: int = 0, bw_down: int = 0, date: int = 0):
        self.bw_up = bw_up
        self.bw_down = bw_down
        self.date = date
