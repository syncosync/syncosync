"""
Model for the trafficshaping
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from sosmodel.configbasemodel import ConfigBaseModel
from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import TrafficShapeSwitch
from sosmodel.sosconstants import MAX_BW


class TrafficShapeModel(ConfigBaseModel):
    """
    Represents the traffic shape settings
    """

    _out_day: int
    _in_day: int
    _out_night: int
    _in_night: int
    _day: int
    _night: int
    _maxout: int
    _maxin: int

    def __init__(self):
        super().__init__()
        self.configname = "trafficshape"
        # values in mbit
        self._out_day = 10000  # 10 gbit should be fine at this time
        self._in_day = 10000  # 10 gbit should be fine at this time
        self._out_night = 10000  # 10 gbit should be fine at this time
        self._in_night = 10000  # 10 gbit should be fine at this time
        self._day = 360  # starts at 6am
        self._night = 0  # starts at 0am
        self._maxout = 10000  # should be set to the internet connection, 10 gbit should be fine at this time
        self._maxin = 10000  # should be set to the internet connection, 10 gbit should be fine at this ti

    def set(
        self,
        out_day: int,
        in_day: int,
        out_night: int,
        in_night: int,
        day: int,
        night: int,
        maxout: int,
        maxin: int,
    ) -> None:
        self.out_day = out_day
        self.in_day = in_day
        self.out_night = out_night
        self.in_night = in_night
        self.day = day
        self.night = night
        self.maxout = maxout
        self.maxin = maxin

    @property
    def out_day(self) -> int:
        return self._out_day

    @out_day.setter
    def out_day(self, value: int) -> None:
        """
        sets the outgoing value
        :param 0 > outgoing > MAX_BW
        """
        if value > MAX_BW or value < 1:
            raise ValueError("Invalid value for outgoing")
        self._out_day = value

    @property
    def in_day(self) -> int:
        return self._in_day

    @in_day.setter
    def in_day(self, value: int) -> None:
        """
        sets the incoming value
        :param 0 > incoming > MAX_BW
        """
        if value > MAX_BW or value < 1:
            raise ValueError("Invalid value for incoming")
        self._in_day = value

    @property
    def out_night(self) -> int:
        return self._out_night

    @out_night.setter
    def out_night(self, value: int) -> None:
        """
        sets the outgoing value
        :param 0 > outgoing > MAX_BW
        """
        if value > MAX_BW or value < 1:
            raise ValueError("Invalid value for outgoing")
        self._out_night = value

    @property
    def in_night(self) -> int:
        return self._in_night

    @in_night.setter
    def in_night(self, value: int) -> None:
        """
        sets the incoming value
        :param 0 > incoming > MAX_BW
        """
        if value > MAX_BW or value < 1:
            raise ValueError("Invalid value for incoming")
        self._in_night = value

    @property
    def day(self) -> int:
        return self._day

    @day.setter
    def day(self, value: int) -> None:
        """
        sets the day start time in minutes since 0:00
        :param 0 > outgoing > 24*60
        """
        if value > (24 * 60) or value < 0:
            raise ValueError("Invalid value for day")
        self._day = value

    @property
    def night(self) -> int:
        return self._night

    @night.setter
    def night(self, value: int) -> None:
        """
        sets the day start time in minutes since 0:00
        :param 0 > outgoing > 24*60
        """
        if value > (24 * 60) or value < 0:
            raise ValueError("Invalid value for night")
        self._night = value

    @property
    def maxout(self) -> int:
        return self._maxout

    @maxout.setter
    def maxout(self, value: int) -> None:
        """
        sets the maxout value
        :param 0 > maxout > MAX_BW
        """
        if value > MAX_BW or value < 1:
            raise ValueError("Invalid value for maxout")
        self._maxout = value

    @property
    def maxin(self) -> int:
        return self._maxin

    @maxin.setter
    def maxin(self, value: int) -> None:
        """
        sets the maxin value
        :param 0 > maxin > MAX_BW
        """
        if value > MAX_BW or value < 1:
            raise ValueError("Invalid value for maxin")
        self._maxin = value


class TrafficShapeSpecificModel(SerializerBase):
    """
    This holds the specific settings and caches it for other access
    """

    outgoing: int
    incoming: int
    day: bool

    def __init__(self):
        self.outgoing = 10000
        self.incoming = 10000
        self.day = True


class TrafficShapeSwitchModel(SerializerBase):
    """
    Used for switching between day/night/off
    """

    mode: TrafficShapeSwitch

    def __init__(self):
        self.mode = TrafficShapeSwitch.DAY
