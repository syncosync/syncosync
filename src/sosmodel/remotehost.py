"""
This is the model for the remote host, the one to which the local data will be synced
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from sosmodel.configbasemodel import ConfigBaseModel


class RemoteHostModel(ConfigBaseModel):
    """
    Represents the remote host attributes
    """

    ## The hostname of the remote host
    _hostname = ""
    ## The remote port, defaults to 55055. This is also the listening port for incoming requests from the remote host.
    _port = ""

    def __init__(self, hostname: str = "", port: str = "55055"):
        super().__init__()
        self.configname = "remotehost"
        self.hostname = hostname
        self.port = port

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, value):
        self._hostname = value

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, value):
        self._port = value
