from enum import IntEnum

from sosmodel.serializerbase import SerializerBase


class NicMode(IntEnum):
    DHCP = 0
    STATIC = 1
    AUTO = 2  # this method is only for inet6 relevant
    MANUAL = 3  # this is basically "off"


class NicConfigModel(SerializerBase):
    """
    Represents a single NIC of the system
    """

    def __init__(
        self,
        interface=None,
        ip4_mode=NicMode.DHCP,
        ip6_mode=NicMode.MANUAL,
        ip4_address=None,
        ip4_netmask=None,
        ip4_gateway=None,
        ip6_address=None,
        ip6_netmask=None,
        ip6_gateway=None,
        linkspeed="0",
        wlan=False,
    ):
        self.interface = interface
        # self.enabled = enabled # this should not be necessary -> manual mode
        self.ip4_mode = ip4_mode  # this is the inet (ipv4) mode (dhcp, static, manual)
        self.ip6_mode = ip6_mode  # this is the inet6 mode (auto, dhcp, static, manual)
        self.ip4_address = ip4_address
        self.ip4_netmask = ip4_netmask
        self.ip4_gateway = ip4_gateway
        self.ip6_address = ip6_address
        self.ip6_netmask = ip6_netmask
        self.ip6_gateway = ip6_gateway
        self.linkspeed = linkspeed
        self.wlan = wlan

    def is_valid(self):
        # TODO implement
        pass

    def __eq__(self, other):
        # this is necessary as linkspeed is dynamic
        return (
            self.interface == other.interface
            and self.ip4_mode == other.ip4_mode
            and self.ip6_mode == other.ip6_mode
            and self.ip4_address == other.ip4_address
            and self.ip4_netmask == other.ip4_netmask
            and self.ip4_gateway == other.ip4_gateway
            and self.ip6_address == other.ip6_address
            and self.ip6_netmask == other.ip6_netmask
            and self.ip6_gateway == other.ip6_gateway
            and self.wlan == other.wlan
        )

    def __ne__(self, other):
        # this is necessary as linkspeed is dynamic
        return (
            self.interface != other.interface
            or self.ip4_mode != other.ip4_mode
            or self.ip6_mode != other.ip6_mode
            or self.ip4_address != other.ip4_address
            or self.ip4_netmask != other.ip4_netmask
            or self.ip4_gateway != other.ip4_gateway
            or self.ip6_address != other.ip6_address
            or self.ip6_netmask != other.ip6_netmask
            or self.ip6_gateway != other.ip6_gateway
            or self.wlan != other.wlan
        )
