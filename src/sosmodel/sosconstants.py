"""
a lot of constants needed in the sos world
"""
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2022  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os.path
import textwrap

from sosmodel.sos_enums import MailInfoLevels, Partition, Sshd, SshdAccess

# Logging
LOGFILE_BASENAME = "syncosync.log"
LOGFILE_DESTINATION = "/tmp/syncosync/"
LOG_ARCHIVE = "/mnt/system/logs/"
LOG_ARCHIVE_BASENAME = "syncosync"
# Amount of compressed logs to keep
AMOUNT_OF_ARCHIVES = 10

# the config file version - should we need it ever
SOS_VERSION = "1"

# config related
SPECIFIC_GET_TIME = (
    60  # time in to wait to check if a service has changed in it's specific config
)
LSYNCD_CONF_FILE = "/etc/lsyncd/lsyncd.conf.lua"
WPASUPPLICANT_FILE = "/etc/wpa_supplicant/wpa_supplicant.conf"
SOS_CONF_DIR_RELATIVE = "etc/syncosync/"
SOS_CONF_DIR = os.path.join("/", SOS_CONF_DIR_RELATIVE)
SOS_CONF_GENERAL = "sosconfig.json"
SOS_CONF_HOSTNAME = "hostname.json"
SOS_CONF_WIFI = "wifi.json"
SOS_CONF_TIMEZONE = "timezone.json"
SOS_CONF_NAMESERVER = "nameserver.json"
SOS_CONF_NICCONFIG = "nicconfig.json"
SOS_CONF_MAILCONFIG = "mailconfig.json"
SOS_CONF_REMOTEHOST = "remotehost.json"
SOS_CONF_TRAFFICSHAPE = "trafficshape.json"
SOS_CONF_REMOTEHOST_SSD = "remotehost_systemsetupdata.json"
SOS_CONF_STOREDMODE = os.path.join(SOS_CONF_DIR, "storedmode.json")
SOS_CONF_TIMESTAMP = "last_configure_time.json"
SOS_CONF_TIMESTAMP_FILEPATH = os.path.join(SOS_CONF_DIR, SOS_CONF_TIMESTAMP)
SOS_CONF_TIMESTAMP_FILEPATH_RELATIVE = os.path.join(
    SOS_CONF_DIR_RELATIVE, SOS_CONF_TIMESTAMP
)
# This is where my own keys are
SOS_KEYFILE_DIR = os.path.join(SOS_CONF_DIR, "sshkeys")
# syncosync private key
SOS_KEYFILE = os.path.join(SOS_KEYFILE_DIR, "syncosync")
# syncosync public key
SOS_KEYFILE_PUB = os.path.join(SOS_KEYFILE_DIR, "syncosync.pub")
# This is where the pub key from the remote host get's copied to after it is accepted
REMOTE_KEYFILE_DIR = os.path.join(SOS_CONF_DIR, "remotekeys")
# This is the remote public key itself
REMOTE_KEYFILE_PUB = os.path.join(REMOTE_KEYFILE_DIR, "syncosync.pub")
# This is the directory where all account keys are stored
ACCOUNT_KEYFILE_DIR = os.path.join(SOS_CONF_DIR, "accountkeys")
# The relative path for storing config backups
SOS_CONF_BACKUP_PATH = "syncosync/syncosync/configbackup/"
# number of config backups we should store
SOS_CONF_BACKUP_MAX = 10
# The metadata of the last stored configuration backup
SOS_CONF_BACKUP_METADATA = os.path.join(SOS_CONF_DIR, "configbackup.json")
# sshd related
# All the sshd config files
SSHD_CFG = dict()
SSHD_CFG[Sshd.INTRANET, SshdAccess.CLOSE] = os.path.join(
    SOS_CONF_DIR, "sshd_config_intranet_close"
)
SSHD_CFG[Sshd.INTRANET, SshdAccess.OPEN] = os.path.join(
    SOS_CONF_DIR, "sshd_config_intranet_open"
)
SSHD_CFG[Sshd.EXTRANET, SshdAccess.CLOSE] = os.path.join(
    SOS_CONF_DIR, "sshd_config_extranet_close"
)
SSHD_CFG[Sshd.EXTRANET, SshdAccess.OPEN] = os.path.join(
    SOS_CONF_DIR, "sshd_config_extranet_open"
)

# The network related items
CONFIGRESTORE_ITEMS_NETWORK = [
    "/etc/network/interfaces.d",
    os.path.join(SOS_CONF_DIR, SOS_CONF_HOSTNAME),
    os.path.join(SOS_CONF_DIR, SOS_CONF_TIMEZONE),
    os.path.join(SOS_CONF_DIR, SOS_CONF_NAMESERVER),
    os.path.join(SOS_CONF_DIR, SOS_CONF_REMOTEHOST),
    os.path.join(SOS_CONF_DIR, SOS_CONF_TRAFFICSHAPE),
]
CONFIGRESTORE_ITEMS = [
    os.path.join(SOS_CONF_DIR, SOS_CONF_GENERAL),
    os.path.join(SOS_CONF_DIR, SOS_CONF_MAILCONFIG),
    SOS_KEYFILE_DIR,
    REMOTE_KEYFILE_DIR,
    ACCOUNT_KEYFILE_DIR,
    SOS_CONF_TIMESTAMP_FILEPATH,
    SSHD_CFG[Sshd.INTRANET, SshdAccess.CLOSE],
    SSHD_CFG[Sshd.INTRANET, SshdAccess.OPEN],
    SSHD_CFG[Sshd.EXTRANET, SshdAccess.CLOSE],
    SSHD_CFG[Sshd.EXTRANET, SshdAccess.OPEN],
]
CONFIGBACKUP_ITEMS = (
    CONFIGRESTORE_ITEMS + CONFIGRESTORE_ITEMS_NETWORK + ["sosaccounts.json"]
)

CONFIGBACKUP_MANIFEST_FILE = "configbackup.json"

MAX_BW = 10000  # 10000 mbit
# SOS keyxchange
SOS_KEYX_HOME = "/home/soskeyx"
# this is where a remote key gets copied to before it is accepted
SOS_KEYX_PUBKEY = os.path.join(SOS_KEYX_HOME, "syncosync.pub")
SOS_KEYX_SSDPATH_IN = os.path.join(SOS_KEYX_HOME, "systemsetupdata_in.json")
SOS_KEYX_SSDPATH_OUT = os.path.join(SOS_KEYX_HOME, "systemsetupdata_out.json")
SOS_KEYX_TIMEOUT = 60  # so long we will wait while receiving - and sending

# the temp files - mainly for status sharing/caching
RUNDIR = "/tmp/syncosync/"  # this is, where all cache files go, could be run or tmp, tmp is also user writeable
# holds the uuid for the external ssh banner
SSH_EXTERNAL_UUID = "/mnt/rjail/uuid"
# some files could go to the HDD, so they are persistent, because they are very complicated to regenerate
# it should be considered only to place files on HDD, which will be written only, when HDD is up
# also: those files should not be written, when disk is not mounted normal
HDDSUBDIR = "syncosync"
HDDRUNDIR = "/mnt/local/syncosync/syncosync"
SOSACCOUNTACCESS_CACHE = os.path.join(
    RUNDIR, "sosaccounts-access.json"
)  # this is, where the last access json is cached
SOSACCOUNTACCESS_PERSISTENT = os.path.join(
    HDDRUNDIR, "sosaccounts-access.json"
)  # this is, where the last access json is written
ACTSTATE_FILE = os.path.join(RUNDIR, "sysstate.json")
LAST_RESULT = os.path.join(RUNDIR, "result.json")
TRAFFICSHAPE_SPECIFIC_CACHE = os.path.join(RUNDIR, "trafficshape_specific_cache.json")
SOSTOCHECK_CACHE = os.path.join(
    RUNDIR, "sostocheck.json"
)  # this is the rsyncwrapperscript cache file
RRTOCHECK_CACHE = os.path.join(HDDRUNDIR, "rrtocheck.json")
RR_RESULT = os.path.join(HDDRUNDIR, "result.json")
SOSACCOUNTSPACE_CACHE = os.path.join(
    RUNDIR, "sosaccounts-space.json"
)  # this is, where the space json is cached
SOSACCOUNTMAIL_CACHE = os.path.join(
    RUNDIR, "sosaccounts-mail.json"
)  # this is, where the last mail json is cached
SOSACCOUNTMAIL_PERSISTENT = os.path.join(
    HDDRUNDIR, "sosaccounts-mail.json"
)  # this is, where the last mail json is
# cached
DRIVESTATUS_CACHE = os.path.join(RUNDIR, "bootstatus.json")  # bootstatus is cached here
DRIVESPACE_CACHE = os.path.join(
    RUNDIR, "space.json"
)  # this is, where the last access json is cached
SYNCSTAT_CACHE = os.path.join(
    RUNDIR, "syncstat.json"
)  # this is, where the last access json is cached
CONFIGBACKUP_FLAG = os.path.join(
    RUNDIR, "configbackup"
)  # if exists, configbackup should be necessary

# static stuff
STATIC_BASE_DIR = "/usr/share/syncosync/"
STATIC_MANUAL_DIR = "/usr/share/syncosync-manuals/"
SOS_SYSTEM_DESCRIPTIONS = os.path.join(STATIC_BASE_DIR, "system-descriptions/")
RECORD_DIR = os.path.join(
    STATIC_BASE_DIR, "demodata/"
)  # this is used from the recording script for simulation
SOSACCOUNT_GROUP_NAME = "sosaccount"
SOSADMIN_USER = "sosadmin"
FACTORY_CONF_DIR = os.path.join(STATIC_BASE_DIR, "configs/")
STATIC_LSYNCD_CONF = os.path.join(FACTORY_CONF_DIR, "lsyncd.conf.lua")

# key related stuff
SOS_KEYX_DIR = os.path.join(STATIC_BASE_DIR, "keyxkeys")
SOSKEYX_KEYFILE = os.path.join(SOS_KEYX_DIR, "soskeyx")
SOSKEYX_KEYFILE_PUB = os.path.join(SOS_KEYX_DIR, "soskeyx.pub")

# volumes and mounts
SOS_PREFIX = "sos"
SOS_VOLUMES = [Partition.LOCAL, Partition.REMOTE, Partition.SYSTEM]
MOUNTDEV = {}
MOUNT = {}
MOUNTOPTS = {}
# This is for the local partition
MOUNTDEV[Partition.LOCAL] = "local"
MOUNT[Partition.LOCAL] = "/mnt/local"
MOUNTOPTS[Partition.LOCAL] = ["usrjquota=aquota.user","jqfmt=vfsv0"]
# This is for the remote partition
MOUNTDEV[Partition.REMOTE] = "remote"
MOUNT[Partition.REMOTE] = "/mnt/rjail/remote"
MOUNTOPTS[Partition.REMOTE] = []
# This is for the system partition
MOUNTDEV[Partition.SYSTEM] = "system"
MOUNT[Partition.SYSTEM] = "/mnt/system"
MOUNTOPTS[Partition.SYSTEM] = []
# some numbers
SYSTEM_MIN_SIZE = 10  # this is counted in extends 4MB blocks = 40MB
SYSTEM_RECOMMEND_SIZE = 2500  # in 4MB blocks, should be 10 gigs
# some timings - mainly for syncstat
CHECK_TIME_TRUE = (
    600  # if the remote host was available last time, check only every ten minutes
)
CHECK_TIME_FALSE = 30  # if the remote host was down, check every ten seconds
SYNCSTAT_REFRESH_TIME = (
    5  # update syncstat every 5 seconds (note: this is not remote host check!)
)
DU_WARN_LEVEL = 90  # warns if usage is 10% below max
DU_ERR_LEVEL = 98  # warns if disk usage is 2% below max

MAIL_INFO_LEVELS = [
    MailInfoLevels.NONE,
    MailInfoLevels.INFO,
    MailInfoLevels.WARNING,
    MailInfoLevels.ERROR,
]

# FIXME: is there some better way to implement this?
# enum for error replies
emessage = dict()
EFALSE = "EFALSE"
emessage[EFALSE] = "not successful"
ETRUE = "ETRUE"
emessage[ETRUE] = "host is up and key is fine"
ENOREPLY = "ENOREPLY"
emessage[ENOREPLY] = "host is not reachable (wrong hostname or port?)"
ENA = "ENA"
emessage[ENA] = "not available (hostname is not set?)"
ENOSSH = "ENOSSH"
emessage[ENOSSH] = "no ssh service on this port (wrong port?)"
EBADKEY = "EBADKEY"
emessage[EBADKEY] = "no private key or wrong private key"
ACTIVE = "ACTIVE"
emessage[ACTIVE] = "there is an active session"
IDLE = "IDLE"
emessage[IDLE] = "idle"

# enum for content for harddisks
content = dict()
NO_PARTITION = "NO_PARTITION"
content[NO_PARTITION] = "Invalid/empty"

# Path to chroot environment
CHROOT_GEN_ENV = "/var/lib/syncosync/chroot"

# How many bandwidth values we should store in the syncstat object?
# if we store one value per 5 seconds, this is 10 min.
BW_BUFFER_SIZE = 120

# Housekeeper related paths
trans_place = "/usr/share/syncosync/LANG/"
HTML_MAIL_FILE = os.path.join(STATIC_BASE_DIR, "mail/mail.html")
SOS_HK_FILE = "/etc/syncosync/housekeeper.json"
LAST_MAIL_CACHE = "/etc/syncosync/lastmail.json"


def get_version() -> str:
    """
    gets version string of sos
    :return: str, e.g. 0.2-20200611124900
    """
    if os.path.isfile(os.path.join(STATIC_BASE_DIR, "BASEVERSION")):
        version_file = open(os.path.join(STATIC_BASE_DIR, "BASEVERSION"))
        version = version_file.read().strip()
    else:
        version = "unknown"
    if os.path.isfile(os.path.join(STATIC_BASE_DIR, "VERSIONTIMESTAMP")):
        version_file = open(os.path.join(STATIC_BASE_DIR, "VERSIONTIMESTAMP"))
        version = version + "-" + version_file.read().strip()
    else:
        version = version + "-unknown"
    return version


EPILOG = textwrap.dedent(
    f"""\
                            Version {get_version()}, part of:
                            syncosync - secure peer to peer backup synchronization
                            Copyright (C)2021-2022 syncosync.org
                            """
)

# Flask Cookie IDs
COOKIE_LANGUAGE = "language"
