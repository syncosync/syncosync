from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import PartitioningState


class PartitioningStatus(SerializerBase):
    overall_status: PartitioningState

    def __int__(self):
        self.overall_status = PartitioningState.RESIZE_NOT_POSSIBLE
