"""
This is the model for configuration backups
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import List, Optional

from sosmodel.remotehost import RemoteHostModel
from sosmodel.serializerbase import SerializerBase
from sosmodel.sosaccount import SosAccountAddModel


class ConfigBackupModel(SerializerBase):
    """
    Holds the attributes to identify a configuration backup file
    """

    # timestamp
    # This is the timestamp when the backup was done. This is the integer part of time.time(), so, seconds since
    # epoch (January 1, 1970, 00:00:00 (UTC)) or Unix time
    timestamp: int

    # hostname
    # this is the hostname of the system at the time the backup was made
    # so the hostname of the actual machine could be different, but there is no reason to change it in the metadata
    hostname: str

    # filepath
    # this is the complete path and filename of the file
    filepath: str

    # md5sum
    # this is the md5sum of the payload (should be derived from manifest)
    md5sum: str

    def __init__(self):
        self.timestamp = 0
        self.hostname = ""
        self.filepath = ""
        self.md5sum = ""


class ConfigBackupContentModel(SerializerBase):
    sosaccounts: List[SosAccountAddModel]
    hostname: Optional[str]
    remotehost: Optional[RemoteHostModel]
    """
    Holds all models which are of interest to be shown in an info dialog
    """

    def __init__(self):
        self.sosaccounts: List[SosAccountAddModel] = []
        self.hostname = None
        self.remotehost = None


class ConfigBackupManifestModel(SerializerBase):
    """
    The Manifest File to hold along with the backups
    """

    timestamp: int
    md5sum: str
    hostname: str
    salt: str

    def __init__(self):
        self.timestamp = 0
        self.md5sum = ""
        self.hostname = ""
        self.salt = ""

    def set(self, timestamp: int, md5sum: str, hostname: str, salt: str):
        self.timestamp = timestamp
        self.md5sum = md5sum
        self.hostname = hostname
        self.salt = salt
