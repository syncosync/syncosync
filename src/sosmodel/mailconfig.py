"""
This is the model for the syncosync configuration
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from sosmodel.configbasemodel import ConfigBaseModel
from sosmodel.sos_enums import MailAuthenticationType, MailInfoLevels, MailSslType


class MailConfigModel(ConfigBaseModel):
    # The sender name to be displayed as the sender name
    sender_name: str

    # Mail FROM to be displayed as the sending mail address
    mail_from: str

    # The mail address of the admin to send reports etc to
    admin_mail_address: str

    # The mail address of the remote admin
    remote_admin_mail_address: str

    # the time in days between the reception of consecutive warning/error mails for the admin
    admin_info_period: int

    # smtp_host or smart host
    smtp_host: str

    # the user on this host
    smtp_user: str

    # and the password for this user
    smtp_passwd: str

    # the port (defaults to 465)
    smtp_port: int

    # SSL Type to be used
    ssl_type: MailSslType

    # Password authentication method
    authentication_type: MailAuthenticationType

    # Mail Info Level (like sosaccounts)
    mail_info_level: MailInfoLevels

    # System Mails (send also mails for e.g. autoupdates)
    system_mails: bool

    def __init__(self):
        super().__init__()
        self.configname = "mailconfig"
        self.sender_name = ""
        self.mail_from = ""
        self.admin_mail_address = ""
        self.remote_admin_mail_address = ""
        self.admin_info_period = 7
        self.remote_admin_info_period = 1
        self.smtp_host = ""
        self.smtp_user = ""
        self.smtp_passwd = ""
        self.smtp_port = 465
        self.ssl_type = MailSslType.SSL_TLS
        self.authentication_type = MailAuthenticationType.PASSWORD_NORMAL
        self.mail_info_level = MailInfoLevels.INFO
        self.system_mails = True