import unittest

from sosmodel.serializerbase import SerializerBaseJsonEncoder
from sosmodel.sosaccount import SosAccountAddModel


class SosAccountAddModelSerializationTest(unittest.TestCase):
    def test_list_serialize(self):
        list_of_add = []
        acc1: SosAccountAddModel = SosAccountAddModel()
        acc1.name = "test1"
        list_of_add.append(acc1)
        acc2: SosAccountAddModel = SosAccountAddModel()
        acc2.name = "test2"
        list_of_add.append(acc2)
        accountjson = SerializerBaseJsonEncoder.to_json(list_of_add)
        print(accountjson)
        self.assertEqual(
            accountjson,
            """[{"backup_period": 7, "info_period": 7, "is_password_set": false, "key_fingerprint": "", "last_access": 0, "last_mail": 0, "mail_address": "", "mail_info_level": 2, "max_space_allowed": 100, "name": "test1", "password": null, "remind_duration": 22, "rlongname": "", "space_used": 0, "ssh_pub_key": null}, {"backup_period": 7, "info_period": 7, "is_password_set": false, "key_fingerprint": "", "last_access": 0, "last_mail": 0, "mail_address": "", "mail_info_level": 2, "max_space_allowed": 100, "name": "test2", "password": null, "remind_duration": 22, "rlongname": "", "space_used": 0, "ssh_pub_key": null}]""",
        )

    # def test_bad_name(self):
    #     acc1: SosAccountAddModel = SosAccountAddModel()
    #     acc1.name = "+name"
    #     self.failUnlessRaises(ValueError)


if __name__ == "__main__":
    unittest.main()
