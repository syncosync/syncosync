# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import SupportedLanguage


class UiLanguage(SerializerBase):
    _language: SupportedLanguage

    def __init__(self):
        self._language = SupportedLanguage.UNSET

    @property
    def language(self) -> SupportedLanguage:
        return self._language

    @language.setter
    def language(self, value: SupportedLanguage) -> None:
        self._language = value
