# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:##www.gnu.org/licenses/>.
from enum import IntEnum
from typing import Optional


class VolMount(IntEnum):
    # fmt: off
    NONE = 0
    INCOMPLETE = 1
    NORMAL = 2
    SWAP = 3
    # fmt: on


class NicMode(IntEnum):
    # fmt: off
    DHCP = 0
    STATIC = 1
    # v6 only
    AUTO = 2
    # off
    MANUAL = 3
    # fmt: on


class SupportedLanguage(IntEnum):
    UNSET = 0
    ENGLISH = 1
    GERMAN = 2

    @staticmethod
    def to_iso(
        language: "SupportedLanguage",
        fallback_if_unset: Optional["SupportedLanguage"] = None,
    ) -> str:
        """
        Converts the given language (SupportedLanguage value) to an ISO 639.1 or 639.2 code.
        See https://www.loc.gov/standards/iso639-2/php/code_list.php
        """

        iso_codes = {0: "en-US", 1: "en-US", 2: "de"}

        if language not in iso_codes:
            return "" if not fallback_if_unset else iso_codes[fallback_if_unset]
        else:
            return iso_codes[language]

    @staticmethod
    def to_iso_core(
        language: "SupportedLanguage",
        fallback_if_unset: Optional["SupportedLanguage"] = None,
    ) -> str:
        """
        Converts the given language (SupportedLanguage value) to ISO for core/weblate usage
        """

        iso_codes = {0: "en", 1: "en", 2: "de"}
        if language not in iso_codes:
            return "" if not fallback_if_unset else iso_codes[fallback_if_unset]
        else:
            return iso_codes[language]


class RemoteUpStatus(IntEnum):
    """
    Handling the status for checking remote hosts
    """

    # not available (system not in right mode to check?)
    NOT_AVAILABLE = 0
    # host is not reachable (wrong hostname or port?)
    NO_REPLY = 1
    # not successful, may be an error we did not account for
    FAULTY = 2
    # host is up and key is fine
    UP = 3
    # No SSH session present
    NO_SSH = 4
    # SSH key file is corrupted or the remote host rejected it
    BAD_KEY = 5
    # sshd is started but is closed
    HOST_CLOSED = 6
    # hostname is not set
    NOT_SET = 7

    @staticmethod
    def message(reason):
        if reason == RemoteUpStatus.NOT_AVAILABLE:
            return "Status is not available (Not set, Not right mode)"
        if reason == RemoteUpStatus.NO_REPLY:
            return "Remote host is not reachable (this is no reply on this port from this host)"
        if reason == RemoteUpStatus.FAULTY:
            return "Unknown error"
        if reason == RemoteUpStatus.UP:
            return "Remote host is up and key is correct"
        if reason == RemoteUpStatus.NO_SSH:
            return "No ssh session present"
        if reason == RemoteUpStatus.BAD_KEY:
            return "ssh key is not accepted"


class SyncStatus(IntEnum):
    # not available (hostname is not set?)
    NOT_AVAILABLE = 0
    # there is an active session / synchronization ongoing
    ACTIVE = 1
    # Connection is idling, no active synchronization
    IDLE = 2


class SyncstatStatus(IntEnum):
    # not available
    NOT_AVAILABLE = 0
    # available
    AVAILABLE = 1


class SystemStatus(IntEnum):
    """system states - ui and other tools may decide on these states what to do"""

    NOTHING = 0  # Nothing to report
    DETECTION = 1  # Detection on its way
    # The drive detection led to a situation where the UI should request interaction
    DISK_SETUP_NEED = 2
    SOS_VOLUMES_NOT_MOUNTED = 4  # The sosvolumes are not mounted
    SOS_KEY_NOT_GENERATED = 8  # syncosync key is not there
    REMOTE_HOST_NOT_CONFIGURED = 16  # either host is not set or key is not set
    # there is no metadata of save backup, most probably, the system is unconfigured
    SOS_CONF_BACKUP_NOT_DONE = 32
    # the metadata of actual config differs with last metadata on local volume
    SOS_CONF_BACKUP_NOT_MATCH = 64
    INTRANET_SSHD_DOWN = 128
    INTRANET_SSHD_CLOSE = 256
    EXTRANET_SSHD_DOWN = 512
    EXTRANET_SSHD_CLOSE = 1024
    PARTITIONING_NEED = 2048  # drives are formatted but not partitioned yet
    NO_CONF_BACKUP_ON_VG = 4096  # on the volume group there is no config backup
    VG_UUID_NOT_MATCH = 8192  # the vguuid is not the one from the system
    REMOTE_KEY_NOT_SET = 16384
    DISK_ERROR = 32768 # any error on the HDD 

    @staticmethod
    def message(reason):
        """Returns message for decryption what the status means

        Args:
            reason (SytemStatus): The bitwise value as described in SystemStaus

        Returns:
            str: Human readable message
        """
        result = []
        if reason == SystemStatus.NOTHING:
            return ["Nothing to report"]
        if reason & SystemStatus.DETECTION:
            result.append("Detecion is on it's way")
        if reason & SystemStatus.DISK_SETUP_NEED:
            result.append("Manual disk reconfiguration is needed")
        if reason & SystemStatus.SOS_VOLUMES_NOT_MOUNTED:
            result.append("The syncosync volumes are not mounted")
        if reason & SystemStatus.SOS_KEY_NOT_GENERATED:
            result.append("The syncosync key is not existing")
        if reason & SystemStatus.REMOTE_HOST_NOT_CONFIGURED:
            result.append("The remote host is not configured")
        if reason & SystemStatus.SOS_CONF_BACKUP_NOT_DONE:
            result.append("The configuration was never backuped")
        if reason & SystemStatus.SOS_CONF_BACKUP_NOT_MATCH:
            result.append(
                "The configuration backup does not match the backup on the HDD"
            )
        if reason & SystemStatus.INTRANET_SSHD_DOWN:
            result.append("The intranet sshd is not started")
        if reason & SystemStatus.INTRANET_SSHD_CLOSE:
            result.append("The intranet sshd is in close state")
        if reason & SystemStatus.EXTRANET_SSHD_DOWN:
            result.append("The extranet sshd is not started")
        if reason & SystemStatus.EXTRANET_SSHD_CLOSE:
            result.append("The extranet sshd is in close state")
        if reason & SystemStatus.PARTITIONING_NEED:
            result.append("Drives are formatted but not partitioned yet")
        if reason & SystemStatus.NO_CONF_BACKUP_ON_VG:
            result.append("Volume Group contains no configuration backup")
        if reason & SystemStatus.VG_UUID_NOT_MATCH:
            result.append("VG UUID in stored configuration and on disk do not match")
        if reason & SystemStatus.REMOTE_KEY_NOT_SET:
            result.append("There is no remote key set")
        if reason & SystemStatus.DISK_ERROR:
            result.append("There is an error on the disk")
        return result


class Partition(IntEnum):
    """
    The three different partitions of a sos system
    """

    # local holds all local data from backup clients in the same intranet
    LOCAL = 0
    # remote is the partition which get's the synced data from the remote host
    REMOTE = 1
    # system holds logs etc. but _not_ backup configurations
    SYSTEM = 2


class SystemMode(IntEnum):
    """
    Possible Modes the system could get in (see wiki: Startup,-Recovery-and-Disaster-Szenarios)
    """

    # Startup -> the system does not know, what mode it is right now
    STARTUP = 0
    # In SETUP DISKS Mode there are disks which are not formatted yet to be used with sos
    SETUP_DISKS = 1
    # default mode -> mount drives, start ssh and lsyncd
    DEFAULT = 3
    # default mode -> mount drives, start internal ssh but _not_ external ssh and lsyncd
    DEFAULT_NO_SYNC = 4
    # Remote Recovery -> the system will recover from a remote host
    REMOTE_RECOVERY = 5
    # After remote recovery, check if everything is fine, also good for ui
    RR_FINAL = 6
    # Disaster Swap -> the system will swap local and remote volumes
    DISASTER_SWAP = 7
    # This mode will be finally reached
    SHUTDOWN = 8
    # Intermediate mode is used for actions while mode switching
    INTERMEDIATE = 9
    # There are no volumes specified
    SETUP_VOLUMES = 10
    # Volume Resize mode is intermediate, to switch off all external access and unmount drives
    RESIZE_VOLUMES = 11
    # There is an error on the Disk
    DISK_ERROR = 12
    # Unknown Mode (should not happen)
    UNKNOWN = 255

    @staticmethod
    def message(mode):
        if mode == SystemMode.STARTUP:
            return "Startup"
        if mode == SystemMode.SETUP_DISKS:
            return "Setup Disks"
        if mode == SystemMode.SETUP_VOLUMES:
            return "Setup Volumes"
        if mode == SystemMode.DEFAULT:
            return "Default"
        if mode == SystemMode.DEFAULT_NO_SYNC:
            return "Default No Sync"
        if mode == SystemMode.REMOTE_RECOVERY:
            return "Remote Recovery"
        if mode == SystemMode.RR_FINAL:
            return "Remote Recovery Final"
        if mode == SystemMode.DISASTER_SWAP:
            return "Disaster Swap"
        if mode == SystemMode.SHUTDOWN:
            return "Shutdown"
        if mode == SystemMode.INTERMEDIATE:
            return "Intermediate"
        if mode == SystemMode.RESIZE_VOLUMES:
            return "Resize Volumes"
        if mode == SystemMode.DISK_ERROR:
            return "Disk Error"
        if mode == SystemMode.UNKNOWN:
            return "Unknown"


class SwitchAction(IntEnum):
    """
    This as an additional action parameter for changing sysstates
    """

    # Default -> nothing special, could be derived from context
    DEFAULT = 0
    # DISASTER_SWAP_PERSISTENT makes disasters swap persistent
    DISASTER_SWAP_PERSISTENT = 1


class SysType(IntEnum):
    """
    systype (describing the hardware)
    """

    # unknown
    UNKNOWN = 0
    # generic
    GENERIC = 1
    # Raspberry Pi 4
    RPI4 = 2
    # Banana Pi
    BPI = 3
    # qemu emulation
    QEMU = 4

    @staticmethod
    def typename(value) -> str:
        if value == SysType.GENERIC:
            return "generic"
        elif value == SysType.QEMU:
            return "qemu"
        elif value == SysType.RPI4:
            return "rpi4"
        elif value == SysType.BPI:
            return "bpi"
        else:
            return "generic"


class Sshd(IntEnum):
    """
    Internal oder External sshd
    """

    INTRANET = 0
    EXTRANET = 1


class SshdAccess(IntEnum):
    """
    if access is open or close
    """

    OFF = 0
    OPEN = 1
    CLOSE = 2


class SsdAction(IntEnum):
    """
    This is the idea that for setting up a remote system an action could be defined.
    """

    # Interactive: like documented in the wiki: leader provides key and pulls key and free blocks
    # then provides partitions
    LEADER_SEND_KEY = 0  # the leader sends a key, the follower can accept it
    FOLLOWER = 1  # following the leader, there is no more to do
    LEADER_REQUEST_DATA = 2  # just get the actual ssd from the follower
    LEADER_INIT_PARTITIONS = 3  # This is for the initial partitioning
    # e.g. for changing relation of partitions or after adding / removing disks
    LEADER_RESIZE_PARTITIONS = 4

    @staticmethod
    def to_readable(action) -> str:
        """returns the text for the action"""
        if action == SsdAction.LEADER_SEND_KEY:
            return "Leader send key"
        if action == SsdAction.FOLLOWER:
            return "Follower"
        if action == SsdAction.LEADER_REQUEST_DATA:
            return "Leader request data"
        if action == SsdAction.LEADER_INIT_PARTITIONS:
            return "Leader init partitions"
        if action == SsdAction.LEADER_RESIZE_PARTITIONS:
            return "Leader resize partitions"
        return "Unknown action"

    @staticmethod
    def is_leader(action) -> bool:
        """
        Indicate whether the SsdAction is a follower or leader action
        """
        if action == SsdAction.FOLLOWER:
            return False
        else:
            return True


class GenericUiResponseStatus(IntEnum):
    """
    Enum indicating the type/status of the response given to the UI if an error occurs or a simple OK is sufficient
    """

    OK = 0
    ERROR = 1


class MailSslType(IntEnum):
    """
    Mail SSL type indicating the security protocol to be used
    """

    NO_SECURITY = 0
    STARTTLS = 1
    SSL_TLS = 2

    @staticmethod
    def typename(value):
        if value == MailSslType.NO_SECURITY:
            return ""
        if value == MailSslType.STARTTLS:
            return "starttls "
        if value == MailSslType.SSL_TLS:
            return "tls "


class MailAuthenticationType(IntEnum):
    PASSWORD_NORMAL = 0
    PASSWORD_CRYPTED = 1


class MailInfoLevels(IntEnum):
    NONE = 0
    ERROR = 1
    WARNING = 2
    INFO = 3

    @staticmethod
    def levelname(mailinfolevel):
        if mailinfolevel == MailInfoLevels.NONE:
            return "none"
        if mailinfolevel == MailInfoLevels.ERROR:
            return "error"
        if mailinfolevel == MailInfoLevels.WARNING:
            return "warning"
        if mailinfolevel == MailInfoLevels.INFO:
            return "info"


class TrafficShapeSwitch(IntEnum):
    OFF = 0
    DAY = 1
    NIGHT = 2


class EraseDiskLevel(IntEnum):
    NO_ERASE = 0
    QUICK_ERASE = 1
    # TODO: SECURE_ERASE = 2


class BpiLed(IntEnum):
    UP = 0
    DOWN = 1
    ERROR = 2


class PartitioningState(IntEnum):
    OK = 0  # LEADER or system SSD matches remote SSD
    RESIZE_NEEDED = 1  # Receiver detects SSD mismatch -> resize needed
    RESIZE_NOT_POSSIBLE = 2  # Receiver detects fatal SSD mismatch
