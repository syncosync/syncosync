#!/usr/bin/python3
# vi:se ts=4 sts=4 et ai:
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from distutils.core import setup

if os.path.isfile("package_static/BASEVERSION"):
    version_file = open("package_static/BASEVERSION")
    version = version_file.read().strip()
else:
    version = "unknown"
if os.path.isfile("package_static/VERSIONTIMESTAMP"):
    version_file = open("package_static/VERSIONTIMESTAMP")
    version = version + "-" + version_file.read().strip()
else:
    version = version + "-unknown"

setup(
    name="syncosync",
    version=version,
    description="backup synchronisation",
    long_description="secure peer to peer backup synchronization",
    author="Stephan Skrodzki, Till Skrodzki, Daniel Willmann, Mika Rosin",
    author_email="stevieh@syncosync.org, grennith@syncosync.org",
    url="https://syncosync.org",
    packages=[
        "sosui",
        "soscli",
        "soscore",
        "sosmodel",
        "sosutils",
        "sosui.routes",
        "sosui.routes.api",
        "sosui.routes.api.status",
        "sosui.routes.api.setup",
        "sosui.routes.api.account_management",
        "sosui.routes.api.auth",
        "sosui.routes.api.public",
        "sosui.routes.api.remote_recovery",
    ],
    package_dir={
        "": "src",
    },
    scripts=[
        "src/soscli/configbackup.py",
        "src/soscli/drivemanager.py",
        "src/soscli/gen_chroot_env.py",
        "src/soscli/factory_reset.py",
        "src/soscli/hostname.py",
        "src/soscli/timezone.py",
        "src/soscli/mail.py",
        "src/soscli/nameserver.py",
        "src/soscli/nicconfig.py",
        "src/soscli/remotehost.py",
        "src/soscli/remote_recovery.py",
        "src/soscli/sosaccountaccessd.py",
        "src/soscli/sosaccounts.py",
        "src/soscli/sosadmin_passwd.py",
        "src/soscli/sosconfig.py",
        "src/soscli/soskey.py",
        "src/soscli/soskeyx.py",
        "src/soscli/soshousekeeper.py",
        "src/soscli/sosrsyncwrapper.py",
        "src/soscli/sshdmanager.py",
        "src/soscli/syncstat.py",
        "src/soscli/sysstate.py",
        "src/soscli/trafficshape.py",
        "src/syncosync.py",
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Embedded",
        "Intended Audience :: End Users",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Natural Language :: English",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Topic :: Utilities",
    ],
    platforms="POSIX",
    license="GNU Affero General Public License V3",
    requires=[
        "asyncinotify",
        "dirsync",
        "flask",
        "flask_cors",
        "jinja2",
        "werkzeug",
        "psutil",
        "gnupg",
        "paramiko",
        "cryptography",
        "pyparted",
        "loguru",
    ],
)
