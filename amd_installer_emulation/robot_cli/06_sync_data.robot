*** Settings ***
Documentation          Sync Data to sos accounts

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections


*** Test Cases ***
sysstate is ok
                                  Switch Connection  alice
    ${content}                    Execute Command    sysstate.py -g
    Should contain                ${content}   Actual Mode: Default


syncosync service started
    [Documentation]               Is sosaccountaccess service running?
                                  Switch Connection  alice
    ${output}=                    Execute Command    systemctl status sosaccountaccess.service
    Should contain                ${output}          active (running)


sync data to ali_ta1
    [Documentation]               Put some demodata on alice
                                  Switch Connection  alice  
                                  Run                rsync -a --rsh="ssh -p 2222 -o StrictHostKeyChecking=no -l ali_ta1 -i ali_ta1" ali_ta1.data/* localhost:
                                  Check For File   /mnt/local/syncosync/ali_ta1/data/ali_ta1_0.data
    ${heresum}                    Run                md5sum < ali_ta1.data/ali_ta1_0.data
    ${theresum}                   Execute Command    md5sum < /mnt/local/syncosync/ali_ta1/data/ali_ta1_0.data
    Should Be Equal               ${heresum}  ${theresum}

sync data to bob_ta1
    [Documentation]               Put some demodata on bob
                                  Switch Connection  bob  
                                  Run                sshpass -pBobta1pwd rsync -a --rsh="ssh -p 2223 -o StrictHostKeyChecking=no -l bob_ta1" bob_ta1.data/* localhost:    
                                  Check For File   /mnt/local/syncosync/bob_ta1/data/bob_ta1_0.data
    ${heresum}                    Run                md5sum < bob_ta1.data/bob_ta1_0.data
    ${theresum}                   Execute Command    md5sum < /mnt/local/syncosync/bob_ta1/data/bob_ta1_0.data
    Should Be Equal               ${heresum}  ${theresum}

is space used ok
    [Documentation]               check for space used (takes 20s)
                                  Switch Connection  alice
                                  Sleep  20
    ${content}                    Execute Command  sosaccounts.py -g ali_ta1
    Should contain                ${content}  "space_used": 7340

is data synced to bob
    [Documentation]               is the data above synced to bob?
                                  Compare Local Remote  alice  bob  ali_ta1/data    

is data synced to alice
    [Documentation]               is the data above synced to alice?
                                  Compare Local Remote  bob  alice  bob_ta1/data    

erase data on ali_ta1
    [Documentation]               erase the data on ali_ta1 and check results
                                  Switch Connection  alice 
                                  Execute Command    sosaccounts.py -e '{"name":"ali_ta1"}'
                                  Compare Local Remote  alice  bob  ali_ta1/data    

is space used 0
    [Documentation]               check for space used (takes 20s)
                                  Switch Connection  alice
                                  Sleep  20
    ${content}                    Execute Command  sosaccounts.py -g ali_ta1
    Should contain                ${content}  "space_used": 1024

create the data again          
    [Documentation]               Just put again the data for later checks
                                  Run  rsync -a --rsh="ssh -p 2222 -o StrictHostKeyChecking=no -l ali_ta1 -i ali_ta1" ali_ta1.data/* localhost:

is space used ok again
    [Documentation]               check for space used (takes 20s)
                                  Switch Connection  alice
                                  Sleep  20
    ${content}                    Execute Command  sosaccounts.py -g ali_ta1
    Should contain                ${content}  "space_used": 73401344

