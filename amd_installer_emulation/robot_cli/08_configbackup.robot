*** Settings ***
Documentation          Config Backup and restore testing

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections


*** Test Cases ***
create a configbackup
    [Documentation]               Just create a configuration backup and look if it is there
    Switch Connection             alice 
    ${content}                    Execute Command    sysstate.py --default
    Should Contain                ${content}         successful
    ${content}                    Execute Command    configbackup.py -b
    Should Contain                ${content}         backup successful to file
    ${match} 	${cfgbu_ali} =    Should Match Regexp  ${content}    .*to file (.*)
                                  SSHLibrary.File Should Exist  ${cfgbu_ali}
    
    
create a configbackup on bob
    [Documentation]               Just create a configuration backup on bob and check if it is on alice
                                  Switch Connection  bob
    ${content}                    Execute Command    configbackup.py -b
    Should Contain                ${content}         backup successful to file 
    ${match} 	${filepath} =    Should Match Regexp  ${content}    .*to file (.*)

                                  Compare Local Remote  bob  alice  syncosync
    
restore configbackup 
    [Documentation]               Restore configbackup on alice
                                  Switch Connection  alice  
    ${content}                    Execute Command    configbackup.py -b
    Should Contain                ${content}         backup successful to file 
    ${match} 	${filepath} =    Should Match Regexp  ${content}    .*to file (.*)
                                  SSHLibrary.File Should Exist  ${filepath}
    ${content}                    Execute Command    sysstate.py --default -c '${filepath}'
    Should Contain              ${content}         successful
    ${content}                    Execute Command    sysstate.py --default
    Should Contain              ${content}         successful

restore with additional dir
    [Documentation]               Restore with an additional directory
                                  Switch Connection  alice
    ${content}                    Execute Command    configbackup.py -b
    Should Contain                ${content}         backup successful to file 
    ${match} 	${filepath} =    Should Match Regexp  ${content}    .*to file (.*)
                                  SSHLibrary.File Should Exist  ${filepath}
    ${content}                    Execute Command    cp -r /mnt/local/syncosync/ali_ta1 /mnt/local/syncosync/add_acc
                                  SSHLibrary.File Should Exist  ${filepath}
    ${content}                    Execute Command    sysstate.py --default -c '${filepath}'
    Should Contain                ${content}         Created account for orphaned dir: add_acc
    ${content}                    Execute Command    sysstate.py --default
    ${content}                    Execute Command    sosaccounts.py -g add_acc
    Should Contain                ${content}         Created from config restore       
    ${content}                    Execute Command    quota -u ali_ta1
    Log                           ${content}
  
delete this account again
    [Documentation]               Delete add acc
                                  Switch Connection  alice  
    ${content}                    Execute Command    sysstate.py --default
    Should Contain                ${content}         successful
    ${content}                    Execute Command    sosaccounts.py -v INFO -d '{"name":"add_acc"}'  return_stderr=True
    Log                           ${content}
    ${content}                    Execute Command    sosaccounts.py -l
    Should Not Contain            ${content}         add_acc
    # now we have also to wait, until add_acc is not listed anymore on bob
    Compare Local Remote          alice  bob  ""

 