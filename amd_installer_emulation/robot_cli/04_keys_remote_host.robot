*** Settings ***
Documentation          Exchange and Check remote keys

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Test Cases ***
delete all syncosync keys
    [Documentation]               Delete all keys on alice and bob
    [Tags]                        reset
                                  Switch Connection  alice  
    ${content}                    Execute Command    rm /etc/syncosync/sshkeys/*
    ${content}                    Execute Command    rm /etc/syncosync/remotekeys/*
                                  Switch Connection  bob
    ${content}                    Execute Command    rm /etc/syncosync/sshkeys/*
    ${content}                    Execute Command    rm /etc/syncosync/remotekeys/*
    
    
change and check remote host on alice
    [Documentation]               Change and check the remote host on alice
                                  Switch Connection  alice  
    ${stdout}                     Execute Command   remotehost.py -s '{"hostname": "10.0.2.2", "port": "55057"}'
    ${rc}=                        Execute Command   grep "10.0.2.2" /etc/lsyncd/lsyncd.conf.lua      return_stdout=False      return_rc=True
    Should Be Equal               ${rc}             ${0}
    ${rc}=                        Execute Command   grep "55057" /etc/lsyncd/lsyncd.conf.lua      return_stdout=False      return_rc=True
    Should Be Equal               ${rc}             ${0}
    ${content}                    Execute Command   remotehost.py -g
    Should Contain                ${content}        10.0.2.2
    ${content}                    Execute Command   remotehost.py -c
    Should Contain                ${content}        RemoteUpStatus.NO_REPLY
    ${stdout}                     Execute Command   remotehost.py -s '{"hostname": "10.0.2.2", "port": "55056"}'
    ${content}                    Execute Command   remotehost.py -c
    Should Not Contain          ${content}        RemoteUpStatus.NO_REPLY

change remote host on bob
    [Documentation]               Change and check the remote host on bob
                                  Switch Connection  bob  
    ${stdout}                     Execute Command   remotehost.py -s '{"hostname": "10.0.2.2", "port": "55055"}'


create syncosync keys
    [Documentation]               Create syncosync keys alice
                                  Switch Connection  alice  
    ${content}                    Execute Command    soskey.py -n -y
    ${content}                    Execute Command    soskey.py -g
    Should Not Contain            ${content}        null
                                  Switch Connection  bob
    ${content}                    Execute Command    soskey.py -n -y

automatically exchange syncosync keys
    [Documentation]               Automatically exchange syncosync keys
    # bob is the setup follower
                                  Switch Connection  bob
    ${content}                    Execute Command  sysstate.py -g
                                  Log               ${content}
                                  Log To Console    sysstate on bob:  ${content}
                                  Log To Console    Start soskeyx.py -v DEBUG -r on bob
    ${content}                    Start Command      soskeyx.py -v DEBUG -r
                                  Log To Console     ${content}


    ${content}                    Execute Command    BuiltIn.Sleep  2
    # Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  20
    # alice is the setup leader
                                  Switch Connection  alice
    ${content}                    Execute Command  sysstate.py -g
                                  Log               ${content}
                                  Log To Console    sysstate on alice:  ${content}
                                  Log To Console    Start soskeyx.py -v DEBUG -s on alice
    ${content}                    Execute Command    soskeyx.py -v DEBUG -s
                                  Log To Console     ${content}
    ${content}                    Execute Command    BuiltIn.Sleep  2
    # Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  30
    # now check if the key has arrived
			                      Check For File  /home/soskeyx/syncosync.pub
    ${stdout}                     Execute Command    mkdir /etc/syncosync/remotekeys
    ${stdout}                     Execute Command    soskeyx.py -a
    ${content}                    Execute Command    sysstate.py --default
    ${content}                    Execute Command    BuiltIn.Sleep  2
    ${content}                    Execute Command    sysstate.py -g
    # and the same at bob
                                  Switch Connection  bob
    ${content}			          Read Command Output
    #Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  30
	 Wait Until Keyword Succeeds  2min  5s   SSHLibrary.File Should Exist  /home/soskeyx/syncosync.pub
    ${stdout}                     Execute Command    mkdir /etc/syncosync/remotekeys
    ${stdout}                     Execute Command    soskeyx.py -a
    ${content}                    Execute Command    sysstate.py --default
    ${content}                    Execute Command    BuiltIn.Sleep  2
    ${content}                    Execute Command    sysstate.py -g

check remote host on alice
    [Documentation]               Check remote host on alice
    ${content}                    Execute Command    BuiltIn.Sleep  2
                                  Remote Check  alice
    
check remote host on bob
    [Documentation]               Check remote host on bob
    ${content}                    Execute Command    BuiltIn.Sleep  2
                                  Remote Check  bob
