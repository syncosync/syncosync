*** Settings ***
Documentation          Test miscenalleous config stuff

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Test Cases ***

start default mode
    [Documentation]               Start default no sync mode
                                  Switch Connection  alice
    ${stdout}                     Execute Command   sysstate.py --default_no_sync
    Should Contain               ${stdout}          Switching to Default No Sync successful

add a nameserver
    [Documentation]               Add a secondary nameserver
                                  Switch Connection  alice  
    ${stdout}                     Execute Command   nameserver.py -s '{"second_nameserver": "8.8.8.8"}'
    ${rc}=                        Execute Command   grep "nameserver 8.8.8.8" /etc/resolv.conf      return_stdout=False      return_rc=True
    Should Be Equal               ${rc}             ${0}
    ${content}                    Execute Command   nameserver.py -g
    Should Contain                ${content}        8.8.8.8

change hostname
    [Documentation]               Change the hostname
                                  Switch Connection  alice  
    ${stdout}                     Execute Command   hostname.py -s '{"hostname": "testit"}'
    ${rc}=                        Execute Command   grep "testit" /etc/hostname      return_stdout=False      return_rc=True
    Should Be Equal               ${rc}             ${0}
    ${content}                    Execute Command   hostname.py -g
    Should Contain                ${content}        testit
    ${stdout}                     Execute Command   hostname.py -s '{"hostname": "alice"}'

get the network interfaces
    [Documentation]               Get the network interfaces list
                                  Switch Connection  alice  
    ${stdout}                     Execute Command   nicconfig.py -g
    Should Contain                ${stdout}       eth0

check if a configuration backup exists
    [Documentation]               We have changed the systype, so there should be at least one backup
                                  Switch Connection  alice  
    ${content}                    Execute Command   ls -1 -t /mnt/local/syncosync/syncosync/configbackup/sosconf_*.tgz | head -1
    Should Contain                ${content}        sosconf
