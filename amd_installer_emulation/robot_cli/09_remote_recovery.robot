*** Settings ***
Documentation          Sync Data to sos accounts

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Variables ***
${cfgbu_ali_file}       empty
${RR_LOOP}              1

*** Test Cases ***
sync data to ali_ta1
    [Documentation]               Put some demodata on alice
                                  Switch Connection  alice  
                                  Wait For State  Default
                                  Run                rsync -a --rsh="ssh -p 2222 -o StrictHostKeyChecking=no -l ali_ta1 -i ali_ta1" ali_ta1.data/* localhost:
                                  Check For File   /mnt/local/syncosync/ali_ta1/data/ali_ta1_0.data
    ${heresum}                    Run                md5sum < ali_ta1.data/ali_ta1_0.data
    ${theresum}                   Execute Command    md5sum < /mnt/local/syncosync/ali_ta1/data/ali_ta1_0.data
    Should Be Equal               ${heresum}  ${theresum}

check if ali_ta1 data is on bob
    [Documentation]               If it has not been there before, we have to check if the data is there
                                  Compare Local Remote   alice  bob  ali_ta1/data


create configbackup
                                  Configbackup Alice

remote recovery
    [Documentation]               Remote Recovery Test Loop
                                  Should Not Be Equal   ${cfgbu_ali_file}   empty
    Log                           Looping ${rr_loop} time(s)   console=yes
    FOR                           ${index}   IN RANGE   ${RR_LOOP}
        Log                       Iteration ${index}   console=yes
                                    Switch Connection  bob
                                    Check For File     /mnt/rjail/remote/syncosync/ali_ta1/data/ali_ta1_0.data   5min
 
        Factory Reset               alice
                                    Switch Connection  alice
        ${stdout}                   Execute Command    drivemanager.py -f -y
        Should Contain              ${stdout}          Success, free space 112 extends
        Sleep                       5s
        ${stdout}                   Execute Command   sysstate.py --setup_volumes
        Should Contain              ${stdout}         successful
        ${stdout}                   Execute Command   sysstate.py --remote_recovery -c /root/${cfgbu_ali_file} -a sosadmin
        Should Contain              ${stdout}         successful
                                    Wait For State  Remote Recovery Final
                                    Check For File   /mnt/local/syncosync/ali_ta1/data/ali_ta1_0.data
        ${stdout}                   Execute Command   sysstate.py --default
        Should Contain              ${stdout}         successful
        Log                       Passed   console=yes
    END

trigger lsyncd on bob
    [Documentation]               We have to restore the remote data on alice again
                                  Switch Connection  bob
    ${content}                    Execute Command     service lsyncd restart
                                  Switch Connection  alice
                                  Check For File   /mnt/rjail/remote/syncosync/bob_ta1/data/bob_ta1_0.data   15min

