*** Settings ***
Documentation          Keywords and other settings for syncosync.robot tests

Library                SSHLibrary
Library                Process
Library                Collections
Library                OperatingSystem
Library                String
Library                SeleniumLibrary

*** Variables ***
${LANG}                en-US
${ALICEHOST}           localhost
${ALICEPORT}           2222
${ALICEUIPORT}         5555
${BOBHOST}             localhost
${BOPPORT}             2223
${BOBUIPORT}           5556
${USERNAME}            root
${PASSWORD}            none
${HEADLESS}            False
${SLOW}                False
${TARGET}              buster
${URL ALICE}           http://${ALICEHOST}:${ALICEUIPORT}/${LANG}/
${URL BOB}             http://${BOBHOST}:${BOBUIPORT}/${LANG}/
${SCREENSHOT_PATH}     ../../syncosync-manuals/${LANG}/images/screenshots
${BROWSER}             Chrome
${DELAY}               3
${TIMEOUT}             20

*** Keywords ***
Compare Remote
    [Arguments]                   ${system}  ${subpath}  ${dircontent}
    [Documentation]               looks at systems remote location, if some data for name has arrived
                                  Switch Connection  ${system}
    ${content}                    Execute Command    ls /mnt/rjail/remote/syncosync/${subpath}
                                  Log  ${system}: ${content}
    Should Be Equal               ${content}         ${dircontent}  

Compare Local Remote
    [Arguments]                   ${local}  ${remote}  ${subpath}
    [Documentation]               Compares the data from name on the local and remote system.
    ...                           As syncing takes some time, it waits for a timeout
                                  Switch Connection  ${local}
    ${starttime}                  Get Time  epoch
    ${content}                    Execute Command    ls /mnt/local/syncosync/${subpath}
                                  Log  ${local}: ${content}
                                  Wait Until Keyword Succeeds  5min  5s  Compare Remote  ${remote}  ${subpath}  ${content}
    ${endtime}                    Get Time  epoch
    ${duration}                   Evaluate  ${endtime}-${starttime}
                                  Log  Compare Local Remote time: ${duration} seconds  console=yes
Remote Check Loop
    [Arguments]                   ${system}
    [Documentation]               Just check if the remote host is up
                                  Switch Connection  ${system}
    ${stdout}                     Execute Command   remotehost.py -c
    Should contain                ${stdout}         RemoteUpStatus.UP

Remote Check
    [Documentation]               Checks if the remote host is up with some timeout
    [Arguments]                   ${system}
    ${starttime}                  Get Time  epoch
                                  Wait Until Keyword Succeeds  2min  5s  Remote Check Loop  ${system}
    ${endtime}                    Get Time  epoch
    ${duration}                   Evaluate  ${endtime}-${starttime}
                                  Run Keyword If  ${duration} > 10  Log  Remote Check time: ${duration} seconds  console=yes

Check For File Loop
    [Arguments]                   ${file}
    [Documentation]               Just check if the file is there
    ${content}                    Execute Command  ls ${file}
    Should Be Equal               ${content}         ${file}  

Check For File
    [Documentation]               Checks if a file is there - as this may happen sometimes later (via exchange) 
    [Arguments]                   ${file}   ${waittime}=2min
    ${starttime}                  Get Time  epoch
                                  Wait Until Keyword Succeeds  ${waittime}  5s  Check For File Loop  ${file}
    ${endtime}                    Get Time  epoch
    ${duration}                   Evaluate  ${endtime}-${starttime}
                                  Run Keyword If  ${duration} > 30  Log  Check For File time: ${duration} seconds  console=yes

Wait For State Loop
    [Arguments]                   ${statename}
    ${content}                    Execute Command  sysstate.py -g
                                  Log   ${content}    
    Should contain              ${content}  Actual Mode: ${statename}

Wait For State
    [Documentation]               Waits until a state is reached
    [Arguments]                   ${statename}
    ${starttime}                  Get Time  epoch
                                  Wait Until Keyword Succeeds  4min  10s  Wait For State Loop  ${statename}
    ${endtime}                    Get Time  epoch
    ${duration}                   Evaluate  ${endtime}-${starttime}
                                  Run Keyword If  ${duration} > 30  Log  Wait For State time: ${duration} seconds  console=yes

Wait For Result Loop
    [Arguments]                   ${command}   ${result}
    ${content}                    Execute Command  ${command}
                                  Log   ${content}    
    Should contain              ${content}  ${result}

Wait For Result
    [Documentation]               Waits until a command contains a desired result
    [Arguments]                   ${command}   ${result}
    ${starttime}                  Get Time  epoch
                                  Wait Until Keyword Succeeds  4min  10s  Wait For Result Loop  ${command}   ${result}
    ${endtime}                    Get Time  epoch
    ${duration}                   Evaluate  ${endtime}-${starttime}
                                  Run Keyword If  ${duration} > 30  Log  Wait For Result time: ${duration} seconds  console=yes

Factory Reset
    [Arguments]                   ${system}
    [Documentation]               Reset the system and restart syncosync service
                                  Switch Connection  ${system}
    ${content}                    Execute Command  factory_reset.py -r -e -y
                                  Log   ${content}
                                  Execute Command  service syncosync restart
    ${content}                    Wait For State   Setup Disks

Configbackup Alice
    [Documentation]               Configuration backup and download from alice
                                   Switch Connection  alice
        ${content}                  Execute Command    configbackup.py -b       timeout=120
        Should Contain              ${content}         backup successful to file
        ${match} 	${cfgbu_ali} =   Should Match Regexp  ${content}    .*to file (.*)
                                    SSHLibrary.File Should Exist  ${cfgbu_ali}
                                    Execute Command  cp ${cfgbu_ali} /root/
        ${cfgbu_ali_file_local}     Get Substring   ${cfgbu_ali}  44
        Set Global Variable         ${cfgbu_ali_file}   ${cfgbu_ali_file_local}
                                    # we store this file also locally for later actions
                                    Run               rm sosconf_*_alice*.gpg
                                    Run               scp -o StrictHostKeyChecking=no -i robotkey -P 2222 root@localhost:${cfgbu_ali} .


Open Connection On Host
    [Arguments]                   ${host}  ${port}  ${name}
                                  Open Connection   ${host}  port=${port}   alias=${name}
                                  Login With Public Key    root  robotkey
    
Open Connection And Log In
                                  Run  ssh-keygen -f ~/.ssh/known_hosts -R "[localhost]:2222"
                                  Run  ssh-keygen -f ~/.ssh/known_hosts -R "[localhost]:2223"
                                  Wait Until Keyword Succeeds  2min  5s  Open Connection On Host  ${ALICEHOST}  ${ALICEPORT}  alice   
                                  Wait Until Keyword Succeeds  2min  5s  Open Connection On Host  ${BOBHOST}  ${BOPPORT}  bob   

Start Qemus
    [Arguments]                   ${alice_a}   ${alice_b}   ${alice_c}   ${bob_a}   ${bob_b}   ${bob_c}
                                  Run   ./start_img.sh -t ${TARGET} -a ${alice_a} -b ${alice_b} -c ${alice_c} -d ${bob_a} -e ${bob_b} -f ${bob_c}


Open Chrome
    [Arguments]    ${url}=${URL ALICE}     ${alias}=alice
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Run Keyword If 	${HEADLESS} == True  Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    ${browser_index}    Create Webdriver    Chrome    options=${chrome_options}   alias=${alias}
    Set Window Size  1280   1280
    Set Selenium Speed   ${DELAY}
    Set Selenium Timeout  ${TIMEOUT}
    Goto   ${url}
    RETURN        ${browser_index}

Valid Login
    [Arguments]          ${password}=sosadmin
    Wait Until Page Contains  Login
    Wait Until Page Contains  Password
    Input Text           password  ${password}
    Click Button         Login
    Log Location
    Page Should Contain Element  xpath: //button[@id="logout-button"]

Valid Logout
    Page Should Contain Element  xpath: //button[@id="logout-button"]
    Click Element        xpath: //button[@id="logout-button"]
    Page Should Contain  Login

Setup
    [Documentation]      Switch to setup
    Page Should Contain Element  xpath: //button[@id="setup-button"]
    Click Element        setup-button
    Wait Until Page Contains   Networking

Leave Setup
    [Documentation]      Leave Setup
    Page Should Contain Element    xpath: //button[@id="leave-setup-button"]
    Click Element        leave-setup-button
    Page Should Not Contain  Networking

Open WebUI And SSH
    Open Connection And Log In
    Open Chrome    ${URL_ALICE}   alice
    Open Chrome    ${URL_BOB}   bob

Close WebUI And SSH
    Close All Browsers
    Close All Connections