#!/bin/bash -e

# This scripts sets up a x86 based sos appliance
# it downloads - if not available - a netinst.iso from debian
# it uses a preesed.cfg file
# after successful execution, there will be a file sos_x86.qcow
# (c) 2019 Stephan Skrodzki <stevieh@syncosync.org>

# according to https://wiki.debian.org/DebianInstaller/Preseed/EditIso

# Absolute path to this script. /home/user/bin/foo.sh
SCRIPT=$(readlink -f $0)

startpath=`pwd`

target='bullseye'

# Usage info
show_help() {
  cat <<EOF
Usage: ${0##*/}
Initialise the base image in a target 

    -t          target dir (bookworm / bullseye / buster defaults to "bullseye")
EOF
}

OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts "t:h" opt; do
  case "$opt" in
  t) target=$OPTARG;;
  h)
    show_help
    exit 0
    ;;
  '?')
    show_help >&2
    exit 1
    ;;
  esac
done

if [ -c /dev/kvm ]
then
    ACCEL="-cpu host -accel kvm"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    # Big Sur requires signing/entitlements: https://web.archive.org/web/20210406175617/https://www.arthurkoziel.com/qemu-on-macos-big-sur/
    ACCEL="-cpu host -accel hvf"
else
    echo No kvm found and not on darwin. This means, qemu will be so slooooooow.
    ACCEL=""
fi

if [ -f $target/config ]; then
  source $target/config
else
  echo "no $target/config file found"
  exit 1
fi

cd $target

echo "Checking for availability and consitency of original "$NETINST
if [ ! -f $NETINST.iso ]; then
  wget -c -nd "$URL_ROOT$NETINST.iso"
fi
if md5sum --quiet -c $NETINST.md5; then
  echo $NETINST.iso is there and correct
else
  echo "md5sum for $NETINST.iso is not correct, please check"
  exit 1
fi

if [ ! -f preseed.cfg ]; then
  echo "no preseed.cfg file there? Aborting"
  exit 1
fi

if [ -d isofiles ]; then
  rm -rf isofiles
fi

if [ -f installer.log ]; then
  rm installer.log
fi

set +e
mkdir cache/ || true

if md5sum -c cache/syncosync.qcow.md5 ; then
  echo "Skipping syncosync.qcow build"
  exit 0
fi
set -e

echo "Building syncosync.qcow"

echo "Running nc to capture syslogs... To view installer steps use: tail -f installer.log"
nc -u -l 10514 > installer.log &
NC_PID=$!
echo nc pid is $NC_PID

mkdir isofiles
xorriso -osirrox on -indev $NETINST.iso -extract / isofiles
chmod -R +w isofiles
gunzip isofiles/install.amd/initrd.gz
echo preseed.cfg | cpio -H newc -o -A -F isofiles/install.amd/initrd
gzip isofiles/install.amd/initrd
cp isofiles/install.amd/initrd.gz .
cp isofiles/install.amd/vmlinuz .

echo "Creating disk image for Debian $target x86_64..."
qemu-img create -f qcow2 $PROJECT.qcow 4G

echo "Running Debian Installer...To view installer steps use: tail -f installer.log in a different terminal"
qemu-system-x86_64 $ACCEL -hda $PROJECT.qcow -netdev user,id=net0,hostname=$PROJECT,domainname=localdomain -device rtl8139,netdev=net0,mac=52:54:98:76:54:32 -boot once=n -m 2048 -kernel vmlinuz -append "auto=true DEBIAN_FRONTEND=text log_host=10.0.2.2 log_port=10514 --- console=ttyS0 net.ifnames=0 biosdevname=0" -initrd initrd.gz -cdrom $NETINST.iso -nographic

md5sum config preseed.cfg syncosync.qcow > cache/syncosync.qcow.md5

if [ -d isofiles ]; then
  rm -rf isofiles
fi

echo "Finally stopping nc"
if kill -0 $NC_PID >/dev/null 2>&1; then
  kill $NC_PID
fi
sleep 2
if kill -0 $NC_PID >/dev/null 2>&1; then
  echo killing nc $NC_PID with -9 as it is still running
  kill -9 $NC_PID
fi

cd $startpath
