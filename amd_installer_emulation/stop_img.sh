#!/bin/bash

# stops all images started with start_pid.sh
# (c) 2019,2020,2021,2022,2023 Stephan Skrodzki <stevieh@syncosync.org>

SCRIPTDIR=`dirname $0`
targets='bookworm bullseye buster'

for target in $targets
do   
    if [  -f $target/img_run.stat ]; then    
        echo stopping in $target  
        while read pid port; do
            echo stopping pid $pid on port $port
            timeout --verbose 10s ssh -n -o StrictHostKeyChecking=no -i robotkey -p $port root@localhost "poweroff"
            sleep 10
            if kill -0 $pid >/dev/null 2>&1; then
                echo killing pid $pid as it is still running
                kill $pid
            fi
            sleep 2
            if kill -0 $pid >/dev/null 2>&1; then
                echo killing pid $pid with -9 as it is still running
                kill -9 $pid
            fi
        done <$target/img_run.stat
        rm $target/img_run.stat
    fi
done