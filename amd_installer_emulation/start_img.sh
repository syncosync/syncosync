#!/bin/bash -e

# starts multiple images
# (c) 2019,2020,2021,2022,2023 Stephan Skrodzki <stevieh@syncosync.org>

OLDWPD=$PWD
SCRIPTDIR=`dirname $0`
target='bullseye'
if [ $SCRIPTDIR != "." ]
then
  cd $SCRIPTDIR
fi

# it is always safe to stop images first
./stop_img.sh

if [ -c /dev/kvm ]
then
    ACCEL="-cpu host -accel kvm"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    # Big Sur requires signing/entitlements: https://web.archive.org/web/20210406175617/https://www.arthurkoziel.com/qemu-on-macos-big-sur/
    ACCEL="-cpu host -accel hvf"
else
    echo No kvm found and not on darwin. This means, qemu will be so slooooooow.
    ACCEL=""
fi


start_qemu() {

  if [ ! -f $target/$1.qcow ]; then
    echo $target/$1.qcow is not existing. You should build it with init_img.sh first
    return
  fi
  hdd=""
  if [ ! -z "$2" ] ; then
      if [ ! -f "$target/$2" ]; then
          echo disk image $target/$2 is not existing, we start without it.
      else
          hdd+="-drive file=$target/$2,index=1,media=disk,format=raw "
      fi
  fi
  if [ ! -z "$3" ] ; then
      if [ ! -f $target/$3 ]; then
          echo disk image $target/$3 is not existing, we start without it.
      else
          hdd+="-drive file=$target/$3,index=2,media=disk,format=raw "
      fi
  fi
  if [ ! -z "$4" ] ; then
      if [ ! -f $target/$4 ]; then
          echo disk image $target/$4 is not existing, we start without it.
      else
          hdd+="-drive file=$target/$4,index=3,media=disk,format=raw "
      fi
  fi
  if [ $nothing -eq 0 ]
  then
      echo "Starting $1"
      nohup qemu-system-x86_64 $ACCEL -name $1 -hda $target/$1.qcow $hdd -netdev user,id=net0,dhcpstart=10.0.2.$THISIP,hostname=$1,domainname=localdomain,hostfwd=tcp::$THISSSHPORT-:22,hostfwd=tcp::$THISSOSPORT-:55055,hostfwd=tcp::$THISUIPORT-:80,hostfwd=tcp::$THISHTTPSPORT-:443 -device rtl8139,netdev=net0,mac=52:54:98:76:54:$THISMAC -boot once=n -m 2048  -nodefaults $graphics >$target/$1.log &
      echo $! $THISSSHPORT >>$target/img_run.stat
  else
      command="nohup qemu-system-x86_64 $ACCEL -name $target/$1 -hda $1.qcow $hdd -netdev user,id=net0,dhcpstart=10.0.2.$6,hostname=$1,domainname=localdomain,hostfwd=tcp::$2-:22,hostfwd=tcp::$3-:55055,hostfwd=tcp::$4-:80 -device rtl8139,netdev=net0,mac=52:54:98:76:54:$5 -boot once=n -m 2048 -nodefaults $graphics >$target/$1.log &"
      echo command: $command 
  fi
  ((THISSSHPORT++))
  ((THISSOSPORT++))
  ((THISUIPORT++))
  ((THISHTTPSPORT++))
  ((THISMAC++))
  ((THISIP++))

}

# Usage info
show_help() {
  cat <<EOF
Usage: ${0##*/}
Start syncosync instances with X86 qemus 

    -a          alice 1st image (defaults to alice.img)
    -b          alice 2nd image
    -c          alice 3rd image

    -d          bob 1st image (defaults to bob.img)
    -e          bob 2nd image
    -f          bob 3rd image

    -g          start with graphics (to get logins)
    -h          display this help and exit
    -n          do nothing, just show commands
    -t          target dir (defaults to "${target}")
EOF
}

graphics="-nographic"
ali1="alice.img"
ali2=""
ali3=""
bob1="bob.img"
bob2=""
bob3=""
nothing=0

OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts "a:b:c:d:e:f:t:hgn" opt; do
  case "$opt" in
  a) ali1=$OPTARG;;
  b) ali2=$OPTARG;;
  c) ali3=$OPTARG;;
  d) bob1=$OPTARG;;
  e) bob2=$OPTARG;;
  f) bob3=$OPTARG;;
  t) target=$OPTARG;;
  n) nothing=1;;
  h)
    show_help
    exit 0
    ;;
  g)
    graphics=""
    ;;

  '?')
    show_help >&2
    exit 1
    ;;
  esac
done

shift "$((OPTIND - 1))" # Shift off the options and optional --.

if [ -z ${target+x} ]; then
    echo target is not set. Aborting.
    exit 1
fi

if [ ! -d "$target" ]; then
    echo no dir "$target" existing. Aborting.
    exit 1
fi

if [ -f $target/img_run.stat ]; then
    echo $target/img_run.stat is still existing.
    echo issue ./stop_img.sh first
    exit 1
fi

THISSSHPORT=2222
THISSOSPORT=55055
THISUIPORT=5555
THISHTTPSPORT=4443
THISMAC=10
THISIP=15

start_qemu alice $ali1 $ali2 $ali3
start_qemu bob $bob1 $bob2 $bob3

cd $OLDWPD
