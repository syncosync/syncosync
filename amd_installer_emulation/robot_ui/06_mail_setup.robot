*** Settings ***
Documentation          Setup: Mails

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary
Suite Setup            Open WebUI And SSH
Suite Teardown         Close WebUI And SSH

*** Test Cases ***

UI Login
    Switch Browser       alice
    Valid Login

Setup
    Setup

Mail Setup
    [Documentation]    Change Mail Setup from UI
    Switch Connection  alice
    Click Element      mail_setup
    Input Text         xpath: //input[@id="name"]    syncosync Housekeeper
    Input Text         xpath: //input[@id="username"]    syncosync@example.com
    Input Text         xpath: //input[@id="receiverAddress"]    admin@example.com
    Input Text         xpath: //input[@id="remoteReceiverAddress"]    remote_admin@example.com
    Input Text         xpath: //input[@id="adminInfoPeriod"]    7
    Input Text         xpath: //input[@id="hostname"]    smarthost.example.com
    Input Text         xpath: //input[@id="credential"]    sender@example.com
    Input Text         xpath: //input[@id="password"]    joshua
    Click Element        xpath: //button[@type="submit"]
    Wait For Result    mail.py -g   \"smtp_host\": \"smarthost.example.com\"
    Capture Page Screenshot   ${SCREENSHOT_PATH}/mail_setup.png

Leave Setup
    Leave Setup

UI Logout
    Valid Logout
























