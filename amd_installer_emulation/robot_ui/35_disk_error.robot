*** Settings ***
Documentation          Disk Error Page

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary
Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Variables ***
${URL ALICE}           http://localhost:5555/en-US/
${BROWSER}             Chrome
${DELAY}               1
${TIMEOUT}             20

*** Test Cases ***

Prepare
    Open Chrome          ${URL ALICE}   alice
    Set Selenium Speed   ${DELAY}
    Set Selenium Timeout  ${TIMEOUT}

switch to disk error
    [Documentation]             Switch Manually to Disk Error
                                Switch Connection  alice
    ${content}                  Execute Command    sysstate.py -v DEBUG --disk_error
                                Log   ${content}
    ${content}                  Wait For State   Disk Error

Check for Disk Error Dot
    Switch Browser                      alice
    Wait Until Page Contains Element    xpath: //div[@ngbtooltip="Disk error"]

Check for Disk Error Page
    Switch Browser                      alice
    Valid Login
    Page Should Contain                 Disk Error
    Capture Page Screenshot             ${SCREENSHOT_PATH}/disk_error_page.png
    Valid Logout

switch to default
    [Documentation]             Switch Manually to Default
                                Switch Connection  alice
    ${content}                  Execute Command    sysstate.py -v DEBUG --default
                                Log   ${content}
    ${content}                  Wait For State   Default
