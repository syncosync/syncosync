*** Settings ***
Documentation          Remote Recovery

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary
Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Variables ***
${URL ALICE}           http://localhost:5555/en-US/
${BROWSER}             Chrome
${DELAY}               1
${TIMEOUT}             20

*** Test Cases ***

create configbackup
                                Configbackup Alice

reset system
    [Documentation]             Reset the system and restart syncosync service
                                Switch Connection  alice
    ${content}                  Execute Command  factory_reset.py -r -e -y
                                Log   ${content}
                                Execute Command  service syncosync restart
    ${content}                  Wait For State   Setup Disks

Valid Login
    ${alice_win}                Open Chrome          ${URL ALICE}
                                Set Selenium Speed   ${DELAY}
                                Set Selenium Timeout  ${TIMEOUT}
                                Page Should Contain  Login
                                Input Text           password  sosadmin
                                Click Button         Login
                                Log Location
                                Page Should Contain Element  xpath: //button[@id="logout-button"]

Disk Setup  
                                Page Should Contain  Disk Setup
                                Page Should Contain  This box is in factory state
                                Page Should Contain  QEMU HARDDISK
                                Page Should Contain  Format all disks

Format Disks
                                Click Button         Format all disks
                                Page Should Contain  Format disks?
                                Click Button         Confirm
                                Wait Until Page Contains  Disks have been formatted   60
                                Click Button         OK
    
Fresh setup
                                Wait Until Page Contains  Fresh Setup   60
                                Click Button         Remote Recovery (configuration backup)

Start remote recovery   
                                Page Should Contain  syncosync admin password for configuration restore needed
    ${content}                  Run   ls -1 -t -d $PWD/sosconf* | head -1
                                Choose File          xpath: //input[@id="file-input"]   ${content}
                                Input Text           password  sosadmin
                                Capture Page Screenshot   ${SCREENSHOT_PATH}/remote_recovery_config_backup_file_dialog.png
                                Click Button         Restore

Remote Recovery in Progress
                                Wait Until Page Contains  Remote Recovery in Progress
                                Capture Page Screenshot   ${SCREENSHOT_PATH}/remote_recovery_in_progress.png


Remote Recovery finished
                                Wait Until Page Contains  Remote Recovery Final Report   600
                                Page Should Contain       Restored account: ali_ta1
                                Page Should Contain       Configuration restore successful
                                Capture Page Screenshot   ${SCREENSHOT_PATH}/remote_recovery_final_report.png
                                Click Button              Switch to Default Operation

Default Page   
                                Wait Until Page Contains  Accounts
                                Page Should Contain       ali_ta1