*** Settings ***
Documentation          Check UI Login

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary
Suite Setup            Open WebUI And SSH
Suite Teardown         Close WebUI And SSH

*** Test Cases ***

Disaster Swap Login
    Switch Browser       alice
    Capture Page Screenshot   ${SCREENSHOT_PATH}/default_login.png
    Click Link     Disaster Swap
    Page Should Contain  Remote's admin-password
    Input Text           password  sosadmin
    Capture Page Screenshot   ${SCREENSHOT_PATH}/disaster_swap_password.png
    Click Button         Disaster swap
    Log Location
    Wait Until Page Contains   Switch back
    Capture Page Screenshot   ${SCREENSHOT_PATH}/disaster_swap_login.png


Check for bob_ta1
    Valid Login
    Capture Page Screenshot   ${SCREENSHOT_PATH}/disaster_swap_status.png
    Wait until Page Contains   bob_ta1
    Valid Logout

Switch Back To Alice
    Click Link              Switch back
    Page Should Contain     Local admin-password 
    Input Text              password  sosadmin
    Capture Page Screenshot   ${SCREENSHOT_PATH}/switch_back_password.png
    Click Button            Switch back 
    Wait Until Page Contains   Disaster Swap

Check for ali_ta1
    Valid Login
    Wait until Page Contains   ali_ta1
    Valid Logout


Disaster Swap Login Again
    Click Link     Disaster Swap
    Page Should Contain  Remote's admin-password
    Input Text           password  sosadmin
    Click Button         Disaster swap
    Log Location
    Wait Until Page Contains   Switch back

Make Disaster Swap Persistent
    Valid Login
    Wait until Page Contains   bob_ta1
    Setup
    Capture Page Screenshot   ${SCREENSHOT_PATH}/disaster_mode_setup.png
    Click Element         system_setup
    Click Element         disaster_swap_persistence_tab 
    Capture Page Screenshot   ${SCREENSHOT_PATH}/disaster_swap_persistence.png
    Page Should Contain   Disaster Swap Persistence
    Click Button          Make Disaster Swap Mode Persistant
    Page Should Contain   After confirmation
    Capture Page Screenshot   ${SCREENSHOT_PATH}/confirm_disaster_swap_persistence.png
    Click Button          Confirm
    Page Should Contain   bob_ta1
    Page Should Not Contain   Disaster Swap
    Valid Logout

Disaster Swap to Alice Again
    Click Link     Disaster Swap
    Page Should Contain  Remote's admin-password
    Input Text           password  sosadmin
    Click Button         Disaster swap
    Log Location
    Wait Until Page Contains   Switch back

Make Disaster Swap to Alice Persistent
    Valid Login
    Wait until Page Contains   ali_ta1
    Setup
    Click Element         system_setup
    Click Element         disaster_swap_persistence_tab 
    Page Should Contain   Disaster Swap Persistence
    Click Button          Make Disaster Swap Mode Persistant
    Page Should Contain   After confirmation
    Click Button          Confirm
    Wait until Page Contains   ali_ta1
    Page Should Not Contain   Disaster Swap
    Valid Logout

