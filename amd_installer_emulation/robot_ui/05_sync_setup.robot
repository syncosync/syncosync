*** Settings ***
Documentation          Setup: Sync

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary
Suite Setup            Open WebUI And SSH
Suite Teardown         Close WebUI And SSH

*** Test Cases ***

UI Login
    Switch Browser       alice
    Valid Login

Setup
    Setup

Change Shaping Bitrates
    [Documentation]      Open Traffic Shaping
    Switch Connection    alice
    Click Element        sync_setup
    Capture Page Screenshot   ${SCREENSHOT_PATH}/sync_setup.png
    Click Element        trafficshaping_tab
    Input Text           xpath: //input[@id="bandwithUpDay"]    1000
    Input Text           xpath: //input[@id="bandwithDownDay"]    2000
    Input Text           xpath: //input[@id="bandwithUpNight"]    3000
    Input Text           xpath: //input[@id="bandwithDownNight"]    4000
    Click Element        xpath: //button[@type="submit"]
    Wait For Result      trafficshape.py -g   \"in_day\": 2000
    ${content}           Execute Command  trafficshape.py -g
    Should Contain       ${content}   \"out_day\": 1000 
    Should Contain       ${content}   \"out_night\": 3000 
    Should Contain       ${content}   \"in_night\": 4000 
    Input Text           xpath: //input[@id="bandwithUpDay"]    10000
    Input Text           xpath: //input[@id="bandwithDownDay"]   10000
    Input Text           xpath: //input[@id="bandwithUpNight"]    10000
    Input Text           xpath: //input[@id="bandwithDownNight"]    10000
    Click Element        xpath: //button[@type="submit"]
    Capture Page Screenshot   ${SCREENSHOT_PATH}/trafficshaping_setup.png

# Change Partitioning Part One
#     [Documentation]      Change Partitioning
#     Switch Connection    alice
#     Click Element        sync_setup
#     Click Element        partitioning_tab
#     Capture Page Screenshot   ${SCREENSHOT_PATH}/partitioning_intro.png
#     Click Element        next

#     Switch Browser       bob
#     Valid Login
#     Setup
#     Click Element        sync_setup
#     Click Element        partitioning_tab
#     Click Element        next

# Change Partitioning Part Two
#     [Documentation]      Exchange SSD
#     Switch Browser       alice
#     Wait Until Page Contains    System Setup Data Synchronization
#     Capture Page Screenshot   ${SCREENSHOT_PATH}/partitioning_select_role.png
#     Click Element        lead_partitioning
#     Switch Browser       bob
#     Click Element        follow_partitioning
#     Wait Until Page Contains    Follow Partner
#     Switch Browser       alice
#     Wait Until Page Contains    Adjust Partitions
#     Capture Page Screenshot   ${SCREENSHOT_PATH}/adjust_partitions.png
#     Drag And Drop By Offset   xpath: //span[@class='ngx-slider-span ngx-slider-pointer ngx-slider-pointer-min']   100   0
#     Click Button         Partition Volume
#     Wait Until Page Contains  Partition volume?
#     Click Button         Confirm
#     Wait Until Page Contains  Volume has been partitioned accordingly
#     Click Button         OK
#     Click Button         Send partitioning data

# Follow Partitioning Bob
#     Switch Browser       bob
#     Wait Until Page Contains  Follow Partner
#     Click Button         Receive partitioning distribution
#     Wait Until Page Contains  Accept partitioning
#     Click Button         Confirm
#     Wait Until Page Contains  Volume has been partitioned accordingly
#     Click Button         OK


Sync Speed Selection
    Switch Browser       alice
    Switch Connection    alice
    Leave Setup
    Click Element        xpath: //button[@id="shaping-button"]
    Capture Page Screenshot   ${SCREENSHOT_PATH}/syncspeed_selection.png
    Click Element        xpath: //label[@for="night"]
    Wait For Result      trafficshape.py -c   \"day\": false
    Click Element        xpath: //button[@id="shaping-button"]
    Click Element        xpath: //label[@for="no_sync"]
    Wait For State       Default No Sync
    Capture Page Screenshot   ${SCREENSHOT_PATH}/no_sync_mode.png
    Click Element        xpath: //button[@id="shaping-button"]
    Click Element        xpath: //label[@for="day"]
    Wait For Result      trafficshape.py -c   \"day\": true
    Wait For State       Default


UI Logout
    Switch Browser       alice
    Valid Logout
    # Switch Browser       bob
    # Valid Logout

