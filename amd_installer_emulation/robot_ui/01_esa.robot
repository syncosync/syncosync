*** Settings ***
Documentation          Fresh Setup with ESA

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary
Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Variables ***
${URL ALICE}           http://localhost:5555/en-US/
${BROWSER}             Chrome
${DELAY}               1
${TIMEOUT}             20

*** Test Cases ***

Prepare Alice
    Factory Reset        alice
    Open Chrome          ${URL ALICE}   alice
    Set Selenium Speed   ${DELAY}
    Set Selenium Timeout  ${TIMEOUT}
    Wait Until Page Contains   Login
    Capture Page Screenshot   ${SCREENSHOT_PATH}/factory_state_login.png
    Input Text           password  sosadmin
    Click Button         Login
    Log Location
    Page Should Contain Element  xpath: //button[@id="logout-button"]

Prepare Bob
    Factory Reset        bob
    Open Chrome          ${URL BOB}   bob
    Switch Browser       bob
    Wait Until Page Contains   Login
    Input Text           password  sosadmin
    Click Button         Login
    Page Should Contain Element  xpath: //button[@id="logout-button"]

Disk Setup Alice
    Switch Browser       alice
    Wait Until Page Contains   Disk Setup
    Page Should Contain  This box is in factory state
    Page Should Contain  QEMU HARDDISK
    Page Should Contain  Format all disks
    Capture Page Screenshot   ${SCREENSHOT_PATH}/factory_format_disks.png

Disk Setup Bob
    Switch Browser       bob
    Wait Until Page Contains   Disk Setup
    Page Should Contain  This box is in factory state
    Page Should Contain  QEMU HARDDISK
    Page Should Contain  Format all disks

Format Disks Alice
    Switch Browser       alice
    Click Button         Format all disks
    Wait Until Page Contains   Format disks?
    Click Button         Confirm
    Wait Until Page Contains  Disks have been formatted
    Click Button         OK
    
Format Disks Bob
    Switch Browser       bob
    Click Button         Format all disks
    Wait Until Page Contains   Format disks?
    Click Button         Confirm
    Wait Until Page Contains  Disks have been formatted
    Click Button         OK
    
Fresh setup Alice
    Switch Browser       alice
    Wait Until Page Contains  Fresh Setup    
    Capture Page Screenshot   ${SCREENSHOT_PATH}/fresh_setup_recovery.png
    Click Button         Fresh setup   
    Wait Until Page Contains   Confirm fresh setup
    Click Button         Confirm

Fresh setup Bob
    Switch Browser       bob
    Wait Until Page Contains   Fresh Setup    
    Click Button         Fresh setup   
    Wait Until Page Contains   Confirm fresh setup
    Click Button         Confirm

Easy Setup Assistant Alice
    Switch Browser       alice
    Wait Until Page Contains   Easy Setup Assistant
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_introduction.png
    Click Button         Next

Easy Setup Assistant Bob
    Switch Browser       bob
    Wait Until Page Contains   Easy Setup Assistant
    Click Button         Next

Host Name Configuration Alice
    Switch Browser       alice
    Wait Until Page Contains   Host Name Configuration
    Input Text           hostname   alice
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_hostname.png
    Click Button         Change hostname
  
Host Name Configuration Bob
    Switch Browser       bob
    Wait Until Page Contains   Host Name Configuration
    Input Text           hostname   bob
    Click Button         Change hostname
  
Alice Dhcp Off
    Switch Browser       alice
    Wait Until Page Contains   Network Interfaces Configuration
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_nic.png
    Click Element        eth0_v4Dhcp
    Click Element       xpath: //button[@type="submit"]
#    Click Button         Next

NIC Configuration Bob
    Switch Browser       bob
    Wait Until Page Contains   Network Interfaces Configuration
    Click Button         Next

Nameserver Configuration Alice
     Switch Browser       alice
     Wait Until Page Contains   Nameserver Configuration
     Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_nameserver.png
     Click Button         Back

Alice Dhcp On
    Switch Browser       alice
    Wait Until Page Contains   Network Interfaces Configuration
    Click Element        eth0_v4Dhcp
    Click Element       xpath: //button[@type="submit"]

# Nameserver Configuration Bob
#     Switch Browser       bob
#     Page Should Contain  Nameserver Configuration
#     Click Button         Next

Mail Configuration Alice
    Switch Browser       alice
    Wait Until Page Contains   Mail Setup
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_mailsetup.png
    Click Button         Next

Mail Configuration Bob
    Switch Browser       bob
    Wait Until Page Contains   Mail Setup
    Click Button         Next

Admin Password Alice
    Switch Browser       alice
    Wait Until Page Contains   syncosync Admin Password
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_admin_password.png
    Click Button         Next
    
Admin Password Bob
    Switch Browser       bob
    Wait Until Page Contains   syncosync Admin Password
    Click Button         Next

Local Synchronization Key Alice
    Switch Browser       alice
    Wait Until Page Contains   Local Synchronization Key
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_local_key_empty.png
    Click Button         Generate new key
    Click Button         Next
    Wait Until Page Contains   Remote Host Configuration
    Click Button         Back
    Wait Until Page Contains   Local Synchronization Key
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_local_key_with_key.png
    Click Button         Next

Local Synchronization Key Bob
    Switch Browser       bob
    Wait Until Page Contains   Local Synchronization Key
    Click Button         Generate new key
    Click Button         Next

Sync Setup Alice
    Switch Browser       alice
    Wait Until Page Contains   Synchronization Setup    timeout=120
    Input Text           hostname   bob.example.org
    Input Text           port   55055
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_sync_setup.png
    Input Text           hostname   10.0.2.2
    Input Text           port   55056
    Click Button         Save
    
Sync Setup Bob
    Switch Browser       bob
    Wait Until Page Contains   Synchronization Setup    timeout=120
    Input Text           hostname   10.0.2.2
    Input Text           port   55055
    Click Button         Save
    Click Button         Follow key exchange
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_sync_follower_wait.png

Sync Lead Key Exchange Alice
    Switch Browser       alice
    Wait Until Page Contains   Synchronization Setup    timeout=120
    Click Button         Lead key exchange
    Wait Until Page Contains  Accept key    timeout=120
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_sync_accept_key.png
    Click Button         Confirm

Check Remote Key Displayed
    [Tags]               knownfailure
    Element Text Should Not Be    xpath: //div[@id="remote_key"]    ${EMPTY}

Next Partitioning
    Click Button         Next
    
Sync Follow Accept Key Bob
    Switch Browser       bob
    Wait Until Page Contains  Accept key    timeout=120
    Click Button         Confirm
    Click Button         Next

Lead Partitioning Alice
    Switch Browser       alice
    Wait Until Page Contains   Volume Partitioning    timeout=120
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_partition_decision.png
    Click Button         Lead Partitioning
    Wait Until Page Contains  Adjust Partitions     timeout=120
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_lead_partitioning.png
    Click Button         Partition Volume
    Wait Until Page Contains  Partition volume?     timeout=120
    Click Button         Confirm
    Wait Until Page Contains  Volume has been partitioned accordingly       timeout=120
    Click Button         OK
    Click Button         Send partitioning data

Follow Partitioning Bob
    Switch Browser       bob
    Wait Until Page Contains   Volume Partitioning      timeout=120
    Click Button         Follow Partitioning
    Wait Until Page Contains  Follow Partner     timeout=120
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_follow_partitioning.png
    Click Button         Receive partitioning distribution
    Wait Until Page Contains  Accept partitioning     timeout=120
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_accept_partitioning.png
    Click Button         Confirm
    Wait Until Page Contains  Volume has been partitioned accordingly      timeout=120
    Click Button         OK
    
ESA Success Alice
    Switch Browser       alice
    Wait Until Page Contains   ESA Finish
    Capture Page Screenshot   ${SCREENSHOT_PATH}/esa_finish.png
    Click Button         Continue
    Wait Until Page Contains  Disk  
    Capture Page Screenshot   ${SCREENSHOT_PATH}/default_mode_no_accounts.png

ESA Success Bob
    Switch Browser       bob
    Wait Until Page Contains   ESA Finish
    Click Button         Continue
    Wait Until Page Contains  Disk  

Logout Alice
    Switch Browser       alice
    Valid Logout

Logout Bob
    Switch Browser       bob
    Valid Logout
