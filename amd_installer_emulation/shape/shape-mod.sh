#!/bin/bash
# old version
#tc qdisc del dev eth0 root
#tc qdisc add dev eth0 root handle 1:0 htb default 2
#tc class add dev eth0 parent 1:0 classid 1:1 htb rate 5Mbit
#tc qdisc add dev eth0 parent 1:1 sfq
#tc filter add dev eth0 parent 1:0 protocol ip u32 match ip dport 55056 0xffff flowid 1:1
if [ $# -lt 2 ]
  then
    echo "We need two arguments or outgoing and imconing bandwidth in Mbit"
fi
if [ -z "$1" ]
  then
      echo "No outgoing bandwidth specified"
  else
      tc class change dev eth0 parent 1a1a: classid 1a1a:142 htb rate $1Mbit
fi
if [ -z "$1" ]
  then
      echo "No incoming bandwidth specified"
  else
      tc class change dev ifb6682 parent 1a1a: classid 1a1a:209 htb rate $2Mbit
fi
