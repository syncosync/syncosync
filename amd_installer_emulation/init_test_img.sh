#!/bin/bash -e

# This scripts sets up a x86 based sos appliance
# it downloads - if not available - a netinst.iso from debian
# it uses a preesed.cfg file
# after successful execution, there will be a file sos_x86.qcow
# (c) 2019 Stephan Skrodzki <stevieh@syncosync.org>

# according to https://wiki.debian.org/DebianInstaller/Preseed/EditIso

set +e

startpath=`pwd`

target='bullseye'

# Usage info
show_help() {
  cat <<EOF
Usage: ${0##*/}
Initialise the base image in a target 

    -t          target dir (bookworm / bullseye / buster defaults to "bullseye")
EOF
}

OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts "t:h" opt; do
  case "$opt" in
  t) target=$OPTARG;;
  h)
    show_help
    exit 0
    ;;
  '?')
    show_help >&2
    exit 1
    ;;
  esac
done

if [ ! -d $target ]; then
    echo $target not existing
    exit 1
fi

cd $target

mkdir cache/ || true

if md5sum -c cache/sos_test.qcow.md5 ; then
  echo "Skipping sos_test.qcow build"
else
  echo "Building sos_test.qcow"
  if [ -c /dev/kvm ]
  then
      ACCEL="-cpu host -accel kvm"
  elif [[ "$OSTYPE" == "darwin"* ]]; then
      # Big Sur requires signing/entitlements: https://web.archive.org/web/20210406175617/https://www.arthurkoziel.com/qemu-on-macos-big-sur/
      ACCEL="-cpu host -accel hvf"
  else
      ACCEL=""
  fi

  if [ -f config ]; then
    source config
  else
    echo "no config file found"
    exit 1
  fi

  echo copying image to syncosync.cow to sos_test.cow
  
  if [ ! -f syncosync.qcow ]; then
    cd ..
    ./init_base_img.sh -t $target
    cd $target
  fi

  cp syncosync.qcow sos_test.qcow

  # Next stage: creating single images for alice and bot

  echo Setting ssh keys and qemu systype

  cd ..
  
  if [ ! -f robotkey.pub ]; then
    echo "no robotkey existing, creating one"
    ssh-keygen -N '' -m pem -f "robotkey" -C "Robot key"
  fi

  robotkey=$(cat robotkey.pub)

  if [ -f developer_key.pub ]; then
    developer_key=$(cat developer_key.pub)
  else
    developer_key=""
  fi

  ./set_ssh_keys.exp "$target" "$robotkey" "$developer_key"

  cd $target
  if [ -f syncosync.qcow ] && [ -f sos_test.qcow ] && [ -f config ] ; then
    md5sum syncosync.qcow sos_test.qcow config > cache/sos_test.qcow.md5
  else
    echo "one of syncosync.qcow sos_test.qcow config not existing in $target"
    exit 1
  fi
fi
set -e

qemu-img create -f qcow2 -b sos_test.qcow -F qcow2 alice.qcow
qemu-img create -f qcow2 -b sos_test.qcow -F qcow2 bob.qcow

cd $startpath
