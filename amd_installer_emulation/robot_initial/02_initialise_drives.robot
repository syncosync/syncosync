*** Settings ***
Documentation          Init Drives and check mounting

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Test Cases ***
Check sysstate
    [Documentation]         Check Sysstate
                            Switch Connection  alice
    ${output}               Execute Command    sysstate.py -g
                            Log  ${output}

format drives
    [Documentation]              Format all available drives
                                 Switch Connection  alice
    ${stdout}                    Execute Command    sysstate.py --setup_disks
    ${stdout}                    Execute Command    drivemanager.py -f -y
    Should Contain               ${stdout}          Success, free space 112 extends
    ${stdout}                    Execute Command    pvs --separator ","
    Should Contain               ${stdout}          /dev/sdb,sos-
                                 Switch Connection  bob
    ${stdout}                    Execute Command    sysstate.py --setup_disks
    ${stdout}                    Execute Command    drivemanager.py -f -y
    Should Contain               ${stdout}          Success, free space 112 extends
    ${stdout}                    Execute Command    pvs --separator ","
    Should Contain               ${stdout}          /dev/sdb,sos-

create volumes
    [Documentation]              Create Volumes
                                 Switch Connection  alice  
    ${stdout}                    Execute Command    sysstate.py --setup_volumes
    ${stdout}                    Execute Command    drivemanager.py -c '{"local": 56 , "remote": 56 }' -y
    Should Contain               ${stdout}          Success
    ${stdout}                    Execute Command    lvs --separator ","
    Should Contain               ${stdout}          local,sos
    Should Contain               ${stdout}          remote,sos
    Should Contain               ${stdout}          system,sos
                                 Switch Connection  bob
    ${stdout}                    Execute Command    sysstate.py --setup_volumes
    ${stdout}                    Execute Command    drivemanager.py -c '{"local": 56 , "remote": 56 }' -y
    Should Contain               ${stdout}          Success
    ${stdout}                    Execute Command    lvs --separator ","
    Should Contain               ${stdout}          local,sos
    Should Contain               ${stdout}          remote,sos
    Should Contain               ${stdout}          system,sos

check mounted volumes
    [Documentation]              Mount Volumes
                                 Switch Connection  alice  
    ${stdout}                    Execute Command    sysstate.py --default_no_sync
    Should Contain               ${stdout}          Switching to Default No Sync successful
    ${stdout}                    Execute Command    mount
    Should Contain               ${stdout}          -local on /mnt/local type ext4 (rw,relatime,jqfmt=vfsv0,usrjquota=aquota.user)
    Should Contain               ${stdout}          -remote on /mnt/rjail/remote type ext4 (rw,relatime)
    Should Contain               ${stdout}          -system on /mnt/system type ext4 (rw,relatime
                                 Switch Connection  bob
    ${stdout}                    Execute Command    sysstate.py --default_no_sync
    Should Contain               ${stdout}          Switching to Default No Sync successful
