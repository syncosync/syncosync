#!/bin/bash

startpath=`pwd`

target='bullseye'

# Usage info
show_help() {
  cat <<EOF
Usage: ${0##*/}
Initialise the base image in a target 

    -t          target dir ((bookworm / bullseye / buster defaults to "bullseye")
EOF
}

OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts "t:h" opt; do
  case "$opt" in
  t) target=$OPTARG;;
  h)
    show_help
    exit 0
    ;;
  '?')
    show_help >&2
    exit 1
    ;;
  esac
done

if [ ! -d $target ]; then
    echo $target not existing
    exit 1
fi

cd $target

# for this magic, see: https://unix.stackexchange.com/questions/281589/how-to-run-mkfs-on-file-image-partitions-without-mounting
diskimg=diskimg                                          # filename of resulting disk image
size=$((500 * (1 << 20)))                                # desired size in bytes, 500MB in this case
alignment=1048576                                        # align to next MB (https://www.thomas-krenn.com/en/wiki/Partition_Alignment)
size=$(((size + alignment - 1) / alignment * alignment)) # ceil(size, 1MB)

echo Generate test images for drivesetup with a size of 500MB each

############################################
# alice.img and bob.img
# this is really an empty image
############################################

echo -n create emtpy disks for alice and bob: alice.img, bob.img ...
if [ -f alice.img ]; then
  rm alice.img
fi
if [ -f bob.img ]; then
  rm bob.img
fi
truncate -s $size alice.img
truncate -s $size bob.img
echo Ready
############################################
# empty-alice.img and empty-bob.img
# this is really an empty image
############################################
echo -n create emtpy disk for testing: empty.img ... 
if [ -f empty-alice.img ]; then
  rm empty-alice.img
fi
if [ -f empty-bob.img ]; then
  rm empty-bob.img
fi
truncate -s $size empty-alice.img
truncate -s $size empty-bob.img
echo Ready
############################################
# linux.img
# this is just a standard hdd with a linux partition on it
############################################
if [ -f linux.img.gz ]; then
  gunzip -f -k linux.img.gz
fi
diskimg=linux.img
if [ ! -f $diskimg ]; then
  echo -n create linux disk: $diskimg ...
  if [ -f "${diskimg}" ]; then
    rm "${diskimg}"
  fi
  if [ -f "${diskimg}".ext3 ]; then
    rm "${diskimg}".ext3
  fi
  # mkfs.ext3 requires size as an (undefined) block-count; seem to be units of 1k
  if ! command -v mkfs.ext3 2>&1 >/dev/null
  then
      echo "mkfs.ext3 is not installed. Hint: For macos, brew install e2fsprogs (and adjust PATH)"
      exit 1
  fi
  mkfs.ext3 -b 1024 -L "linuxext3" "${diskimg}".ext3 $((size >> 10))
  # insert the filesystem to a new file at offset 1MB
  dd if="${diskimg}".ext3 of="${diskimg}" conv=sparse obs=512 seek=$((alignment / 512))
  # extend the file by 1MB
  truncate -s "+${alignment}" "${diskimg}"
  echo -n apply partitioning ...
  if [[ "$OSTYPE" == "darwin"* ]]; then
      echo Missing adaptation for the usage of parted. Aborting.
      exit 1
  else
      parted --align optimal "${diskimg}" mklabel gpt mkpart linux "${alignment}B" '100%' set 1 boot on
  fi
  if [ -f "${diskimg}".ext3 ]; then
    rm "${diskimg}".ext3
  fi
fi
echo $diskimg ready
############################################
# dos.img
# this is just a standard hdd with a dos partition on it
############################################
if [ -f dos.img.gz ]; then
  gunzip -f -k dos.img.gz
fi
diskimg=dos.img
if [ ! -f $diskimg ]; then
  echo -n Create dos disk: $diskimg ...
  if [ -f "${diskimg}" ]; then
    rm "${diskimg}"
  fi
  if [ -f "${diskimg}".fat ]; then
    rm "${diskimg}".fat
  fi
  # mkfs.fat requires size as an (undefined) block-count; seem to be units of 1k
  if [[ "$OSTYPE" == "darwin"* ]]; then
      newfs_msdos -s $((size >> 10)) -F 32 "${diskimg}".fat
  else
      mkfs.fat -C -F32 -n "DOS" "${diskimg}".fat $((size >> 10))
  fi

  # insert the filesystem to a new file at offset 1MB
  dd if="${diskimg}".fat of="${diskimg}" conv=sparse obs=512 seek=$((alignment / 512))
  # extend the file by 1MB
  truncate -s "+${alignment}" "${diskimg}"
  if [[ "$OSTYPE" == "darwin"* ]]; then
      echo Missing adaptation for the usage of parted. Aborting.
      exit 1
  else
      parted --align optimal "${diskimg}" mklabel gpt mkpart fat "${alignment}B" '100%' set 1 boot on
  fi
  if [ -f "${diskimg}".fat ]; then
    rm "${diskimg}".fat
  fi
fi
echo $diskimg ready

cd $startpath

############################################
# ali_ta1.data
# for robot tests to alice
############################################
if [ -d ali_ta1.data ]; then
  rm -r ali_ta1.data
fi
if [ ! -d ali_ta1.data ]; then
  echo -n Create testdata for ali_ta1 ...
  mkdir ali_ta1.data
  for i in $(seq 0 6); do
    echo $i
    dd if=/dev/zero of=ali_ta1.data/ali_ta1_$i.data bs=10M count=1
  done
  echo Ready
fi
############################################
# bob_ta1.data
# for robot tests to bob
############################################
if [ -d bob_ta1.data ]; then
  rm -r bob_ta1.data
fi
if [ ! -d bob_ta1.data ]; then
  echo -n Create testdata for bob_ta1 ...
  mkdir bob_ta1.data
  for i in $(seq 0 6); do
    echo $i
    dd if=/dev/zero of=bob_ta1.data/bob_ta1_$i.data bs=10M count=1
  done
  echo Ready
fi
############################################
# ali_ta1.pub key for ali_ta1
# for robot tests
############################################
if [ ! -f ali_ta1.pub ]; then
  echo "No key for ali_ta1 existing, creating"
  ssh-keygen -N '' -b 2048 -t rsa -m pem -f "ali_ta1" -C "Alice Test Account 1"
fi
############################################
# bob_ta1.pub key bob_ta1
# for robot tests
############################################
if [ ! -f bob_ta1.pub ]; then
  echo "No key for ali_ta1 existing, creating"
  ssh-keygen -N '' -b 2048 -t rsa -m pem -f "bob_ta1" -C "Bob Test Account 1"
fi
echo Successful generated testdrives, account test data and account test keys
