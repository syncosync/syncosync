import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './helpers/guards/auth.guard';
import { StateSetupDisksGuard } from './helpers/guards/state-setup-disks.guard';
import { StateRemoteRecoveryGuard } from './helpers/guards/state-remote-recovery.guard';
import { StateVolumeSetupGuard } from './helpers/guards/state-volume-setup.guard';
import { StateDiskErrorGuard } from './helpers/guards/state-disk-error.guard';

const syncosyncModule = () =>
  import('./pages/syncosync/syncosync.module').then((x) => x.SyncosyncModule);
const authModule = () =>
  import('./pages/auth/auth.module').then((x) => x.AuthModule);
const disksetupModule = () =>
  import('./pages/disksetup/disksetup.module').then((x) => x.DisksetupModule);
const remoteRecoveryModule = () =>
  import('./pages/remote-recovery/remote-recovery.module').then(
    (x) => x.RemoteRecoveryModule
  );
const volumeSetupModule = () =>
  import('./pages/volumesetup/volumesetup.module').then(
    (x) => x.VolumesetupModule
  );
const diskerrorModule = () =>
  import('./pages/disk-error/disk-error.module').then((x) => x.DiskErrorModule);

const routes: Routes = [
  { path: 'auth', loadChildren: authModule },
  {
    path: 'syncosync',
    loadChildren: syncosyncModule,
    canActivate: [
      AuthGuard,
      StateSetupDisksGuard,
      StateRemoteRecoveryGuard,
      StateVolumeSetupGuard,
      StateDiskErrorGuard,
    ],
  },
  {
    path: 'remote-recovery',
    loadChildren: remoteRecoveryModule,
    canActivate: [
      AuthGuard,
      StateSetupDisksGuard,
      StateVolumeSetupGuard,
      StateDiskErrorGuard,
    ],
  },
  {
    path: 'disksetup',
    loadChildren: disksetupModule,
    canActivate: [
      AuthGuard,
      StateRemoteRecoveryGuard,
      StateVolumeSetupGuard,
      StateDiskErrorGuard,
    ],
  },
  {
    path: 'volumesetup',
    loadChildren: volumeSetupModule,
    canActivate: [
      AuthGuard,
      StateSetupDisksGuard,
      StateRemoteRecoveryGuard,
      StateDiskErrorGuard,
    ],
  },
  {
    path: 'diskerror',
    loadChildren: diskerrorModule,
    canActivate: [
      AuthGuard,
      StateSetupDisksGuard,
      StateRemoteRecoveryGuard,
      StateVolumeSetupGuard,
    ],
  },
  // otherwise redirect to home
  { path: '**', redirectTo: 'auth' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
