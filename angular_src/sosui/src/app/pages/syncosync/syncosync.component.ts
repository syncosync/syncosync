import { Component, OnInit } from '@angular/core';
import { SetupUiSwitchingService } from '../../../syncosync_modules/common/services/setup-ui-switching.service';
import { SysstateService } from '@syncosync_public';

import { SystemMode } from '@syncosync_common';

@Component({
  selector: 'app-syncosync',
  templateUrl: './syncosync.component.html',
  styleUrls: ['./syncosync.component.css'],
})
export class SyncosyncComponent implements OnInit {
  SystemMode = SystemMode;
  constructor(
    public setupUiSwitchingService: SetupUiSwitchingService,
    public sysstateService: SysstateService
  ) {}

  ngOnInit(): void {
    //
  }
}
