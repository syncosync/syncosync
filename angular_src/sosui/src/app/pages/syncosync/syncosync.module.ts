import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SyncosyncRoutes } from './syncosync.routes';
import { StatusComponent } from './pages/status/status.component';
import { SyncosyncComponent } from './syncosync.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StatusbarComponent } from './statusbar/statusbar.component';
import { NetworkSetupComponent } from './pages/network-setup/network-setup.component';
import { SyncosyncCommonModule } from '../../../syncosync_modules/common/syncosync-common.module';
import { SyncosyncAuthenticatedModalsModule } from '../../../syncosync_modules/authenticated/modals/syncosync-authenticated-modals.module';
import { SyncosyncComponentsModule } from '../../../syncosync_modules/authenticated/components/syncosync-components.module';
import { SyncosyncAuthenticatedFormsModule } from '../../../syncosync_modules/authenticated/forms/syncosync-authenticated-forms.module';
import { SyncosyncAuthenticatedGraphsModule } from '../../../syncosync_modules/authenticated/graphs/syncosync-authenticated-graphs.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BootstrapIconsModule } from 'ng-bootstrap-icons';
import { allIcons } from 'ng-bootstrap-icons/icons';
import { RestErrorInterceptor } from '../../helpers/interceptors/rest-error-interceptor.service';
import { SyncosyncPublicGraphsModule } from '../../../syncosync_modules/public/graphs/syncosync-public-graphs.module';
import { SyncosyncPublicModule } from '../../../syncosync_modules/public/syncosync-public.module';
import { SyncSetupComponent } from './pages/sync-setup/sync-setup.component';
import { AdminPasswordComponent } from './pages/admin-password/admin-password.component';
import { EmailSetupComponent } from './pages/email-setup/email-setup.component';
import { SyncosyncAuthenticatedModule } from '../../../syncosync_modules/authenticated/syncosync-authenticated.module';
import { SystemSetupComponent } from './pages/system-setup/system-setup.component';
import { KeyExchangeTabComponent } from './pages/sync-setup/key-exchange-tab/key-exchange-tab.component';
import { PartitioningTabComponent } from './pages/sync-setup/partitioning-tab/partitioning-tab.component';
import { MaintenanceCollectionComponent } from './pages/system-setup/maintenance-collection/maintenance-collection.component';

@NgModule({
  declarations: [
    StatusComponent,
    SyncosyncComponent,
    NetworkSetupComponent,
    StatusbarComponent,
    NavbarComponent,
    SyncSetupComponent,
    AdminPasswordComponent,
    EmailSetupComponent,
    SystemSetupComponent,
    KeyExchangeTabComponent,
    PartitioningTabComponent,
    MaintenanceCollectionComponent,
  ],
  imports: [
    BootstrapIconsModule.pick(allIcons),
    CommonModule,
    SyncosyncRoutes,
    NgbModule,
    ChartsModule,
    ReactiveFormsModule,
    FormsModule,
    SyncosyncCommonModule,
    SyncosyncAuthenticatedModalsModule,
    SyncosyncComponentsModule,
    SyncosyncAuthenticatedFormsModule,
    SyncosyncAuthenticatedGraphsModule,
    SyncosyncPublicGraphsModule,
    SyncosyncPublicModule,
    SyncosyncAuthenticatedModule,
  ],
  exports: [BootstrapIconsModule, SyncosyncComponent, StatusbarComponent],
  providers: [
    NgbActiveModal,
    { provide: HTTP_INTERCEPTORS, useClass: RestErrorInterceptor, multi: true },
  ],
})
export class SyncosyncModule {}
