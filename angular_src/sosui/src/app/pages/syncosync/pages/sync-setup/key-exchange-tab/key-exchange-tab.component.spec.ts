import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyExchangeTabComponent } from './key-exchange-tab.component';

describe('KeyExchangeTabComponent', () => {
  let component: KeyExchangeTabComponent;
  let fixture: ComponentFixture<KeyExchangeTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [KeyExchangeTabComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyExchangeTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
