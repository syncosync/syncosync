import { Component } from '@angular/core';
import { PartitioningResizeSliderAssistantComponent } from '../../../../../../syncosync_modules/authenticated/assistants/partitioning-partials/partitioning-slide/partitioning-resize-slider-assistant.component';
import { UpdateSystemSetupDataComponent } from '../../../../../../syncosync_modules/authenticated/assistants/partitioning-partials/update-sync-setup-data/update-system-setup-data.component';
import { PartitioningResizePrecedingInformationComponent } from '../../../../../../syncosync_modules/authenticated/assistants/partitioning-partials/partitioning-resize-preceding-information/partitioning-resize-preceding-information.component';
import { HandleCurrentPartitioningStatusComponent } from '../../../../../../syncosync_modules/authenticated/assistants/partitioning-partials/handle-current-partitioning-status/handle-current-partitioning-status.component';
import { ShowFinalPartitioningInfoComponent } from '../../../../../../syncosync_modules/authenticated/assistants/partitioning-partials/show-final-partitioning-info/show-final-partitioning-info.component';

@Component({
  selector: 'app-partitioning-tab',
  templateUrl: './partitioning-tab.component.html',
  styleUrls: ['./partitioning-tab.component.css'],
})
export class PartitioningTabComponent {
  setupPartialsToShow = [];
  constructor() {
    //
  }

  ngOnInit(): void {
    // Alternative B Phase 1: https://gitlab.com/syncosync/syncosync/-/issues/137
    this.setupPartialsToShow.push(
      PartitioningResizePrecedingInformationComponent
    );
    // Alternative B Phase 2a: https://gitlab.com/syncosync/syncosync/-/issues/137
    this.setupPartialsToShow.push(UpdateSystemSetupDataComponent);
    // Alternative B Phase 2b; Repartition if applicable
    this.setupPartialsToShow.push(HandleCurrentPartitioningStatusComponent);
    // Alternative B Phase 3: https://gitlab.com/syncosync/syncosync/-/issues/137
    this.setupPartialsToShow.push(PartitioningResizeSliderAssistantComponent);
    this.setupPartialsToShow.push(ShowFinalPartitioningInfoComponent);
  }
}
