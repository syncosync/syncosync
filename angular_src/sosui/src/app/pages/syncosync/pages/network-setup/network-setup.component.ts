import { Component, OnDestroy, OnInit } from '@angular/core';
import { SysstateService } from '@syncosync_public';
import { SystemMode } from '@syncosync_common';

@Component({
  selector: 'app-network-setup',
  templateUrl: './network-setup.component.html',
  styleUrls: ['./network-setup.component.css'],
})
export class NetworkSetupComponent implements OnInit, OnDestroy {
  SystemMode = SystemMode;

  constructor(public sysstateService: SysstateService) {}

  ngOnDestroy() {}

  ngOnInit() {}
}
