import { Component } from '@angular/core';
import { AbstractSubscriberComponent } from '@syncosync_common';
import { SetupUiSwitchingService } from '../../../../syncosync_modules/common/services/setup-ui-switching.service';
import { SysstateService } from '@syncosync_public';
import { SystemMode } from '@syncosync_common';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent extends AbstractSubscriberComponent {
  SystemMode = SystemMode;
  constructor(
    public setupUiSwitchingService: SetupUiSwitchingService,
    public sysstateService: SysstateService
  ) {
    super();
  }
}
