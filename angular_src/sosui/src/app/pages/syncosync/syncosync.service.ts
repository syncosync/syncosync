import {Injectable} from '@angular/core';
import {SyncosyncModule} from './syncosync.module';
import * as Rx from 'rxjs';

@Injectable({
    providedIn: SyncosyncModule
})
export class SyncosyncService {
    private subject: Rx.Subject<MessageEvent>;

    constructor() {
    }
}
