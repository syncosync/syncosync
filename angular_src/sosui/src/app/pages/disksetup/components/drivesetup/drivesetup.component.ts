import { Component, EventEmitter } from '@angular/core';
import {
  AdminPassword,
  ConfirmationModalResult,
  Drive,
  Drives,
  DrivesetupDriveConfiguration,
  GenericUiResponse,
  GenericUiResponseStatus,
  HelpStateService,
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
  SystemMode,
} from '@syncosync_common';
import {
  DrivesetupService,
  SosConfigService,
  SystemService,
} from '@syncosync_authenticated';
import { PasswordCheckComponent } from '../../../../../syncosync_modules/common/forms/password-check/password-check.component';
import { AbstractEmptyFormDirective } from '../../../../../syncosync_modules/common/components/abstract-empty-form-directive/abstract-empty-form.directive';
import { takeUntil } from 'rxjs/operators';
import { SysstateService } from '@syncosync_public';

@Component({
  selector: 'app-drivesetup',
  templateUrl: './drivesetup.component.html',
  styleUrls: ['./drivesetup.component.css'],
})
export class DrivesetupComponent extends AbstractEmptyFormDirective<DrivesetupDriveConfiguration> {
  drives: Drives = null;
  checkboxChanged: EventEmitter<Drive> = new EventEmitter<Drive>();
  current_vg_uuid: string = null;
  selected_drive: Drive = null;

  constructor(
    public drivesetupService: DrivesetupService,
    public sosConfigService: SosConfigService,
    private modalService: SyncosyncModalService,
    public helpStateService: HelpStateService,
    private systemService: SystemService,
    private sysstateService: SysstateService
  ) {
    super();
  }

  updateVgSelection(checked: boolean, drive: Drive): void {
    if (checked) {
      console.log('Checked ' + drive.device);
      this.selected_drive = drive;
    } else {
      console.log('Unchecked ' + drive.device);
      this.selected_drive = null;
    }
  }

  useAndExpand(): void {
    if (this.current_vg_uuid == this.selected_drive.vg_uuid) {
      // Simple expand of the current VG
      this.modalService
        .showConfirmationModal(
          LocalizedMessage.DRIVESETUP_DISK_FORMAT_CONFIRMATION_TITLE,
          LocalizedMessage.DRIVESETUP_DISK_USE_AND_EXPAND_CONFIRMATION_CONTENT,
          'Volume Group UUID: ' + this.selected_drive.vg_uuid
        )
        .subscribe((userDecision) => {
          if (userDecision == ConfirmationModalResult.CONFIRM) {
            this.formatDrivesConfirmed(this.selected_drive.vg_uuid);
          }
        });
    } else {
      // The user tries to restore a configuration/VG.
      // This requires a password.
      this.modalService
        .showRequestDataModal<AdminPassword, PasswordCheckComponent>(
          LocalizedMessage.PASSWORD_OF_OTHER_CONFIG_REQUIRED,
          PasswordCheckComponent,
          LocalizedMessage.PASSWORD_OF_OTHER_CONFIG_REQUIRED_EXPLANATION
        )
        .subscribe((result) => {
          if (result === undefined || result === null) {
            // User aborted password input
            // nop
          } else {
            // Send password along with select option and give it a try...
            this.model.password_confirmation = result.password;
            this.tryRestore(this.selected_drive.vg_uuid);
          }
        });
    }
  }

  eraseAllAndUseForVg(): void {
    this.modalService
      .showConfirmationModal(
        LocalizedMessage.DRIVESETUP_DISK_FORMAT_CONFIRMATION_TITLE,
        LocalizedMessage.DRIVESETUP_DISK_ERASE_ALL_CONFIRMATION_CONTENT
      )
      .subscribe((userDecision) => {
        if (userDecision == ConfirmationModalResult.CONFIRM) {
          this.formatDrivesConfirmed(null);
        }
      });
  }

  shutdownSystem(): void {
    // TODO: Show confirmation modal for the request below!
    this.modalService
      .showConfirmationModal(
        LocalizedMessage.SHUTDOWN_SYSTEM_CONFIRMATION_TITLE,
        LocalizedMessage.SHUTDOWN_SYSTEM_CONFIRMATION_CONTENT
      )
      .subscribe((userDecision) => {
        if (userDecision == ConfirmationModalResult.CONFIRM) {
          this.shutdownSystemConfirmed();
        }
      });
  }

  protected initialize(): void {
    super.initialize();
    this.sosConfigService.getConfig().subscribe((config) => {
      if (config === undefined || config === null) {
        // TODO
      } else {
        this.current_vg_uuid = config.vg_sos_uuid;
        if (this.drives != null) {
          this.selectDefault();
        }
      }
    });
    this.drivesetupService.getDrives().subscribe((drives) => {
      if (drives === undefined || drives === null) {
        // TODO
      } else {
        this.drives = drives;
        if (this.current_vg_uuid != null) {
          this.selectDefault();
        }
      }
    });
  }

  protected createFormGroup(): void {
    // nop
  }

  private selectDefault() {
    for (const drive of this.drives.drives) {
      if (drive.vg_uuid == this.current_vg_uuid) {
        this.selected_drive = drive;
        break;
      }
    }
  }

  private formatDrivesConfirmed(vg_uuid: string) {
    this.model.selected_vg_uuid = vg_uuid;

    const resultEmitter: EventEmitter<GenericUiResponse> =
      new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined) {
        if (result.status == GenericUiResponseStatus.OK) {
          this.modalService.showInfoModal(
            InfoLevel.INFO,
            result.localized_reason,
            result.additional_information
          );
          this.formFinished.emit(this.model);
        } else {
          this.modalService.showInfoModal(
            InfoLevel.WARNING,
            result.localized_reason,
            result.additional_information
          );
        }
      }
    });
    this.modalService.showProcessingModal<GenericUiResponse>(
      LocalizedMessage.DRIVESETUP_DISK_FORMAT_IN_PROGRESS_TITLE,
      LocalizedMessage.DRIVESETUP_DISK_FORMAT_IN_PROGRESS_CONTENT,
      this.drivesetupService.sendDriveConfiguration(this.model),
      null,
      resultEmitter
    );
  }

  private tryRestore(vg_uuid: string) {
    this.model.selected_vg_uuid = vg_uuid;

    const resultEmitter: EventEmitter<GenericUiResponse> =
      new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined) {
        if (result.status == GenericUiResponseStatus.OK) {
          this.modalService.showInfoModal(
            InfoLevel.INFO,
            LocalizedMessage.CONFIGURATION_SUCCESSFULLY_RESTORED,
            result.additional_information
          );
          // Redirect to setup overview immediately as the user has basically finished drivesetup at this point
          this.sysstateService.latestSysstateObservable$
            .pipe(takeUntil(this.unsubscribe))
            .subscribe((res) => {
              if (res.actmode != SystemMode.SETUP_DISKS) {
                this.unsubscribe.next();
                this.formFinished.emit(this.model);
                return false;
              } else {
                return true;
              }
            });
        } else {
          this.modalService.showInfoModal(
            InfoLevel.WARNING,
            result.localized_reason,
            result.additional_information
          );
        }
      }
    });
    this.modalService.showProcessingModal<GenericUiResponse>(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.DRIVESETUP_CONFIG_RESTORE_IN_PROGRESS_CONTENT,
      this.drivesetupService.sendDriveConfiguration(this.model),
      null,
      resultEmitter
    );
  }

  private shutdownSystemConfirmed() {
    const resultEmitter: EventEmitter<GenericUiResponse> =
      new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined) {
        // Shutdown should just go through?
        this.modalService.showInfoModal(
          InfoLevel.WARNING,
          result.localized_reason,
          result.additional_information
        );
      }
    });
    this.modalService.showProcessingModal<GenericUiResponse>(
      LocalizedMessage.SHUTDOWN_SYSTEM_IN_PROGRESS_TITLE,
      LocalizedMessage.SHUTDOWN_SYSTEM_IN_PROGRESS_CONTENT,
      this.systemService.shutdownSystem(),
      null,
      resultEmitter
    );
  }

  onSubmit(): void {
    // NOP for now...
  }

  protected getInitModel(): DrivesetupDriveConfiguration {
    return new DrivesetupDriveConfiguration();
  }
}
