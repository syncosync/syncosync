import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Drive, Drives, HelpStateService } from '@syncosync_common';

@Component({
  selector: 'app-drivebox',
  templateUrl: './drivebox.component.html',
  styleUrls: ['./drivebox.component.css'],
})
export class DriveboxComponent implements OnInit {
  @Input() drive: Drive = null;
  // TODO:  VG UUID that was last known (provided by sysstate)
  @Input() lastUsedVgUuid: string = null;
  @Input() otherCheckboxTriggered: EventEmitter<Drive> = null;
  @Input() drives: Drives = null;
  @Input() current_vg_uuid = '';

  constructor(public helpStateService: HelpStateService) {
    //
  }

  ngOnInit(): void {
    this.otherCheckboxTriggered.subscribe((drive) => {
      if (drive == null) {
        // A drive was "unselected", we could just hide whatever text we may be showing...
      } else if (drive != this.drive) {
        // A drive was selected, we need to "unselect" our checkbox (unless drive == this)
        console.log('Unchecking ' + this.drive.device);
      }
    });
  }
}
