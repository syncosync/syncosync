import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivesetupComponent } from './drivesetup.component';

describe('DrivesetupComponent', () => {
  let component: DrivesetupComponent;
  let fixture: ComponentFixture<DrivesetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DrivesetupComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrivesetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
