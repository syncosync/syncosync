import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisksetupComponent } from './disksetup.component';

describe('DisksetupComponent', () => {
  let component: DisksetupComponent;
  let fixture: ComponentFixture<DisksetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DisksetupComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisksetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
