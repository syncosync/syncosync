import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisksetupComponent } from './disksetup.component';

const routes: Routes = [
  {
    path: '',
    component: DisksetupComponent,
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DisksetupRoutes {}
