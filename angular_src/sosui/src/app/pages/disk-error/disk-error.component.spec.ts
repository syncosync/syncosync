/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DiskErrorComponent } from './disk-error.component';

describe('DiskErrorComponent', () => {
  let component: DiskErrorComponent;
  let fixture: ComponentFixture<DiskErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DiskErrorComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiskErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
