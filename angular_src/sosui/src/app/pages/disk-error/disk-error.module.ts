import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiskErrorComponent } from './disk-error.component';
import { StatusbarComponent } from './statusbar/statusbar.component';
import { SyncosyncCommonModule } from '../../../syncosync_modules/common/syncosync-common.module';
import { SyncosyncPublicModule } from '../../../syncosync_modules/public/syncosync-public.module';
import { SyncosyncComponentsModule } from '../../../syncosync_modules/authenticated/components/syncosync-components.module';
import { SyncosyncAuthenticatedGraphsModule } from '../../../syncosync_modules/authenticated/graphs/syncosync-authenticated-graphs.module';
import { SyncosyncAuthenticatedFormsModule } from '../../../syncosync_modules/authenticated/forms/syncosync-authenticated-forms.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DiskErrorRoutes } from './disk-error.routes';

@NgModule({
  imports: [
    CommonModule,
    SyncosyncCommonModule,
    SyncosyncAuthenticatedFormsModule,
    SyncosyncComponentsModule,
    NgbModule,
    SyncosyncPublicModule,
    SyncosyncAuthenticatedGraphsModule,
    DiskErrorRoutes,
  ],
  declarations: [DiskErrorComponent, StatusbarComponent],
    exports: [StatusbarComponent],
})
export class DiskErrorModule {}
