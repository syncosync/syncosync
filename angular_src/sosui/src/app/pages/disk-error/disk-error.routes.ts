import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DiskErrorComponent } from './disk-error.component';

const routes: Routes = [
  {
    path: '',
    component: DiskErrorComponent,
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiskErrorRoutes {}
