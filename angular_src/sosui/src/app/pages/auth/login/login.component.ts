import { Component } from '@angular/core';
import {
  AuthService,
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
  SystemMode,
} from '@syncosync_common';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { SysstateService } from '@syncosync_public';
import { AbstractEmptyFormDirective } from '../../../../syncosync_modules/common/components/abstract-empty-form-directive/abstract-empty-form.directive';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent extends AbstractEmptyFormDirective<string> {
  loading = false;
  showPasswordError = false;
  returnUrl: string;
  remoteRecoveryUrl = '/remote-recovery';
  volumeSetupUrl = '/volumesetup';
  disksetupUrl = '/disksetup';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private sysstateService: SysstateService,
    private syncosyncModalService: SyncosyncModalService
  ) {
    super();
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/syncosync';
  }

  onSubmit(): void {
    this.loading = true;
    this.authService
      .login(this.model)
      .pipe(first())
      .subscribe(
        (data) => {
          // If the system requires a disk setup, we need to redirect to the ESA
          this.loading = false;
          const actmode = this.sysstateService.getLatestSysstate().actmode;
          if (actmode == SystemMode.SETUP_DISKS) {
            this.router.navigate([this.disksetupUrl]);
          } else if (
            actmode == SystemMode.REMOTE_RECOVERY ||
            actmode == SystemMode.RR_FINAL
          ) {
            this.router.navigate([this.remoteRecoveryUrl]);
          } else if (actmode == SystemMode.SETUP_VOLUMES) {
            this.router.navigate([this.volumeSetupUrl], {}).then((r) => {
              if (!r) {
                console.log('Failed navigating to volumesetup.');
              }
            });
          } else if (actmode == SystemMode.SHUTDOWN) {
            this.syncosyncModalService.showInfoModal(
              InfoLevel.INFO,
              LocalizedMessage.SHUTDOWN_SYSTEM_SHUTTING_DOWN
            );
          } else {
            this.router.navigate([this.returnUrl]);
          }
        },
        (error) => {
          this.loading = false;
          // this.alertService.error(error);
          this.showPasswordError = true;
        }
      );
  }
  protected getInitModel(): string {
    return '';
  }
  protected createFormGroup(): void {
    //nop
  }
}
