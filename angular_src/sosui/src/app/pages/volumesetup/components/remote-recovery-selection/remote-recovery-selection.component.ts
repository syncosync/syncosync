import { Component } from '@angular/core';
import {
  ConfirmationModalResult,
  InfoLevel,
  LocalizedMessage,
  SetupAssistantDirective,
  SyncosyncModalService,
  SysStateTransport,
  SystemMode,
} from '@syncosync_common';
import { Router } from '@angular/router';
import { RemoteRecoveryConfigBackupFormComponent } from '../../../../../syncosync_modules/authenticated/forms/remote-recovery-config-backup-form/remote-recovery-config-backup-form.component';
import { takeUntil } from 'rxjs/operators';
import { SysstateService } from '@syncosync_public';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-remote-recovery-selection',
  templateUrl: './remote-recovery-selection.component.html',
  styleUrls: ['./remote-recovery-selection.component.css'],
})
export class RemoteRecoverySelectionComponent extends SetupAssistantDirective<null> {
  constructor(
    private router: Router,
    private modalService: SyncosyncModalService,
    private sysstateService: SysstateService
  ) {
    super();
  }

  private readonly remoteRecoveryPageUri = '/remote-recovery';

  continueFreshSetup(): void {
    this.modalService
      .showConfirmationModal(
        LocalizedMessage.REMOTE_RECOVERY_CONFIRM_FRESH_SETUP_TITLE,
        LocalizedMessage.REMOTE_RECOVERY_CONFIRM_FRESH_SETUP_CONTENT
      )
      .subscribe((userDecision) => {
        if (userDecision == ConfirmationModalResult.CONFIRM) {
          this.configurationFinished.emit(null);
        }
      });
  }

  remoteRecoveryUsingConfigurationBackup(): void {
    this.modalService
      .showRequestDataModal<
        SysStateTransport,
        RemoteRecoveryConfigBackupFormComponent
      >(
        LocalizedMessage.PASSWORD_OF_OTHER_CONFIG_REQUIRED,
        RemoteRecoveryConfigBackupFormComponent
      )
      .subscribe((result) => {
        if (result === undefined || result === null) {
          // User aborted password input
          // nop
        } else {
          if (
            result.state_desired == SystemMode.REMOTE_RECOVERY ||
            result.state_desired == SystemMode.RR_FINAL
          ) {
            const unsubscribe: Subject<void> = new Subject();
            this.sysstateService.latestSysstateObservable$
              .pipe(takeUntil(unsubscribe))
              .subscribe((res) => {
                if (res.actmode == SystemMode.SETUP_DISKS) {
                  return true;
                } else if (
                  res.actmode == SystemMode.REMOTE_RECOVERY ||
                  res.actmode == SystemMode.RR_FINAL
                ) {
                  unsubscribe.next();
                  this.router
                    .navigate([this.remoteRecoveryPageUri], {})
                    .then((r) => {
                      if (!r) {
                        console.log('Failed navigating to setup.');
                      }
                    });
                  return false;
                } else {
                  console.log(
                    'Did not get observe one of the expected states.'
                  );
                  return false;
                }
              });
          }
          // Send password along with select option and give it a try...
          // TODO
          // this.tryRestore(this.selected_drive.vg_uuid);
        }
      });
  }
}
