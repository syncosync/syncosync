import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteRecoverySelectionComponent } from './remote-recovery-selection.component';

describe('RemoteRecoverySelectionComponent', () => {
  let component: RemoteRecoverySelectionComponent;
  let fixture: ComponentFixture<RemoteRecoverySelectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RemoteRecoverySelectionComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteRecoverySelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
