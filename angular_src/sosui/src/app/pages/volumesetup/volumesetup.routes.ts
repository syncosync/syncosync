import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VolumesetupComponent } from './volumesetup.component';

const routes: Routes = [
  {
    path: '',
    component: VolumesetupComponent,
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VolumesetupRoutes {}
