import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { VolumesetupComponent } from './volumesetup.component';
import { StatusbarComponent } from './statusbar/statusbar.component';
import { SyncosyncAuthenticatedFormsModule } from '../../../syncosync_modules/authenticated/forms/syncosync-authenticated-forms.module';
import { VolumesetupRoutes } from './volumesetup.routes';
import { RemoteRecoverySelectionComponent } from './components/remote-recovery-selection/remote-recovery-selection.component';
import { SyncosyncCommonModule } from '../../../syncosync_modules/common/syncosync-common.module';
import { SyncosyncComponentsModule } from '../../../syncosync_modules/authenticated/components/syncosync-components.module';
import { SyncosyncAuthenticatedGraphsModule } from '../../../syncosync_modules/authenticated/graphs/syncosync-authenticated-graphs.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    SyncosyncCommonModule,
    VolumesetupRoutes,
    SyncosyncAuthenticatedFormsModule,
    SyncosyncComponentsModule,
    NgbModule,
    SyncosyncAuthenticatedGraphsModule,
  ],
  declarations: [
    VolumesetupComponent,
    StatusbarComponent,
    RemoteRecoverySelectionComponent,
  ],
  exports: [StatusbarComponent],
})
export class VolumesetupModule {}
