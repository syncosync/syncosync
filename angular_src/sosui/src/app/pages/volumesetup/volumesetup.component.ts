import { Component, OnInit } from '@angular/core';
import { RemoteRecoverySelectionComponent } from './components/remote-recovery-selection/remote-recovery-selection.component';
import {
  AdminPasswordPartialComponent,
  EsaIntroductionPartialComponent,
  GenerateSyncKeySetupPartialComponent,
  HostnameSetupPartialComponent,
  KeyExchangeSetupPartialComponent,
  MailConfigurationSetupPartialComponent,
  NameserverSetupPartialComponent,
  NetworkInterfacesSetupPartialComponent,
  PartitioningPartialComponent,
} from '@syncosync_authenticated';
import { EsaOutroPartialComponentComponent } from '../../../syncosync_modules/authenticated/assistants/setup-partials/esa-outro-partial-component/esa-outro-partial-component.component';

@Component({
  selector: 'app-disksetup',
  templateUrl: './volumesetup.component.html',
  styleUrls: ['./volumesetup.component.css'],
})
export class VolumesetupComponent implements OnInit {
  setupPartialsToShow = [];
  constructor() {
    //
  }

  ngOnInit(): void {
    this.setupPartialsToShow.push(RemoteRecoverySelectionComponent);
    this.setupPartialsToShow.push(EsaIntroductionPartialComponent);
    this.setupPartialsToShow.push(HostnameSetupPartialComponent);
    this.setupPartialsToShow.push(NetworkInterfacesSetupPartialComponent);
    this.setupPartialsToShow.push(NameserverSetupPartialComponent);
    // this.setupPartialsToShow.push(SshPortSetupPartialComponent);
    this.setupPartialsToShow.push(MailConfigurationSetupPartialComponent);
    this.setupPartialsToShow.push(AdminPasswordPartialComponent);
    this.setupPartialsToShow.push(GenerateSyncKeySetupPartialComponent);
    this.setupPartialsToShow.push(KeyExchangeSetupPartialComponent);
    this.setupPartialsToShow.push(PartitioningPartialComponent);
    // TODO: Add users
    this.setupPartialsToShow.push(EsaOutroPartialComponentComponent);
  }
}
