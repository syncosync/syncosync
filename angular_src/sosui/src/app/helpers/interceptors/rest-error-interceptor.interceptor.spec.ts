import {TestBed} from '@angular/core/testing';

import {RestErrorInterceptor} from './rest-error-interceptor.service';

describe('RestErrorInterceptorInterceptor', () => {
    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            RestErrorInterceptor
        ]
    }));

    it('should be created', () => {
        const interceptor: RestErrorInterceptor = TestBed.inject(RestErrorInterceptor);
        expect(interceptor).toBeTruthy();
    });
});
