import { TestBed } from '@angular/core/testing';

import { StateDiskErrorGuard } from './state-disk-error.guard';

describe('StateDiskErrorGuard', () => {
  let guard: StateDiskErrorGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(StateDiskErrorGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
