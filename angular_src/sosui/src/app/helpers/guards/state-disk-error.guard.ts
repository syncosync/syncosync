import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { SysstateService } from '@syncosync_public';
import { SyncosyncModalService, SystemMode } from '@syncosync_common';

@Injectable({
  providedIn: 'root',
})
export class StateDiskErrorGuard implements CanActivate {
  constructor(
    private router: Router,
    private sysstateService: SysstateService,
    private syncosyncModalService: SyncosyncModalService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const latestSysstate = this.sysstateService.getLatestSysstate();

    if (latestSysstate.actmode == SystemMode.DISK_ERROR) {
      void this.router.navigate(['/diskerror'], {});
      return false;
    } else {
      // Not in disk error mode, redirect accordingly...
      return true;
    }
  }
}
