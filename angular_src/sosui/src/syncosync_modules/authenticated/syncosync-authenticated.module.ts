import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SyncosyncAuthenticatedFormsModule } from './forms/syncosync-authenticated-forms.module';
import { SetupPartialsModule } from './assistants/setup-partials/setup-partials.module';
import { SyncosyncAuthenticatedModalsModule } from './modals/syncosync-authenticated-modals.module';
import { PartitioningResizePrecedingInformationComponent } from './assistants/partitioning-partials/partitioning-resize-preceding-information/partitioning-resize-preceding-information.component';
import { UpdateSystemSetupDataComponent } from './assistants/partitioning-partials/update-sync-setup-data/update-system-setup-data.component';
import { PartitioningResizeSliderAssistantComponent } from './assistants/partitioning-partials/partitioning-slide/partitioning-resize-slider-assistant.component';
import { HandleCurrentPartitioningStatusComponent } from './assistants/partitioning-partials/handle-current-partitioning-status/handle-current-partitioning-status.component';
import { ShowFinalPartitioningInfoComponent } from './assistants/partitioning-partials/show-final-partitioning-info/show-final-partitioning-info.component';
import { PartitioningSliderComponent } from './forms/partitioning/partitioning-slider/partitioning-slider.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';

@NgModule({
  declarations: [
    PartitioningResizePrecedingInformationComponent,
    UpdateSystemSetupDataComponent,
    PartitioningResizeSliderAssistantComponent,
    HandleCurrentPartitioningStatusComponent,
    ShowFinalPartitioningInfoComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    SyncosyncAuthenticatedFormsModule,
    SetupPartialsModule,
    NgxSliderModule,
  ],
  exports: [SyncosyncAuthenticatedModalsModule, SetupPartialsModule],
})
export class SyncosyncAuthenticatedModule {}
