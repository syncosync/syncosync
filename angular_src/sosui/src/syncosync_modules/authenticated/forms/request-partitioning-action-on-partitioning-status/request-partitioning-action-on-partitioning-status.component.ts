import { Component, EventEmitter, OnInit } from '@angular/core';
import {
  AbstractEditableFormDirective,
  ConfirmationModalResult,
  GenericUiResponse,
  GenericUiResponseStatus,
  HelpStateService,
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
  SystemMode,
} from '@syncosync_common';
import { PartitioningStatus } from '../../../common/model/partitioningStatus';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';
import { PartitioningService } from '../../services/partitioning.service';
import { Observable, Subject } from 'rxjs';
import { PartitioningState } from '../../../common/model/sosEnums';
import { takeUntil } from 'rxjs/operators';
import { SysstateService } from '@syncosync_public';

@Component({
  selector: 'app-request-partitioning-action-on-partitioning-status',
  templateUrl:
    './request-partitioning-action-on-partitioning-status.component.html',
  styleUrls: [
    './request-partitioning-action-on-partitioning-status.component.css',
  ],
})
export class RequestPartitioningActionOnPartitioningStatusComponent
  extends AbstractEditableFormDirective<PartitioningStatus>
  implements OnInit
{
  partitioningStates: typeof PartitioningState = PartitioningState;

  constructor(
    public partitioningService: PartitioningService,
    public helpStateService: HelpStateService,
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    private modalService: SyncosyncModalService,
    protected sysstateService: SysstateService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.modelChanged.subscribe(() => {
      // immediately indicate success upon detecting the partitioning state to be okay
      if (this.model.overall_status == PartitioningState.OK) {
        this.formFinished.emit(this.model);
      }
    });
  }

  protected getInitModel(): PartitioningStatus {
    return new PartitioningStatus();
  }

  protected modelRequest(): Observable<PartitioningStatus> {
    return this.partitioningService.getPartitioningStatus();
  }

  onSubmit(): void {
    this.modalService
      .showConfirmationModal(
        LocalizedMessage.FIX_PARTITIONING_REQUEST_TITLE,
        LocalizedMessage.FIX_PARTITIONING_REQUEST_CONTENT
      )
      .subscribe((userDecision) => {
        if (userDecision == ConfirmationModalResult.CONFIRM) {
          this.automagicallyFixPartitions();
        }
      });
  }

  automagicallyFixPartitions() {
    const resultEmitter: EventEmitter<GenericUiResponse> =
      new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe((result: GenericUiResponse) => {
      if (result !== undefined && result.status == GenericUiResponseStatus.OK) {
        //this.formFinished.emit(this.model);
        this.waitForStateChange();
      } else if (result !== undefined) {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.WARNING,
          result.localized_reason,
          result.additional_information
        );
      } else {
        this.showPartitioningFailed('Unknown reason, please check logs');
      }
    });

    this.syncosyncModalService.showProcessingModal<GenericUiResponse>(
      LocalizedMessage.PARTITIONING_IN_PROGRESS_TITLE,
      LocalizedMessage.PARTITIONING_IN_PROGRESS_CONTENT,
      this.partitioningService.automaticallyFixPartitions(),
      null,
      resultEmitter
    );
  }

  private waitForStateChange(): void {
    this.processingSpinnerStateService.addSpinner();
    const unsubscribe: Subject<void> = new Subject();
    this.sysstateService.latestSysstateObservable$
      .pipe(takeUntil(unsubscribe))
      .subscribe((res) => {
        if (
          res.actmode == SystemMode.SETUP_VOLUMES ||
          res.actmode == SystemMode.RESIZE_VOLUMES
        ) {
          return true;
        } else if (
          res.actmode == SystemMode.DEFAULT ||
          res.actmode == SystemMode.DEFAULT_NO_SYNC
        ) {
          unsubscribe.next();

          // Trigger the initialization again effectively checking for proper partitioning this time?
          this.initialize();

          this.processingSpinnerStateService.removeSpinner();
          return false;
        } else {
          console.log('Did not get one of the expected states.');
          this.processingSpinnerStateService.removeSpinner();
          return false;
        }
      });
  }

  protected showPartitioningFailed(result: string): void {
    this.syncosyncModalService.showInfoModal(
      InfoLevel.WARNING,
      LocalizedMessage.PARTITION_DRIVES_FAILED_GENERIC_MESSAGE,
      result
    );
  }
}
