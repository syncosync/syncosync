import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestPartitioningActionOnPartitioningStatusComponent } from './request-partitioning-action-on-partitioning-status.component';

describe('RequestPartitioningActionOnPartitioningStatusComponent', () => {
  let component: RequestPartitioningActionOnPartitioningStatusComponent;
  let fixture: ComponentFixture<RequestPartitioningActionOnPartitioningStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RequestPartitioningActionOnPartitioningStatusComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(
      RequestPartitioningActionOnPartitioningStatusComponent
    );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
