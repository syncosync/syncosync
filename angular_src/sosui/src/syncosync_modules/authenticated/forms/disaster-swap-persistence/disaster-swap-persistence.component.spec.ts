import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisasterSwapPersistenceComponent } from './disaster-swap-persistence.component';

describe('DisasterSwapPersistenceComponent', () => {
  let component: DisasterSwapPersistenceComponent;
  let fixture: ComponentFixture<DisasterSwapPersistenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DisasterSwapPersistenceComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisasterSwapPersistenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
