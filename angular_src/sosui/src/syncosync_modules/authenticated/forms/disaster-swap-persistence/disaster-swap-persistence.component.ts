import { Component, EventEmitter } from '@angular/core';
import { AbstractEmptyFormDirective } from '../../../../syncosync_modules/common/components/abstract-empty-form-directive/abstract-empty-form.directive';
import {
  ConfirmationModalResult,
  HelpStateService,
  SystemMode,
  InfoLevel,
  LocalizedMessage,
  SysStateTransport,
  SyncosyncModalService,
} from '@syncosync_common';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';
import { SwitchAction } from 'src/syncosync_modules/common/model/sosEnums';
import { Router } from '@angular/router';
import { ModeManagementService } from '../../services/mode-management.service';
import { SysStateResult } from '../../../common/model/sysState';

@Component({
  selector: 'app-disaster-swap-persistence',
  templateUrl: './disaster-swap-persistence.component.html',
  styleUrls: ['./disaster-swap-persistence.component.css'],
})
export class DisasterSwapPersistenceComponent extends AbstractEmptyFormDirective<SysStateTransport> {
  constructor(
    public helpStateService: HelpStateService,
    private modeManagementService: ModeManagementService,
    private router: Router,
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super();
  }

  onSubmit(): void {
    this.syncosyncModalService
      .showConfirmationModal(
        LocalizedMessage.MAKE_DISASTER_SWAP_PERSISTENT_CONFIRMATION_MODAL_TITLE,
        LocalizedMessage.MAKE_DISASTER_SWAP_PERSISTENT_CONFIRMATION_MODAL_CONTENT
      )
      .subscribe((userDecision) => {
        if (userDecision == ConfirmationModalResult.CONFIRM) {
          this.switchPersistant();
        }
      });
  }

  protected createFormGroup(): void {
    //
  }

  private switchPersistant() {
    const resultEmitter: EventEmitter<SysStateResult> =
      new EventEmitter<SysStateResult>();
    resultEmitter.subscribe((result) => {
      console.log('result', result);

      if (result !== undefined && result !== null) {
        if (result.mode == SystemMode.DEFAULT) {
          // Success
          this.formFinished.emit(this.model);
          window.location.reload();
        } else {
          this.syncosyncModalService.showInfoModal(
            InfoLevel.ERROR,
            LocalizedMessage.DISASTER_SWAP_PERSISTENT_FAILED_GENERIC_MESSAGE,
            result.report.toString()
          );
        }
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.UNKNOWN_ERROR
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCESSING_DISASTER_SWAP_REQUEST_CONTENT,
      this.modeManagementService.setMode(this.model),
      null,
      resultEmitter
    );
    this.router.navigate(['/syncosync/status']);
  }

  protected getInitModel(): SysStateTransport {
    const model = new SysStateTransport();
    model.state_desired = SystemMode.DEFAULT;
    model.switch_action = SwitchAction.DISASTER_SWAP_PERSISTENT;
    return model;
  }
}
