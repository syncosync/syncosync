import { Component, EventEmitter } from '@angular/core';
import {
  Calculators,
  ConfirmationModalResult,
  InfoLevel,
  LocalizedMessage,
  PartitioningData,
  SyncosyncModalService,
  SystemSetupData,
} from '@syncosync_common';
import { ProcessingSpinnerStateService } from '../../../../common/services/processing-spinner-state.service';
import { PartitioningService } from '../../../services/partitioning.service';
import { AbstractPartitioningDataHandlerDirective } from '../abstract-partitioning-data-handler.directive';
import { SyncConfigurationService } from '@syncosync_authenticated';
import { plainToClass } from 'class-transformer';
import { Router } from '@angular/router';
import { SysstateService } from '@syncosync_public';

@Component({
  selector: 'app-partitioning-follow',
  templateUrl: './partitioning-follow.component.html',
  styleUrls: ['./partitioning-follow.component.css'],
})
export class PartitioningFollowComponent extends AbstractPartitioningDataHandlerDirective {
  constructor(
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    protected partitioningService: PartitioningService,
    protected syncConfigurationService: SyncConfigurationService,
    protected router: Router,
    protected sysstateService: SysstateService
  ) {
    super(
      syncosyncModalService,
      processingSpinnerStateService,
      partitioningService,
      router,
      sysstateService
    );
  }

  public onSubmit(): void {
    throw new Error('Method not implemented.');
  }
  protected createFormGroup(): void {
    // nop
  }

  tryToReceivePartitioningData(): void {
    const resultEmitter: EventEmitter<SystemSetupData> =
      new EventEmitter<SystemSetupData>();
    resultEmitter.subscribe((result: SystemSetupData) => {
      if (result !== undefined && result !== null) {
        const resultingObj = plainToClass(SystemSetupData, result);
        // Flip remote and local since the SSD received is the one of our partner
        const partitionData: PartitioningData = resultingObj.partition_data;
        partitionData.flipLocalRemote();
        this.acceptPartitioningDistributionConfirmationDialog(partitionData);
      }
    });
    this.syncosyncModalService.showProcessingModal<SystemSetupData>(
      LocalizedMessage.PARTITIONING_RECEIVING_DISTRIBUTION_PROCESSING_MODAL_TITLE,
      LocalizedMessage.PARTITIONING_RECEIVING_DISTRIBUTION_PROCESSING_MODAL_CONTENT,
      this.syncConfigurationService.followKeyExchange(),
      this.syncConfigurationService.cancelKeyExchange(),
      resultEmitter
    );
  }

  private acceptPartitioningDistributionConfirmationDialog(
    partitioningData: PartitioningData
  ) {
    // TODO: Validate common SSD
    const remoteSizeHumanReadable = Calculators.bytesToHumanReadableSize(
      Calculators.extendsToBytes(partitioningData.remote)
    );
    const localSizeHumanReadable = Calculators.bytesToHumanReadableSize(
      Calculators.extendsToBytes(partitioningData.local)
    );
    const totalPartitioned = partitioningData.remote + partitioningData.local;
    const percent = partitioningData.local / totalPartitioned;
    const localPercentage = Math.floor(percent * 100);

    const remotePercentage = 100 - localPercentage;
    this.syncosyncModalService
      .showConfirmationModal(
        LocalizedMessage.PARTITIONING_ACCEPT_DISTRIBUTION_CONFIRMATION_TITLE,
        LocalizedMessage.PARTITIONING_ACCEPT_DISTRIBUTION_CONFIRMATION_EXPLANATION,
        localPercentage +
          '% (' +
          localSizeHumanReadable +
          ') : ' +
          remotePercentage +
          '% (' +
          remoteSizeHumanReadable +
          ')'
      )
      .subscribe((userDecision) => {
        if (userDecision === ConfirmationModalResult.CONFIRM) {
          this.partitionDrives(partitioningData);
        } else {
          this.syncosyncModalService.showInfoModal(
            InfoLevel.WARNING,
            LocalizedMessage.PARTITIONING_FAILED_NOT_ACCEPTED
          );
        }
      });
  }
}
