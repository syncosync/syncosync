import { Directive, EventEmitter } from '@angular/core';
import {
  AbstractEditableFormDirective,
  GenericUiResponse,
  InfoLevel,
  LocalizedMessage,
  PartitioningData,
  SyncosyncModalService,
  SystemMode,
} from '@syncosync_common';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';
import { PartitioningService } from '../../services/partitioning.service';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SysstateService } from '@syncosync_public';

@Directive()
export abstract class AbstractPartitioningDataHandlerDirective extends AbstractEditableFormDirective<PartitioningData> {
  protected constructor(
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    protected partitioningService: PartitioningService,
    protected router: Router,
    protected sysstateService: SysstateService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  protected getInitModel(): PartitioningData {
    return new PartitioningData();
  }

  protected modelRequest(): Observable<PartitioningData> {
    return this.partitioningService.getPartitioning();
  }

  protected partitionDrives(partitioningData: PartitioningData): void {
    const resultEmitter: EventEmitter<GenericUiResponse> =
      new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe((result: GenericUiResponse) => {
      if (result !== undefined) {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.INFO,
          result.localized_reason,
          result.additional_information
        );
        this.waitForStateChange(partitioningData);
      } else {
        // TODO...
        this.showPartitioningFailed('Unknown reason, please check logs');
      }
    });

    this.syncosyncModalService.showProcessingModal<GenericUiResponse>(
      LocalizedMessage.PARTITIONING_IN_PROGRESS_TITLE,
      LocalizedMessage.PARTITIONING_IN_PROGRESS_CONTENT,
      this.partitioningService.partitionDrives(partitioningData),
      null,
      resultEmitter
    );
  }

  private waitForStateChange(partitioningData: PartitioningData): void {
    this.processingSpinnerStateService.addSpinner();
    const unsubscribe: Subject<void> = new Subject();
    this.sysstateService.latestSysstateObservable$
      .pipe(takeUntil(unsubscribe))
      .subscribe((res) => {
        if (
          res.actmode == SystemMode.SETUP_VOLUMES ||
          res.actmode == SystemMode.RESIZE_VOLUMES ||
          res.actmode == SystemMode.INTERMEDIATE
        ) {
          return true;
        } else if (
          res.actmode == SystemMode.DEFAULT ||
          res.actmode == SystemMode.DEFAULT_NO_SYNC
        ) {
          unsubscribe.next();
          this.setModel(partitioningData);
          this.formFinished.emit(partitioningData);

          this.processingSpinnerStateService.removeSpinner();
          return false;
        } else {
          console.log('partitioning_data_handler: did not get one of the expected states:', res.actmode);
          this.processingSpinnerStateService.removeSpinner();
          return false;
        }
      });
  }

  protected showPartitioningFailed(result: string): void {
    this.syncosyncModalService.showInfoModal(
      InfoLevel.WARNING,
      LocalizedMessage.PARTITION_DRIVES_FAILED_GENERIC_MESSAGE,
      result
    );
  }
}
