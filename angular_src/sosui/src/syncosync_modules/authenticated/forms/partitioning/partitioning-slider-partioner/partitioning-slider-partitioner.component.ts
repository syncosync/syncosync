import { Component } from '@angular/core';
import {
  Calculators,
  ConfirmationModalResult,
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
} from '@syncosync_common';
import { PartitioningData } from '@syncosync_common';
import { ProcessingSpinnerStateService } from '../../../../common/services/processing-spinner-state.service';
import { PartitioningService } from '../../../services/partitioning.service';
import { AbstractPartitioningDataHandlerDirective } from '../abstract-partitioning-data-handler.directive';
import { SyncConfigurationService } from '@syncosync_authenticated';
import { Router } from '@angular/router';
import { SysstateService } from '@syncosync_public';

@Component({
  selector: 'app-partitioning-slider-partitioner',
  templateUrl: './partitioning-slider-partitioner.component.html',
  styleUrls: ['./partitioning-slider-partitioner.component.scss'],
})
export class PartitioningSliderPartitionerComponent extends AbstractPartitioningDataHandlerDirective {
  value: number = 50;

  constructor(
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    protected partitioningService: PartitioningService,
    protected syncConfigurationService: SyncConfigurationService,
    protected router: Router,
    protected sysstateService: SysstateService
  ) {
    super(
      syncosyncModalService,
      processingSpinnerStateService,
      partitioningService,
      router,
      sysstateService
    );
  }

  onSubmit(): void {
    const totalSize = this.model.free + this.model.remote + this.model.local;
    const modelCopy: PartitioningData = Object.assign({}, this.model);
    const valueDistributionLocalPercentage = this.value;
    modelCopy.local = Math.floor(
      (valueDistributionLocalPercentage / 100) * totalSize
    );
    modelCopy.remote = totalSize - modelCopy.local;
    modelCopy.free = 0;
    if (
      modelCopy.local < modelCopy.local_min ||
      modelCopy.remote < modelCopy.remote_min
    ) {
      console.log('Invalid local and remote values (smaller than min)');
      this.syncosyncModalService.showInfoModal(
        InfoLevel.WARNING,
        LocalizedMessage.PARTITION_DRIVES_FAILED_GENERIC_MESSAGE,
        'Invalid local/remote values (less than min).'
      );
      return;
    }
    this.confirmationDialogForPartitioning(
      valueDistributionLocalPercentage,
      modelCopy
    );
  }

  confirmationDialogForPartitioning(
    valueDistributionLocalPercentage: number,
    partitioningData: PartitioningData
  ): void {
    const remoteSizeHumanReadable = Calculators.bytesToHumanReadableSize(
      Calculators.extendsToBytes(partitioningData.remote)
    );
    const localSizeHumanReadable = Calculators.bytesToHumanReadableSize(
      Calculators.extendsToBytes(partitioningData.local)
    );
    const remotePercentage = 100 - valueDistributionLocalPercentage;
    this.syncosyncModalService
      .showConfirmationModal(
        LocalizedMessage.PARTITION_DRIVES_CONFIRMATION_TITLE,
        LocalizedMessage.PARTITION_DRIVES_CONFIRMATION_CONTENT,
        valueDistributionLocalPercentage +
          '% (' +
          localSizeHumanReadable +
          ') : ' +
          remotePercentage +
          '% (' +
          remoteSizeHumanReadable +
          ')'
      )
      .subscribe((userDecision) => {
        if (userDecision === ConfirmationModalResult.CONFIRM) {
          this.partitionDrives(partitioningData);
        }
      });
  }

  setValue($event: any) {
    if (this.value !== undefined && $event !== undefined) {
      this.value = $event;
    }
  }
}
