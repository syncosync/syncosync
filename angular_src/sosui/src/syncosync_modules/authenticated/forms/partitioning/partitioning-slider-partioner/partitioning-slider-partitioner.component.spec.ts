import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitioningSliderPartitionerComponent } from './partitioning-slider-partitioner.component';

describe('PartitioningSliderComponent', () => {
  let component: PartitioningSliderPartitionerComponent;
  let fixture: ComponentFixture<PartitioningSliderPartitionerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PartitioningSliderPartitionerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartitioningSliderPartitionerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
