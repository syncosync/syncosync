import {
  Component,
  EventEmitter,
  Input,
  Output,
  SimpleChanges,
} from '@angular/core';
import { AbstractPartitioningDataHandlerDirective } from '../abstract-partitioning-data-handler.directive';
import { ChangeContext, Options } from '@angular-slider/ngx-slider';
import {
  Calculators,
  PartitioningData,
  SyncosyncModalService,
} from '@syncosync_common';
import { ProcessingSpinnerStateService } from '../../../../common/services/processing-spinner-state.service';
import { PartitioningService } from '../../../services/partitioning.service';
import { SyncConfigurationService } from '@syncosync_authenticated';
import { Router } from '@angular/router';
import { SysstateService } from '@syncosync_public';

@Component({
  selector: 'app-partitioning-slider',
  templateUrl: './partitioning-slider.component.html',
  styleUrls: ['./partitioning-slider.component.scss'],
})
export class PartitioningSliderComponent extends AbstractPartitioningDataHandlerDirective {
  @Input() disableSlider = null;
  @Input() initialSetup = null;

  @Output() valueEmitter = new EventEmitter<number>();
  value = 50;
  freeSizeBytes = 0;
  options: Options = {
    floor: 0,
    ceil: 100,
    step: 5,
    // minLimit to display local_min (allocated data)
    minLimit: 0,
    // maxLimit to display remote_min (allocated data)
    maxLimit: 100,
    showTicks: true,
    showSelectionBar: true,
    // showSelectionBar: true,
    translate: (value: number): string => {
      const totalSize = this.model.free + this.model.remote + this.model.local;
      const approxSize = (totalSize * value) / 100;
      const sizeHumanReadable = Calculators.bytesToHumanReadableSize(
        Calculators.extendsToBytes(approxSize)
      );
      return `${value}% (${sizeHumanReadable})`;
    },
    getLegend: (value: number): string => {
      if (value % 5 === 0) {
        return `${value}%`;
      } else {
        return '';
      }
    },
  };

  ngOnChanges(_: SimpleChanges) {
    // changes.prop contains the old and the new value...
    if (this.disableSlider !== null) {
      this.setDisabled(this.disableSlider);
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onUserChangeEnd(_changeContext: ChangeContext): void {
    const totalSize = this.model.free + this.model.remote + this.model.local;
    this.model.local = Math.floor((this.value / 100) * totalSize);
    this.model.remote = totalSize - this.model.local;
    this.model.free = 0;
    this.modelChanged.emit(this.model);

    this.valueEmitter.emit(this.value);
    console.log("Value", this.value);
  }

  constructor(
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    protected partitioningService: PartitioningService,
    protected syncConfigurationService: SyncConfigurationService,
    protected router: Router,
    protected sysstateService: SysstateService
  ) {
    super(
      syncosyncModalService,
      processingSpinnerStateService,
      partitioningService,
      router,
      sysstateService
    );
    if (this.disableSlider !== null) {
      this.setDisabled(this.disableSlider);
    }

  }

  public setModel(newModel: PartitioningData): void {
    super.setModel(newModel);
    const totalSize = this.model.free + this.model.remote + this.model.local;
    console.log("TotalSize before", totalSize);
    if (this.initialSetup === false) {
      const totalSize = this.model.remote + this.model.local;
      console.log("TotalSize", totalSize);
    }
    this.freeSizeBytes = Calculators.extendsToBytes(this.model.free);
    this.options.maxLimit = Math.floor(
      ((totalSize - this.model.remote_min) / totalSize) * 100
    );
    this.options.minLimit = Math.ceil((this.model.local_min / totalSize) * 100);

    this.options.ticksArray = [];
    for (let i = 0; i <= 100; i += 10) {
      this.options.ticksArray.push(i);
    }

    if (this.model.remote === 0 && this.model.local === 0) {
      // no distribution yet, suggest 50%
      this.value = 50;
    } else {
      // assuming drive have already been partitioned, roughly estimate the percentage of the distribution
      const totalPartitioned = this.model.remote + this.model.local;
      const percent = this.model.local / totalPartitioned;
      this.value = Math.floor(percent * 100);
    }
  }

  onSubmit(): void {
    // No onSubmit to be used in the slider itself
  }

  public setDisabled(disabled: boolean): void {
    this.options = Object.assign({}, this.options, { disabled: disabled });
  }
}
