import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitioningSliderComponent } from './partitioning-slider.component';

describe('PartitioningSliderComponent', () => {
  let component: PartitioningSliderComponent;
  let fixture: ComponentFixture<PartitioningSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PartitioningSliderComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PartitioningSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
