import { Component, EventEmitter, Input } from '@angular/core';
import {
  AbstractEditableFormDirective,
  LocalizedMessage,
  PartitioningData,
  SyncosyncModalService,
  SystemSetupData,
} from '@syncosync_common';
import { ProcessingSpinnerStateService } from '../../../../common/services/processing-spinner-state.service';
import { PartitioningService } from '../../../services/partitioning.service';
import { SyncConfigurationService } from '@syncosync_authenticated';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-partitioning-lead',
  templateUrl: './partitioning-lead.component.html',
  styleUrls: ['./partitioning-lead.component.css'],
})
export class PartitioningLeadComponent extends AbstractEditableFormDirective<PartitioningData> {
  @Input() disableButtons = false;

  constructor(
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    protected partitioningService: PartitioningService,
    protected syncConfigurationService: SyncConfigurationService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  sendPartitioningData(): void {
    const resultEmitter: EventEmitter<SystemSetupData> =
      new EventEmitter<SystemSetupData>();
    resultEmitter.subscribe((result: SystemSetupData) => {
      if (result !== undefined && result !== null) {
        // TODO: Validate resulting data with partitioning data
        this.formFinished.emit(result.partition_data);
      }
    });
    this.syncosyncModalService.showProcessingModal<SystemSetupData>(
      LocalizedMessage.PARTITIONING_SENDING_DISTRIBUTION_PROCESSING_MODAL_TITLE,
      LocalizedMessage.PARTITIONING_SENDING_DISTRIBUTION_PROCESSING_MODAL_CONTENT,
      this.partitioningService.leadPartitioning(),
      this.syncConfigurationService.cancelKeyExchange(),
      resultEmitter
    );
  }

  protected createFormGroup(): void {
    // nop
  }

  protected modelRequest(): Observable<PartitioningData> {
    return this.partitioningService.getPartitioning();
  }

  protected getInitModel(): PartitioningData {
    return new PartitioningData();
  }

  onSubmit(): void {
    // TODO: Dialog to confirm the data is about to be sent?
    this.sendPartitioningData();
  }

  public setModel(newModel: PartitioningData): void {
    super.setModel(newModel);
    if (this.model.local + this.model.remote !== 0) {
      this.disableButtons = false;
    }
  }
}
