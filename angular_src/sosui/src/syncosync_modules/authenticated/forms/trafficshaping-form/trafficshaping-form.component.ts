import { Component, EventEmitter } from '@angular/core';
import { Options } from '@angular-slider/ngx-slider';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
  AbstractEditableFormDirective,
  HelpStateService,
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
  Trafficshape,
} from '@syncosync_common';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';
import { Observable } from 'rxjs';
import { TrafficshapingService } from '../../services/trafficshaping.service';

@Component({
  selector: 'app-trafficshaping-form',
  templateUrl: './trafficshaping-form.component.html',
  styleUrls: ['./trafficshaping-form.component.css'],
})
export class TrafficshapingFormComponent extends AbstractEditableFormDirective<Trafficshape> {
  valueDay = 0;
  valueNight = 0;

  options1: Options = {
    floor: 0,
    ceil: 143,
    showSelectionBar: true,
    tickValueStep: 0.04,
    translate: (value: number): string => {
      const rawQuotient = value / 6;
      const remainder = rawQuotient % 1;
      const hours = rawQuotient - remainder;
      const minutes = Math.round(remainder * 60);

      const zerofilled_hours = ('00' + hours).slice(-2);
      const zerofilled_minutes = ('00' + minutes).slice(-2);

      return zerofilled_hours + ':' + zerofilled_minutes;
    },
  };
  options2: Options = {
    floor: 0,
    ceil: 143,
    showSelectionBarEnd: true,
    translate: (value: number): string => {
      const rawQuotient = value / 6;
      const remainder = rawQuotient % 1;
      const hours = rawQuotient - remainder;
      const minutes = Math.round(remainder * 60);

      const zerofilled_hours = ('00' + hours).slice(-2);
      const zerofilled_minutes = ('00' + minutes).slice(-2);

      return zerofilled_hours + ':' + zerofilled_minutes;
    },
  };

  constructor(
    public activeModal: NgbActiveModal,
    public helpStateService: HelpStateService,
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    private trafficshapingService: TrafficshapingService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  onSubmit(): void {
    this.model.day = this.valueDay * 10;
    this.model.night = this.valueNight * 10;
    const resultEmitter: EventEmitter<Trafficshape> =
      new EventEmitter<Trafficshape>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        this.setModel(result);
        this.formFinished.emit(this.model);
        this.activeModal.close('Closed');
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.FAILED_REQUESTING_DATA
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCESSING_TRAFFICSHAPING_SAVE_MESSAGE,
      this.trafficshapingService.setTrafficshape(this.model),
      null,
      resultEmitter
    );
  }
  protected requestData(): void {
    const getterObservable: Observable<Trafficshape> = this.modelRequest();
    if (getterObservable == null) {
      this.model = this.getInitModel();
      return;
    }
    getterObservable.subscribe((result) => {
      if (result !== undefined && result !== null) {
        this.setModel(result);
        // Remove once we get rid of form groups...
        this.createFormGroup();
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.FAILED_REQUESTING_DATA
        );
      }
    });
  }
  public setModel(newModel: Trafficshape): void {
    super.setModel(newModel);
    this.valueDay = this.model.day / 10;
    this.valueNight = this.model.night / 10;
  }

  protected getInitModel(): Trafficshape {
    return new Trafficshape();
  }
  protected modelRequest(): Observable<Trafficshape> {
    return this.trafficshapingService.getTrafficshape();
  }
  protected createFormGroup(): void {
    // nop
  }
}
