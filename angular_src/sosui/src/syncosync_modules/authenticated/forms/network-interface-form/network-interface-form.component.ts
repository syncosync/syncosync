import { Component, EventEmitter } from '@angular/core';
import { NetworkConfigService } from '../../services/network-config.service';
import {
  AbstractEditableFormDirective,
  NicConfig,
  NicMode,
  SyncosyncModalService,
  HelpStateService,
  InfoLevel,
  LocalizedMessage,
} from '@syncosync_common';
import { Observable } from 'rxjs';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';

@Component({
  selector: 'app-network-interface-form',
  templateUrl: './network-interface-form.component.html',
  styleUrls: ['./network-interface-form.component.css'],
})
export class NetworkInterfaceFormComponent extends AbstractEditableFormDirective<
  NicConfig[]
> {
  // TODO: we have a list of nics in a single form!
  nicModes: typeof NicMode = NicMode;

  constructor(
    public networkConfigService: NetworkConfigService,
    public helpStateService: HelpStateService,
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  onSubmit(): void {
    const resultEmitter: EventEmitter<NicConfig[]> = new EventEmitter<
      NicConfig[]
    >();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        this.setModel(result);
        this.formFinished.emit(this.model);
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.NETWORK_CONFIG_SAVE_FAILED_MESSAGE
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCESSING_NETWORK_CONFIG_SAVE_MESSAGE,
      this.networkConfigService.setNetworkInterfaces(this.model),
      null,
      resultEmitter
    );
  }

  getColor(nicMode: NicMode): string {
    if (nicMode !== NicMode.MANUAL) {
      return 'on';
    } else {
      return 'off';
    }
  }

  toogleIpv4DhcpStatic(nicConfig: NicConfig): void {
    if (nicConfig.ip4_mode === NicMode.DHCP) {
      nicConfig.ip4_mode = NicMode.STATIC;
    } else {
      nicConfig.ip4_mode = NicMode.DHCP;
    }
  }

  toggleInterfaceModeIpv4(nicConfig: NicConfig): void {
    if (nicConfig.ip4_mode === NicMode.MANUAL) {
      nicConfig.ip4_mode = NicMode.DHCP;
    } else {
      nicConfig.ip4_mode = NicMode.MANUAL;
    }
  }

  toggleInterfaceModeIpv6(nicConfig: NicConfig): void {
    if (nicConfig.ip6_mode === NicMode.MANUAL) {
      nicConfig.ip6_mode = NicMode.AUTO;
    } else {
      nicConfig.ip6_mode = NicMode.MANUAL;
    }
  }

  protected getInitModel(): NicConfig[] {
    return [];
  }

  protected modelRequest(): Observable<NicConfig[]> {
    return this.networkConfigService.getNetworkInterfaces();
  }
}
