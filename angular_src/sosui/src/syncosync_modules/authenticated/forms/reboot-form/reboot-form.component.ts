import { Component, EventEmitter } from '@angular/core';
import { AbstractEmptyFormDirective } from '../../../common/components/abstract-empty-form-directive/abstract-empty-form.directive';
import { SystemService } from '@syncosync_authenticated';
import {
  ConfirmationModalResult,
  GenericUiResponse,
  GenericUiResponseStatus,
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
} from '@syncosync_common';

@Component({
  selector: 'app-reboot-form',
  templateUrl: './reboot-form.component.html',
  styleUrls: ['./reboot-form.component.css'],
})
export class RebootFormComponent extends AbstractEmptyFormDirective<null> {
  constructor(
    private systemService: SystemService,
    private syncosyncModalService: SyncosyncModalService
  ) {
    super();
  }

  protected getInitModel(): null {
    return null;
  }

  onSubmit(): void {
    this.syncosyncModalService
      .showConfirmationModal(
        LocalizedMessage.REBOOT_SYSTEM_CONFIRMATION_TITLE,
        LocalizedMessage.REBOOT_SYSTEM_CONFIRMATION_CONTENT
      )
      .subscribe((result: ConfirmationModalResult) => {
        if (
          result === undefined ||
          result === null ||
          result === ConfirmationModalResult.CANCEL
        ) {
          // User aborted password input
          return;
        }
        this.rebootSystem();
      });
  }

  private rebootSystem(): void {
    const resultEmitter: EventEmitter<GenericUiResponse> =
      new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe((result) => {
      if (
        result !== undefined &&
        result !== null &&
        result.status === GenericUiResponseStatus.OK
      ) {
        this.formFinished.emit(this.model);
        this.syncosyncModalService.showInfoModal(
          InfoLevel.INFO,
          LocalizedMessage.REBOOT_SYSTEM_REBOOTING
        );
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.REBOOT_SYSTEM_FAILED
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.REBOOT_SYSTEM_IN_PROGRESS_CONTENT,
      this.systemService.rebootSystem(),
      null,
      resultEmitter
    );
  }
}
