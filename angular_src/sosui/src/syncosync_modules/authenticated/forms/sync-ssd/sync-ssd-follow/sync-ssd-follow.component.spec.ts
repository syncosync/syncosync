import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SyncSsdFollowComponent } from './sync-ssd-follow.component';

describe('SyncSsdFollowComponent', () => {
  let component: SyncSsdFollowComponent;
  let fixture: ComponentFixture<SyncSsdFollowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SyncSsdFollowComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SyncSsdFollowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
