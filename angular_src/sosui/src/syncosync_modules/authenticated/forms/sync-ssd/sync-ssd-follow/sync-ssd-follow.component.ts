import { Component, EventEmitter } from '@angular/core';
import {
  HelpStateService,
  LocalizedMessage,
  SyncosyncModalService,
  SystemSetupData,
} from '@syncosync_common';
import { AbstractEmptyFormDirective } from '../../../../common/components/abstract-empty-form-directive/abstract-empty-form.directive';
import { SyncSsdService } from '../../../services/sync-ssd.service';
import { SyncConfigurationService } from '@syncosync_authenticated';

@Component({
  selector: 'app-sync-ssd-follow',
  templateUrl: './sync-ssd-follow.component.html',
  styleUrls: ['./sync-ssd-follow.component.css'],
})
export class SyncSsdFollowComponent extends AbstractEmptyFormDirective<SystemSetupData> {
  constructor(
    private syncSsdService: SyncSsdService,
    private modalService: SyncosyncModalService,
    public helpStateService: HelpStateService,
    public syncConfigurationService: SyncConfigurationService
  ) {
    super();
  }

  public onSubmit(): void {
    this.followSsdSyncDecision();
  }

  followSsdSyncDecision(): void {
    const resultEmitter: EventEmitter<SystemSetupData> =
      new EventEmitter<SystemSetupData>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        // Everything is fine, nothing else to do
        this.formFinished.emit(result);
      }
    });
    this.modalService.showProcessingModal<SystemSetupData>(
      LocalizedMessage.UPDATE_SSD_FOLLOW_SYNC_MODAL_TITLE,
      LocalizedMessage.UPDATE_SSD_FOLLOW_SYNC_MODAL_CONTENT,
      this.syncConfigurationService.followKeyExchange(),
      this.syncConfigurationService.cancelKeyExchange(),
      resultEmitter
    );
  }

  protected getInitModel(): SystemSetupData {
    return undefined;
  }
}
