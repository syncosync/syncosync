import { Component, EventEmitter } from '@angular/core';
import {
  HelpStateService,
  LocalizedMessage,
  SyncosyncModalService,
  SystemSetupData,
} from '@syncosync_common';
import { SyncSsdService } from '../../../services/sync-ssd.service';
import { SyncConfigurationService } from '@syncosync_authenticated';
import { AbstractEmptyFormDirective } from '../../../../common/components/abstract-empty-form-directive/abstract-empty-form.directive';

@Component({
  selector: 'app-sync-ssd-lead',
  templateUrl: './sync-ssd-lead.component.html',
  styleUrls: ['./sync-ssd-lead.component.css'],
})
export class SyncSsdLeadComponent extends AbstractEmptyFormDirective<SystemSetupData> {
  constructor(
    private syncSsdService: SyncSsdService,
    private modalService: SyncosyncModalService,
    public helpStateService: HelpStateService,
    public syncConfigurationService: SyncConfigurationService
  ) {
    super();
  }

  public onSubmit(): void {
    this.leadSsdSyncDecision();
  }
  protected getInitModel(): SystemSetupData {
    return new SystemSetupData();
  }

  leadSsdSyncDecision(): void {
    const resultEmitter: EventEmitter<SystemSetupData> =
      new EventEmitter<SystemSetupData>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        // Everything is fine, nothing else to do
        this.formFinished.emit(result);
      }
    });
    this.modalService.showProcessingModal<SystemSetupData>(
      LocalizedMessage.UPDATE_SSD_LEAD_SYNC_MODAL_TITLE,
      LocalizedMessage.UPDATE_SSD_LEAD_SYNC_MODAL_CONTENT,
      this.syncSsdService.leadSyncSsd(),
      this.syncConfigurationService.cancelKeyExchange(),
      resultEmitter
    );
  }
}
