import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SyncSsdLeadComponent } from './sync-ssd-lead.component';

describe('SyncSsdLeadComponent', () => {
  let component: SyncSsdLeadComponent;
  let fixture: ComponentFixture<SyncSsdLeadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SyncSsdLeadComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SyncSsdLeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
