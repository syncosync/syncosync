import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrafficshapingSwitchingFormComponent } from './trafficshaping-switching-form.component';

describe('TrafficshapingSwitchingFormComponent', () => {
  let component: TrafficshapingSwitchingFormComponent;
  let fixture: ComponentFixture<TrafficshapingSwitchingFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TrafficshapingSwitchingFormComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrafficshapingSwitchingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
