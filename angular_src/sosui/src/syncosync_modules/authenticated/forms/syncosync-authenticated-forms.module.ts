import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AdminPasswordFormComponent,
  GenerateSyncKeyComponent,
  DisasterSwapPersistenceComponent,
  HostnameFormComponent,
  NameserverFormComponent,
  NetworkInterfaceFormComponent,
  SshPortFormComponent,
  SyncSetupRemoteConfigComponent,
} from '@syncosync_authenticated';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SyncosyncCommonModule } from '../../common/syncosync-common.module';
import { EmailSetupFormComponent } from './email-setup-form/email-setup-form.component';
import { PartitioningSliderPartitionerComponent } from './partitioning/partitioning-slider-partioner/partitioning-slider-partitioner.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { PartitioningFollowComponent } from './partitioning/partitioning-follow/partitioning-follow.component';
import { PartitioningLeadComponent } from './partitioning/partitioning-lead/partitioning-lead.component';
import { TimezoneSetupFormComponent } from './timezone-setup-form/timezone-setup-form.component';
import { TrafficshapingFormComponent } from './trafficshaping-form/trafficshaping-form.component';
import { RemoteRecoveryConfigBackupFormComponent } from './remote-recovery-config-backup-form/remote-recovery-config-backup-form.component';
import { AccountAddFormComponent } from './account-add-form/account-add-form.component';
import { AccountEditFormComponent } from './account-edit-form/account-edit-form.component';
import { TrafficshapingSwitchingFormComponent } from './trafficshaping-switching-form/trafficshaping-switching-form.component';
import { FactoryResetFormComponent } from './factory-reset-form/factory-reset-form.component';
import { ShutdownFormComponent } from './shutdown-form/shutdown-form.component';
import { RebootFormComponent } from './reboot-form/reboot-form.component';
import { SyncSsdLeadComponent } from './sync-ssd/sync-ssd-lead/sync-ssd-lead.component';
import { SyncSsdFollowComponent } from './sync-ssd/sync-ssd-follow/sync-ssd-follow.component';
import { RequestPartitioningActionOnPartitioningStatusComponent } from './request-partitioning-action-on-partitioning-status/request-partitioning-action-on-partitioning-status.component';
import { PartitioningSliderComponent } from './partitioning/partitioning-slider/partitioning-slider.component';

@NgModule({
  declarations: [
    HostnameFormComponent,
    NameserverFormComponent,
    NetworkInterfaceFormComponent,
    SshPortFormComponent,
    GenerateSyncKeyComponent,
    DisasterSwapPersistenceComponent,
    SyncSetupRemoteConfigComponent,
    AdminPasswordFormComponent,
    EmailSetupFormComponent,
    PartitioningSliderPartitionerComponent,
    PartitioningSliderComponent,
    PartitioningFollowComponent,
    PartitioningLeadComponent,
    TimezoneSetupFormComponent,
    TrafficshapingFormComponent,
    RemoteRecoveryConfigBackupFormComponent,
    AccountAddFormComponent,
    AccountEditFormComponent,
    TrafficshapingSwitchingFormComponent,
    FactoryResetFormComponent,
    ShutdownFormComponent,
    RebootFormComponent,
    SyncSsdLeadComponent,
    SyncSsdFollowComponent,
    RequestPartitioningActionOnPartitioningStatusComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    SyncosyncCommonModule,
    NgxSliderModule,
  ],
  exports: [
    HostnameFormComponent,
    NameserverFormComponent,
    NetworkInterfaceFormComponent,
    SshPortFormComponent,
    GenerateSyncKeyComponent,
    DisasterSwapPersistenceComponent,
    SyncSetupRemoteConfigComponent,
    AdminPasswordFormComponent,
    EmailSetupFormComponent,
    AdminPasswordFormComponent,
    PartitioningSliderPartitionerComponent,
    PartitioningSliderComponent,
    PartitioningFollowComponent,
    PartitioningLeadComponent,
    TrafficshapingFormComponent,
    TimezoneSetupFormComponent,
    FactoryResetFormComponent,
    ShutdownFormComponent,
    RebootFormComponent,
    SyncSsdLeadComponent,
    SyncSsdFollowComponent,
    RequestPartitioningActionOnPartitioningStatusComponent,
  ],
})
export class SyncosyncAuthenticatedFormsModule {}
