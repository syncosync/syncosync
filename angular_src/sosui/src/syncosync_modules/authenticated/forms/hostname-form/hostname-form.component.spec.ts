import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HostnameFormComponent} from './hostname-form.component';

describe('HostnameFormComponent', () => {
    let component: HostnameFormComponent;
    let fixture: ComponentFixture<HostnameFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HostnameFormComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HostnameFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
