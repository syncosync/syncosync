import { Component, EventEmitter } from '@angular/core';
import { NetworkConfigService } from '../../services/network-config.service';
import {
  AbstractEditableFormDirective,
  Hostname,
  SyncosyncModalService,
  HelpStateService,
  LocalizedMessage,
} from '@syncosync_common';
import { Observable } from 'rxjs';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';

@Component({
  selector: 'app-hostname-form',
  templateUrl: './hostname-form.component.html',
  styleUrls: ['./hostname-form.component.css'],
})
export class HostnameFormComponent extends AbstractEditableFormDirective<Hostname> {
  constructor(
    public networkConfigService: NetworkConfigService,
    public helpStateService: HelpStateService,
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  onSubmit(): void {
    const resultEmitter: EventEmitter<Hostname> = new EventEmitter<Hostname>();
    resultEmitter.subscribe((result: Hostname) => {
      if (result !== undefined && result !== null) {
        this.setModel(result);
        this.formFinished.emit(this.model);
      } else {
        // TODO: Show error
        this.formFinished.emit(null);
      }
    });
    this.syncosyncModalService.showProcessingModal<Hostname>(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.HOSTNAME_CHANGE_PROCESSING_MODAL_CONTENT,
      this.networkConfigService.setHostname(this.model),
      null,
      resultEmitter
    );
  }

  protected createFormGroup(): void {
    //nop
  }

  protected modelRequest(): Observable<Hostname> {
    return this.networkConfigService.getHostname();
  }

  protected getInitModel(): Hostname {
    return new Hostname();
  }
}
