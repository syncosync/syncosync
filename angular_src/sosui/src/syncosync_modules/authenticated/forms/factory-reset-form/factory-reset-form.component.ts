import { Component, EventEmitter } from '@angular/core';
import {
  AdminPassword,
  AuthService,
  GenericUiResponse,
  GenericUiResponseStatus,
  InfoLevel,
  HelpStateService,
  LocalizedMessage,
  SyncosyncModalService,
} from '@syncosync_common';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';
import { AbstractEmptyFormDirective } from '../../../common/components/abstract-empty-form-directive/abstract-empty-form.directive';
import { FactoryReset } from '../../../common/model/factoryReset';
import { PasswordCheckComponent } from '../../../common/forms/password-check/password-check.component';
import { FactoryResetService } from '../../services/factory-reset.service';
import { EraseDiskLevel } from '../../../common/model/sosEnums';

@Component({
  selector: 'app-factory-reset-form',
  templateUrl: './factory-reset-form.component.html',
  styleUrls: ['./factory-reset-form.component.css'],
})
export class FactoryResetFormComponent extends AbstractEmptyFormDirective<FactoryReset> {
  eraseDiskLevel = EraseDiskLevel;

  protected getInitModel(): FactoryReset {
    return new FactoryReset();
  }
  public onSubmit(): void {
    // TODO: Open a confirmation modal for the password to be entered
    //  pass along the model in order to actually send the reset request
    this.syncosyncModalService
      .showRequestDataModal<AdminPassword, PasswordCheckComponent>(
        LocalizedMessage.PASSWORD_CONFIRMATION_REQUEST_TITLE,
        PasswordCheckComponent,
        LocalizedMessage.PASSWORD_CONFIRMATION_REQUEST_EXPLANATION,
        this.model.password_check
      )
      .subscribe((result: AdminPassword) => {
        if (result === undefined || result === null) {
          // User aborted password input
          return;
        }
        // Send the factory reset command
        this.triggerFactoryReset();
      });
  }

  constructor(
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    public helpStateService: HelpStateService,
    private factoryResetService: FactoryResetService,
    private authService: AuthService
  ) {
    super();
  }

  private triggerFactoryReset() {
    const resultEmitter: EventEmitter<GenericUiResponse> =
      new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe((result) => {
      if (
        result !== undefined &&
        result !== null &&
        result.status === GenericUiResponseStatus.OK
      ) {
        this.formFinished.emit(this.model);
        // TODO: Show modal informing about reboot in progress
        this.authService.logout();
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.FACTORY_RESET_FAILED
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCESSING_FACTORY_RESET_IN_PROGRESS,
      this.factoryResetService.triggerFactoryReset(this.model),
      null,
      resultEmitter
    );
  }
}
