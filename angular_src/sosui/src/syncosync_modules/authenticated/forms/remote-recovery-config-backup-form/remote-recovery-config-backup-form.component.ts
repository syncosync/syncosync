import { Component, EventEmitter } from '@angular/core';
import {
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
  SysStateTransport,
  SystemMode,
} from '@syncosync_common';
import { AbstractEmptyFormDirective } from '../../../common/components/abstract-empty-form-directive/abstract-empty-form.directive';
import { ModeManagementService } from '@syncosync_authenticated';
import { SysStateResult } from '../../../common/model/sysState';

@Component({
  selector: 'app-remote-recovery-config-backup-form',
  templateUrl: './remote-recovery-config-backup-form.component.html',
  styleUrls: ['./remote-recovery-config-backup-form.component.css'],
})
export class RemoteRecoveryConfigBackupFormComponent extends AbstractEmptyFormDirective<SysStateTransport> {
  file: File = null;

  protected createFormGroup(): void {
    //
  }

  protected getInitModel(): SysStateTransport {
    const model = new SysStateTransport();
    model.state_desired = SystemMode.REMOTE_RECOVERY;
    model.config_backup_password = '';
    return model;
  }

  constructor(
    protected syncosyncModalService: SyncosyncModalService,
    private modeManagementService: ModeManagementService
  ) {
    super();
    //
  }

  onSubmit(): void {
    if (
      this.model.config_backup_password === null ||
      this.model.config_backup_password.length == 0
    ) {
      return;
    }
    const reader = new FileReader();
    reader.onload = () => {
      //me.modelvalue = reader.result;
      if (typeof reader.result === 'string') {
        let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
        if (encoded.length % 4 > 0) {
          encoded += '='.repeat(4 - (encoded.length % 4));
        }
        this.sendForm(encoded);
      } else {
        alert('File not read as string...');
        console.log(reader.result);
      }
    };
    reader.onerror = function (error) {
      console.log('Failed reading file. Error: ', error);
    };
    // TODO: Validate file size/type beforehand...
    reader.readAsDataURL(this.file);
  }

  sendForm(base64encodedFile: string): void {
    this.model.config_backup_encoded = base64encodedFile;
    this.model.config_backup_name = this.file.name;

    const resultEmitter: EventEmitter<SysStateResult> =
      new EventEmitter<SysStateResult>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        if (result.mode == SystemMode.REMOTE_RECOVERY) {
          // Swapped successfully
          this.formFinished.emit(this.model);
        } else {
          this.syncosyncModalService.showInfoModal(
            InfoLevel.INFO,
            LocalizedMessage.REMOTE_RECOVERY_FAILED_GENERIC_MESSAGE,
            result.report.toString()
          );
        }
      } else {
        // nop since 500 error is caught by guard/interceptor
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCESSING_REMOTE_RECOVERY_REQUEST_CONTENT,
      this.modeManagementService.setMode(this.model),
      null,
      resultEmitter
    );
  }

  newFileSelected($event: File): void {
    // TODO: Validate type, name, size
    this.file = $event;
  }
}
