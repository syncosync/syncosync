import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailConfigurationSetupPartialComponent } from './mail-configuration-setup-partial.component';

describe('MailConfigurationSetupPartialComponent', () => {
  let component: MailConfigurationSetupPartialComponent;
  let fixture: ComponentFixture<MailConfigurationSetupPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MailConfigurationSetupPartialComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailConfigurationSetupPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
