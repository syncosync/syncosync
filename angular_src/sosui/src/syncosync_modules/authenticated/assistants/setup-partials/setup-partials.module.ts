import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SyncosyncAuthenticatedFormsModule } from '../../forms/syncosync-authenticated-forms.module';
import { AdminPasswordPartialComponent } from './admin-password-partial/admin-password-partial.component';
import { HostnameSetupPartialComponent } from './hostname-setup-partial/hostname-setup-partial.component';
import { NetworkInterfacesSetupPartialComponent } from './network-interfaces-setup-partial/network-interfaces-setup-partial.component';
import { NameserverSetupPartialComponent } from './nameserver-setup-partial/nameserver-setup-partial.component';
import { SshPortSetupPartialComponent } from './ssh-port-setup-partial/ssh-port-setup-partial.component';
import { KeyExchangeSetupPartialComponent } from './key-exchange-setup-partial/key-exchange-setup-partial.component';
import { MailConfigurationSetupPartialComponent } from './mail-configuration-setup-partial/mail-configuration-setup-partial.component';
import { PartitioningPartialComponent } from './partitioning-partial/partitioning-partial.component';
import { SyncosyncComponentsModule } from '../../components/syncosync-components.module';
import { TrafficshapingSetupPartialComponent } from './trafficshaping-setup-partial/trafficshaping-setup-partial.component';
import {
  EsaIntroductionPartialComponent,
  GenerateSyncKeySetupPartialComponent,
} from '@syncosync_authenticated';
import { PartitioningSelectionComponent } from './partitioning-partial/partitioning-selection/partitioning-selection.component';
import { EsaOutroPartialComponentComponent } from './esa-outro-partial-component/esa-outro-partial-component.component';

@NgModule({
  declarations: [
    GenerateSyncKeySetupPartialComponent,
    AdminPasswordPartialComponent,
    HostnameSetupPartialComponent,
    NetworkInterfacesSetupPartialComponent,
    NameserverSetupPartialComponent,
    SshPortSetupPartialComponent,
    KeyExchangeSetupPartialComponent,
    MailConfigurationSetupPartialComponent,
    PartitioningPartialComponent,
    TrafficshapingSetupPartialComponent,
    EsaIntroductionPartialComponent,
    PartitioningSelectionComponent,
    EsaOutroPartialComponentComponent,
  ],
  imports: [
    CommonModule,
    SyncosyncAuthenticatedFormsModule,
    SyncosyncComponentsModule,
  ],
  exports: [GenerateSyncKeySetupPartialComponent],
})
export class SetupPartialsModule {}
