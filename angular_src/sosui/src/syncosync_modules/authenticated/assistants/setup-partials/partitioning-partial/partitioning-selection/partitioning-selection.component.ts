import { Component } from '@angular/core';
import {
  AbstractEditableFormDirective,
  SosKey,
  SyncosyncModalService,
} from '@syncosync_common';
import { SyncConfigurationService } from '@syncosync_authenticated';
import { ProcessingSpinnerStateService } from '../../../../../common/services/processing-spinner-state.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-partitioning-selection',
  templateUrl: './partitioning-selection.component.html',
  styleUrls: ['./partitioning-selection.component.css'],
})
export class PartitioningSelectionComponent extends AbstractEditableFormDirective<SosKey> {
  isFollowerDecision = false;
  isLeaderDecision = false;
  constructor(
    public syncConfigurationService: SyncConfigurationService,

    private modalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super(modalService, processingSpinnerStateService);
  }

  onSubmit(): void {
    // nop
  }

  protected getInitModel(): SosKey {
    return new SosKey();
  }

  protected modelRequest(): Observable<SosKey> {
    return this.syncConfigurationService.getRemoteKey();
  }

  leadPartitioningDecision(): void {
    this.isFollowerDecision = false;
    this.isLeaderDecision = true;
  }
  followPartitioningDecision(): void {
    this.isFollowerDecision = true;
    this.isLeaderDecision = false;
  }
}
