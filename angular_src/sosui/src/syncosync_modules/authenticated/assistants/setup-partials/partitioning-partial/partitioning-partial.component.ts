import { Component, OnInit } from '@angular/core';
import { IsLeader, SetupAssistantDirective } from '@syncosync_common';
import { ProcessingSpinnerStateService } from '../../../../common/services/processing-spinner-state.service';
import { IsLeaderService } from '../../../services/is-leader.service';
import { SyncConfigurationService } from '../../../services/sync-configuration.service';

@Component({
  selector: 'app-partitioning-partial',
  templateUrl: './partitioning-partial.component.html',
  styleUrls: ['./partitioning-partial.component.css'],
})
export class PartitioningPartialComponent
  extends SetupAssistantDirective<null>
  implements OnInit
{
  public isFollower: boolean = null;
  //public remoteKey: Observable<SosKey> = new SosKey();

  constructor(
    public syncConfigurationService: SyncConfigurationService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    private isLeaderService: IsLeaderService
  ) {
    super();
  }

  ngOnInit(): void {
    this.processingSpinnerStateService.addSpinner();
    //this.remoteKey = this.syncConfigurationService.getRemoteKey();
    this.isLeaderService.isLeader().subscribe((result: IsLeader) => {
      if (result !== undefined) {
        this.isFollower = !result.is_leader;
      } else {
        // TODO: InfoModal with big ouch
      }
      this.processingSpinnerStateService.removeSpinner();
    });
  }
}
