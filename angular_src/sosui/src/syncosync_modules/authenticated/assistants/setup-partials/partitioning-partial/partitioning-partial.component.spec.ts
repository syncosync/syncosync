import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitioningPartialComponent } from './partitioning-partial.component';

describe('PartitioningPartialComponent', () => {
  let component: PartitioningPartialComponent;
  let fixture: ComponentFixture<PartitioningPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PartitioningPartialComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartitioningPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
