import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyExchangeSetupPartialComponent } from './key-exchange-setup-partial.component';

describe('SyncSetupRemoteConfigSetupPartialComponent', () => {
  let component: KeyExchangeSetupPartialComponent;
  let fixture: ComponentFixture<KeyExchangeSetupPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KeyExchangeSetupPartialComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyExchangeSetupPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
