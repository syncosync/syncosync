import { Component } from '@angular/core';
import { SetupAssistantDirective, SystemSetupData } from '@syncosync_common';
import { KeyExchangeButtonsComponent } from '../../../components/key-exchange-buttons/key-exchange-buttons.component';
import { SyncSetupRemoteConfigComponent } from '../../../forms/sync-setup-remote-config/sync-setup-remote-config.component';

@Component({
  selector: 'app-sync-setup-remote-config-setup-partial',
  templateUrl: './key-exchange-setup-partial.component.html',
  styleUrls: ['./key-exchange-setup-partial.component.css'],
})
export class KeyExchangeSetupPartialComponent extends SetupAssistantDirective<SystemSetupData> {
  constructor() {
    super();
    this.canBeSkipped = true;
    this.canBeReturnedTo = true;
  }

  enableKeyExchangeButtons(
    keyExchangeButtons: KeyExchangeButtonsComponent,
    syncSetupRemoteConfigComponent: SyncSetupRemoteConfigComponent
  ): void {
    if (
      syncSetupRemoteConfigComponent.model != null &&
      syncSetupRemoteConfigComponent.model.hostname?.length > 0 &&
      syncSetupRemoteConfigComponent.model.port > 0 &&
      syncSetupRemoteConfigComponent.model.port <= 65535
    ) {
      keyExchangeButtons.disableButtons = false;
    }
  }
}
