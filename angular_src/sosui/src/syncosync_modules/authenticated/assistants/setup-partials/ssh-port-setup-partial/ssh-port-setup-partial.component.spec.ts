import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SshPortSetupPartialComponent } from './ssh-port-setup-partial.component';

describe('SshPortSetupPartialComponent', () => {
  let component: SshPortSetupPartialComponent;
  let fixture: ComponentFixture<SshPortSetupPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SshPortSetupPartialComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SshPortSetupPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
