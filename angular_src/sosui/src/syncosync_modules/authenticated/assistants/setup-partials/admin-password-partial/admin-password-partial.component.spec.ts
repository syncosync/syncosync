import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPasswordPartialComponent } from './admin-password-partial.component';

describe('AdminPasswordPartialComponent', () => {
  let component: AdminPasswordPartialComponent;
  let fixture: ComponentFixture<AdminPasswordPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminPasswordPartialComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPasswordPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
