import { Component, OnInit } from '@angular/core';
import { AdminPassword, SetupAssistantDirective } from '@syncosync_common';

@Component({
  selector: 'app-admin-password-partial',
  templateUrl: './admin-password-partial.component.html',
  styleUrls: ['./admin-password-partial.component.css'],
})
export class AdminPasswordPartialComponent
  extends SetupAssistantDirective<AdminPassword>
  implements OnInit
{
  constructor() {
    super();
    this.canBeSkipped = true;
    this.canBeReturnedTo = true;
  }

  ngOnInit(): void {
    //
  }
}
