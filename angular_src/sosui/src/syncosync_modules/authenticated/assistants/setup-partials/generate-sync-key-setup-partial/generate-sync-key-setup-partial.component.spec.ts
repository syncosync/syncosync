import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateSyncKeySetupPartialComponent } from './generate-sync-key-setup-partial.component';

describe('GenerateSyncKeySetupPartialComponent', () => {
  let component: GenerateSyncKeySetupPartialComponent;
  let fixture: ComponentFixture<GenerateSyncKeySetupPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenerateSyncKeySetupPartialComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateSyncKeySetupPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
