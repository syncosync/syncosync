import { Component } from '@angular/core';
import { SetupAssistantDirective, SosKey } from '@syncosync_common';

@Component({
  selector: 'app-generate-sync-key-setup-partial',
  templateUrl: './generate-sync-key-setup-partial.component.html',
  styleUrls: ['./generate-sync-key-setup-partial.component.css'],
})
export class GenerateSyncKeySetupPartialComponent extends SetupAssistantDirective<SosKey> {
  // TODO: Update upon loading of underlying component
  constructor() {
    super();
  }

  handleModelChange(newModel: SosKey): void {
    if (newModel === undefined || newModel === null) {
      return;
    } else if (
      newModel.fingerprint !== null &&
      newModel.fingerprint.length > 0
    ) {
      this.setCanBeSkipped(true);
      this.canBeReturnedTo = true;
    }
  }
}
