import { Component, OnInit } from '@angular/core';
import { NicConfig, SetupAssistantDirective } from '@syncosync_common';

@Component({
  selector: 'app-network-interfaces-setup-partial',
  templateUrl: './network-interfaces-setup-partial.component.html',
  styleUrls: ['./network-interfaces-setup-partial.component.css'],
})
export class NetworkInterfacesSetupPartialComponent extends SetupAssistantDirective<
  NicConfig[]
> {
  constructor() {
    super();
    this.canBeSkipped = true;
    this.canBeReturnedTo = true;
  }
}
