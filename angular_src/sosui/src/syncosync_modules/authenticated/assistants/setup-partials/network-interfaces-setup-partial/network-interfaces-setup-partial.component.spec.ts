import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkInterfacesSetupPartialComponent } from './network-interfaces-setup-partial.component';

describe('NetworkInterfacesSetupPartialComponent', () => {
  let component: NetworkInterfacesSetupPartialComponent;
  let fixture: ComponentFixture<NetworkInterfacesSetupPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NetworkInterfacesSetupPartialComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkInterfacesSetupPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
