import { Component } from '@angular/core';
import { SetupAssistantDirective, HelpStateService } from '@syncosync_common';

@Component({
  selector: 'app-esa-introduction-partial',
  templateUrl: './esa-introduction-partial.component.html',
  styleUrls: ['./esa-introduction-partial.component.css'],
})
export class EsaIntroductionPartialComponent extends SetupAssistantDirective<null> {
  constructor(public helpStateService: HelpStateService) {
    super();
    this.canBeSkipped = true;
    this.canBeReturnedTo = true;
  }
  ngOnInit(): void {
    //
  }
}
