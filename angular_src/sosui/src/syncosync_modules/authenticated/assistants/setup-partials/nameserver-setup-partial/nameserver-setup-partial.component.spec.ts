import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NameserverSetupPartialComponent } from './nameserver-setup-partial.component';

describe('NameserverSetupPartialComponent', () => {
  let component: NameserverSetupPartialComponent;
  let fixture: ComponentFixture<NameserverSetupPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NameserverSetupPartialComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NameserverSetupPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
