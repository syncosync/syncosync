import { Component, OnInit } from '@angular/core';
import {
  NicConfig,
  NicMode,
  ResolvConfig,
  SetupAssistantDirective,
} from '@syncosync_common';
import { NetworkConfigService } from '../../../services/network-config.service';
import { Observable } from 'rxjs';
import { ProcessingSpinnerStateService } from '../../../../common/services/processing-spinner-state.service';

@Component({
  selector: 'app-nameserver-setup-partial',
  templateUrl: './nameserver-setup-partial.component.html',
  styleUrls: ['./nameserver-setup-partial.component.css'],
})
export class NameserverSetupPartialComponent
  extends SetupAssistantDirective<ResolvConfig>
  implements OnInit
{
  constructor(
    private networkConfigService: NetworkConfigService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super();
    this.canBeSkipped = true;
    this.canBeReturnedTo = true;
  }

  ngOnInit(): void {
    this.requestData();
  }

  protected modelRequest(): Observable<NicConfig[]> {
    return this.networkConfigService.getNetworkInterfaces();
  }

  public requestData(): void {
    this.processingSpinnerStateService.addSpinner();
    this.modelRequest().subscribe((result) => {
      this.processingSpinnerStateService.removeSpinner();
      // Check if ANY nic has DHCP set
      if (result === undefined || result === null) {
        return;
      }
      for (const nic of result) {
        if (nic.ip4_mode === NicMode.DHCP || nic.ip6_mode === NicMode.DHCP) {
          this.setShouldBeSkipped(true);
          return;
        }
      }
    });
  }
}
