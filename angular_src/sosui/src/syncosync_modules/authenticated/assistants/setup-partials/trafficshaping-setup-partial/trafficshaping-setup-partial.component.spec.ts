import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrafficshapingSetupPartialComponent } from './trafficshaping-setup-partial.component';

describe('TrafficshapingSetupPartialComponent', () => {
  let component: TrafficshapingSetupPartialComponent;
  let fixture: ComponentFixture<TrafficshapingSetupPartialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TrafficshapingSetupPartialComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrafficshapingSetupPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
