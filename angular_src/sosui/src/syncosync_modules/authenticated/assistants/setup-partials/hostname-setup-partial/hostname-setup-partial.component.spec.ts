import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HostnameSetupPartialComponent } from './hostname-setup-partial.component';

describe('HostnameSetupPartialComponent', () => {
  let component: HostnameSetupPartialComponent;
  let fixture: ComponentFixture<HostnameSetupPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HostnameSetupPartialComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostnameSetupPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
