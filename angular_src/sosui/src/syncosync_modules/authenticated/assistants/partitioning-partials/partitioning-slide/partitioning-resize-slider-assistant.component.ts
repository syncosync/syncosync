import { Component, OnInit } from '@angular/core';
import {
  HelpStateService,
  IsLeader,
  SetupAssistantDirective,
  SystemSetupData,
} from '@syncosync_common';
import { SyncConfigurationService } from '@syncosync_authenticated';
import { IsLeaderService } from '../../../services/is-leader.service';
import { ProcessingSpinnerStateService } from '../../../../common/services/processing-spinner-state.service';

@Component({
  selector: 'app-partitioning-resize-slider-assistant',
  templateUrl: './partitioning-resize-slider-assistant.component.html',
  styleUrls: ['./partitioning-resize-slider-assistant.component.css'],
})
export class PartitioningResizeSliderAssistantComponent
  extends SetupAssistantDirective<SystemSetupData>
  implements OnInit
{
  public isLeader: boolean = null;
  public disableLeadPartitionButton = true;

  constructor(
    public helpStateService: HelpStateService,
    public syncConfigurationService: SyncConfigurationService,
    private isLeaderService: IsLeaderService,
    private processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super();
    this.canBeSkipped = false;
    this.canBeReturnedTo = false;
  }

  ngOnInit(): void {
    this.processingSpinnerStateService.addSpinner();
    //this.remoteKey = this.syncConfigurationService.getRemoteKey();
    this.isLeaderService.isLeader().subscribe((result: IsLeader) => {
      if (result !== undefined) {
        this.isLeader = result.is_leader;
      } else {
        // TODO: InfoModal with big ouch
      }
      this.processingSpinnerStateService.removeSpinner();
    });
  }
}
