import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitioningResizeSliderAssistantComponent } from './partitioning-resize-slider-assistant.component';

describe('PartitiongSlideComponent', () => {
  let component: PartitioningResizeSliderAssistantComponent;
  let fixture: ComponentFixture<PartitioningResizeSliderAssistantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PartitioningResizeSliderAssistantComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(
      PartitioningResizeSliderAssistantComponent
    );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
