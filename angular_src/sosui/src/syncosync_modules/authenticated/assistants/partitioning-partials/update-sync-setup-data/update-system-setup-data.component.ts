import { Component, OnInit } from '@angular/core';
import {
  HelpStateService,
  SetupAssistantDirective,
  SystemSetupData,
} from '@syncosync_common';
import { SyncConfigurationService } from '@syncosync_authenticated';
import { PartitioningService } from '../../../services/partitioning.service';

@Component({
  selector: 'app-update-sync-setup-data',
  templateUrl: './update-system-setup-data.component.html',
  styleUrls: ['./update-system-setup-data.component.css'],
})
export class UpdateSystemSetupDataComponent
  extends SetupAssistantDirective<SystemSetupData>
  implements OnInit
{
  /**
   * This component is supposed to support the user in syncing the SSD (system setup data) to detect mismatches of
   * partitioning.
   * Having synced the SSD, the following procedure is required to be run on receiver side. Sending party is the leader
   * dictating the partitioning to be used for adjustments:
   * If everything is fine, means, no partition resize necessary, then move on
   * If not then on receiver side show
   *  - Partition Information show resize partitions,
   *  or
   *  - if resizing is not possible: display an error and ask to get support.
   *  After successful resizing, show Phase 3.
   *
   *  For this purpose, the SOS Core provides an enum PartitioningState indicating SSD is ok (0), a resize is needed (1)
   *  or resize is not possible (2).
   *  If 0: Move on
   *  If 1: Show resize partitions
   *  Else (2): Show error to ask for support
   * @param helpStateService
   * @param syncConfigurationService
   * @param partitioningService used to fetch the PartitioningStatus
   */
  constructor(
    public helpStateService: HelpStateService,
    public syncConfigurationService: SyncConfigurationService,
    private partitioningService: PartitioningService
  ) {
    super();
    this.canBeSkipped = false;
    this.canBeReturnedTo = true;
  }

  ngOnInit(): void {
    //
  }
}
