import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSystemSetupDataComponent } from './update-system-setup-data.component';

describe('UpdateSyncSetupDataComponent', () => {
  let component: UpdateSystemSetupDataComponent;
  let fixture: ComponentFixture<UpdateSystemSetupDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateSystemSetupDataComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSystemSetupDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
