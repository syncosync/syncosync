import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleCurrentPartitioningStatusComponent } from './handle-current-partitioning-status.component';

describe('HandleCurrentPartitioningStatusComponent', () => {
  let component: HandleCurrentPartitioningStatusComponent;
  let fixture: ComponentFixture<HandleCurrentPartitioningStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HandleCurrentPartitioningStatusComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HandleCurrentPartitioningStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
