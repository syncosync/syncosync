import { Component, OnInit } from '@angular/core';
import { SetupAssistantDirective } from '@syncosync_common';
import { PartitioningStatus } from '../../../../common/model/partitioningStatus';

@Component({
  selector: 'app-handle-current-partitioning-status',
  templateUrl: './handle-current-partitioning-status.component.html',
  styleUrls: ['./handle-current-partitioning-status.component.css'],
})
export class HandleCurrentPartitioningStatusComponent
  extends SetupAssistantDirective<PartitioningStatus>
  implements OnInit
{
  constructor() {
    super();
    this.canBeSkipped = false;
    this.canBeReturnedTo = false;
  }

  ngOnInit(): void {}
}
