import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowFinalPartitioningInfoComponent } from './show-final-partitioning-info.component';

describe('ShowFinalPartitioningInfoComponent', () => {
  let component: ShowFinalPartitioningInfoComponent;
  let fixture: ComponentFixture<ShowFinalPartitioningInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShowFinalPartitioningInfoComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ShowFinalPartitioningInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
