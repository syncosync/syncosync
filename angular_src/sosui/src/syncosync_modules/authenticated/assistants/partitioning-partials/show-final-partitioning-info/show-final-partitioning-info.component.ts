import { Component, OnInit } from '@angular/core';
import { SetupAssistantDirective, SystemSetupData } from '@syncosync_common';

@Component({
  selector: 'app-show-final-partitioning-info',
  templateUrl: './show-final-partitioning-info.component.html',
  styleUrls: ['./show-final-partitioning-info.component.css'],
})
export class ShowFinalPartitioningInfoComponent
  extends SetupAssistantDirective<null>
  implements OnInit
{
  constructor() {
    super();
    this.canBeSkipped = false;
    this.canBeReturnedTo = true;
  }

  ngOnInit(): void {
    // nop
  }
}
