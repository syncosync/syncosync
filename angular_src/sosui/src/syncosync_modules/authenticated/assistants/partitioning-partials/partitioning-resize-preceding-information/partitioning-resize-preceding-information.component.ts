import { Component, OnInit } from '@angular/core';
import { SetupAssistantDirective } from '@syncosync_common';

@Component({
  selector: 'app-partitioning-resize-preceding-information',
  templateUrl: './partitioning-resize-preceding-information.component.html',
  styleUrls: ['./partitioning-resize-preceding-information.component.css'],
})
export class PartitioningResizePrecedingInformationComponent
  extends SetupAssistantDirective<null>
  implements OnInit
{
  constructor() {
    super();
    this.canBeSkipped = true;
    this.canBeReturnedTo = true;
  }

  ngOnInit(): void {
    //
  }
}
