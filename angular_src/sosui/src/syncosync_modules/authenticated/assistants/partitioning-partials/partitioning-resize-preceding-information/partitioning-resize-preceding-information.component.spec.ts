import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitioningResizePrecedingInformationComponent } from './partitioning-resize-preceding-information.component';

describe('PartitioningResizePrecedingInformationComponent', () => {
  let component: PartitioningResizePrecedingInformationComponent;
  let fixture: ComponentFixture<PartitioningResizePrecedingInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PartitioningResizePrecedingInformationComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(
      PartitioningResizePrecedingInformationComponent
    );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
