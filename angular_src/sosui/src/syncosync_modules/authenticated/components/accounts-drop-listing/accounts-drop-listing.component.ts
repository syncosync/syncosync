import { Component, Input } from '@angular/core';
import { AccountApiService } from '../../services/account-api.service';
import {
  AbstractSubscriberComponent,
  AccountAdd,
  LocalizedMessage,
  Status,
  SyncosyncModalService,
} from '@syncosync_common';
import { AccountAddFormComponent } from '../../forms/account-add-form/account-add-form.component';

@Component({
  selector: 'app-accounts-drop-listing',
  templateUrl: './accounts-drop-listing.component.html',
  styleUrls: ['./accounts-drop-listing.component.css'],
})
export class AccountsDropListingComponent extends AbstractSubscriberComponent {
  @Input() latestStatus: Status;

  constructor(
    public accountService: AccountApiService,
    private modalService: SyncosyncModalService
  ) {
    super();
  }

  // TODO: Generically add forms to a modal that cannot be closed and only offers a Cancel and a Submit button
  addAccount(): void {
    this.modalService
      .showRequestDataModal<AccountAdd, AccountAddFormComponent>(
        LocalizedMessage.ADD_ACCOUNT_MODAL_HEADER,
        AccountAddFormComponent
      )
      .subscribe((result) => {
        if (result === undefined || result === null) {
          // User aborted password input
          // nop
        } else {
          // TODO: show nothing?
        }
      });
  }
}
