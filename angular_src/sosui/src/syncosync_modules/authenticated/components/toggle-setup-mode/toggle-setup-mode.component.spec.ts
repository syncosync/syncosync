import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ToggleSetupModeComponent} from './toggle-setup-mode.component';

describe('ToggleSetupModeComponent', () => {
    let component: ToggleSetupModeComponent;
    let fixture: ComponentFixture<ToggleSetupModeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ToggleSetupModeComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ToggleSetupModeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
