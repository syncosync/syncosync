import { Component, ElementRef, ViewChild } from '@angular/core';
import { AbstractSubscriberComponent } from '@syncosync_common';
import { SetupUiSwitchingService } from '../../../common/services/setup-ui-switching.service';

@Component({
  selector: 'app-toggle-setup-mode',
  templateUrl: './toggle-setup-mode.component.html',
  styleUrls: ['./toggle-setup-mode.component.css'],
})
export class ToggleSetupModeComponent extends AbstractSubscriberComponent {
  @ViewChild('setupToggle', { static: true }) setupModeCheckbox: ElementRef;

  constructor(public setupUiSwitchingService: SetupUiSwitchingService) {
    super();
  }
}
