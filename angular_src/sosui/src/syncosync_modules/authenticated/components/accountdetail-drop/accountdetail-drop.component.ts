import { Component, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AccountDeleteComponent } from '../../modals/account-delete/account-delete.component';
import { AccountEraseDataComponent } from '../../modals/account-erase-data/account-erase-data.component';
import {
  AccountInfo,
  AccountEdit,
  LocalizedMessage,
  MailInfoLevels,
  Status,
  SyncosyncModalService,
} from '@syncosync_common';
import { AccountEditFormComponent } from '../../forms/account-edit-form/account-edit-form.component';

@Component({
  selector: 'app-accountdetail-drop',
  templateUrl: './accountdetail-drop.component.html',
  styleUrls: ['./accountdetail-drop.component.css'],
})
export class AccountdetailDropComponent {
  @Input() account: AccountInfo;
  @Input() latestStatus: Status;
  mailInfoLevels = MailInfoLevels;
  showDetails = false;

  constructor(
    private ngbModalService: NgbModal,
    private modalService: SyncosyncModalService
  ) {}

  editModal($event: MouseEvent, account: AccountInfo): void {
    const accountEdit = new AccountEdit();
    Object.keys(account).forEach((key) => (accountEdit[key] = account[key]));
    this.modalService
      .showRequestDataModal<AccountEdit, AccountEditFormComponent>(
        accountEdit.name,
        AccountEditFormComponent,
        null,
        accountEdit
      )
      .subscribe((result) => {
        if (result === undefined || result === null) {
          // User aborted password input
          // nop
        } else {
          // TODO: show nothing?
        }
      });
  }

  deleteModal($event: MouseEvent, account: AccountInfo): void {
    const deleteModalRef = this.ngbModalService.open(AccountDeleteComponent);
    deleteModalRef.componentInstance.account = account;
  }

  eraseDataModal($event: MouseEvent, account: AccountInfo): void {
    const deleteModalRef = this.ngbModalService.open(AccountEraseDataComponent);
    deleteModalRef.componentInstance.account = account;
  }
}
