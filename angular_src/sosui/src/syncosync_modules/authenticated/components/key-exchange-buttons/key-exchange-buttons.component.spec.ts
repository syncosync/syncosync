import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KeyExchangeButtonsComponent} from './key-exchange-buttons.component';

describe('LeadKeyExchangeButtonComponent', () => {
    let component: KeyExchangeButtonsComponent;
    let fixture: ComponentFixture<KeyExchangeButtonsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [KeyExchangeButtonsComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(KeyExchangeButtonsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
