import { Injectable } from '@angular/core';
import {
  AuthService,
  Drives,
  DrivesetupDriveConfiguration,
  GenericUiResponse,
  SosServiceBaseAuthenticatedService,
} from '@syncosync_common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DrivesetupService extends SosServiceBaseAuthenticatedService {
  protected serviceName = 'DrivesetupService';

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  public getDrives(): Observable<Drives> {
    return this.httpClient
      .get<Drives>(syncosyncEndpoints.authenticated.drivesetupUrl)
      .pipe(catchError(this.handleError<Drives>('getDrives')));
  }

  public sendDriveConfiguration(
    driveConfiguration: DrivesetupDriveConfiguration
  ): Observable<GenericUiResponse> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<GenericUiResponse>(
        syncosyncEndpoints.authenticated.drivesetupUrl,
        driveConfiguration,
        options
      )
      .pipe(
        tap((_) => this.log('Sent drive configuration')),
        catchError(
          this.handleError<GenericUiResponse>('sendDriveConfiguration')
        )
      );
  }
}
