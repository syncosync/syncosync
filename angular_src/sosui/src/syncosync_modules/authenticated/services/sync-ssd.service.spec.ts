import { TestBed } from '@angular/core/testing';

import { SyncSsdService } from './sync-ssd.service';

describe('SyncSsdService', () => {
  let service: SyncSsdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SyncSsdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
