import { Injectable } from '@angular/core';
import {
  AuthService,
  MailConfig,
  SosServiceBaseAuthenticatedService,
} from '@syncosync_common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Timezone } from '../../common/model/timezone';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';
import { AvailableTimezones } from '../../common/model/availableTimezones';

@Injectable({
  providedIn: 'root',
})
export class TimezoneService extends SosServiceBaseAuthenticatedService {
  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  protected serviceName = 'TimezoneService';

  public getTimeZone(): Observable<Timezone> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<Timezone>(syncosyncEndpoints.authenticated.timezoneURL, options)
      .pipe(
        tap((_) => this.log('Got timezone')),
        catchError(this.handleError<Timezone>('getTimezones'))
      );
  }

  public getAvailableTimezones(): Observable<AvailableTimezones> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<AvailableTimezones>(
        syncosyncEndpoints.authenticated.availableTimezonesURL,
        options
      )
      .pipe(
        tap((_) => this.log('Available Timezones')),
        catchError(this.handleError<AvailableTimezones>('available Timezone'))
      );
  }

  public setTimezone(newTimezone: Timezone): Observable<Timezone> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<Timezone>(
        syncosyncEndpoints.authenticated.timezoneURL,
        newTimezone,
        options
      )
      .pipe(
        tap((_) => this.log('upateTimezone')),
        catchError(this.handleError<Timezone>('setTimezone'))
      );
  }
}
