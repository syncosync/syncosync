import { Injectable } from '@angular/core';
import {
  AuthService,
  SosServiceBaseAuthenticatedService,
  SystemSetupData,
} from '@syncosync_common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SyncSsdService extends SosServiceBaseAuthenticatedService {
  protected serviceName = 'SyncSsdService';

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  leadSyncSsd(): Observable<SystemSetupData> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<SystemSetupData>(
        syncosyncEndpoints.authenticated.syncSsdLeadUrl,
        options
      )
      .pipe(
        tap((_) => this.log('Synced SystemSetupData as leader')),
        catchError(this.handleError<SystemSetupData>('leadSyncSsd'))
      );
  }
}
