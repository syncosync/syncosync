import {TestBed} from '@angular/core/testing';

import {SyncConfigurationService} from './sync-configuration.service';

describe('SyncConfigurationService', () => {
    let service: SyncConfigurationService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(SyncConfigurationService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
