import { Injectable } from '@angular/core';
import {
  AuthService,
  SosServiceBaseAuthenticatedService,
} from '@syncosync_common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationBackupManagementService extends SosServiceBaseAuthenticatedService {
  protected serviceName = 'ConfigurationBackupManagementService';

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  public downloadLatestConfigBackup(): Observable<any> {
    return this.httpClient
      .get(syncosyncEndpoints.authenticated.downloadLatestConfigbackupUrl, {
        responseType: 'blob',
        observe: 'response',
      })
      .pipe(
        map((res) => {
          const contentDisposition = res.headers.get('content-disposition');
          let fileName = null;
          if (contentDisposition) {
            const fileNameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            const matches = fileNameRegex.exec(contentDisposition);
            if (matches != null && matches[1]) {
              fileName = matches[1].replace(/['"]/g, '');
            }
          }
          if (fileName === null) {
            return throwError('Unable to read filename from response header.');
          }
          return {
            file: new Blob([res.body], {
              type: res.headers.get('Content-Type'),
            }),
            filename: fileName,
          };
        }),
        tap((_) => this.log('Got latest configbackup')),
        catchError(this.handleError<any>('downloadLatestConfigBackup'))
      );
  }
}
