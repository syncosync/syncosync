import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';
import {
  AuthService,
  GenericUiResponse,
  RemoteHost,
  SosKey,
  SosServiceBaseAuthenticatedService,
  SystemSetupData,
} from '@syncosync_common';

@Injectable({
  providedIn: 'root',
})
export class SyncConfigurationService extends SosServiceBaseAuthenticatedService {
  protected serviceName = 'SyncConfigurationService';
  private localKey: SosKey = null;

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  public getLocalKey(): SosKey {
    return this.localKey;
  }

  getCurrentLocalKey(): Observable<SosKey> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<SosKey>(
        syncosyncEndpoints.authenticated.syncConfigurationLocalKey,
        options
      )
      .pipe(
        tap((key) => {
          if (key !== undefined && key !== null) {
            this.localKey = key;
          }
        }),
        catchError(this.handleError<SosKey>('getCurrentLocalKey'))
      );
  }

  generateLocalKey(): Observable<SosKey> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<SosKey>(
        syncosyncEndpoints.authenticated.syncConfigurationLocalKey,
        options
      )
      .pipe(
        tap((key) => {
          if (key !== undefined && key !== null) {
            this.localKey = key;
          }
        }),
        catchError(this.handleError<SosKey>('generateLocalKey'))
      );
  }

  getCurrentRemoteHostConfig(): Observable<RemoteHost> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<RemoteHost>(
        syncosyncEndpoints.authenticated.syncConfigurationRemoteHostConfig,
        options
      )
      .pipe(
        tap((_) => this.log('Got remote host config')),
        catchError(this.handleError<RemoteHost>('getCurrentRemoteHostConfig'))
      );
  }

  saveRemoteHostConfig(remoteHost: RemoteHost): Observable<RemoteHost> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<RemoteHost>(
        syncosyncEndpoints.authenticated.syncConfigurationRemoteHostConfig,
        remoteHost,
        options
      )
      .pipe(
        tap((_) => this.log('Saved remote host config')),
        catchError(this.handleError<RemoteHost>('saveRemoteHostConfig'))
      );
  }

  followKeyExchange(): Observable<SystemSetupData> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<SystemSetupData>(
        syncosyncEndpoints.authenticated.keyExchangeFollow,
        options
      )
      .pipe(
        tap((_) => this.log('Cancelled follow key exchange')),
        catchError(this.handleError<SystemSetupData>('followKeyExchange'))
      );
  }

  leadKeyExchange(): Observable<SystemSetupData> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<SystemSetupData>(
        syncosyncEndpoints.authenticated.keyExchangeLead,
        options
      )
      .pipe(
        tap((_) => this.log('Cancelled follow key exchange')),
        catchError(this.handleError<SystemSetupData>('leadKeyExchange'))
      );
  }

  cancelKeyExchange(): Observable<GenericUiResponse> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<GenericUiResponse>(
        syncosyncEndpoints.authenticated.keyExchangeCancel,
        options
      )
      .pipe(
        tap((_) => this.log('Cancelled key exchange')),
        catchError(this.handleError<GenericUiResponse>('cancelKeyExchange'))
      );
  }

  removeRemoteKey(): Observable<SosKey> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .delete<SosKey>(syncosyncEndpoints.authenticated.remoteKey, options)
      .pipe(
        tap((_) => this.log('Removed remote key')),
        catchError(this.handleError<SosKey>('removeRemoteKey'))
      );
  }

  acceptRemoteKey(): Observable<GenericUiResponse> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<GenericUiResponse>(
        syncosyncEndpoints.authenticated.remoteKey,
        options
      )
      .pipe(
        tap((_) => this.log('Accepted remote key')),
        catchError(this.handleError<GenericUiResponse>('acceptRemoteKey'))
      );
  }

  getRemoteKey(): Observable<SosKey> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<SosKey>(syncosyncEndpoints.authenticated.remoteKey, options)
      .pipe(
        tap((_) => this.log('Removed remote key')),
        catchError(this.handleError<SosKey>('getRemoteKey'))
      );
  }
}
