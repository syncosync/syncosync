import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, timer } from 'rxjs';
import {
  catchError,
  repeatWhen,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { Router } from '@angular/router';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import {
  AccountAdd,
  AccountInfo,
  AccountList,
  AuthService,
  SosServiceBaseAuthenticatedService,
} from '@syncosync_common';

@Injectable({
  providedIn: 'root',
})
export class AccountApiService extends SosServiceBaseAuthenticatedService {
  latestAccountlistSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    new AccountList()
  );
  latestAccountlistObservable$: Observable<AccountList> =
    this.latestAccountlistSubject.asObservable();
  protected serviceName = 'AccountApiService';
  private latestAccountlist: AccountList = new AccountList();
  private accountlistTimer: Observable<number> = null;

  constructor(
    private http: HttpClient,
    protected authService: AuthService,
    private router: Router
  ) {
    super(http, authService);
    this.accountlistTimer = timer(0, 5000);
    this.accountlistTimer
      .pipe(
        takeUntil(this.unsubscribe),
        repeatWhen(() => this.continue),
        switchMap(() => this.getAccountlist()),
        catchError(this.handleError<any>('getAccountlist', false))
      )
      .subscribe((res) => {
        console.log('Accountlist update');
        if (res !== undefined) {
          this.latestAccountlist.update(res);
          this.latestAccountlistSubject.next(this.latestAccountlist);
        }
      });
  }

  private static log(message: string) {
    console.log(`AccountApiService: ${message}`);
  }

  updateAccount(editedAccount: AccountAdd): Observable<any> {
    console.log(syncosyncEndpoints.authenticated.accountUpdateUrl);
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.http
      .put(
        syncosyncEndpoints.authenticated.accountUpdateUrl,
        editedAccount,
        options
      )
      .pipe(
        tap((_) => AccountApiService.log('Updated account')),
        catchError(this.handleError<any>('updateAccount', false))
      );
  }

  public getLatestAccounts(): AccountList {
    return this.latestAccountlist;
  }

  addAccount(accountAdd: AccountAdd): Observable<null> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.http
      .post(syncosyncEndpoints.authenticated.accountAddUrl, accountAdd, options)
      .pipe(
        tap((_) => AccountApiService.log('Added account')),
        catchError(this.handleError<any>('addAccount', false))
      );
  }

  deleteAccount(accountToDelete: AccountInfo): Observable<null> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.http
      .post(
        syncosyncEndpoints.authenticated.accountDeleteUrl,
        accountToDelete,
        options
      )
      .pipe(
        tap((_) => AccountApiService.log('Delete account')),
        catchError(this.handleError<any>('deleteAccount', false))
      );
  }

  eraseDataAccount(accountToEraseData: AccountInfo): Observable<null> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.http
      .post(
        syncosyncEndpoints.authenticated.accountEraseDataUrl,
        accountToEraseData,
        options
      )
      .pipe(
        tap((_) => AccountApiService.log('Erase account data')),
        catchError(this.handleError<any>('eraseDataAccount', false))
      );
  }

  private getAccountlist(): Observable<AccountInfo[]> {
    return this.http
      .get<AccountInfo[]>(syncosyncEndpoints.authenticated.accountListUrl)
      .pipe(catchError(this.handleError<AccountInfo[]>('getAccountlist')));
  }
}
