import {TestBed} from '@angular/core/testing';

import {ModeManagementService} from './mode-management.service';

describe('ModeManagementService', () => {
    let service: ModeManagementService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ModeManagementService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
