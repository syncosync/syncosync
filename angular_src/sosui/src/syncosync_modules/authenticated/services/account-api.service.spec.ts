import { TestBed } from '@angular/core/testing';

import { AccountApiService } from './account-api.service';

describe('AccountmodApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccountApiService = TestBed.inject(AccountApiService);
    expect(service).toBeTruthy();
  });
});
