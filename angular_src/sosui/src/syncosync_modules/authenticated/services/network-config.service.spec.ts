import { TestBed } from '@angular/core/testing';

import { NetworkConfigService } from './network-config.service';

describe('NetworkConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NetworkConfigService = TestBed.inject(NetworkConfigService);
    expect(service).toBeTruthy();
  });
});
