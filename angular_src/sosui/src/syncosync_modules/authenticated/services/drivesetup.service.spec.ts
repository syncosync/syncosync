import {TestBed} from '@angular/core/testing';

import {DrivesetupService} from './drivesetup.service';

describe('DrivesetupService', () => {
    let service: DrivesetupService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(DrivesetupService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
