import {TestBed} from '@angular/core/testing';

import {SosConfigService} from './sos-config.service';

describe('SosConfigService', () => {
    let service: SosConfigService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(SosConfigService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
