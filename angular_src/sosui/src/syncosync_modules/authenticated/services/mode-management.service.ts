import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';
import {
  AuthService,
  SosServiceBaseAuthenticatedService,
  SysStateTransport,
} from '@syncosync_common';
import { SysStateResult } from '../../common/model/sysState';

@Injectable({
  providedIn: 'root',
})
export class ModeManagementService extends SosServiceBaseAuthenticatedService {
  protected serviceName = 'ModeManagementService';

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  // Fetches the extended sysstate
  public getMode(): Observable<SysStateTransport> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<SysStateTransport>(syncosyncEndpoints.authenticated.modeUrl, options)
      .pipe(
        tap((_) => this.log('Got mode')),
        catchError(this.handleError<SysStateTransport>('getMode'))
      );
  }

  public setMode(modeToSet: SysStateTransport): Observable<SysStateResult> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<SysStateResult>(
        syncosyncEndpoints.authenticated.modeUrl,
        modeToSet,
        options
      )
      .pipe(
        tap((_) => this.log('Update mode')),
        catchError(this.handleError<SysStateResult>('setMode'))
      );
  }
}
