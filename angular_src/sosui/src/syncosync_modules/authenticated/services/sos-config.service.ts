import { Injectable } from '@angular/core';
import {
  AuthService,
  SosConfig,
  SosServiceBaseAuthenticatedService,
} from '@syncosync_common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SosConfigService extends SosServiceBaseAuthenticatedService {
  protected serviceName = 'SosConfigService';

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  public getConfig(): Observable<SosConfig> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<SosConfig>(syncosyncEndpoints.authenticated.sosConfigUrl, options)
      .pipe(
        tap((_) => this.log('Got sosConfig')),
        catchError(this.handleError<SosConfig>('getConfig'))
      );
  }
}
