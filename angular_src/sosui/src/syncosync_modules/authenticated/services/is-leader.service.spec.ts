import { TestBed } from '@angular/core/testing';

import { IsLeaderService } from './is-leader.service';

describe('IsLeaderService', () => {
  let service: IsLeaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IsLeaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
