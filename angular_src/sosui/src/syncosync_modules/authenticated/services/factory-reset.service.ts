import { Injectable } from '@angular/core';
import {
  AuthService,
  GenericUiResponse,
  SosServiceBaseAuthenticatedService,
} from '@syncosync_common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';
import { FactoryReset } from '../../common/model/factoryReset';

@Injectable({
  providedIn: 'root',
})
export class FactoryResetService extends SosServiceBaseAuthenticatedService {
  protected serviceName = 'FactoryResetService';

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  public triggerFactoryReset(
    reset_model: FactoryReset
  ): Observable<GenericUiResponse> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<GenericUiResponse>(
        syncosyncEndpoints.authenticated.factory_reset,
        reset_model,
        options
      )
      .pipe(
        tap((_) => this.log('triggered factory reset')),
        catchError(this.handleError<GenericUiResponse>('triggerFactoryReset'))
      );
  }
}
