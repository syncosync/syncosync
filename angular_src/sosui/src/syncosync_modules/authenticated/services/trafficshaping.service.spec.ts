import { TestBed } from '@angular/core/testing';

import { TrafficshapingService } from './trafficshaping.service';

describe('TrafficshapingService', () => {
  let service: TrafficshapingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrafficshapingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
