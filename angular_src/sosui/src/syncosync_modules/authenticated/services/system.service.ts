import { Injectable } from '@angular/core';
import {
  AuthService,
  GenericUiResponse,
  SosServiceBaseAuthenticatedService,
} from '@syncosync_common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SystemService extends SosServiceBaseAuthenticatedService {
  protected serviceName = 'SyncConfigurationService';

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  shutdownSystem(): Observable<GenericUiResponse> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<GenericUiResponse>(
        syncosyncEndpoints.authenticated.shutdownSystemUrl,
        options
      )
      .pipe(
        tap((_) => this.log('Shutting down the system')),
        catchError(this.handleError<GenericUiResponse>('shutdownSystem'))
      );
  }

  rebootSystem(): Observable<GenericUiResponse> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<GenericUiResponse>(
        syncosyncEndpoints.authenticated.rebootSystemUrl,
        options
      )
      .pipe(
        tap((_) => this.log('Shutting down the system')),
        catchError(this.handleError<GenericUiResponse>('shutdownSystem'))
      );
  }
}
