import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  AuthService,
  MailConfig,
  SosServiceBaseAuthenticatedService,
  SyncSpeedSelection,
} from '@syncosync_common';
import { Observable } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SyncSpeedSelectionService extends SosServiceBaseAuthenticatedService {
  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }
  public getTrafficshape(): Observable<SyncSpeedSelection> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<SyncSpeedSelection>(
        syncosyncEndpoints.authenticated.trafficshapingURL,
        options
      )
      .pipe(
        tap((_) => this.log('Got SyncSpeedSelection')),
        catchError(
          this.handleError<SyncSpeedSelection>('getSyncSpeedSelection')
        )
      );
  }

  public setTrafficshape(
    newTrafficshape: SyncSpeedSelection
  ): Observable<SyncSpeedSelection> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<SyncSpeedSelection>(
        syncosyncEndpoints.authenticated.sync - speed - selectionURL,
        newTrafficshape,
        options
      )
      .pipe(
        tap((_) => this.log('upate SyncSpeedSelection')),
        catchError(
          this.handleError<SyncSpeedSelection>('setSyncSpeedSelection')
        )
      );
  }

  protected serviceName: string;
}
