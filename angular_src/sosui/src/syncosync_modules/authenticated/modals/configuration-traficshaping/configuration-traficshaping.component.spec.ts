import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ConfigurationTraficshapingComponent} from './configuration-traficshaping.component';

describe('ConfigurationTraficshapingComponent', () => {
    let component: ConfigurationTraficshapingComponent;
    let fixture: ComponentFixture<ConfigurationTraficshapingComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ConfigurationTraficshapingComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfigurationTraficshapingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
