import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AccountApiService} from "../../services/account-api.service";
import {AccountInfo} from "@syncosync_common";


@Component({
    selector: 'app-account-erase-data',
    templateUrl: './account-erase-data.component.html',
    styleUrls: ['./account-erase-data.component.css']
})
export class AccountEraseDataComponent implements OnInit {
    @Input() account: AccountInfo;

    constructor(private accountmodService: AccountApiService,
                public activeModal: NgbActiveModal) {
    }

    ngOnInit() {
    }

    eraseDataAccount() {
        console.log('Erasing account data ' + this.account.name);
        this.accountmodService.eraseDataAccount(this.account)
            .subscribe(result => {
                if (result) {
                    this.closeModal();
                } else {
                    // TODO: show error
                }
            });
        // TODO: only upon success - else show error
    }

    closeModal() {
        this.activeModal.close('Erase Data');
    }
}
