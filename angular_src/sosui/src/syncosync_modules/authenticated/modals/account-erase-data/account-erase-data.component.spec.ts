import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AccountEraseDataComponent} from './account-erase-data.component';

describe('EraseDataAccountComponent', () => {
    let component: AccountEraseDataComponent;
    let fixture: ComponentFixture<AccountEraseDataComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AccountEraseDataComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountEraseDataComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
