import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AccountApiService} from "../../services/account-api.service";
import {AccountInfo} from "@syncosync_common";


@Component({
    selector: 'app-account-delete',
    templateUrl: './account-delete.component.html',
    styleUrls: ['./account-delete.component.css']
})
export class AccountDeleteComponent implements OnInit {
    @Input() account: AccountInfo;

    constructor(private accountmodService: AccountApiService,
                public activeModal: NgbActiveModal) {
    }

    ngOnInit() {
    }

    deleteAccount() {
        console.log('Deleting account ' + this.account.name);
        this.accountmodService.deleteAccount(this.account)
            .subscribe(result => {
                if (result) {
                    this.closeModal();
                } else {
                    // TODO: show error
                }
            });
        // TODO: only upon success - else show error
    }

    closeModal() {
        this.activeModal.close('Delete');
    }
}
