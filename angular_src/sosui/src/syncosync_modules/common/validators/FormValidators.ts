import { AbstractControl, ValidatorFn, Validators } from '@angular/forms';

export function optionalValidator(
  validators?: (ValidatorFn | null | undefined)[]
): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    return control.value ? Validators.compose(validators)(control) : null;
  };
}

export function validateAccountLongName(
  control: AbstractControl
): { [key: string]: any } | null {
  const valid = /^[A-Za-z\s0-9]{6,}$/.test(control.value);
  return valid
    ? null
    : { invalidLongName: { valid: false, value: control.value } };
}

export function validateAccountname(
  control: AbstractControl
): { [key: string]: any } | null {
  const valid = /^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$/.test(
    control.value
  );
  return valid
    ? null
    : { invalidAccountname: { valid: false, value: control.value } };
}

export function validateAccountPassword(
  control: AbstractControl
): { [key: string]: any } | null {
  const valid =
    control.value === null || // we allow null since account passwords may be empty
    control.value.length === 0 ||
    /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(control.value);
  return valid
    ? null
    : { invalidPassword: { valid: false, value: control.value } };
}

export function validateHostnameIp(
  control: AbstractControl
): { [key: string]: any } | null {
  // tslint:disable-next-line:max-line-length
  const valid =
    /(^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9-]*[A-Za-z0-9])$)/.test(
      control.value
    );
  return valid
    ? null
    : { invalidHostnameIp: { valid: false, value: control.value } };
}

export function validateIPv4IPv6(
  control: AbstractControl
): { [key: string]: any } | null {
  // tslint:disable-next-line:max-line-length
  const valid =
    /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))/.test(
      control.value
    );
  return valid
    ? null
    : { invalidIPv4IPv6: { valid: false, value: control.value } };
}

export function validateIPv4(
  control: AbstractControl
): { [key: string]: any } | null {
  // tslint:disable-next-line:max-line-length
  const valid =
    /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$))/.test(
      control.value
    );
  return valid ? null : { invalidIPv4: { valid: false, value: control.value } };
}

export function validateIPv6(
  control: AbstractControl
): { [key: string]: any } | null {
  // tslint:disable-next-line:max-line-length
  const valid =
    /((^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))/.test(
      control.value
    );
  return valid ? null : { invalidIPv6: { valid: false, value: control.value } };
}

export function validatePort(
  control: AbstractControl
): { [key: string]: any } | null {
  const valid =
    /^([1-9]{1}[0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$/.test(
      control.value
    );
  return valid ? null : { invalidPort: { valid: false, value: control.value } };
}

export function validateSystemname(
  control: AbstractControl
): { [key: string]: any } | null {
  const valid = /^[a-zA-Z0-9]{8,20}$/.test(control.value);
  return valid
    ? null
    : { invalidSystemname: { valid: false, value: control.value } };
}

export function validateAdminPassword(
  control: AbstractControl
): { [key: string]: any } | null {
  const valid = /^[a-zA-Z0-9]{8,20}$/.test(control.value);
  return valid
    ? null
    : { invalidPassword: { valid: false, value: control.value } };
}

export function validateMailAddress(
  control: AbstractControl
): { [key: string]: any } | null {
  const valid =
    // eslint-disable-next-line no-control-regex
    /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])|^$/.test(
      control.value
    );
  return valid
    ? null
    : { invalidPassword: { valid: false, value: control.value } };
}

export function validateInfoPeriod(
  control: AbstractControl
): { [key: string]: any } | null {
  const valid = /^(3[01]|[12][0-9]|[1-9])$/.test(control.value);
  return valid
    ? null
    : { invalidInfoPeriod: { valid: false, value: control.value } };
}
