import { Directive, OnInit } from '@angular/core';
import {
  UntypedFormControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { validateAccountLongName } from '../validators/FormValidators';

@Directive({
  selector: '[validateLongName]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ValidateLongNameDirective,
      multi: true,
    },
  ],
})
export class ValidateLongNameDirective implements Validator, OnInit {
  ngOnInit() {
    //
  }

  validate(formControl: UntypedFormControl): ValidationErrors | null {
    if (
      formControl.value === undefined ||
      formControl.value === null ||
      formControl.value.trim().length === 0
    ) {
      return null;
    }
    return validateAccountLongName(formControl);
  }
}
