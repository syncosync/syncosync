import {Directive} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors} from "@angular/forms";
import {validateInfoPeriod} from "../validators/FormValidators";

@Directive({
    selector: '[validateInfoperiod]',
    providers: [
        {provide: NG_VALIDATORS, useExisting: ValidateInfoperiodDirective, multi: true}
    ]
})
export class ValidateInfoperiodDirective {


    constructor() {
    }

    validate(formControl: AbstractControl): ValidationErrors | null {
        return validateInfoPeriod(formControl);

    }
}