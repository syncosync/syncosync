import {Directive, OnInit} from '@angular/core';
import {UntypedFormControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {validateAccountname} from "../validators/FormValidators";

@Directive({
    selector: '[validateAccountName]',
    providers: [
        {provide: NG_VALIDATORS, useExisting: ValidateAccountNameDirective, multi: true}
    ]
})
export class ValidateAccountNameDirective implements Validator, OnInit {

    ngOnInit() {
    }

    validate(formControl: UntypedFormControl): ValidationErrors | null {
        return validateAccountname(formControl);
    }
}