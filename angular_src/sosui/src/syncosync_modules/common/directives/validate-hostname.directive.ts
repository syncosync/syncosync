import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { validateHostnameIp } from '../validators/FormValidators';

@Directive({
  selector: '[validateHostname]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ValidateHostnameDirective,
      multi: true,
    },
  ],
})
export class ValidateHostnameDirective implements Validator {
  constructor() {
    // nop
  }

  validate(formControl: AbstractControl): ValidationErrors | null {
    return validateHostnameIp(formControl);
  }
}
