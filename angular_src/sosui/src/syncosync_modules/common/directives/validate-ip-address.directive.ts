import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { validateIPv4IPv6 } from '@syncosync_common';

@Directive({
  selector: '[validateIpAddress]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ValidateIpAddressDirective,
      multi: true,
    },
  ],
})
export class ValidateIpAddressDirective implements Validator {
  constructor() {
    // nop
  }

  validate(formControl: AbstractControl): ValidationErrors | null {
    if (formControl.value == '') {
      return null;
    } else {
      return validateIPv4IPv6(formControl);
    }
  }
}
