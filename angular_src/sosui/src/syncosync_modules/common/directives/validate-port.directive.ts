import {Directive} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {validatePort} from "../validators/FormValidators";

@Directive({
    selector: '[validatePort]',
    providers: [
        {provide: NG_VALIDATORS, useExisting: ValidatePortDirective, multi: true}
    ]
})
export class ValidatePortDirective implements Validator {

    constructor() {
    }

    validate(formControl: AbstractControl): ValidationErrors | null {
        return validatePort(formControl);

    }
}
