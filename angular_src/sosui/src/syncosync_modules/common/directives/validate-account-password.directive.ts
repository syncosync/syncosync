import { Directive, OnInit } from '@angular/core';
import {
  UntypedFormControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { validateAccountPassword } from '../validators/FormValidators';

@Directive({
  selector: '[validateAccountPassword]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ValidateAccountPasswordDirective,
      multi: true,
    },
  ],
})
export class ValidateAccountPasswordDirective implements Validator {
  validate(formControl: UntypedFormControl): ValidationErrors | null {
    return validateAccountPassword(formControl);
  }
}
