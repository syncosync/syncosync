import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appSetupPartial]',
})
export class SetupPartialDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
