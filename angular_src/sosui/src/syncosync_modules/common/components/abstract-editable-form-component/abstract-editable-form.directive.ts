import { Directive } from '@angular/core';
import { Observable } from 'rxjs';
import { SyncosyncModalService } from '../../services/syncosync-modal.service';
import { InfoLevel } from '../../model/infoLevel';
import { LocalizedMessage } from '../../model/localizedMessage';
import { AbstractEmptyFormDirective } from '../abstract-empty-form-directive/abstract-empty-form.directive';
import { ProcessingSpinnerStateService } from '../../services/processing-spinner-state.service';

@Directive()
export abstract class AbstractEditableFormDirective<
  T = any
> extends AbstractEmptyFormDirective<T> {
  /**
   * Essentially, a FormComponent may yield an empty form (e.g. AdminPassword). Since we want the
   */
  // In case an implementation does not require the spinner to be set (e.g. modals...)
  disableSpinnerUsage = false;

  protected constructor(
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super();
  }

  protected initialize(): void {
    this.requestData();
  }

  protected abstract modelRequest(): Observable<T>;

  protected requestData(): void {
    const getterObservable: Observable<T> = this.modelRequest();
    if (getterObservable == null) {
      this.model = this.getInitModel();
      this.modelChanged.emit(this.model);
      return;
    }
    if (!this.disableSpinnerUsage) {
      this.processingSpinnerStateService.addSpinner();
    }
    getterObservable.subscribe((result) => {
      if (!this.disableSpinnerUsage) {
        this.processingSpinnerStateService.removeSpinner();
      }
      if (result !== undefined && result !== null) {
        this.setModel(result);
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.FAILED_REQUESTING_DATA
        );
      }
    });
  }
}
