import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupAssistantDirective } from './setup-assistant-partial.injectable';

describe('SetupAssistantPartial', () => {
  let component: SetupAssistantDirective<any>;
  // @ts-ignore
  let fixture: ComponentFixture<SetupAssistantDirective>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SetupAssistantDirective],
    }).compileComponents();
  }));

  beforeEach(() => {
    // @ts-ignore
    fixture = TestBed.createComponent(SetupAssistantDirective);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
