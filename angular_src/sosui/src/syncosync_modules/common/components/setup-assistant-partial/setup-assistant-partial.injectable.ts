import { Directive, EventEmitter, Output } from '@angular/core';

/***
 * Abstract to force subclasses to implement it since
 * https://github.com/angular/angular/issues/25249 and similar issues still seem to apply...
 ***/
@Directive()
export abstract class SetupAssistantDirective<T> {
  // Boolean indicating whether this step can be overwritten/run again
  protected canBeReturnedTo = false;
  protected canBeSkipped = false;
  protected shouldBeSkipped = false;

  /* EventEmitter to be triggered when the step/configuration has finished */
  @Output() configurationFinished: EventEmitter<T> = new EventEmitter<T>();
  @Output() canBeSkippedChanged: EventEmitter<boolean> =
    new EventEmitter<boolean>();
  @Output() shouldBeSkippedChanged: EventEmitter<boolean> =
    new EventEmitter<boolean>();

  protected constructor() {
    //
  }

  public isCanBeSkipped(): boolean {
    return this.canBeSkipped;
  }

  public setCanBeSkipped(value: boolean): void {
    this.canBeSkipped = value;
    this.canBeSkippedChanged.emit(value);
  }

  public isCanBeReturnedTo(): boolean {
    return this.canBeReturnedTo;
  }

  /**
   * Can be overwritten by sub classes to have checks run whether a step can be skipped for whatever reason
   */
  public isShouldBeSkipped(): boolean {
    return this.shouldBeSkipped;
  }

  public setShouldBeSkipped(value: boolean): void {
    this.shouldBeSkipped = value;
    this.shouldBeSkippedChanged.emit(value);
  }
}
