import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbstractFormDirective } from './abstract-form.directive';

describe('AbstractFormComponentComponent', () => {
  let component: AbstractFormDirective;
  let fixture: ComponentFixture<AbstractFormDirective>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AbstractFormDirective],
    }).compileComponents();
  }));

  beforeEach(() => {
    /*fixture = TestBed.createComponent(AbstractFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
