import { Component } from '@angular/core';
import {
  AbstractSubscriberComponent,
  SyncosyncModalService,
} from '@syncosync_common';
import { StatusService } from '@syncosync_public';
import { TrafficshapingSwitchingFormComponent } from '../../../authenticated/forms/trafficshaping-switching-form/trafficshaping-switching-form.component';

@Component({
  selector: 'app-toggle-shaping-button',
  templateUrl: './toggle-shaping-button.component.html',
  styleUrls: ['./toggle-shaping-button.component.css'],
})
export class ToggleShapingButtonComponent extends AbstractSubscriberComponent {
  constructor(
    private modalService: SyncosyncModalService,
    private statusService: StatusService
  ) {
    super();
  }

  changeTrafficshapeMode(): void {
    // TODO: Display TrafficshapingSwitchingFormComponent
    this.modalService
      .showRequestDataModal(
        'Trafficshaping mode',
        TrafficshapingSwitchingFormComponent
      )
      .subscribe((result) => {
        if (result === undefined || result === null) {
          // User aborted trafficshaping mode switch
          // nop
        } else {
          // TODO ?
        }
      });
  }

  isActive(): boolean {
    const status = this.statusService.getStatus();
    return status !== null && status.day;
  }
}
