import {TestBed} from '@angular/core/testing';

import {SosServiceBaseAuthenticatedService} from './sos-service-base-authenticated.service';

describe('SosServiceBaseAuthenticatedServiceService', () => {
    let service: SosServiceBaseAuthenticatedService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(SosServiceBaseAuthenticatedService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
