import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService, LoginState } from './auth.service';
import { SosServiceBaseService } from './sos-service-base.service';

@Injectable({
  providedIn: 'root',
})
export abstract class SosServiceBaseAuthenticatedService extends SosServiceBaseService {
  protected constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
    authService.registerNotification(this);
  }

  notifyLoginState(loginState: LoginState): void {
    switch (loginState) {
      case LoginState.LOGIN:
        this.continue.next();
        break;
      case LoginState.LOGOUT:
      default:
        this.unsubscribe.next();
    }
  }
}
