import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthService, LoginState} from "./auth.service";
import {SosServiceBaseService} from "./sos-service-base.service";

@Injectable({
    providedIn: 'root'
})
export abstract class SosServiceBaseUnauthenticatedService extends SosServiceBaseService {

    protected constructor(
        protected httpClient: HttpClient,
        protected authService: AuthService
    ) {
        super(httpClient, authService);
        authService.registerNotification(this);
    }

    notifyLoginState(loginState: LoginState) {
        switch (loginState) {
            case LoginState.LOGIN:
                this.unsubscribe.next()
                break;
            case LoginState.LOGOUT:
            default:
                this.continue.next()
        }
    }
}
