import {TestBed} from '@angular/core/testing';

import {SyncosyncModalService} from './syncosync-modal.service';

describe('SyncosyncModalService', () => {
    let service: SyncosyncModalService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(SyncosyncModalService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
