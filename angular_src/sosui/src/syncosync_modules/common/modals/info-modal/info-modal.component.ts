import { Component } from '@angular/core';
import { SyncosyncModalParent } from '../syncosync-modal-parent/syncosync-modal-parent.injectable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { InfoLevel } from '../../model/infoLevel';
import { LocalizedMessage } from '../../model/localizedMessage';

@Component({
  selector: 'app-info-modal',
  templateUrl: './info-modal.component.html',
  styleUrls: ['./info-modal.component.css'],
})
export class InfoModalComponent extends SyncosyncModalParent<
  LocalizedMessage,
  InfoLevel,
  string
> {
  public infoLevelEnum = InfoLevel;

  constructor(public activeModal: NgbActiveModal) {
    super(activeModal);
  }
}
