import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Type,
  ViewChild,
} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SyncosyncModalParent } from '../syncosync-modal-parent/syncosync-modal-parent.injectable';
import { AbstractFormDirective } from '../../components/abstract-form-directive/abstract-form.directive';
import { LocalizedMessage } from '../../model/localizedMessage';
import { SetupPartialDirective } from '../../directives/setup-partial.directive';

@Component({
  selector: 'app-request-data-modal',
  templateUrl: './request-data-modal.component.html',
  styleUrls: ['./request-data-modal.component.css'],
})
export class RequestDataModalComponent<T>
  extends SyncosyncModalParent<
    Type<AbstractFormDirective<T>>,
    LocalizedMessage | string,
    LocalizedMessage | string
  >
  implements OnInit
{
  @ViewChild(SetupPartialDirective, { static: true })
  setupPartialDirective: SetupPartialDirective;

  @Output() onResult: EventEmitter<T> = new EventEmitter();
  componentRef: ComponentRef<AbstractFormDirective<T>> = null;

  @Input()
  model: T;

  constructor(
    public activeModal: NgbActiveModal,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
    super(activeModal);
  }

  ngOnInit(): void {
    const viewContainerRef = this.setupPartialDirective.viewContainerRef;
    viewContainerRef.clear();
    const componentFactory =
      this.componentFactoryResolver.resolveComponentFactory<
        AbstractFormDirective<T>
      >(this.content);
    this.componentRef =
      viewContainerRef.createComponent<AbstractFormDirective<T>>(
        componentFactory
      );
    if (this.model) {
      this.componentRef.instance.setModelDirect(this.model);
    }
    this.componentRef.instance.formFinished.subscribe((result: T) => {
      this.onResult.emit(result);
      this.closeModal();
    });
  }
}
