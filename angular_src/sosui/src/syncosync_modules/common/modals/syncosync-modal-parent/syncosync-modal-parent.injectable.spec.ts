import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SyncosyncModalParent} from './syncosync-modal-parent.injectable';

describe('SyncosyncModalParent', () => {
    let component: SyncosyncModalParent<any, any>;
    let fixture: ComponentFixture<SyncosyncModalParent<any, any>>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SyncosyncModalParent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        // @ts-ignore
        fixture = TestBed.createComponent(SyncosyncModalParent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
