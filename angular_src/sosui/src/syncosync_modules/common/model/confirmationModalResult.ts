export enum ConfirmationModalResult {
    CANCEL,
    CONFIRM
}