// suppressing lint since we are using these attribute names in the json
/* tslint:disable:variable-name */

import { MailInfoLevels } from './sosEnums';

export abstract class Sosaccount {
  name: string;
  rlongname: string;
  mail_address: string;
  backup_period: number;
  remind_duration = 22;
  info_period = 7;
  mail_info_level: MailInfoLevels = MailInfoLevels.WARNING;
  max_space_allowed = 100;
  key_fingerprint: string = null;
  last_access: number;
  space_used: number;
  is_password_set: boolean;
}

export class AccountInfo extends Sosaccount {
  last_mail: number;
  overdue: number; // seconds
}

export class AccountAdd extends Sosaccount {
  ssh_pub_key: string = null;
  password: string = null;
}

export class AccountEdit extends AccountAdd {
  delete_password = false;
}

export class AccountList {
  private accounts: AccountInfo[] = [];

  private static accountCompare(a: AccountInfo, b: AccountInfo) {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  }

  public update(latestAccounts: AccountInfo[]): void {
    this.accounts.sort(AccountList.accountCompare);
    latestAccounts.sort(AccountList.accountCompare);

    const latestAccountDict = new Map();
    for (const account of latestAccounts) {
      latestAccountDict.set(account.name, account);
    }

    const killList: AccountInfo[] = [];

    for (const account of this.accounts) {
      if (latestAccountDict.has(account.name)) {
        // update account...
        Object.assign(account, latestAccountDict.get(account.name));
        latestAccountDict.delete(account.name);
      } else {
        // delete account
        killList.push(account);
      }
    }

    // delete accounts that are missing in the dict and were added to the killList earlier
    for (const account of killList) {
      const index = this.accounts.indexOf(account);
      if (index !== -1) {
        this.accounts.splice(index, 1);
      }
    }

    // add accounts that are still in the latestAccountDict
    for (const account of latestAccountDict.values()) {
      this.accounts.push(account);
    }

    this.accounts.sort(AccountList.accountCompare);
  }

  public getAccounts(): AccountInfo[] {
    return this.accounts;
  }
}
