export enum SupportedLanguage {
  UNSET = 0,
  ENGLISH = 1,
  GERMAN = 2,
}

export class SupportedLanguageUtil {
  private static iso_codes = {
    0: '',
    1: 'en-US', // language switch uses "us", angular "en-US", ISO would be "en"....
    2: 'de',
  };

  public static toIso(language: SupportedLanguage): string {
    for (const key in SupportedLanguageUtil.iso_codes) {
      if (parseInt(key) === language.valueOf()) {
        return SupportedLanguageUtil.iso_codes[key];
      }
    }
    return '';
  }

  public static fromIso(languageAsStr: string): SupportedLanguage {
    for (const key in SupportedLanguageUtil.iso_codes) {
      if (SupportedLanguageUtil.iso_codes[key] === languageAsStr) {
        return (<any>SupportedLanguage)[SupportedLanguage[key]];
      }
    }
    return SupportedLanguage.UNSET;
  }
}
