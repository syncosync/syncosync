import { AdminPassword } from '@syncosync_common';
import { EraseDiskLevel } from './sosEnums';

export class FactoryReset {
  password_check: AdminPassword = new AdminPassword();
  reset_networking = false;
  erase_disk = EraseDiskLevel.NO_ERASE;
}
