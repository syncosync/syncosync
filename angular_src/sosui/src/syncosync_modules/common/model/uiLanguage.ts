import {SupportedLanguage} from "./supportedLanguage";

export class UiLanguage {
    language: SupportedLanguage = SupportedLanguage.UNSET;
}