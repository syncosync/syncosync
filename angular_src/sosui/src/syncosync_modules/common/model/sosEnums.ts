export enum NicMode {
  DHCP = 0,
  STATIC = 1,
  AUTO = 2, // v6 only
  MANUAL = 3, // off
}

export enum RemoteUpStatus {
  // not available
  NOT_AVAILABLE = 0,
  // host is not reachable (wrong hostname or port?)
  NO_REPLY = 1,
  // not successful
  FAULTY = 2,
  // host is up and key is fine
  UP = 3,
  NO_SSH = 4,
  BAD_KEY = 5,
  HOST_CLOSED = 6,
  // hostname is not set?
  NOT_SET,
}

export enum SyncStatus {
  // not available (hostname is not set?)
  NOT_AVAILABLE = 0,
  // there is an active session / synchronization ongoing
  ACTIVE = 1,
  // Connection is idling, no active synchronization
  IDLE = 2,
}

export enum SyncstatStatus {
  // not available
  NOT_AVAILABLE = 0,
  // available
  AVAILABLE = 1,
}

export enum MailSslType {
  NO_SECURITY = 0,
  STARTTLS = 1,
  SSL_TLS = 2,
}

export enum MailAuthenticationType {
  PASSWORD_NORMAL,
  PASSWORD_CRYPTED,
}

export enum SystemStatus {
  NOTHING = 0,
  DETECTION = 1,
  DISK_SETUP_NEED = 2,
  SOS_VOLUMES_NOT_MOUNTED = 4,
  SOS_KEY_NOT_GENERATED = 8,
  REMOTE_HOST_NOT_CONFIGURED = 16,
  SOS_CONF_BACKUP_NOT_DONE = 32,
  SOS_CONF_BACKUP_NOT_MATCH = 64,
  INTRANET_SSHD_DOWN = 128,
  INTRANET_SSHD_CLOSE = 256,
  EXTRANET_SSHD_DOWN = 512,
  EXTRANET_SSHD_CLOSE = 1024,
  PARTITIONING_NEED = 2048,
  NO_CONF_BACKUP_ON_VG = 4096,
  VG_UUID_NOT_MATCH = 8192,
  REMOTE_KEY_NOT_SET = 16384,
  DISK_ERROR = 32768,
}

export enum SystemMode {
  STARTUP = 0,
  SETUP_DISKS = 1,
  DEFAULT = 3,
  DEFAULT_NO_SYNC = 4,
  REMOTE_RECOVERY = 5,
  RR_FINAL = 6,
  DISASTER_SWAP = 7,
  SHUTDOWN = 8,
  INTERMEDIATE = 9,
  SETUP_VOLUMES = 10,
  RESIZE_VOLUMES = 11,
  DISK_ERROR = 12,
  UNKNOWN = 255,
}

export enum VolMount {
  NONE = 0,
  INCOMPLETE = 1,
  NORMAL = 2,
  SWAP = 3,
}

export enum SysType {
  UNKNOWN = 0,
  GENERIC = 1,
  RPI4 = 2,
  BPI = 3,
  QEMU = 4,
}

export enum MailInfoLevels {
  NONE = 0,
  ERROR = 1,
  WARNING = 2,
  INFO = 3,
}

export enum SshdAccess {
  OFF = 0,
  OPEN = 1,
  CLOSE = 2,
}

export enum SwitchAction {
  DEFAULT = 0,
  DISASTER_SWAP_PERSISTENT = 1,
}

export enum EraseDiskLevel {
  NO_ERASE = 0,
  QUICK_ERASE = 1,
}

export enum PartitioningState {
  OK = 0,
  RESIZE_NEEDED = 1,
  RESIZE_NOT_POSSIBLE = 2,
}
