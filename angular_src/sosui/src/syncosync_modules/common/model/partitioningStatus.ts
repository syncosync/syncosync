import { PartitioningState } from './sosEnums';

export class PartitioningStatus {
  public overall_status: PartitioningState =
    PartitioningState.RESIZE_NOT_POSSIBLE;
}
