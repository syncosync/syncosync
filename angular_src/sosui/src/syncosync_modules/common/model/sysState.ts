import { SystemMode, SystemStatus, SwitchAction } from './sosEnums';

export class SysStateTransport {
  status: SysStateModel = new SysStateModel();
  state_desired: SystemMode = null;
  config_backup_encoded: string = null;
  config_backup_name: string = null;
  config_backup_password: string = null;
  switch_action: SwitchAction = SwitchAction.DEFAULT;
}

export class SysStateModel {
  sysstate: SystemStatus = SystemStatus.NOTHING;
  actmode: SystemMode = SystemMode.STARTUP;
  storedmode: SystemMode = SystemMode.STARTUP;
}

export class SysStateResult {
  mode: SystemMode = SystemMode.UNKNOWN;
  report: string[] = [];
  date = 0;
}
