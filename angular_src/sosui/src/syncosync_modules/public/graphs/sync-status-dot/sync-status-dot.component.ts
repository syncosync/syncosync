import { Component, Input, OnInit } from '@angular/core';
import { StatusService } from '../../services/status.service';
import { takeUntil } from 'rxjs/operators';
import { LocalizationService } from '../../../common/services/localization.service';

import {
  AbstractSubscriberComponent,
  RemoteUpStatus,
  Status,
  SyncStatus,
  SystemMode,
} from '@syncosync_common';

enum SyncDisplayState {
  ACTIVE,
  OK,
  WARNING,
  DISASTER_SWAP,
  SETUP_DISKS,
  SETUP_VOLUMES,
  DISK_ERROR,
  REMOTE_RECOVERY,
  RR_FINAL,
  DEFAULT_NO_SYNC,
}

@Component({
  selector: 'app-sync-status-dot',
  templateUrl: './sync-status-dot.component.html',
  styleUrls: ['./sync-status-dot.component.css'],
})
export class SyncStatusDotComponent
  extends AbstractSubscriberComponent
  implements OnInit
{
  @Input() size = 100;
  syncDisplayState = SyncDisplayState;
  statusIconToShow: SyncDisplayState = SyncDisplayState.OK;
  remoteUpStatus = RemoteUpStatus;
  systemMode = SystemMode;
  syncStatus = SyncStatus;

  constructor(
    public statusService: StatusService,
    private localizationService: LocalizationService
  ) {
    super();
  }

  ngOnInit(): void {
    console.log('OnInit');
    this.statusService.latestStatusObservable$
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((status) => {
        this.handleStatus(status);
      });
  }

  /*
    remoteUp:
     ENA: Not Available -> show yellow
     ETRUE: Host is up and responding -> show green
     EFALSE: host is not up or not responding or no key or whatever -> show red
     */
  getColor(): string {
    const latestStatus = this.statusService.getStatus();
    if (latestStatus === undefined) {
      return 'warning';
    }
    switch (latestStatus.remote_up) {
      case RemoteUpStatus.NOT_AVAILABLE:
        return 'warning';
      case RemoteUpStatus.UP:
        return 'ok';
      default:
        return 'error';
    }
  }

  getRRColor(): string {
    const latestStatus = this.statusService.getStatus();
    if (latestStatus === undefined) {
      return 'warning';
    }
    switch (latestStatus.remote_up) {
      case RemoteUpStatus.NOT_AVAILABLE:
        return 'warning';
      case RemoteUpStatus.UP:
        return 'warning';
      default:
        return 'error';
    }
  }

  private handleStatus(status: Status): void {
    if (status === undefined || status === null) {
      return;
    }
    if (status.actmode === SystemMode.DISASTER_SWAP) {
      this.statusIconToShow = SyncDisplayState.DISASTER_SWAP;
    } else if (status.actmode === SystemMode.SETUP_DISKS) {
      this.statusIconToShow = SyncDisplayState.SETUP_DISKS;
    } else if (status.actmode === SystemMode.SETUP_VOLUMES) {
      this.statusIconToShow = SyncDisplayState.SETUP_VOLUMES;
    } else if (status.actmode === SystemMode.DISK_ERROR) {
      this.statusIconToShow = SyncDisplayState.DISK_ERROR;
    } else if (status.actmode === SystemMode.REMOTE_RECOVERY) {
      this.statusIconToShow = SyncDisplayState.REMOTE_RECOVERY;
    } else if (status.actmode === SystemMode.DEFAULT_NO_SYNC) {
      this.statusIconToShow = SyncDisplayState.DEFAULT_NO_SYNC;
    } else if (status.actmode === SystemMode.RR_FINAL) {
      this.statusIconToShow = SyncDisplayState.RR_FINAL;
    } else if (
      status.syncdown === SyncStatus.ACTIVE ||
      status.syncup === SyncStatus.ACTIVE
    ) {
      this.statusIconToShow = SyncDisplayState.ACTIVE;
    } else if (status.remote_up !== RemoteUpStatus.UP) {
      // display an exclamation mark indicating an issue...
      this.statusIconToShow = SyncDisplayState.WARNING;
    } else {
      this.statusIconToShow = SyncDisplayState.OK;
    }
  }
}
