import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SyncStatusDotComponent} from './sync-status-dot.component';

describe('SyncStatusDotComponent', () => {
    let component: SyncStatusDotComponent;
    let fixture: ComponentFixture<SyncStatusDotComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SyncStatusDotComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SyncStatusDotComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
