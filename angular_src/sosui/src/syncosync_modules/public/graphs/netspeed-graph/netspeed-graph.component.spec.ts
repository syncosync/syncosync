import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NetspeedGraphComponent} from './netspeed-graph.component';

describe('NetspeedGraphComponent', () => {
    let component: NetspeedGraphComponent;
    let fixture: ComponentFixture<NetspeedGraphComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NetspeedGraphComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NetspeedGraphComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
