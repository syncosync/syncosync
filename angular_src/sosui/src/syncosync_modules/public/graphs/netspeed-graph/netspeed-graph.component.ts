import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { BaseChartDirective, Color, Label } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { takeUntil } from 'rxjs/operators';
import { NetworkStatusService } from '../../services/network-status.service';
import { StatusService } from '../../services/status.service';
import {
  AbstractSubscriberComponent,
  BandwidthModel,
  Status,
} from '@syncosync_common';

@Component({
  selector: 'app-downspeed-graph',
  templateUrl: './netspeed-graph.component.html',
  styleUrls: ['./netspeed-graph.component.css'],
})
export class NetspeedGraphComponent
  extends AbstractSubscriberComponent
  implements OnInit, OnDestroy
{
  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Download', pointRadius: 0, pointHitRadius: 0 },
    { data: [], label: 'Upload', pointRadius: 0, pointHitRadius: 0 },
  ];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: ChartOptions & { annotation: any } = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{ display: false }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          ticks: {
            beginAtZero: true,
            min: 0,
            suggestedMax: 10,
          },
        },
      ],
    },
    tooltips: {
      enabled: false,
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno',
          },
        },
      ],
    },
  };
  public lineChartColors: Color[] = [
    {
      // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)',
    },
    {
      // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(
    private networkStatusService: NetworkStatusService,
    private authenticatedStatusService: StatusService
  ) {
    super();
  }

  ngOnInit() {
    // TODO: Handle error in setup...
    this.networkStatusService.fetchBandwidthHistory().subscribe((history) => {
      if (history !== undefined) {
        // console.log("Setting up netspeed graph if needed");
        this.setupNetworkGraph(history);
      }
    });
    this.authenticatedStatusService.latestStatusObservable$
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((authenticatedStatus) => {
        if (authenticatedStatus !== undefined) {
          // console.log("Updating netspeed graph", this.lineChartData[0].data.length);
          this.handleNewStatus(authenticatedStatus);
        }
      });
  }

  private setupNetworkGraph(bandwidthHistory: BandwidthModel[]) {
    // transform down and up...
    if (this.lineChartData[0].data.length === 0) {
      this.lineChartData[0].data = bandwidthHistory.map(
        (bandwidthModelEntry) => bandwidthModelEntry.bw_down
      );
    }
    if (this.lineChartData[1].data.length === 0) {
      this.lineChartData[1].data = bandwidthHistory.map(
        (bandwidthModelEntry) => bandwidthModelEntry.bw_up
      );
      for (let i = 0; i < this.lineChartData[1].data.length; i++) {
        this.lineChartLabels.push('');
      }
    }
  }

  private handleNewStatus(status: Status) {
    // basically a workaround for now to ensure the initial bandwidth history has been received
    if (this.lineChartData[1].data.length > 0) {
      this.lineChartData[1].data.shift();
      this.lineChartData[1].data.push(status.bw_up);
    }
    if (this.lineChartData[0].data.length > 0) {
      this.lineChartData[0].data.shift();
      this.lineChartData[0].data.push(status.bw_down);
    }
    this.updateChart();
  }

  private updateChart() {
    this.chart.update(0);
  }
}
