import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LanguageSwitchComponent } from '@syncosync_public';
import { SyncosyncPublicGraphsModule } from './graphs/syncosync-public-graphs.module';
import { SingleLineNotificationComponent } from './components/single-line-notification/single-line-notification.component';
import { SyncosyncInfoModalButtonComponent } from './components/syncosync-info-modal-button/syncosync-info-modal-button.component';
import { SyncosyncInfoTextComponent } from './components/syncosync-info-text/syncosync-info-text.component';

@NgModule({
  declarations: [
    LanguageSwitchComponent,
    SingleLineNotificationComponent,
    SyncosyncInfoModalButtonComponent,
    SyncosyncInfoTextComponent,
  ],
  imports: [FormsModule, CommonModule],
  exports: [
    SyncosyncPublicGraphsModule,
    LanguageSwitchComponent,
    SingleLineNotificationComponent,
    SyncosyncInfoModalButtonComponent,
  ],
})
export class SyncosyncPublicModule {}
