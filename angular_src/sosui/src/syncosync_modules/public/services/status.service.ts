import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, timer } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import {
  AuthService,
  SosServiceBaseUnauthenticatedService,
  Status,
} from '@syncosync_common';

@Injectable({
  providedIn: 'root',
})
export class StatusService extends SosServiceBaseUnauthenticatedService {
  newStatusSubject: BehaviorSubject<Status> = new BehaviorSubject<Status>(
    new Status()
  );
  latestStatusObservable$: Observable<Status> =
    this.newStatusSubject.asObservable();
  statusInterval: Observable<number> = null;
  protected serviceName = 'StatusService';
  private latestStatus: Status = new Status();

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);

    // We never pause this
    this.statusInterval = timer(0, 5000);
    this.statusInterval
      .pipe(switchMap(() => this.fetchStatus()))
      .subscribe((res) => {
        if (res === undefined) {
          return;
        }
        // console.log("res.date", res.date)
        // res.date = Date.now().valueOf(); TODO: did this have a reason?
        /*res.networkState.Down = Math.floor(Math.random() * 100) + 1;
                        res.networkState.Up = Math.floor(Math.random() * 40) + 1;*/
        Object.assign(this.latestStatus, res);
        this.newStatusSubject.next(res);
      });
  }

  public getStatus(): Status {
    return this.latestStatus;
  }

  private fetchStatus(): Observable<Status> {
    return this.httpClient
      .get<Status>(syncosyncEndpoints.public.statusApiUrl)
      .pipe(catchError(this.handleError<Status>('getStatus')));
  }
}
