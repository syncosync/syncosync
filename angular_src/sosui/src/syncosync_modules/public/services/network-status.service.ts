import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import {
  AuthService,
  BandwidthModel,
  Hostname,
  SosServiceBaseUnauthenticatedService,
} from '@syncosync_common';

@Injectable({
  providedIn: 'root',
})
export class NetworkStatusService extends SosServiceBaseUnauthenticatedService {
  protected serviceName = 'NetworkStatusService';

  protected constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  public fetchBandwidthHistory(): Observable<BandwidthModel[]> {
    console.log('Requesting bandwidth history');
    return this.httpClient
      .get<BandwidthModel[]>(
        syncosyncEndpoints.public.networkBandwidthHistoryUrl
      )
      .pipe(
        catchError(this.handleError<BandwidthModel[]>('getBandwidthHistory'))
      );
  }

  public getHostname(): Observable<Hostname> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<Hostname>(syncosyncEndpoints.public.networkHostnameUrl, options)
      .pipe(
        tap((_) => this.log('Got hostname')),
        catchError(this.handleError<Hostname>('getHostname'))
      );
  }
}
