import { Injectable } from '@angular/core';
import {
  AuthService,
  SosServiceBaseUnauthenticatedService,
} from '@syncosync_common';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError } from 'rxjs/operators';
import { SyncosyncVersion } from '../../common/model/syncosyncVersion';

@Injectable({
  providedIn: 'root',
})
export class SyncosyncInfoServiceService extends SosServiceBaseUnauthenticatedService {
  protected serviceName = 'SyncosyncInfoServiceService';

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  public getVersion(): Observable<SyncosyncVersion> {
    return this.httpClient
      .get<SyncosyncVersion>(syncosyncEndpoints.public.syncosyncVersionUrl)
      .pipe(catchError(this.handleError<SyncosyncVersion>('getVersion')));
  }
}
