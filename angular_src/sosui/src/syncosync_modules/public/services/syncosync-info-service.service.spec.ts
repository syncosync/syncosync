import { TestBed } from '@angular/core/testing';

import { SyncosyncInfoServiceService } from './syncosync-info-service.service';

describe('SyncosyncInfoServiceService', () => {
  let service: SyncosyncInfoServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SyncosyncInfoServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
