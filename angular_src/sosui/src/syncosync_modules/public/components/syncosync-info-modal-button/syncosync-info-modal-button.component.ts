import { Component } from '@angular/core';
import { SyncosyncModalService } from '@syncosync_common';
import { SyncosyncInfoTextComponent } from '../syncosync-info-text/syncosync-info-text.component';

@Component({
  selector: 'app-syncosync-info-modal-button',
  templateUrl: './syncosync-info-modal-button.component.html',
  styleUrls: ['./syncosync-info-modal-button.component.css'],
})
export class SyncosyncInfoModalButtonComponent {
  constructor(private modalService: SyncosyncModalService) {}

  openInfoModal(): void {
    this.modalService
      .showRequestDataModal('About syncosync', SyncosyncInfoTextComponent)
      .subscribe((_) => {
        // nop
      });
  }
}
