import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleLineNotificationComponent } from './single-line-notification.component';

describe('SingleLineNotificationComponent', () => {
  let component: SingleLineNotificationComponent;
  let fixture: ComponentFixture<SingleLineNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SingleLineNotificationComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleLineNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
