import { Component, OnInit } from '@angular/core';
import { SyncosyncVersion } from '../../../common/model/syncosyncVersion';
import { Observable } from 'rxjs';
import { SyncosyncInfoServiceService } from '../../services/syncosync-info-service.service';
import { AbstractFormDirective } from '@syncosync_common';

@Component({
  selector: 'app-syncosync-info-text',
  templateUrl: './syncosync-info-text.component.html',
  styleUrls: ['./syncosync-info-text.component.css'],
})
export class SyncosyncInfoTextComponent extends AbstractFormDirective<SyncosyncVersion> {
  protected getInitModel(): SyncosyncVersion {
    return new SyncosyncVersion();
  }

  constructor(
    private syncosyncInfoServiceService: SyncosyncInfoServiceService
  ) {
    super();
  }

  private updateVersion(): void {
    const getterObservable: Observable<SyncosyncVersion> =
      this.syncosyncInfoServiceService.getVersion();

    getterObservable.subscribe((result) => {
      if (result !== undefined && result !== null) {
        this.setModel(result);
      }
    });
  }

  protected initialize(): void {
    this.updateVersion();
  }

  onSubmit(): void {
    //
  }
}
