import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SyncosyncInfoTextComponent } from './syncosync-info-text.component';

describe('SyncosyncInfoTextComponent', () => {
  let component: SyncosyncInfoTextComponent;
  let fixture: ComponentFixture<SyncosyncInfoTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SyncosyncInfoTextComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SyncosyncInfoTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
