export const syncosyncEndpoints = {
  public: {
    networkHostnameUrl: '/api/public/hostname',
    networkBandwidthHistoryUrl: '/api/public/bandwidth_history',
    statusApiUrl: '/api/public/status/sync_status',
    uiLanguageUrl: '/api/public/ui_language',
    sysstateUrl: '/api/public/sysstate',
    syncosyncVersionUrl: '/api/public/version',
  },
  authenticated: {
    hostnameUrl: '/api/setup/hostname',
    resolvConfigUrl: '/api/setup/resolv_config',
    networkInterfacesConfigUrl: '/api/setup/nic_config',
    sshConfigUrl: '/api/setup/ssh_config',
    accountUpdateUrl: '/api/account_management/edit_account',
    accountAddUrl: '/api/account_management/add_account',
    accountDeleteUrl: '/api/account_management/delete_account',
    accountEraseDataUrl: '/api/account_management/erase_data_account',
    accountListUrl: '/api/account_management/account_list',
    syncConfigurationLocalKey: '/api/setup/local_key',
    syncConfigurationRemoteHostConfig: '/api/setup/remote_host',
    keyExchangeCancel: '/api/setup/key_exchange_cancel',
    keyExchangeLead: '/api/setup/key_exchange_lead',
    keyExchangeFollow: '/api/setup/key_exchange_follow',
    remoteKey: '/api/setup/remote_key',
    adminPasswordUrl: '/api/setup/admin_password',
    modeUrl: '/api/setup/mode',
    drivesetupUrl: '/api/setup/drivesetup',
    mailConfigUrl: '/api/setup/mail_setup',
    mailTestMail: '/api/setup/mail_test',
    sosConfigUrl: '/api/setup/sos_config',
    shutdownSystemUrl: '/api/setup/shutdown_system',
    partitioningUrl: '/api/setup/partition_drives',
    isLeaderUrl: '/api/setup/is_leader',
    partitioningLeadUrl: '/api/setup/partitioning_lead',
    trafficshapingURL: '/api/setup/trafficshape',
    trafficshapingModeURL: '/api/setup/trafficshape_mode',
    timezoneURL: '/api/setup/timezone',
    availableTimezonesURL: '/api/setup/available_timezone',
    getRemoteRecoveryProgress: '/api/remote_recovery/progress',
    getRRResult: '/api/remote_recovery/rr_result',
    cancelRemoteRecovery: '/api/remote_recovery/cancel',
    factory_reset: '/api/setup/factory_reset',
    rebootSystemUrl: '/api/setup/reboot_system',
    downloadLatestConfigbackupUrl: '/api/setup/download_configbackup',
    syncSsdLeadUrl: '/api/setup/sync_ssd_lead',
    partitioningStatusUrl: '/api/setup/partitioning_status',
  },
  authLoginUrl: '/api/auth/login',
  resource: {
    localizedMessagesPartialUrl: (isoCode: string) =>
      `/files/localization/${isoCode}.json`,
  },
};
