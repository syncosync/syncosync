const PROXY_CONFIG = [
  {
    context: [
      "/api",
      "/files"
    ],
    // "target": "http://192.168.243.244:5000/",
    "target": "http://localhost:5555/",
    "secure": false,
    "logLevel": "debug",
    "changeOrigin": true
  }
]

module.exports = PROXY_CONFIG;
